<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('user_id');
            $table->integer('user_is_sys_admin');
            $table->string('facebook_id');
            $table->string('user_name');
            $table->string('user_email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('user_password');
            $table->string('user_phone');
            $table->string('mobile')->nullable();
            $table->string('user_about_me');
            $table->string('user_cover_photo');
            $table->string('user_profile_photo');
            $table->string('role_id');
            $table->tinyInteger('status');
            $table->tinyInteger('is_banned');
            $table->string('code',10);
            $table->float('overall_rating');
            $table->string('whatsapp');
            $table->string('messenger');
            $table->tinyInteger('verify_types');
            $table->bigInteger('follower_count');
            $table->bigInteger('following_count');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
