<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about', function (Blueprint $table) {
            $table->bigIncrements('about_id');
            $table->string('about_title');
            $table->longText('about_description');
            $table->string('about_email');
            $table->string('about_phone');
            $table->string('about_website');
            $table->string('about_website');
            $table->tinyInteger('ads_on');
            $table->text('ads_client');
            $table->text('ads_slot');
            $table->tinyInteger('analyt_on');
            $table->text('analyt_track_id');
            $table->text('facebook');
            $table->text('google_plus');
            $table->text('instagram');
            $table->text('youtube');
            $table->text('pinterest');
            $table->text('twitter');
            $table->text('privacypolicy');
            $table->text('GDPR');
            $table->integer('upload_point');
            $table->text('about_name');
            $table->integer('archive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about');
    }
};
