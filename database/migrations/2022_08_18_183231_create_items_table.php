<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('cat_id');
            $table->bigInteger('sub_cat_id');
            $table->bigInteger('item_type_id');
            $table->bigInteger('item_price_type_id');
            $table->bigInteger('item_currency_id');
            $table->bigInteger('item_location_id');
            $table->bigInteger('item_location_id');
            $table->bigInteger('condition_of_item_id');
            $table->text('deal_option_remark');
            $table->text('description');
            $table->text('highlight_info');
            $table->float('price');
            $table->bigInteger('deal_option_id');
            $table->string('brand');
            $table->tinyInteger('business_mode');
            $table->tinyInteger('is_sold_out');
            $table->string('title');
            $table->text('address');
            $table->float('lat',10,6);
            $table->float('lng',10,6);
            $table->tinyInteger('status');
            $table->timestamp('added_user_id');
            $table->timestamp('updated_user_id');
            $table->timestamp('updated_flag');
            $table->bigInteger('touch_count');
            $table->bigInteger('favourite_count');
            $table->bigInteger('feature');
            $table->bigInteger('item_area_id');
            $table->bigInteger('item_cat_id');
            $table->float('price_avl');
            $table->float('price_req');
            $table->float('price_req');
            $table->integer('change_status');
            $table->string('item_code');
            $table->integer('item_hidden');
            $table->integer('item_hidden');
            $table->bigInteger('item_location_area_id');
            $table->integer('item_rahn');
            $table->integer('item_ejare');
            $table->string('malek');
            $table->string('tamas_malek');
            $table->text('desc_malek');
            $table->integer('archive');
            $table->integer('pointer');
            $table->string('whatsapp_number');
            $table->string('personnel');
            $table->string('year');
            $table->string('room');
            $table->string('sanad');
            $table->string('wc');
            $table->string('bathroom');
            $table->string('unit');
            $table->string('floor');
            $table->string('direction');
            $table->string('usemelk')->nullable();
            $table->string('empty')->nullable();
            $table->float('metrazh')->nullable();
            $table->float('zirbana')->nullable();
            $table->float('toolbar')->nullable();
            $table->float('arz')->nullable();
            $table->float('kaf')->nullable();
            $table->string('kaf')->nullable();
            $table->string('sarmayesh')->nullable();
            $table->string('moshakhase')->nullable();
            $table->string('abegarm')->nullable();
            $table->string('kabinet')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
};
