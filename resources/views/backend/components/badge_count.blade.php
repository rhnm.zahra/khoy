         <!-- small box -->
<div class="small-box <?php echo $color; ?>">
  <div class="inner">
    <h3 style="color: white;">
      <?php echo $total_count; ?>
    </h3>

    <p style="color:white;font-size: 16px;"><?php echo $label; ?></p>
  </div>
  <div class="icon">
    <i class="<?php echo $icon; ?>"></i>
  </div>
  <a href="<?php echo $url; ?>" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i> مشاهده </a>
</div>