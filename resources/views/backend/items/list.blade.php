<div class="table-responsive animated fadeInRight">
    <table class="table m-0 table-striped">
        <tr>
            <th><?php echo get_msg('no'); ?></th>
            <th><?php echo get_msg('item_name'); ?></th>
            <th><?php echo get_msg('cat_name'); ?></th>
            <th><?php echo get_msg('subcat_name'); ?></th>
            <th>بازدید</th>
            <th><?php echo get_msg('added_date'); ?></th>

            <?php if ($this->ps_auth->has_access(EDIT)): ?>

                <th><span class="th-title"><?php echo get_msg('btn_edit') ?></span></th>

            <?php endif; ?>

            <?php if ($this->ps_auth->has_access(DEL)): ?>

                <th><span class="th-title"><?php echo get_msg('btn_delete') ?></span></th>

            <?php endif; ?>


            <?php if ($this->ps_auth->has_access(PUBLISH)): ?>

                <th><span class="th-title">بایگانی</span></th>
                <th><span class="th-title">ویژه</span></th>
                <th><span class="th-title"><?php echo get_msg('btn_publish') ?></span></th>

            <?php endif; ?>


        </tr>


        <?php $count = $this->uri->segment(4) or $count = 0; ?>

        <?php if (!empty($items) && count($items->result()) > 0): ?>

            <?php
        $ct=0;
        foreach ($items->result() as $item):
            $ct+=$item->touch_count;
            ?>
                <tr>
                    <td><?php echo ++$count; ?></td>
                    <td><?php echo $item->title; ?></td>
                    <td><?php echo $this->Category->get_one($item->cat_id)->cat_name; ?></td>
                    <td><?php echo $this->Subcategory->get_one($item->sub_cat_id)->name; ?></td>
                    <td><?php echo $item->touch_count; ?></td>

                    <?php $time = $item->added_date;

                    $time = strtotime($time); ?>

                    <td>
                        <div class="sparkbar" data-color="#00a65a"
                             data-height="20"><?php echo jdate("Y/m/d H:i:s", $time); ?></div>
                    </td>


                    <?php if ($this->ps_auth->has_access(EDIT)): ?>

                        <td>
                            <a href='<?php echo $module_site_url . '/edit/' . $item->id; ?>'>
                                <i style='font-size: 18px;' class='fa fa-pencil-square-o'></i>
                            </a>
                        </td>

                    <?php endif; ?>

                    <?php if ($this->ps_auth->has_access(DEL)): ?>

                        <td>
                            <a herf='#' class='btn-delete' data-toggle="modal" data-target="#myModal"
                               id="<?php echo "$item->id"; ?>">
                                <i style='font-size: 18px;' class='fa fa-trash-o'></i>
                            </a>
                        </td>

                    <?php endif; ?>


                    <td>
                        <?php

                        $about = $this->Front->get_about();
                        $today = date('Y-m-d');
                        $valid_date = date('Y-m-d', strtotime($today . ' - ' . ($about->archive + 1) . ' days'));

                        if ($valid_date>substr($item->added_date,0,10)) {
                            if (@$item->archive == 1): ?>
                                <button class="btn btn-sm btn-success unarchive" id='<?php echo $item->id; ?>'>
                                    <i class="fa fa-check"></i>
                                </button>
                            <?php else: ?>
                                <button class="btn btn-sm btn-danger archive" id='<?php echo $item->id; ?>'>
                                    <i class="fa fa-remove"></i>
                                </button>
                            <?php endif;

                        }
                        ?>
                    </td>

                    <?php if ($this->ps_auth->has_access(PUBLISH)): ?>

                        <td>
                            <?php if (@$item->feature == 1): ?>
                                <button class="btn btn-sm btn-success unfeature" id='<?php echo $item->id; ?>'>
                                    بله
                                </button>
                            <?php else: ?>
                            <button class="btn btn-sm btn-danger feature" id='<?php echo $item->id; ?>'>
                                    خیر</button><?php endif; ?>
                        </td>

                        <td>
                            <?php if (@$item->status == 1): ?>
                                <button class="btn btn-sm btn-success unpublish" id='<?php echo $item->id; ?>'>
                                    بله
                                </button>
                            <?php else: ?>
                            <button class="btn btn-sm btn-danger publish" id='<?php echo $item->id; ?>'>
                                    خیر</button><?php endif; ?>
                        </td>

                    <?php endif; ?>

                </tr>

            <?php endforeach; ?>

        <script>
            jQuery('#allcount').html('<?php echo $ct; ?>');
        </script>

        <?php else: ?>

            <?php $this->load->view($template_path . '/partials/no_data'); ?>

        <?php endif; ?>

    </table>
</div>

