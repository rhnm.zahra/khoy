<script>
function runAfterJQ() {
		
		// Publish Trigger
		$(document).delegate('.publish','click',function(){
			
			// get button and id
			var btn = $(this);
			var id = $(this).attr('id');

			// Ajax Call to publish
			$.ajax({
				url: "<?php echo $module_site_url .'/ajx_publish/'; ?>" + id,
				method: 'GET',
				success: function( msg ) {
			       if ( msg == 'true' ) {
						btn.addClass('unpublish').addClass('btn-success')
							.removeClass('publish').removeClass('btn-danger')
							.html('بله');
							
			adata = $('select[name="status"]');

		 if(adata.val() == '') {
	 				
				   $link = '<?php echo site_url() . "/admin/items"; ?>';
                   location.href = $link;
          }
		  
		 if(adata.val() == '0') {
	 			   document.getElementById('submit').click();	
          }
		  
		if(adata.val() == '1') {
	 			   document.getElementById('submit').click();	
          }
		  
		  if(adata.val() == '3') {
	 			   document.getElementById('submit').click();	
          }
		  
					
							   
							}
					else
						alert( "<?php echo get_msg( 'err_sys' ); ?>" );
				}
			});
		});
		
		// Unpublish Trigger
		$(document).delegate('.unpublish','click',function(){

			// get button and id
			var btn = $(this);
			var id = $(this).attr('id');

			// Ajax call to unpublish
			$.ajax({
				url: "<?php echo $module_site_url .'/ajx_unpublish/'; ?>" + id,
				method: 'GET',
				success: function( msg ){
					if ( msg == 'true' ){
						btn.addClass('publish').addClass('btn-danger')
							.removeClass('unpublish').removeClass('btn-success')
							.html('خیر');
							
				bdata = $('select[name="status"]');
				
		 if(bdata.val() == '') {
	 			     $link = '<?php echo site_url() . "/admin/items"; ?>';
                   location.href = $link;
          }
		 if(bdata.val() == '0') {
	 			   document.getElementById('submit').click();	
          }
		  
		if(bdata.val() == '1') {
	 			   document.getElementById('submit').click();	
          }
		  
		  if(bdata.val() == '3') {
	 			   document.getElementById('submit').click();	
          }
		  
							}
					else
						alert( "<?php echo get_msg( 'err_sys' ); ?>" );
				}
			});

	});

		// Feature Trigger
		$(document).delegate('.feature','click',function(){

			// get button and id
			var btn = $(this);
			var id = $(this).attr('id');

			// Ajax Call to publish
			$.ajax({
				url: "<?php echo $module_site_url .'/ajx_feature/'; ?>" + id,
				method: 'GET',
				success: function( msg ) {
			       if ( msg == 'true' ) {
						btn.addClass('unfeature').addClass('btn-success')
							.removeClass('feature').removeClass('btn-danger')
							.html('بله');

			adata = $('select[name="status"]');

		 if(adata.val() == '') {

				   $link = '<?php echo site_url() . "/admin/items"; ?>';
                   location.href = $link;
          }

		 if(adata.val() == '0') {
	 			   document.getElementById('submit').click();
          }

		if(adata.val() == '1') {
	 			   document.getElementById('submit').click();
          }

		  if(adata.val() == '3') {
	 			   document.getElementById('submit').click();
          }



							}
					else
						alert( "<?php echo get_msg( 'err_sys' ); ?>" );
				}
			});
		});

		// Unfeature Trigger
		$(document).delegate('.unfeature','click',function(){

			// get button and id
			var btn = $(this);
			var id = $(this).attr('id');

			// Ajax call to unfeature
			$.ajax({
				url: "<?php echo $module_site_url .'/ajx_unfeature/'; ?>" + id,
				method: 'GET',
				success: function( msg ){
					if ( msg == 'true' ){
						btn.addClass('feature').addClass('btn-danger')
							.removeClass('unfeature').removeClass('btn-success')
							.html('خیر');

				bdata = $('select[name="status"]');

		 if(bdata.val() == '') {
	 			     $link = '<?php echo site_url() . "/admin/items"; ?>';
                   location.href = $link;
          }
		 if(bdata.val() == '0') {
	 			   document.getElementById('submit').click();
          }

		if(bdata.val() == '1') {
	 			   document.getElementById('submit').click();
          }

		  if(bdata.val() == '3') {
	 			   document.getElementById('submit').click();
          }

							}
					else
						alert( "<?php echo get_msg( 'err_sys' ); ?>" );
				}
			});

	});


		// archive Trigger
		$(document).delegate('.archive','click',function(){

			// get button and id
			var btn = $(this);
			var id = $(this).attr('id');

			// Ajax Call to publish
			$.ajax({
				url: "<?php echo $module_site_url .'/ajx_archive/'; ?>" + id,
				method: 'GET',
				success: function( msg ) {
			       if ( msg == 'true' ) {
						btn.addClass('unarchive').addClass('btn-success')
							.removeClass('archive').removeClass('btn-danger')
							.html('<i class="fa fa-check"></i>');

			adata = $('select[name="status"]');

		 if(adata.val() == '') {

				   $link = '<?php echo site_url() . "/admin/items"; ?>';
                   location.href = $link;
          }

		 if(adata.val() == '0') {
	 			   document.getElementById('submit').click();
          }

		if(adata.val() == '1') {
	 			   document.getElementById('submit').click();
          }

		  if(adata.val() == '3') {
	 			   document.getElementById('submit').click();
          }



							}
					else
						alert( "<?php echo get_msg( 'err_sys' ); ?>" );
				}
			});
		});
		// Unarchive Trigger
		$(document).delegate('.unarchive','click',function(){

			// get button and id
			var btn = $(this);
			var id = $(this).attr('id');

			// Ajax call to unarchive
			$.ajax({
				url: "<?php echo $module_site_url .'/ajx_unarchive/'; ?>" + id,
				method: 'GET',
				success: function( msg ){
					if ( msg == 'true' ){
						btn.addClass('archive').addClass('btn-danger')
							.removeClass('unarchive').removeClass('btn-success')
							.html('<i class="fa fa-remove"></i>');

				bdata = $('select[name="status"]');

		 if(bdata.val() == '') {
	 			     $link = '<?php echo site_url() . "/admin/items"; ?>';
                   location.href = $link;
          }
		 if(bdata.val() == '0') {
	 			   document.getElementById('submit').click();
          }

		if(bdata.val() == '1') {
	 			   document.getElementById('submit').click();
          }

		  if(bdata.val() == '3') {
	 			   document.getElementById('submit').click();
          }

							}
					else
						alert( "<?php echo get_msg( 'err_sys' ); ?>" );
				}
			});

	});



}
	// Delete Trigger
	$('.btn-delete').click(function(){
		
		// get id and links
		var id = $(this).attr('id');
		var btnYes = $('.btn-yes').attr('href');
		var btnNo = $('.btn-no').attr('href');

		// modify link with id
		$('.btn-yes').attr( 'href', btnYes + id );
		$('.btn-no').attr( 'href', btnNo + id );
	});
</script>

<?php
	// Delete Confirm Message Modal
	$data = array(
		'title' => get_msg( 'delete_prd_label' ),
		'message' =>  get_msg( 'prd_yes_all_message' ),
		'no_only_btn' => get_msg( 'prd_no_only_label' )
	);
	
	$this->load->view( $template_path .'/components/delete_confirm_modal', $data );
?>
<script>
    jQuery('#city').on('change', function () {

        var value = jQuery('option:selected', this).text().replace(/Value\s/, '');

        var catId = jQuery(this).val();

        jQuery.ajax({
            url: '/index.php/user/get_areas/' + catId,
            method: 'GET',
            dataType: 'JSON',
            success: function (data) {
                jQuery('#city_area').html("");
                jQuery('#city_area').append('<option value="">همه مناطق</option>');
                jQuery.each(data, function (i, obj) {
                    jQuery('#city_area').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                });
                jQuery('#city_area').selectpicker('refresh');
            }
        });


    });

</script>
