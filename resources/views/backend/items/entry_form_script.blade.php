<script>
	<?php if ( $this->config->item( 'client_side_validation' ) == true ): ?>

    function separate(Number) {
        Number += '';
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        x = Number.split('.');
        y = x[0];
        z = x.length > 1 ? '.' + x[1] : '';
        var rgx = /([0123456789٠١٢٣٤٥٦٧٨٩۰۱۲۳۴۵۶۷۸۹]+)([0123456789٠١٢٣٤٥٦٧٨٩۰۱۲۳۴۵۶۷۸۹]{3})/;
        while (rgx.test(y))
            y = y.replace(rgx, '$1' + ',' + '$2');
        return y + z;
    }

	function jqvalidate() {

		$('#item-form').validate({
			rules:{
				
				cat_id: {
		       		indexCheck : ""
		      	},
		      	sub_cat_id: {
		       		indexCheck : ""
		      	}
			},
			messages:{
				
				cat_id:{
			       indexCheck: "<?php echo $this->lang->line('f_item_cat_required'); ?>"
			    },
			    sub_cat_id:{
			       indexCheck: "<?php echo $this->lang->line('f_item_subcat_required'); ?>"
			    }
			},

			submitHandler: function(form) {
		        if ($("#item-form").valid()) {
		            form.submit();
		        }
		    }

		});
		
		jQuery.validator.addMethod("indexCheck",function( value, element ) {
			
			   if(value == 0) {
			    	return false;
			   } else {
			    	return true;
			   };
			   
		});
			

	}

	<?php endif; ?>
	function runAfterJQ() {


        $('.collapse').collapse();

        if(jQuery('#branch_status').val()==1){
            jQuery('.branch_box').show();
        }else{
            jQuery('.branch_box').hide();
        }


        var catId = jQuery('#item_location_id').val();
        var areaid=  '<?php echo $item->item_location_area_id; ?>';
        jQuery.ajax({
            url: '/index.php/user/get_areas/' + catId,
            method: 'GET',
            dataType: 'JSON',
            success: function (data) {
                // jQuery('#item_area_id').html("");
                jQuery('#item_location_area_id').html("");
                jQuery.each(data, function (i, obj) {
                    if(areaid==obj.id)
                    {
                        var selected='selected=""';
                    }else{
                        var selected='';
                    }
                    // jQuery('#item_area_id').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                    jQuery('#item_location_area_id').append('<option '+selected+' value="' + obj.id + '">' + obj.name + '</option>');
                });
                jQuery('#item_location_area_id').selectpicker('refresh');
                // jQuery('#item_area_id').selectpicker('refresh');
                // alert(areaid);
                //var operation_day = "<?php //echo implode(json_decode((@$item->item_area_id),true),',') ?>//";
                //var operation_day = "<?php //echo implode(json_decode((@$item->item_location_area_id),true),',') ?>//";
                // jQuery('#item_area_id').selectpicker('val', operation_day.split(",")); //split them and set value
                // jQuery('#item_location_area_id').selectpicker('val', operation_day.split(",")); //split them and set value
            }
        });

        jQuery.ajax({
            url: '/index.php/user/get_branchs/' + catId,
            method: 'GET',
            dataType: 'JSON',
            success: function (data) {
                jQuery('#item_branch_id').html("");
                jQuery.each(data, function (i, obj) {
                    jQuery('#item_branch_id').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                });


                var bId = jQuery('#item_branch_id').val();
                jQuery.ajax({
                    url: '/index.php/user/get_personnels/' + bId,
                    method: 'GET',
                    dataType: 'JSON',
                    success: function (data) {
                        jQuery('#item_branch_personnels').html("");
                        jQuery.each(data, function (i, obj) {
                            jQuery('#item_branch_personnels').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                        });
                    }
                });
            }
        });


        var branchid = '<?php echo $item->item_branch_id; ?>';
        jQuery.ajax({
            url: '/index.php/user/get_branchs/' + catId,
            method: 'GET',
            dataType: 'JSON',
            success: function (data) {
                // jQuery('#item_area_id').html("");
                jQuery('#item_branch_id').html("");
                jQuery.each(data, function (i, obj) {
                    if (branchid == obj.id) {
                        var selected = 'selected=""';
                    } else {
                        var selected = '';
                    }
                    jQuery('#item_branch_id').append('<option ' + selected + ' value="' + obj.id + '">' + obj.name + '</option>');
                });

                jQuery.ajax({
                    url: '/index.php/user/get_personnels/' + branchid,
                    method: 'GET',
                    dataType: 'JSON',
                    success: function (data) {
                        jQuery('#item_branch_personnels').html("");
                        jQuery.each(data, function (i, obj) {
                            jQuery('#item_branch_personnels').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                        });
                        jQuery("#item_branch_personnels").val(<?php echo @$item->item_branch_personnels;?>);

                    }

                });


            }

        });

        // jQuery("#price_div").show();
        if(jQuery("#item_type_id").val()==4 ){
            jQuery("#moaveze").show();
            var type = jQuery('#change_status').val();
            if (type == 1) {
                jQuery("#change_div").show();
            }
        }

        if(jQuery("#item_type_id").val()==3 ){
            jQuery(".div_rahn").show();

        }

        var catId = jQuery('#item_location_id').val();
        //var areaid=  '<?php //echo $item->item_location_area_id; ?>//';
        jQuery.ajax({
            url: '/index.php/user/get_areas/' + catId,
            method: 'GET',
            dataType: 'JSON',
            success: function (data) {
                jQuery('#item_area_id').html("");
                // jQuery('#item_location_area_id').html("");
                jQuery.each(data, function (i, obj) {
                    /*if(areaid==obj.id)
                    {
                        var selected='selected=""';
                    }else{
                        var selected='';
                    }*/
                    jQuery('#item_area_id').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                    // jQuery('#item_location_area_id').append('<option '+selected+' value="' + obj.id + '">' + obj.name + '</option>');
                });
                // jQuery('#item_location_area_id').selectpicker('refresh');
                // jQuery('#item_area_id').selectpicker('refresh');
                // alert(areaid);

                //var operation_day = "<?php //echo implode(json_decode((@$item->item_area_id),true),',') ?>//";

                jQuery("#item_area_id").val(<?php echo @$item->item_area_id;?>);


                //var operation_day = "<?php //echo implode(json_decode((@$item->item_location_area_id),true),',') ?>//";
                // jQuery('#item_area_id').selectpicker('val', operation_day.split(",")); //split them and set value
                // jQuery('#item_location_area_id').selectpicker('val', operation_day.split(",")); //split them and set value
            }
        });



		$('#cat_id').on('change', function() {

				var value = $('option:selected', this).text().replace(/Value\s/, '');

				var catId = $(this).val();

				$.ajax({
					url: '<?php echo $module_site_url . '/get_all_sub_categories/';?>' + catId,
					method: 'GET',
					dataType: 'JSON',
					success:function(data){
						$('#sub_cat_id').html("");
						$.each(data, function(i, obj){
						    $('#sub_cat_id').append('<option value="'+ obj.id +'">' + obj.name+ '</option>');
						});
						$('#name').val($('#name').val() + " ").blur();
						$('#sub_cat_id').trigger('change');

                        if(jQuery('#sub_cat_id').val()=='subcat257c99f44d21c26ce7cfd138a0b6e697' || jQuery('#sub_cat_id').val()=='subcat64c8a18d84ab74eb3f9830f7e4d0eed1')
                        {
                            jQuery('#metrazh_div').show();
                            jQuery('#zirbana_div').hide();
                            jQuery('#zirbana').val('0');

                        }else if(jQuery('#sub_cat_id').val()=='subcat46ce8ce3a8ee743904b993acde0e10a1'){
                            jQuery('#metrazh_div').hide();
                            jQuery('#metrazh').val('0');
                            jQuery('#zirbana_div').show();
                        }else{
                            jQuery('#metrazh_div').show();
                            jQuery('#zirbana_div').show();
                        }
					}
				});
			});

        jQuery('#sub_cat_id').on('change', function () {
            if(jQuery(this).val()=='subcat257c99f44d21c26ce7cfd138a0b6e697' || jQuery(this).val()=='subcat64c8a18d84ab74eb3f9830f7e4d0eed1')
            {
                jQuery('#metrazh_div').show();
                jQuery('#zirbana_div').hide();
                jQuery('#zirbana').val('0');

            }else if(jQuery(this).val()=='subcat46ce8ce3a8ee743904b993acde0e10a1'){
                jQuery('#metrazh_div').hide();
                jQuery('#metrazh').val('0');
                jQuery('#zirbana_div').show();
            }else{
                jQuery('#metrazh_div').show();
                jQuery('#zirbana_div').show();
            }
        });
        
        jQuery('#item_location_id').on('change', function () {

            var value = jQuery('option:selected', this).text().replace(/Value\s/, '');

            var catId = jQuery(this).val();

            jQuery.ajax({
                url: '/index.php/user/get_areas/' + catId,
                method: 'GET',
                dataType: 'JSON',
                success: function (data) {
                    jQuery('#item_area_id').html("");
                    jQuery('#item_location_area_id').html("");
                    jQuery.each(data, function (i, obj) {
                        jQuery('#item_area_id').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                        jQuery('#item_location_area_id').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                    });
                    jQuery('#name').val(jQuery('#name').val() + " ").blur();
                    jQuery('#item_area_id').selectpicker('refresh');
                    jQuery('#item_location_area_id').selectpicker('refresh');
                }
            });

            jQuery.ajax({
                url: '/index.php/user/get_branchs/' + catId,
                method: 'GET',
                dataType: 'JSON',
                success: function (data) {
                    jQuery('#item_branch_id').html("");
                    jQuery.each(data, function (i, obj) {
                        jQuery('#item_branch_id').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                    });


                    var bId = jQuery('#item_branch_id').val();
                    jQuery.ajax({
                        url: '/index.php/user/get_personnels/' + bId,
                        method: 'GET',
                        dataType: 'JSON',
                        success: function (data) {
                            jQuery('#item_branch_personnels').html("");
                            jQuery.each(data, function (i, obj) {
                                jQuery('#item_branch_personnels').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                            });
                        }
                    });
                }
            });

        });

        jQuery('#item_type_id').on('change', function () {
            var type = jQuery('#item_type_id').val();
            if (type == 4) {
                jQuery("#moaveze").show();
            } else {
                jQuery("#moaveze").hide();
                jQuery("#change_div").hide();
                jQuery('#change_status').val(0);
            }

            if (type == 3) {
                jQuery(".div_rahn").show();
            } else {
                jQuery(".div_rahn").hide();
            }
        });
        jQuery('#change_status').on('change', function () {
            var type = jQuery('#change_status').val();
            if (type == 1) {
                jQuery("#change_div").show();
            } else {
                jQuery("#change_div").hide();
            }
        });

        jQuery('#item_branch_id').on('change', function () {

            var value = jQuery('option:selected', this).text().replace(/Value\s/, '');

            var catId = jQuery(this).val();

            jQuery.ajax({
                url: '/index.php/user/get_personnels/' + catId,
                method: 'GET',
                dataType: 'JSON',
                success: function (data) {
                    jQuery('#item_branch_personnels').html("");
                    jQuery.each(data, function (i, obj) {
                        jQuery('#item_branch_personnels').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                    });
                    jQuery('#item_branch_personnels').selectpicker('refresh');
                }
            });

        });

        $('#us3').locationpicker({
            location: {latitude:  '<?php echo $item->lat;?>', longitude: '<?php echo $item->lng;?>'},
            radius: 300,
            inputBinding: {
                latitudeInput: $('#lat'),
                longitudeInput: $('#lng'),
                radiusInput: $('#us3-radius')
            },
            enableAutocomplete: true,
            onchanged: function (currentLocation, radius, isMarkerDropped) {
                // Uncomment line below to show alert on each Location Changed event
                //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
            }
        });
        
		 $(function() {
			var selectedClass = "";
			$(".filter").click(function(){
			selectedClass = $(this).attr("data-rel");
			$("#gallery").fadeTo(100, 0.1);
			$("#gallery div").not("."+selectedClass).fadeOut().removeClass('animation');
			setTimeout(function() {
			$("."+selectedClass).fadeIn().addClass('animation');
			$("#gallery").fadeTo(300, 1);
			}, 300);
			});
		});

	}

</script>
<?php 
	// replace cover photo modal
	$data = array(
		'title' => get_msg('upload_photo'),
		'img_type' => 'item',
		'img_parent_id' => @$item->id
	);

	$this->load->view( $template_path .'/components/photo_upload_modal', $data );

	// delete cover photo modal
	$this->load->view( $template_path .'/components/delete_cover_photo_modal' ); 
?>

