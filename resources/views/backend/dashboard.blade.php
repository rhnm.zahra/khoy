<section class="content animated fadeInRight">
  <!-- Content Header (Page header) -->
  <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0 text-dark" style="text-align:center;"> به ملک خوی خوش آمدید </h1>
            <?php flash_msg(); ?>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
</section>  

  <!-- Main content -->
 <div class="container-fluid">
    <div class="card-body">
      <div class="row"> 
        <div class="col-lg-3 col-6">
          <!-- small box -->
            <?php 
              $data = array(
                'url' => site_url() . "/admin/categories" ,
                'total_count' => $this->Category->count_all(),
                'label' => get_msg( 'total_category_count_label'),
                'icon' => "fa fa-th-list",
                'color' => "bg-primary"
              );

              $this->load->view( $template_path .'/components/badge_count', $data );
            ?>
        </div>

        <div class="col-lg-3 col-6">
            <!-- small box -->
            <?php 
              $data = array(
                'url' => site_url() . "/admin/subcategories" ,
                'total_count' => $this->Subcategory->count_all_by(),
                'label' => get_msg( 'total_sub_cat_count_label'),
                'icon' => "fa fa-list",
                'color' => "bg-success"
              );

              $this->load->view( $template_path .'/components/badge_count', $data ); 
            ?>
        </div>

        <div class="col-lg-3 col-6">
            <!-- small box -->
            <?php 
              $data = array(
                'url' => site_url() . "/admin/items" ,
                'total_count' => $this->Item->count_all_by(),
                'label' => "جمع آگهی ها",
                'icon' => "fa fa-archive",
                'color' => "bg-warning"
              );

              $this->load->view( $template_path .'/components/badge_count', $data ); 
            ?>
        </div>

        <div class="col-lg-3 col-6">
            <!-- small box -->
            <?php 
              $data = array(
                'url' => site_url() . "/admin/contacts" ,
                'total_count' => $this->Contact->count_all_by(),
                'label' => get_msg( 'total_contact_count_label'),
                'icon' => "fa fa-comment",
                'color' => "bg-danger"
              );

              $this->load->view( $template_path .'/components/badge_count', $data ); 
            ?>
        </div>


        <div class="col-md-12">
          <div class="card">
            <?php
              $data = array(
                'panel_title' => "نمودار گزارشات آگهی",
                'module_name' => 'purchasedproduct' ,
                'total_count' => $this->Itemreport->count_all(),
                'data' => $this->Itemreport->get_item_report(4)->result()
              );

              $this->load->view( $template_path .'/components/item_report_panel', $data );
            ?>
          </div>
        </div>



       
        <!-- ./col -->
        
        <!-- col -->
      </div>
    </div>
  </div>  
       
</div>