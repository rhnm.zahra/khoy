
<?php
	$attributes = array( 'id' => 'area-form', 'enctype' => 'multipart/form-data');
	echo form_open( '', $attributes);
?>
	
<section class="content animated fadeInRight">
	<div class="col-md-12">
		<div class="card card-info">
		    <div class="card-header">
		        <h3 class="card-title"><?php echo get_msg('area_info')?></h3>
		    </div>
	        <!-- /.card-header -->
	        <div class="card-body">
	            <div class="row">
	            

	              	<div class="col-md-6">
						<div class="form-group">
	                   		<label>
	                   			<span style="font-size: 17px; color: red;">*</span>
								<?php echo get_msg('area_name')?>
								<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('cat_name_tooltips')?>">
									<span class='glyphicon glyphicon-info-sign menu-icon'>
								</a>
							</label>

							<?php echo form_input( array(
								'name' => 'name',
								'value' => set_value( 'name', show_data( @$area->name ), false ),
								'class' => 'form-control form-control-sm',
								'placeholder' => get_msg( 'type_name' ),
								'id' => 'name'
							)); ?>
	              		</div>
			            <!-- form group -->
                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('itm_select_location')?>
                            </label>

                            <?php

                            $options=array();
                            $locations = $this->Itemlocation->get_all();
                            foreach($locations->result() as $location) {
                                $options[$location->id]=$location->name;
                            }

                            echo form_dropdown(
                                'location_id',
                                $options,
                                set_value( 'location_id', show_data( @$area->location_id), false ),
                                'class="form-control form-control-sm mr-3" id="location_id"'
                            );
                            ?>
                        </div>

                    </div>
            		
	              		
	            <!-- /.row -->
	        	</div>
	        <!-- /.card-body -->
	   		</div>
	   		<?php 
				if ( isset( $area )) { 
			?>
				<input type="hidden" id="edit_area" name="edit_area" value="1">
			<?php		
				} else {
			?>
				<input type="hidden" id="edit_area" name="edit_area" value="0">
			<?php } ?> 
			<div class="card-footer">
	            <button type="submit" class="btn btn-sm btn-primary">
					<?php echo get_msg('btn_save')?>
				</button>

				<a href="<?php echo $module_site_url; ?>" class="btn btn-sm btn-primary">
					<?php echo get_msg('btn_cancel')?>
				</a>
	        </div>
	       
		</div>

	</div>
</section>
				

	
	

<?php echo form_close(); ?>