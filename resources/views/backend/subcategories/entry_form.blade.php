<?php
	$attributes = array( 'id' => 'subcategory-form', 'enctype' => 'multipart/form-data');
	echo form_open( '', $attributes);
?>

	<section class="content animated fadeInRight">
		<div class="card card-info">
          <div class="card-header">
            <h3 class="card-title"><?php echo get_msg('subcat_info')?></h3>
          </div>

			<form role="form">
        		<div class="card-body">
					<div class="row">
						<div class="col-md-5">
							<div class="form-group">
								<label> <span style="font-size: 17px; color: red;">*</span>
									<?php echo get_msg('Prd_search_cat')?>
								</label>

								<?php
									$options=array();
									$options[0]=get_msg('Prd_search_cat');
									$categories = $this->Category->get_all();
										foreach($categories->result() as $cat) {
											$options[$cat->cat_id]=$cat->cat_name;
									}

									echo form_dropdown(
										'cat_id',
										$options,
										set_value( 'cat_id', show_data( @$subcategory->cat_id), false ),
										'class="form-control form-control-sm mr-3" id="cat_id"'
									);
								?>

							</div>

							<div class="form-group">
								<label> <span style="font-size: 17px; color: red;">*</span>
									<?php echo get_msg('subcat_name')?>
								</label>

								<?php echo form_input( array(
									'name' => 'name',
									'value' => set_value( 'name', show_data( @$subcategory->name), false ),
									'class' => 'form-control form-control-sm',
									'placeholder' => "لطفا نام زیر شاخه را انتخاب کنید",
									'id' => 'name'
								)); ?>

							</div>
						</div>

						
					
					</div>	

				</div>
			
				<div class="card-footer">
			     	<button type="submit" class="btn btn-sm btn-primary">
						<?php echo get_msg('btn_save')?>
					</button>

					<a href="<?php echo $module_site_url; ?>" class="btn btn-sm btn-primary">
						<?php echo get_msg('btn_cancel')?>
					</a>
			    </div>

			</div>


<?php echo form_close(); ?>

</section>