
<?php
$attributes = array('id' => 'about-form','enctype' => 'multipart/form-data');
echo form_open( '', $attributes);
?>
<section class="content animated fadeInRight">
	<div class="card card-info">
		 <div class="card-header">
	        <h3 class="card-title"><?php echo get_msg('app_info_lable')?></h3>
	    </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="row">
              	<div class="col-md-6">
	               						<!-- /.form-group -->
		            <div class="form-group">
						<label><?php echo get_msg('pravicypolicy_label')?>
							<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('pravicypolicy_label')?>">
								<span class='glyphicon glyphicon-info-sign menu-icon'>
							</a>
						</label>
						<textarea class="form-control" name="about_title" placeholder="قوانین و مقررات" rows="9"><?php echo $about->about_title; ?></textarea>
					</div>
                	<!-- /.form-group -->
		            <div class="form-group">
						<label><?php echo "توضیحات اپلیکیشن"?>
							<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('about_description_tooltips')?>">
								<span class='glyphicon glyphicon-info-sign menu-icon'>
							</a>
						</label>
						<textarea class="form-control" name="about_description" placeholder="توضیحات" rows="6"><?php echo $about->about_description; ?></textarea>
					</div>


					
        	  	</div>
              <!-- /.col -->
              	<div class="col-md-6">
              <br>

	


	                <?php if ( !isset( $about )): ?>

					<div class="form-group">
						<label><?php echo get_msg('about_img')?>
							<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('cat_photo_tooltips')?>">
								<span class='glyphicon glyphicon-info-sign menu-icon'>
							</a>
						</label>

						<br/>

						<input class="btn btn-sm" type="file" name="images1">
					</div>

					<?php else: ?>
					
					<label><?php echo get_msg('about_img')?>
						<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('cat_photo_tooltips')?>">
							<span class='glyphicon glyphicon-info-sign menu-icon'>
						</a>
					</label> 
					
					<div class="btn btn-sm btn-primary btn-upload pull-left" data-toggle="modal" data-target="#uploadImage">
						<?php echo get_msg('btn_replace_photo')?>
					</div>
					
					<hr/>
					
	                <?php
							$conds = array( 'img_type' => 'about', 'img_parent_id' => $about->about_id );
							$images = $this->Image->get_all_by( $conds )->result();
						?>
							
						<?php if ( count($images) > 0 ): ?>
							
							<div class="row">

							<?php $i = 0; foreach ( $images as $img ) :?>

								<?php if ($i>0 && $i%3==0): ?>
										
								</div><div class='row'>
								
								<?php endif; ?>
									
								<div class="col-md-4" style="height:100">

									<div class="thumbnail">

										<img src="<?php echo $this->ps_image->upload_thumbnail_url . $img->img_path; ?>">

										<br/>
										
										<p class="text-center">
											
											<a data-toggle="modal" data-target="#deletePhoto" class="delete-img" id="<?php echo $img->img_id; ?>"   
												image="<?php echo $img->img_path; ?>">
												حذف
											</a>
										</p>

									</div>

								</div>

							<?php $i++; endforeach; ?>

							</div>
						
						<?php endif; ?>

					<?php endif; ?>	
				
							<hr/>
					<div class="form-group">
						<div class="form-check">
							<label class="form-check-label">
							
							<?php echo form_checkbox( array(
								'name' => 'analyt_on',
								'id' => 'analyt_on',
								'value' => 'accept',
								'checked' => set_checkbox('analyt_on', 1, ( @$about->analyt_on == 1 )? true: false ),
								'class' => 'form-check-input'
							));	?>

							<?php echo get_msg( 'about_analyt_on' ); ?>

							</label>
						</div>
					</div>

					<div class="form-group">
						<label><?php echo get_msg('about_analyt_track_id')?>
							<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('about_analyt_track_id_tooltips')?>">
								<span class='glyphicon glyphicon-info-sign menu-icon'>
							</a>
						</label>
						<?php 
							echo form_input( array(
								'type' => 'text',
								'name' => 'analyt_track_id',
								'id' => 'analyt_track_id',
								'class' => 'form-control',
								'placeholder' => 'Analytic Tracking Id',
								'value' => set_value( 'analyt_track_id', show_data( @$about->analyt_track_id ), false )
							));
						?>
					</div>
	                <!-- /.form-group -->
	               <br>
				  
					<!-- End Category cover photo -->
					<div class="form-group">
						<label><?php echo get_msg('upload_point_label')?>
							<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('upload_point_label')?>">
								<span class='glyphicon glyphicon-info-sign menu-icon'>
							</a>
						</label>
						<?php 
							echo form_input( array(
								'type' => 'text',
								'name' => 'upload_point',
								'id' => 'upload_point',
								'class' => 'form-control',
								'placeholder' => 'Upload Point',
								'value' => set_value( 'upload_point', show_data( @$about->upload_point ), false )
							));
						?>
					</div>
					<div class="form-group">
						<label>روز بایگانی
									</label>
						<?php
							echo form_input( array(
								'type' => 'text',
								'name' => 'archive',
								'id' => 'archive',
								'class' => 'form-control',
								'placeholder' => 'Upload Point',
								'value' => set_value( 'archive', show_data( @$about->archive ), false )
							));
						?>
					</div>
	            </div>
	            <!-- /.col -->
	            <legend class="ml-3"><?php echo get_msg('about_social_section')?> <p style="color:red;font-size: 12px;">در صورت خالی گذاشتن فیلدی آن فیلد در اپلیکیشن مخفی میشود.</p></legend>
				<br>
				       
	            <div class="col-md-6">
				<div class="form-group">
						<label><?php echo get_msg('about_website_label')?>
							<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('about_website_tooltips')?>">
								<span class='glyphicon glyphicon-info-sign menu-icon'>
							</a>
						</label>
						<?php 
							echo form_input( array(
								'type' => 'text',
								'name' => 'about_website',
								'id' => 'about_website',
								'class' => 'form-control',
								'placeholder' => 'وب سایت',
								'value' => set_value( 'about_website', show_data( @$about->about_website ), false )
							));
						?>
					</div>
						<div class="form-group">
						<label><?php echo get_msg('about_phone_label')?>
							<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('about_phone_tooltips')?>">
								<span class='glyphicon glyphicon-info-sign menu-icon'>
							</a>
						</label>
						<?php 
							echo form_input( array(
								'type' => 'text',
								'name' => 'about_phone',
								'id' => 'about_phone',
								'class' => 'form-control',
								'placeholder' => 'شماره تلفن',
								'value' => set_value( 'about_phone', show_data( @$about->about_phone ), false )
							));
						?>
					</div>
					        	<!-- /.form-group -->
		            <div class="form-group">
						<label><?php echo get_msg('about_email_label')?>
							<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('about_email_tooltips')?>">
								<span class='glyphicon glyphicon-info-sign menu-icon'>
							</a>
						</label>
						<?php 
							echo form_input( array(
								'type' => 'text',
								'name' => 'about_email',
								'id' => 'about_email',
								'class' => 'form-control',
								'placeholder' => 'ایمیل',
								'value' => set_value( 'about_email', show_data( @$about->about_email ), false )
							));
						?>
					</div>
					<div class="form-group">
						<label><?php echo get_msg('about_pinterest_label')?>
							<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('about_pinterest_tooltips')?>">
								<span class='glyphicon glyphicon-info-sign menu-icon'>
							</a>
						</label>
						<?php 
							echo form_input( array(
								'type' => 'text',
								'name' => 'pinterest',
								'id' => 'pinterest',
								'class' => 'form-control',
								'placeholder' => 'Telegram',
								'value' => set_value( 'pinterest', show_data( @$about->pinterest ), false )
							));
						?>
					</div>
              		
        	  	</div>
         		<!-- /.col -->
          		<div class="col-md-6">
					<div class="form-group">
						<label><?php echo get_msg('about_youtube_label')?>
							<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('about_instagram_tooltips')?>">
								<span class='glyphicon glyphicon-info-sign menu-icon'>
							</a>
						</label>
						<?php 
							echo form_input( array(
								'type' => 'text',
								'name' => 'youtube',
								'id' => 'youtube',
								'class' => 'form-control',
								'placeholder' => 'Youtube',
								'value' => set_value( 'youtube', show_data( @$about->youtube ), false )
							));
						?>
					</div>
                        <div class="form-group">
						<label><?php echo get_msg('about_facebook_label')?>
							<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('about_facebook_tooltips')?>">
								<span class='glyphicon glyphicon-info-sign menu-icon'>
							</a>
						</label>
						<?php 
							echo form_input( array(
								'type' => 'text',
								'name' => 'facebook',
								'id' => 'facebook',
								'class' => 'form-control',
								'placeholder' => 'Facebook',
								'value' => set_value( 'facebook', show_data( @$about->facebook ), false )
							));
						?>
					</div>

					

					<div class="form-group">
						<label><?php echo get_msg('about_instagram_label')?>
							<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('about_instagram_tooltips')?>">
								<span class='glyphicon glyphicon-info-sign menu-icon'>
							</a>
						</label>
						<?php 
							echo form_input( array(
								'type' => 'text',
								'name' => 'instagram',
								'id' => 'instagram',
								'class' => 'form-control',
								'placeholder' => 'Instagram',
								'value' => set_value( 'instagram', show_data( @$about->instagram ), false )
							));
						?>
					</div>


					<div class="form-group">
						<label><?php echo get_msg('about_twitter_label')?>
							<a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('about_twitter_tooltips')?>">
								<span class='glyphicon glyphicon-info-sign menu-icon'>
							</a>
						</label>
						<?php 
							echo form_input( array(
								'type' => 'text',
								'name' => 'twitter',
								'id' => 'twitter',
								'class' => 'form-control',
								'placeholder' => 'Twitter',
								'value' => set_value( 'twitter', show_data( @$about->twitter ), false )
							));
						?>
					</div>
                <!-- /.form-group -->
            	</div>
            <!-- /.col -->

            </div>
            <!-- /.row -->
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
			<button type="submit" name="save" class="btn btn-sm btn-primary">
				<?php echo get_msg('btn_save')?>
			</button>

			<a href="<?php echo $module_site_url; ?>" class="btn btn-sm btn-primary">
				<?php echo get_msg('btn_cancel')?>
			</a>
		</div>
      
    </div>
    <!-- /.card info-->
</section>
<?php echo form_close(); ?>