<div class="table-responsive animated fadeInRight">
	<table class="table m-0 table-striped">
		<tr>
			<th><?php echo get_msg('no'); ?></th>
				<th><?php echo "آیکن"; ?></th>
			<th><?php echo get_msg('cat_name'); ?></th></th>
			
			<?php if ( $this->ps_auth->has_access( EDIT )): ?>
				
				<th><span class="th-title"><?php echo get_msg('btn_edit')?></span></th>
			
			<?php endif; ?>
			
			<?php if ( $this->ps_auth->has_access( DEL )): ?>
				
				<th><span class="th-title"><?php echo get_msg('btn_delete')?></span></th>
			
			<?php endif; ?>
			

		</tr>
		
	
	<?php $count = $this->uri->segment(4) or $count = 0; ?>

	<?php if ( !empty( $categories ) && count( $categories->result()) > 0 ): ?>

		<?php foreach($categories->result() as $category): ?>
			
			<tr>
				<td><?php echo ++$count;?></td>
				
								<?php 

                $default_photo = get_default_photo( $category->cat_id, 'category-icon' );

                if($default_photo->img_path != "") {
                 ?>   

                <td><img width="28" height="28" src="<?php echo img_url( 'thumbnail/'. $default_photo->img_path ); ?>"/></td>

              <?php } else { ?>
                <td><img width="28" height="28" src="<?php echo img_url( 'thumbnail/no_image.png'); ?>"/></td>
              <?php } ?>
			  
				<td ><?php echo $category->cat_name;?></td>

				<?php $default_photo = get_default_photo( $category->cat_id, 'category-icon' ); ?>	

				
				<?php if ( $this->ps_auth->has_access( EDIT )): ?>
			
					<td>
						<a href='<?php echo $module_site_url .'/edit/'. $category->cat_id; ?>'>
							<i style='font-size: 18px;' class='fa fa-pencil-square-o'></i>
						</a>
					</td>
				
				<?php endif; ?>

				<?php if ( $this->ps_auth->has_access( DEL )): ?>
					
					<td>
						<a herf='#' class='btn-delete' data-toggle="modal" data-target="#myModal" id="<?php echo $category->cat_id;?>">
							<i style='font-size: 18px;' class='fa fa-trash-o'></i>
						</a>
					</td>
				
				<?php endif; ?>
				
				

			</tr>

		<?php endforeach; ?>

	<?php else: ?>
			
		<?php $this->load->view( $template_path .'/partials/no_data' ); ?>

	<?php endif; ?>

</table>
</div>

