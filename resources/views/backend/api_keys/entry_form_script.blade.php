<script>
	function jqvalidate() {

		$(document).ready(function(){
			$('#apikey-form').validate({
				rules:{
					key:{
						required: true,
						minlength: 4
					}
				},
				messages:{
					key:{
						required: "لطفا کلید را وارد نمایید.",
						minlength: "طول کلید باید بیشتر از 4 باشد"
					}
				}
			});
		});
	}

</script>