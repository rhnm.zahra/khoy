<script>

	<?php if ( $this->config->item( 'client_side_validation' ) == true ): ?>

	function jqvalidate() {

		$('#location-form').validate({
			rules:{
				name:{
					blankCheck : "",
					minlength: 1,
					remote: "<?php echo $module_site_url .'/ajx_exists/'.@$location->id; ?>"
				}
			},
			messages:{
				name:{
					blankCheck : "<?php echo get_msg( 'err_location_name' ) ;?>",
					minlength: "<?php echo get_msg( 'err_location_len' ) ;?>",
					remote: "<?php echo get_msg( 'err_location_exist' ) ;?>."
				}
			}
		});
		// custom validation
		jQuery.validator.addMethod("blankCheck",function( value, element ) {
			
			   if(value == "") {
			    	return false;
			   } else {
			    	return true;
			   }
		})
	}

	<?php endif; ?>

	function runAfterJQ() {

		var edit_location_check = $('#edit_location').val();

		// if(edit_location_check == 0) {
		// 	$location_lat = 0.0;
		// 	$location_lng = 0.0;
		// } else if(edit_location_check == 1) {
		// 	$location_lat = $('#lat').val();
		// 	$location_lng = $('#lng').val();
		// }
	  
	    if(edit_location_check == 0) {
			 $('#us3').locationpicker({
	            location: {latitude: 0.0, longitude: 0.0},
	            radius: 300,
	            inputBinding: {
	                latitudeInput: $('#lat'),
	                longitudeInput: $('#lng'),
	                radiusInput: $('#us3-radius'),
	                locationNameInput: $('#find_location')
	            },
	            enableAutocomplete: true,
	            onchanged: function (currentLocation, radius, isMarkerDropped) {
	                // Uncomment line below to show alert on each Location Changed event
	                //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
	            }
	        });
		}
	
	}

</script>