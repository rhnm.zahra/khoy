<link rel="stylesheet" type="text/css" href="https://static.neshan.org/sdk/openlayers/5.3.0/ol.css">
<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList,URL"></script>
<script type="text/javascript" src="https://static.neshan.org/sdk/openlayers/5.3.0/ol.js"></script>
<style>
    #neshan_map_center_marker {
        width: 30px;
        height: 44px;
        background: transparent center center no-repeat;
        position: absolute;
        left: 50%;
        top: 50%;
        margin: -44px auto auto -15px;
    }
</style>
<?php
$attributes = array('id' => 'item-form', 'enctype' => 'multipart/form-data');
echo form_open('', $attributes);
?>
<p class="alert alert-warning">
    برای انتخاب چند گزینه در فیلدها لطفا از نگهداشتن دکمه ی ctrl روی کیبورد استفاده کنید
</p>
<section class="content animated fadeInRight">

    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title"><?php echo get_msg('prd_info') ?></h3>
        </div>
        <form role="form" enctype="multipart/form-data">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('itm_title_label') ?>
                            </label>
                            <?php echo form_input(array(
                                'name' => 'title',
                                'value' => set_value('title', show_data(@$item->title), false),
                                'class' => 'form-control form-control-sm',
                                'placeholder' => get_msg('itm_title_label'),
                                'id' => 'title'

                            )); ?>

                        </div>

                        <div class="form-group div_rahn" style="display: none;">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                رهن
                            </label>

                            <?php echo form_input(array(
                                'name' => 'rahn',
                                'value' => set_value('rahn', show_data(@number_format($item->item_rahn)), false),
                                'class' => 'form-control form-control-sm',
                                'onkeyup' => 'javascript:this.value=separate(this.value);',
                                'placeholder' => 'رهن',
                                'id' => 'rahn'

                            )); ?>

                        </div>

                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('Prd_search_cat') ?>
                            </label>

                            <?php
                            $options = array();
                            $conds['status'] = 1;
                            $options[0] = get_msg('Prd_search_cat');
                            $categories = $this->Category->get_all_by($conds);
                            foreach ($categories->result() as $cat) {
                                $options[$cat->cat_id] = $cat->cat_name;
                            }

                            echo form_dropdown(
                                'cat_id',
                                $options,
                                set_value('cat_id', show_data(@$item->cat_id), false),
                                'class="form-control form-control-sm mr-3" id="cat_id"'
                            );
                            ?>
                        </div>

                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('itm_select_price') ?>
                            </label>

                            <?php
                            $options = array();
                            $conds['status'] = 1;
                            $options[0] = get_msg('itm_select_price');
                            $pricetypes = $this->Pricetype->get_all_by($conds);
                            foreach ($pricetypes->result() as $price) {
                                $options[$price->id] = $price->name;
                            }

                            echo form_dropdown(
                                'item_price_type_id',
                                $options,
                                set_value('item_price_type_id', show_data(@$item->item_price_type_id), false),
                                'class="form-control form-control-sm mr-3" id="item_price_type_id"'
                            );
                            ?>
                        </div>


                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('itm_select_location') ?>
                            </label>

                            <?php

                            $options = array();
                            $options[0] = get_msg('itm_select_location');
                            $locations = $this->Itemlocation->get_all();
                            foreach ($locations->result() as $location) {
                                $options[$location->id] = $location->name;
                            }

                            echo form_dropdown(
                                'item_location_id',
                                $options,
                                set_value('item_location_id', show_data(@$item->item_location_id), false),
                                'class="form-control form-control-sm mr-3" id="item_location_id"'
                            );
                            ?>
                        </div>

                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                منطقه (برای مشاهده مناطق لطفا ابتدا شهر را انتخاب کنید)
                            </label>

                            <?php

                            $options = array();

                            echo form_dropdown(
                                'item_location_area_id',
                                $options,
                                set_value('item_location_area_id', json_decode(show_data(@$item->item_location_area_id), true), false),
                                'class="form-control form-control-sm mr-3" id="item_location_area_id"'
                            );
                            ?>
                        </div>

                        <div class="form-group" style="   display:none;">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('itm_select_deal_option') ?>
                            </label>

                            <?php
                            $options = array();
                            $conds['status'] = 1;
                            $options[0] = get_msg('deal_option_id_label');
                            $deals = $this->Option->get_all_by($conds);
                            foreach ($deals->result() as $deal) {
                                $options[$deal->id] = $deal->name;
                            }

                            echo form_dropdown(
                                'deal_option_id',
                                $options,
                                set_value('deal_option_id', show_data(@$item->deal_option_id), false),
                                'class="form-control form-control-sm mr-3" id="deal_option_id"'
                            );
                            ?>
                        </div>

                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('item_description_label') ?>
                            </label>

                            <?php echo form_textarea(array(
                                'name' => 'description',
                                'value' => set_value('description', show_data(@$item->description), false),
                                'class' => 'form-control form-control-sm',
                                'placeholder' => get_msg('item_description_label'),
                                'id' => 'description',
                                'rows' => "5"
                            )); ?>

                        </div>

                        <div class="form-group" style="   display:none;">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('prd_high_info') ?>
                            </label>

                            <?php echo form_textarea(array(
                                'name' => 'highlight_info',
                                'value' => set_value('info', show_data(@$item->highlight_info), false),
                                'class' => 'form-control form-control-sm',
                                'placeholder' => "لطفا اطلاعات خواسته شده را پر کنید",
                                'id' => 'info',
                                'rows' => "3"
                            )); ?>

                        </div>
                        <!-- form group -->
                    </div>

                    <div class="col-md-6">

                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('itm_select_type') ?>
                            </label>

                            <?php

                            $options = array();
                            $options[0] = get_msg('itm_select_type');
                            $types = $this->Itemtype->get_all();
                            foreach ($types->result() as $typ) {
                                $options[$typ->id] = $typ->name;
                            }

                            echo form_dropdown(
                                'item_type_id',
                                $options,
                                set_value('item_type_id', show_data(@$item->item_type_id), false),
                                'class="form-control form-control-sm mr-3" id="item_type_id"'
                            );
                            ?>
                        </div>

                        <div class="form-group div_rahn" style="display: none;">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                اجاره
                            </label>

                            <?php echo form_input(array(
                                'name' => 'ejare',
                                'value' => set_value('ejare', show_data(@number_format($item->item_ejare)), false),
                                'class' => 'form-control form-control-sm',
                                'onkeyup' => 'javascript:this.value=separate(this.value);',
                                'placeholder' => 'اجاره',
                                'id' => 'ejare'

                            )); ?>

                        </div>

                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('Prd_search_subcat') ?>
                            </label>

                            <?php
                            if (isset($item)) {
                                $options = array();
                                $options[0] = get_msg('Prd_search_subcat');
                                $conds['cat_id'] = $item->cat_id;
                                $sub_cat = $this->Subcategory->get_all_by($conds);
                                foreach ($sub_cat->result() as $subcat) {
                                    $options[$subcat->id] = $subcat->name;
                                }
                                echo form_dropdown(
                                    'sub_cat_id',
                                    $options,
                                    set_value('sub_cat_id', show_data(@$item->sub_cat_id), false),
                                    'class="form-control form-control-sm mr-3" id="sub_cat_id"'
                                );

                            } else {
                                $conds['cat_id'] = $selected_cat_id;
                                $options = array();
                                $options[0] = get_msg('Prd_search_subcat');

                                echo form_dropdown(
                                    'sub_cat_id',
                                    $options,
                                    set_value('sub_cat_id', show_data(@$item->sub_cat_id), false),
                                    'class="form-control form-control-sm mr-3" id="sub_cat_id"'
                                );
                            }

                            ?>

                        </div>
                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('price') ?>
                            </label>

                            <?php echo form_input(array(
                                'name' => 'price',
                                'value' => set_value('price', show_data(@$item->price), false),
                                'class' => 'form-control form-control-sm',
                                'onkeyup' => 'javascript:this.value=separate(this.value);',
                                'placeholder' => get_msg('price'),
                                'id' => 'price'

                            )); ?>

                        </div>


                        <div class="form-group" style="   display:none;">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('itm_select_currency') ?>
                            </label>

                            <?php
                            $options = array();
                            $conds['status'] = 1;
                            $options[0] = get_msg('itm_select_currency');
                            $currency = $this->Currency->get_all_by($conds);
                            foreach ($currency->result() as $curr) {
                                $options[$curr->id] = $curr->currency_short_form;
                            }

                            echo form_dropdown(
                                'item_currency_id',
                                $options,
                                set_value('item_currency_id', show_data(@$item->item_currency_id), false),
                                'class="form-control form-control-sm mr-3" id="item_currency_id"'
                            );
                            ?>
                        </div>

                        <div class="form-group" style="   display:none;">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('itm_select_condition_of_item') ?>
                            </label>

                            <?php
                            $options = array();
                            $conds['status'] = 1;
                            $options[0] = get_msg('condition_of_item');
                            $conditions = $this->Condition->get_all_by($conds);
                            foreach ($conditions->result() as $cond) {
                                $options[$cond->id] = $cond->name;
                            }

                            echo form_dropdown(
                                'condition_of_item_id',
                                $options,
                                set_value('condition_of_item_id', show_data(@$item->condition_of_item_id), false),
                                'class="form-control form-control-sm mr-3" id="condition_of_item_id"'
                            );
                            ?>
                        </div>

                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('itm_address_label') ?>
                            </label>

                            <?php echo form_input(array(
                                'name' => 'address',
                                'value' => set_value('address', show_data(@$item->address), false),
                                'class' => 'form-control form-control-sm',
                                'placeholder' => get_msg('itm_address_label'),
                                'id' => 'address'
                            )); ?>

                        </div>
                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo "شماره تلفن" ?>
                            </label>

                            <?php echo form_input(array(
                                'name' => 'brand',
                                'value' => set_value('brand', show_data(@$item->brand), false),
                                'class' => 'form-control form-control-sm',
                                'placeholder' => "شماره تلفن",
                                'id' => 'brand'

                            )); ?>

                        </div>

                        <div class="form-group">
                            <label>
                                <?php echo "شماره واتساپ" ?>
                            </label>

                            <?php echo form_input(array(
                                'name' => 'whatsapp_number',
                                'value' => set_value('whatsapp_number', show_data(@$item->whatsapp_number), false),
                                'class' => 'form-control form-control-sm',
                                'placeholder' => "شماره واتساپ",
                                'id' => 'whatsapp_number'

                            )); ?>

                        </div>
                        <div class="form-group">
                            <label>
                                <?php echo "شماره کارشناس" ?>
                            </label>

                            <?php echo form_input(array(
                                'name' => 'personnel',
                                'value' => set_value('personnel', show_data(@$item->personnel), false),
                                'class' => 'form-control form-control-sm',
                                'placeholder' => "شماره کارشناس",
                                'id' => 'personnel'

                            )); ?>

                        </div>

                        <div id="moaveze" style="display: none">
                            <div class="form-group">
                                <label> <span style="font-size: 17px; color: red;">*</span>
                                    <?php echo "تمایل به معاوضه دارید ؟" ?>
                                </label>

                                <?php
                                $options = array();
                                $options[0] = 'خیر';
                                $options[1] = 'بله';

                                echo form_dropdown(
                                    'change_status',
                                    $options,
                                    set_value('change_status', show_data(@$item->change_status), false),
                                    'class="form-control form-control-sm mr-3" id="change_status"'
                                );
                                ?>

                            </div>
                            <div id="change_div" style="border: 2px solid rgb(255 255 255);
    padding: 4px;
    /* background-color: #b5e1e3; */
    background: linear-gradient(1deg, rgb(39 33 155) 0%, rgb(2 88 76) 0%, rgba(133,198,218,1) 54%, rgba(0,212,255,1) 100%);">
                                <p class="alert alert-danger">
                                    مبلغ نقد موجود و مبلغ نقد درخواستی یکی باید تکمیل شود
                                </p>
                                <div class="form-group">
                                    <label>
                                        مبلغ نقد موجود(تومان)
                                    </label>

                                    <?php echo form_input(array(
                                        'name' => 'price_avl',
                                        'value' => set_value('price_avl', show_data(@number_format($item->price_avl)), false),
                                        'class' => 'form-control form-control-sm',
                                        'onkeyup' => 'javascript:this.value=separate(this.value);',
                                        'placeholder' => 'مبلغ نقد موجود(تومان)',
                                        'id' => 'price_avl'
                                    )); ?>

                                </div>
                                <div class="form-group">
                                    <label>
                                        مبلغ نقد درخواستی(تومان)
                                    </label>

                                    <?php echo form_input(array(
                                        'name' => 'price_req',
                                        'value' => set_value('price_req', show_data(@number_format($item->price_req)), false),
                                        'class' => 'form-control form-control-sm',
                                        'onkeyup' => 'javascript:this.value=separate(this.value);',
                                        'placeholder' => 'مبلغ نقد درخواستی(تومان)',
                                        'id' => 'price_req'
                                    )); ?>

                                </div>
                                <div class="form-group">
                                    <label> <span style="font-size: 17px; color: red;">*</span>
                                        مناطق (برای مشاهده مناطق لطفا ابتدا شهر را انتخاب کنید)
                                    </label>

                                    <?php

                                    $options = array();

                                    echo form_multiselect(
                                        'item_area_id[]',
                                        $options,
                                        set_value('item_area_id[]', json_decode(show_data(@$item->item_area_id), true), false),
                                        'class="form-control form-control-sm mr-3" id="item_area_id"'
                                    );
                                    ?>
                                </div>

                                <div class="form-group">
                                    <label> <span style="font-size: 17px; color: red;">*</span>
                                        نوع ملک
                                    </label>

                                    <?php

                                    $options = array();
                                    $cats = $this->Itemcat->get_all();
                                    foreach ($cats->result() as $cat) {
                                        $options[$cat->id] = $cat->name;
                                    }

                                    echo form_multiselect(
                                        'item_cat_id[]',
                                        $options,
                                        set_value('item_cat_id[]', json_decode(show_data(@$item->item_cat_id), true), false),
                                        'class="form-control form-control-sm mr-3" id="item_cat_id"'
                                    );
                                    ?>
                                </div>

                            </div>
                        </div>
                        <br>

                        <div class="form-group">
                            <div class="form-check" style="margin-right:5px;">
                                <label>

                                    <?php echo form_checkbox(array(
                                        'name' => 'is_sold_out',
                                        'id' => 'is_sold_out',
                                        'value' => 'accept',
                                        'checked' => set_checkbox('is_sold_out', 1, (@$item->is_sold_out == 1) ? true : false),
                                        'class' => 'form-check-input'
                                    )); ?>

                                    <?php echo get_msg('itm_is_sold_out'); ?>

                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-check" style="margin-right:5px;">
                                <label>

                                    <?php echo form_checkbox(array(
                                        'name' => 'item_hidden',
                                        'id' => 'item_hidden',
                                        'value' => 'accept',
                                        'checked' => set_checkbox('item_hidden', 1, (@$item->item_hidden == 1) ? true : false),
                                        'class' => 'form-check-input'
                                    )); ?>

                                    آگهی مخفی

                                </label>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-12" id="branchs">

                        <div class="row">

                            <div class="form-group col-md-3">
                                <label> <span style="font-size: 11px; color: red;">*</span>
                                    <?php echo "آیا کارشناس هستید ؟" ?>
                                </label>

                                <?php
                                $options = array();
                                $options[0] = 'خیر';
                                $options[1] = 'بله';

                                echo form_dropdown(
                                    'branch_status',
                                    $options,
                                    set_value('branch_status', show_data(@$item->branch_status), false),
                                    'class="form-control form-control-sm mr-3" id="branch_status"'
                                );
                                ?>

                            </div>
                            <div class="form-group branch_box col-md-3" style="display: none;">
                                <label> <span style="font-size: 11px; color: red;">*</span>
                                    شعبه ( ابتدا شهر را انتخاب کنید)
                                </label>

                                <?php

                                $options = array();

                                echo form_dropdown(
                                    'item_branch_id',
                                    $options,
                                    set_value('item_branch_id', json_decode(show_data(@$item->item_branch_id), true), false),
                                    'class="form-control form-control-sm mr-3"  id="item_branch_id"'
                                );
                                ?>
                            </div>
                            <div class="form-group branch_box col-md-3" style="display: none;">
                                <label> <span style="font-size: 11px; color: red;">*</span>
                                    کارشناسان ( ابتدا شعبه را انتخاب کنید)
                                </label>

                                <?php

                                $options = array();

                                echo form_multiselect(
                                    'item_branch_personnels[]',
                                    $options,
                                    set_value('item_branch_personnels[]', json_decode(show_data(@$item->item_branch_personnels), true), false),
                                    'class="form-control form-control-sm mr-3" title="انتخاب کارشناسان"   id="item_branch_personnels"'
                                );
                                ?>
                            </div>

                        </div>

                    </div>

                    <div class="col-md-6">


                        <!--  <label><?php echo get_msg('deal_option_id_label') ?></label><br>
              <input type="radio" name="deal_option_id"
              <?php if (isset($item->deal_option_id) && $item->deal_option_id == "1") echo "checked"; ?>
              value="1"><?php echo get_msg('meet_up_label'); ?>
              <input type="radio" name="deal_option_id"
              <?php if (isset($item->deal_option_id) && $item->deal_option_id == "2") echo "checked"; ?>
              value="2"><?php echo get_msg('mailing_or_delivery_label'); ?> -->

                        <br><br>


                    </div>

                    <div class="col-md-6">
                        <div class="form-group" style="   display:none;">
                            <div class="form-check">
                                <label>

                                    <?php echo form_checkbox(array(
                                        'name' => 'business_mode',
                                        'id' => 'business_mode',
                                        'value' => 'accept',
                                        'checked' => set_checkbox('business_mode', 1, (@$item->business_mode == 1) ? true : false),
                                        'class' => 'form-check-input'
                                    )); ?>

                                    <?php echo get_msg('itm_business_mode'); ?>
                                    <br><?php echo get_msg('itm_show_shop') ?>
                                </label>
                            </div>
                        </div>


                        <!-- form group -->
                    </div>

                    <div class="col-md-12">
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left text-white" type="button"
                                                data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                                aria-controls="collapseOne">
                                            نقشه
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                     data-parent="#accordionExample">
                                    <div class="card-body row">


                                        <div <?php if($item->item_type_id==1) { ?> style="display: none;" <?php } ?>  class="col-md-6">
                                            <label> <span style="font-size: 17px; color: red;"></span>
                                                در صورت عدم مشاهده نقشه میتوانید از  <a href="https://www.latlong.net" target="_blank">این لینک</a> استفاده نمایید.
                                            </label>
                                            <div id="us3" style="display:none;width: 100%; height: 300px;"></div>
                                            <div id="map" style="width: 100%; height: 300px;"></div>
                                            <div class="clearfix">&nbsp;</div>
                                        </div>


                                        <div <?php if($item->item_type_id==1) { ?> style="display: none;" <?php } ?>  class="col-md-6">
                                            <div class="form-group">
                                                <label><?php echo get_msg('itm_lat_label') ?>
                                                    <a href="#" class="tooltip-ps" data-toggle="tooltip" title="<?php echo get_msg('city_lat_label')?>">
                  <span class='glyphicon glyphicon-info-sign menu-icon'>
                                                    </a>
                                                </label>

                                                <br>

                                                <?php
                                                echo form_input( array(
                                                    'type' => 'text',
                                                    'name' => 'lat',
                                                    'id' => 'lat',
                                                    'class' => 'form-control',
                                                    'placeholder' => '',
                                                    'value' => ''
                                                ));
                                                ?>
                                            </div>

                                            <div class="form-group">
                                                <label><?php echo get_msg('itm_lng_label') ?>
                                                    <a href="#" class="tooltip-ps" data-toggle="tooltip"
                                                       title="<?php echo get_msg('city_lng_tooltips')?>">
                  <span class='glyphicon glyphicon-info-sign menu-icon'>
                                                    </a>
                                                </label>

                                                <br>

                                                <?php
                                                echo form_input( array(
                                                    'type' => 'text',
                                                    'name' => 'lng',
                                                    'id' => 'lng',
                                                    'class' => 'form-control',
                                                    'placeholder' => '',
                                                    'value' =>  ''
                                                ));
                                                ?>
                                            </div>
                                            <!-- form group -->

                                            <div class="form-check" >
                                                <label>

                                                    <?php echo form_checkbox(array(
                                                        'name' => 'pointer',
                                                        'id' => 'pointer',
                                                        'value' => 'accept',
                                                        'checked' => set_checkbox('pointer', 1, (@$item->pointer == 1) ? true : false),
                                                        'class' => 'form-check-input'
                                                    )); ?>

                                                    عدم نمایش دقیق موقعیت روی نقشه

                                                </label>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="row" style="display: block;">
                            <div class="form-group" style="display: block;">
                                <input type="file" id="files" style="display: none" name="images[]"
                                       accept="image/png, image/jpeg"/>
                                <label class="selectorfiles" for="files">افزودن تصویر</label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- row -->
            </div>

            <!-- Grid row -->
            <div class="gallery gallery-bsitem" id="gallery" style="margin-right: 15px; margin-bottom: 15px;">
                <?php
                $images = isset($images) ? $images : [];
                ?>
                <?php $i = 0;
                foreach ($images as $img) : ?>
                    <div class="mb-3 pics animation all 2">
                        <a href="#<?php echo $i; ?>"><img class="img-fluid"
                                                          src="<?php echo '/ssag/divar/uploads/' . $img['img_path']; ?>"
                                                          alt="Card image cap"></a>
                        <input type="hidden" name="updated_images[]" value="<?php echo $img['img_id']; ?>"/>
                        <button type="button" class="removegal">حذف</button>
                    </div>
                    <?php $i++; endforeach; ?>
            </div>
            <!-- Grid row -->
            <style>
                .gallery-bsitem {
                    display: flex;
                    flex-wrap: wrap;
                }

                .gallery-bsitem > div {
                    width: 300px;
                    height: 250px;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    background: #eee;
                    padding: 40px 15px;
                    margin: 15px;
                    border-radius: 5px;
                    flex-direction: column;
                    position: relative;
                }

                .gallery-bsitem > div a,
                .gallery-bsitem > div img {
                    width: 100%;
                    height: 100%;
                }

                .gallery-bsitem > div button {
                    position: absolute;
                    bottom: 0px;
                    left: 0px;
                    width: 100%;
                    height: 30px;
                    background: transparent;
                    border: 1px solid red;
                    color: red;
                    cursor: pointer;
                }

                .selectorfiles {
                    width: 100%;
                    height: 35px;
                    cursor: pointer;
                    background: green;
                    color: #fff;
                    display: flex;
                    text-align: center;
                    justify-content: center;
                    align-items: center;
                    border-radius: 3px;
                }
            </style>
            <script>
                $(function () {
                    $('body').on('click', 'button.removegal', function () {
                        const index = $(this).attr('data-index');
                        if (index != undefined) {
                            const dt = new DataTransfer();
                            const input = document.getElementById('files');
                            const {files} = input;
                            for (let i = 0; i < files.length; i++) {
                                const file = files[i];
                                if (index !== i) ;
                                dt.items.add(file);
                            }
                            input.files = dt.files;
                        }
                        $(this).parent().remove();
                    });
                    $('#files').on('change', function (e) {
                        var files = e.target.files;
                        if (files.length > 7) {
                            return;
                        }
                        for (var i = 0; i < files.length; i++) {
                            var file = files[i];
                            var fileurl = window.URL.createObjectURL(file);
                            var r = Math.floor((Math.random() * 1000) + 1);
                            var template = '<div id="sub' + r + '" class="mb-3 pics animation all 2"><a  style="background-image: url(' + fileurl + ');background-position: center center;background-repeat: no-repeat;background-size: contain;" href="javascript:void(0);"></a><button type="button" class="removegal" data-index="' + i + '">حذف</button></div>';
                            jQuery('#gallery').append(template);
                            jQuery("#files").clone().attr('id', 'ff_'+r).prependTo("#sub" + r);
                        }
                    });
                });
            </script>

            <div class="card-body">
            <hr>
            <h3 class="text-center">
                ویژگی ها
            </h3>


            <div style="margin-top: 20px;"  class="row">
                <label class="col-md-3">

                    <?php echo form_checkbox(array(
                        'name' => 'amlak',
                        'id' => 'amlak',
                        'value' => 'accept',
                        'checked' => set_checkbox('amlak', 1, (@$item->amlak == 1) ? true : false),
                        'class' => 'form-check-input'
                    )); ?>

                    املاک هستید ؟

                </label>
                <label class="col-md-3">

                    <?php echo form_checkbox(array(
                        'name' => 'rahnchange',
                        'id' => 'rahnchange',
                        'value' => 'accept',
                        'checked' => set_checkbox('rahnchange', 1, (@$item->rahnchange == 1) ? true : false),
                        'class' => 'form-check-input'
                    )); ?>

                    قابلیت تبدیل و رهن اجاره

                </label>
                <label class="col-md-3">

                    <?php echo form_checkbox(array(
                        'name' => 'asansor',
                        'id' => 'asansor',
                        'value' => 'accept',
                        'checked' => set_checkbox('asansor', 1, (@$item->asansor == 1) ? true : false),
                        'class' => 'form-check-input'
                    )); ?>

                    آسانسور

                </label>
                <label class="col-md-3">

                    <?php echo form_checkbox(array(
                        'name' => 'parking',
                        'id' => 'parking',
                        'value' => 'accept',
                        'checked' => set_checkbox('parking', 1, (@$item->parking == 1) ? true : false),
                        'class' => 'form-check-input'
                    )); ?>

                    پارکینگ

                </label>
                <label class="col-md-3">

                    <?php echo form_checkbox(array(
                        'name' => 'anbari',
                        'id' => 'anbari',
                        'value' => 'accept',
                        'checked' => set_checkbox('anbari', 1, (@$item->anbari == 1) ? true : false),
                        'class' => 'form-check-input'
                    )); ?>

                    انباری

                </label>
                <label class="col-md-3">

                    <?php echo form_checkbox(array(
                        'name' => 'balkon',
                        'id' => 'balkon',
                        'value' => 'accept',
                        'checked' => set_checkbox('balkon', 1, (@$item->balkon == 1) ? true : false),
                        'class' => 'form-check-input'
                    )); ?>

                    بالکن

                </label>
                <label class="col-md-3">

                    <?php echo form_checkbox(array(
                        'name' => 'vam',
                        'id' => 'vam',
                        'value' => 'accept',
                        'checked' => set_checkbox('vam', 1, (@$item->vam == 1) ? true : false),
                        'class' => 'form-check-input'
                    )); ?>

                    وام دارد

                </label>
                <label class="col-md-3">

                    <?php echo form_checkbox(array(
                        'name' => 'bazsazi',
                        'id' => 'bazsazi',
                        'value' => 'accept',
                        'checked' => set_checkbox('bazsazi', 1, (@$item->bazsazi == 1) ? true : false),
                        'class' => 'form-check-input'
                    )); ?>

                    بازسازی شده

                </label>
            </div>

            <div style="margin-top: 20px;" class="row">
                <div class="form-group col-md-3">
                    <label>
                        سال ساخت
                    </label>

                    <?php
                    $options = array();
                    $options[''] = 'سال ساخت';
                    $options += ITEM_YEAR;
                    echo form_dropdown(
                        'year',
                        $options,
                        set_value('year', show_data(@$item->year), false),
                        'class="form-control form-control-sm mr-3" id="year"'
                    );
                    ?>
                </div>
                <div class="form-group col-md-3">
                    <label>
                        تعداد اتاق
                    </label>

                    <?php
                    $options = array();
                    $options[''] = 'تعداد اتاق';
                    $options += ITEM_ROOM;
                    echo form_dropdown(
                        'room',
                        $options,
                        set_value('room', show_data(@$item->room), false),
                        'class="form-control form-control-sm mr-3" id="room"'
                    );
                    ?>
                </div>

                <div class="form-group col-md-3">
                    <label>
                        تعداد سرویس بهداشتی
                    </label>

                    <?php
                    $options = array();
                    $options[''] = 'تعداد سرویس بهداشتی';
                    $options += ITEM_WC;

                    echo form_dropdown(
                        'wc',
                        $options,
                        set_value('wc', show_data(@$item->wc), false),
                        'class="form-control form-control-sm mr-3" id="wc"'
                    );
                    ?>
                </div>
                <div class="form-group col-md-3">
                    <label>
                        تعداد حمام
                    </label>

                    <?php
                    $options = array();
                    $options[''] = 'تعداد حمام';
                    $options += ITEM_BATHROOM;

                    echo form_dropdown(
                        'bathroom',
                        $options,
                        set_value('bathroom', show_data(@$item->bathroom), false),
                        'class="form-control form-control-sm mr-3" id="bathroom"'
                    );
                    ?>
                </div>
                <div class="form-group col-md-3">
                    <label>
                        تعداد واحد در هر طبقه
                    </label>

                    <?php
                    $options = array();
                    $options[''] = 'تعداد واحد در هر طبقه';
                    $options += ITEM_UNIT;

                    echo form_dropdown(
                        'unit',
                        $options,
                        set_value('unit', show_data(@$item->unit), false),
                        'class="form-control form-control-sm mr-3" id="unit"'
                    );
                    ?>
                </div>
                <div class="form-group col-md-3">
                    <label>
                        تعداد کل طبقات
                    </label>

                    <?php
                    $options = array();
                    $options[''] = 'تعداد کل طبقات';
                    $options += ITEM_FLOORCOUNT;

                    echo form_dropdown(
                        'floorcount',
                        $options,
                        set_value('floorcount', show_data(@$item->floorcount), false),
                        'class="form-control form-control-sm mr-3" id="floorcount"'
                    );
                    ?>
                </div>
                <div class="form-group col-md-3">
                    <label>
                        طبقه چندم
                    </label>

                    <?php
                    $options = array();
                    $options[''] = 'طبقه چندم';
                    $options += ITEM_FLOOR;

                    echo form_dropdown(
                        'floor',
                        $options,
                        set_value('floor', show_data(@$item->floor), false),
                        'class="form-control form-control-sm mr-3" id="floor"'
                    );
                    ?>
                </div>

                <div class="form-group col-md-3">
                    <label>
                        وضعیت تخلیه
                    </label>

                    <?php
                    $options = array();
                    $options[''] = 'وضعیت تخلیه';
                    $options += ITEM_EMPTY;

                    echo form_dropdown(
                        'empty',
                        $options,
                        set_value('empty', show_data(@$item->empty), false),
                        'class="form-control form-control-sm mr-3" id="empty"'
                    );
                    ?>
                </div>
                <div class="form-group col-md-3" id="metrazh_div">
                    <label>
                        متراژ زمین
                    </label>

                    <?php echo form_input(array(
                        'name' => 'metrazh',
                        'value' => set_value('metrazh', show_data(@number_format($item->metrazh)), false),
                        'class' => 'form-control form-control-sm',
                        'onkeyup' => 'javascript:this.value=separate(this.value);',
                        'placeholder' => 'متراژ زمین',
                        'id' => 'metrazh'

                    )); ?>

                </div>
                <div class="form-group col-md-3" id="zirbana_div">
                    <label>
                        متراژ زیربنا
                    </label>

                    <?php echo form_input(array(
                        'name' => 'zirbana',
                        'value' => set_value('zirbana', show_data(@number_format($item->zirbana)), false),
                        'class' => 'form-control form-control-sm',
                        'onkeyup' => 'javascript:this.value=separate(this.value);',
                        'placeholder' => 'متراژ زیربنا',
                        'id' => 'zirbana'

                    )); ?>

                </div>
                <div class="form-group col-md-3">
                    <label>
                        طول بر ساختمان
                    </label>

                    <?php echo form_input(array(
                        'name' => 'toolbar',
                        'value' => set_value('toolbar', show_data(@number_format($item->toolbar)), false),
                        'class' => 'form-control form-control-sm',
                        'onkeyup' => 'javascript:this.value=separate(this.value);',
                        'placeholder' => 'طول بر ساختمان',
                        'id' => 'toolbar'

                    )); ?>

                </div>
                <div class="form-group col-md-3">
                    <label>
                        متراژ کوچه / خیابان
                    </label>

                    <?php echo form_input(array(
                        'name' => 'arz',
                        'value' => set_value('arz', show_data(@number_format($item->arz)), false),
                        'class' => 'form-control form-control-sm',
                        'onkeyup' => 'javascript:this.value=separate(this.value);',
                        'placeholder' => 'متراژ کوچه / خیابان',
                        'id' => 'arz'

                    )); ?>

                </div>
            </div>

            <hr>
            <h3 class="text-center">
                اطلاعات بیشتر
            </h3>

            <div style="margin-top: 20px;" class="row">
                <div class="form-group col-md-3">
                    <label>
                        جهت ساختمان
                    </label>

                    <?php
                    $options = array();
                    //                                $options[''] = 'جهت ساختمان';
                    $options += ITEM_DIRECTION;

                    echo form_multiselect(
                        'direction[]',
                        $options,
                        set_value('direction[]', json_decode(show_data(@$item->direction),true), false),
                        'class="form-control form-control-sm mr-3" title="انتخاب کنید" id="direction"'
                    );
                    ?>
                </div>
                <div class="form-group col-md-3">
                    <label>
                        نوع سند
                    </label>

                    <?php
                    $options = array();
                    //                                        $options[''] = 'مشخصه آشپزخانه';
                    $options += ITEM_SANAD;

                    echo form_multiselect(
                        'sanad[]',
                        $options,
                        set_value('sanad[]', json_decode(show_data(@$item->sanad),true), false),
                        'class="form-control form-control-sm mr-3"  title="انتخاب کنید" id="sanad"'
                    );
                    ?>
                </div>
                <div class="form-group col-md-3">
                    <label>
                        کاربری ملک
                    </label>

                    <?php
                    $options = array();
                    //                                        $options[''] = 'مشخصه آشپزخانه';
                    $options += ITEM_USE;

                    echo form_multiselect(
                        'usemelk[]',
                        $options,
                        set_value('usemelk[]', json_decode(show_data(@$item->usemelk),true), false),
                        'class="form-control form-control-sm mr-3"  title="انتخاب کنید" id="usemelk"'
                    );
                    ?>
                </div>
                <div class="form-group col-md-3">
                    <label>
                        جنس کف
                    </label>

                    <?php
                    $options = array();
                    //                                        $options[''] = 'جنس کف';
                    $options += ITEM_KAF;

                    echo form_multiselect(
                        'kaf[]',
                        $options,
                        set_value('kaf[]', json_decode(show_data(@$item->kaf),true), false),
                        'class="form-control form-control-sm mr-3"  title="انتخاب کنید" id="kaf"'
                    );
                    ?>
                </div>
                <div class="form-group col-md-3">
                    <label>
                        سرمایش گرمایشی
                    </label>

                    <?php
                    $options = array();
                    //                                        $options[''] = 'سرمایش گرمایشی';
                    $options += ITEM_SARMAYESH;

                    echo form_multiselect(
                        'sarmayesh[]',
                        $options,
                        set_value('sarmayesh[]', json_decode(show_data(@$item->sarmayesh),true), false),
                        'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="sarmayesh"'
                    );
                    ?>
                </div>
                <div class="form-group col-md-3">
                    <label>
                        مشخصه ملک
                    </label>

                    <?php
                    $options = array();
                    //                                        $options[''] = 'مشخصه ملک';
                    $options += ITEM_MOSHAKHASE;

                    echo form_multiselect(
                        'moshakhase[]',
                        $options,
                        set_value('moshakhase[]', json_decode(show_data(@$item->moshakhase),true), false),
                        'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="moshakhase"'
                    );
                    ?>
                </div>
                <div class="form-group col-md-3">
                    <label>
                        تامین کننده آب گرم
                    </label>

                    <?php
                    $options = array();
                    //                                        $options[''] = 'تامین کننده آب گرم';
                    $options += ITEM_ABEGARM;

                    echo form_multiselect(
                        'abegarm[]',
                        $options,
                        set_value('abegarm[]', json_decode(show_data(@$item->abegarm),true), false),
                        'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="abegarm"'
                    );
                    ?>
                </div>
                <div class="form-group col-md-3">
                    <label>
                        کابینت بندی
                    </label>

                    <?php
                    $options = array();
                    //                                        $options[''] = 'کابینت بندی';
                    $options += ITEM_KABINET;

                    echo form_multiselect(
                        'kabinet[]',
                        $options,
                        set_value('kabinet[]', json_decode(show_data(@$item->kabinet),true), false),
                        'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="kabinet"'
                    );
                    ?>
                </div>
                <div class="form-group col-md-3">
                    <label>
                        جنس دیوارها
                    </label>

                    <?php
                    $options = array();
                    //                                        $options[''] = 'جنس دیوارها';
                    $options += ITEM_DIVAR;

                    echo form_multiselect(
                        'divar[]',
                        $options,
                        set_value('divar[]', json_decode(show_data(@$item->divar),true), false),
                        'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="divar"'
                    );
                    ?>
                </div>
                <div class="form-group col-md-3">
                    <label>
                        نمای ساختمان
                    </label>

                    <?php
                    $options = array();
                    //                                        $options[''] = 'نمای ساختمان';
                    $options += ITEM_NAMA;

                    echo form_multiselect(
                        'nama[]',
                        $options,
                        set_value('nama[]', json_decode(show_data(@$item->nama),true), false),
                        'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="nama"'
                    );
                    ?>
                </div>
                <div class="form-group col-md-3">
                    <label>
                        امکانات امنیتی
                    </label>

                    <?php
                    $options = array();
                    //                                        $options[''] = 'امکانات امنیتی';
                    $options += ITEM_AMNIAT;

                    echo form_multiselect(
                        'amniat[]',
                        $options,
                        set_value('amniat[]', json_decode(show_data(@$item->amniat),true), false),
                        'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="amniat"'
                    );
                    ?>
                </div>
                <div class="form-group col-md-3">
                    <label>
                        امکانات رفاهی تفریحی
                    </label>

                    <?php
                    $options = array();
                    //                                        $options[''] = 'امکانات رفاهی تفریحی';
                    $options += ITEM_REFAHI;

                    echo form_multiselect(
                        'refahi[]',
                        $options,
                        set_value('refahi[]', json_decode(show_data(@$item->refahi),true), false),
                        'class="form-control form-control-sm mr-3"  title="انتخاب کنید" id="refahi"'
                    );
                    ?>
                </div>
                <div class="form-group col-md-3">
                    <label>
                        مشخصه آشپزخانه
                    </label>

                    <?php
                    $options = array();
                    //                                        $options[''] = 'مشخصه آشپزخانه';
                    $options += ITEM_ASHPAZKHANE;

                    echo form_multiselect(
                        'ashpazkhane[]',
                        $options,
                        set_value('ashpazkhane[]', json_decode(show_data(@$item->ashpazkhane),true), false),
                        'class="form-control form-control-sm mr-3"  title="انتخاب کنید" id="ashpazkhane"'
                    );
                    ?>
                </div>

            </div>

            </div>
            <hr>
            <h3 class="text-center">
                اطلاعات محرمانه آگهی
            </h3>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            نام مالک
                        </label>
                        <?php echo form_input(array(
                            'name' => 'malek',
                            'value' => set_value('malek', show_data(@$item->malek), false),
                            'class' => 'form-control form-control-sm',
                            'placeholder' => 'مالک',
                            'id' => 'malek'

                        )); ?>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            شماره تماس
                        </label>
                        <?php echo form_input(array(
                            'name' => 'tamas_malek',
                            'value' => set_value('tamas_malek', show_data(@$item->tamas_malek), false),
                            'class' => 'form-control form-control-sm',
                            'placeholder' => 'شماره تماس',
                            'id' => 'tamas_malek'

                        )); ?>

                    </div>
                </div>
                <div class="col-md-12 mt-2">
                    <div class="form-group">
                        <label>
                            توضیحات محرمانه
                        </label>
                        <?php echo form_textarea(array(
                            'name' => 'desc_malek',
                            'value' => set_value('desc_malek', show_data(@$item->desc_malek), false),
                            'class' => 'form-control form-control-sm',
                            'placeholder' => 'توضیحات محرمانه',
                            'id' => 'desc_malek'

                        )); ?>

                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row" style="display: block;">
                        <div class="form-group" style="display: block;">
                            <input type="file" id="files_malek" style="display: none" name="images_malek[]"
                                   accept="image/png, image/jpeg"/>
                            <label class="selectorfiles" for="files_malek">افزودن تصویر محرمانه</label>
                        </div>
                    </div>
                </div>
                <!-- Grid row -->
                <div class="gallery gallery-bsitem" id="gallery_malek" style="margin-right: 15px; margin-bottom: 15px;">
                    <?php
                    $images_malek = isset($images_malek) ? $images_malek : [];
                    ?>
                    <?php $i = 0;
                    foreach ($images_malek as $img) : ?>
                        <div class="mb-3 pics animation all 2">
                            <a style="background-image: url('<?php echo '/ssag/Melkekhoy/uploads/' . $img['img_path']; ?>');
                                    background-position: center center;
                                    background-repeat: no-repeat;background-size: contain;"
                               href="#<?php echo $i; ?>"></a>
                            <input type="hidden" name="updated_images_malek[]" value="<?php echo $img['img_id']; ?>"/>
                            <button type="button" class="removegal">حذف</button>
                        </div>
                        <?php $i++; endforeach; ?>
                </div>
                <script>
                    jQuery(function () {

                        jQuery('#files_malek').on('change', function (e) {
                            var files = e.target.files;
                            if (files.length > 7) {
                                return;
                            }
                            for (var i = 0; i < files.length; i++) {
                                var file = files[i];
                                var fileurl = window.URL.createObjectURL(file);
                                var r = Math.floor((Math.random() * 1000) + 1);
                                var template = '<div id="sub_malek' + r + '" class="mb-3 pics animation all 2"><a style="background-image: url(' + fileurl + ');background-position: center center;background-repeat: no-repeat;background-size: contain;" href="javascript:void(0);"></a><button type="button" class="removegal" data-index="' + i + '">حذف</button></div>';
                                jQuery('#gallery_malek').append(template);
                                jQuery("#files_malek").clone().attr('id', 'ffm_'+r).prependTo("#sub_malek" + r);
                            }


                        });
                    });
                </script>
            </div>


            <div class="card-footer">
                <button type="submit" class="btn btn-sm btn-primary">
                    <?php echo "ذخیره و تایید" ?>
                </button>

                <a href="<?php echo $module_site_url; ?>" class="btn btn-sm btn-primary">
                    <?php echo get_msg('btn_cancel') ?>
                </a>
            </div>
        </form>
    </div>
</section>

<script>
    function create_map(clat, clong) {
        var update = function () {
            center = ol.proj.toLonLat(myMap.getView().getCenter());
            jQuery('#lat').val(center[1]);
            jQuery('#lng').val(center[0]);
        }
        var latlong = [clong, clat];
        var myMap = new ol.Map({
            target: 'map',
            key: 'web.O8FFx7qxI6syqKxYUbkQlajvlUwHdaV7YlmQLe5O\n',
            maptype: 'dreamy',
            poi: true,
            traffic: false,
            view: new ol.View({
                center: ol.proj.fromLonLat(latlong),
                zoom: 16
            })
        });
        _markerEl = jQuery('<div id="neshan_map_center_marker" class="ol-unselectable" />').appendTo(jQuery('.ol-overlaycontainer-stopevent'));
        _markerEl.css('background-image', 'url("https://developers.neshan.org/tools/static-map-maker/images/marker_red.png?v=2")');

        update();
        myMap.getView().on('change:center', function () {
            update();
        });
    }

    create_map(38.546409, 44.952996);

</script>