<?php
	$attributes = array('id' => 'search-form', 'enctype' => 'multipart/form-data');
	echo form_open( $module_site_url .'/search', $attributes);
?>

<div class='row my-3'>
	<div class="col-12">
		<div class='form-inline'>
			<div class="form-group" style="padding-top: 3px;padding-left: 2px;">

				<?php echo form_input(array(
					'name' => 'searchterm',
					'value' => set_value( 'searchterm', $searchterm ),
					'class' => 'form-control form-control-sm mr-3',
					'placeholder' => 'جستجو'
				)); ?>

		  	</div>

		 
 	       <div class="form-group" style="padding-top: 3px;padding-left: 2px;">

				<?php
					$options=array();
				    $options[NULL]=get_msg('select_status_label');
					$options[0]="در انتظار";
					$options[1]="تایید شده";
					$options[3]="ویرایش شده";
					$options[2]="گزارش شده";

	
					echo form_dropdown(
						'status',
						$options,
						set_value( 'status', "NULL", false ),
						'class="form-control form-control-sm mr-3" id="status"'
					);
					
					
				?> 

		  	</div>
			
		  	<div class="form-group" style="padding-top: 3px;padding-left: 2px;">

				<?php
					$options=array();
					$options[0]=get_msg('Prd_search_cat');
					
					$categories = $this->Category->get_all( );
					foreach($categories->result() as $cat) {
						
							$options[$cat->cat_id]=$cat->cat_name;
					}
					
					echo form_dropdown(
						'cat_id',
						$options,
						set_value( 'cat_id', show_data( $cat_id ), false ),
						'class="form-control form-control-sm mr-3" id="cat_id"'
					);
				?> 

		  	</div>

	  		<div class="form-group" style="padding-top: 3px;">

				<?php
					if($selected_cat_id != "") {

						$options=array();
						$options[0]=get_msg('Prd_search_subcat');
						$conds['cat_id'] = $selected_cat_id;
						$sub_cat = $this->Subcategory->get_all_by($conds);
						foreach($sub_cat->result() as $subcat) {
							$options[$subcat->id]=$subcat->name;
						}
						echo form_dropdown(
							'sub_cat_id',
							$options,
							set_value( 'sub_cat_id', show_data( $sub_cat_id ), false ),
							'class="form-control form-control-sm mr-3" id="sub_cat_id"'
						);

					} else {

						$conds['cat_id'] = $selected_cat_id;
						$options=array();
						$options[0]=get_msg('Prd_search_subcat');

						echo form_dropdown(
							'sub_cat_id',
							$options,
							set_value( 'sub_cat_id', show_data( $sub_cat_id ), false ),
							'class="form-control form-control-sm mr-3" id="sub_cat_id"'
						);
					}
				?>

		  	</div>

		  	<div class="form-group" style="padding-top: 3px;padding-left: 2px;">

				<?php
					$options=array();
					$options[0]=get_msg('itm_select_type');
					
					$itemtypes = $this->Itemtype->get_all( );
					foreach($itemtypes->result() as $type) {
						
						$options[$type->id]=$type->name;
					}
					
					echo form_dropdown(
						'item_type_id',
						$options,
						set_value( 'item_type_id', show_data( $item_type_id ), false ),
						'class="form-control form-control-sm mr-3" id="item_type_id"'
					);
				?> 

		  	</div>

		  	<div class="form-group" style="padding-top: 3px;padding-left: 2px;">

				<?php
					$options=array();
					$options[0]=get_msg('itm_select_price');
					
					$pricetypes = $this->Pricetype->get_all( );
					foreach($pricetypes->result() as $price) {
						
						$options[$price->id]=$price->name;
					}
					
					echo form_dropdown(
						'item_price_type_id',
						$options,
						set_value( 'item_price_type_id', show_data( $item_price_type_id ), false ),
						'class="form-control form-control-sm mr-3" id="item_price_type_id"'
					);
				?> 

		  	</div>

	
	         <div class="form-group" style="display:none;" style="padding-top: 3px;padding-left: 5px;">
			  	<button type="submit" id='submit' value="submit" name="submit" class="btn btn-sm btn-primary">
			  		<?php echo get_msg( 'btn_search' )?>
			  	</button>
		  	</div>
		  	<div class="form-group" style="padding-top: 3px;padding-left: 5px;">
			  	<button type="submit" id='submit' value="submit" name="submit" class="btn btn-sm btn-primary">
			  		<?php echo get_msg( 'btn_search' )?>
			  	</button>
		  	</div>
		
			<div class="form-group" style="padding-top: 3px;">
			  	<a href='<?php echo $module_site_url .'/index';?>' class='btn btn-sm btn-primary'>
					<?php echo get_msg( 'btn_reset' )?>
				</a>
		  	</div>
		
	 	</div>
		 	<div class='form-inline'>
		 <div class="col-lg-3 col-6">
 	         <br>
			 
         <div class="small-box <?php echo "bg-warning"; ?>">
          <div class="inner">
          <h3 style="color: white;">
            <?php 
			echo $this->Item->count_all_by(); ?>
         </h3>

         <p style="color:white;font-size: 16px;"><?php echo "تمامی آگهی ها"; ?></p>
         </div>
         <div class="icon">
          <i class="<?php echo "fa fa-archive"; ?>"></i>
         </div>
          <a id="items" href="<?php echo site_url() . "/admin/items"; ?>" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i> مشاهده همه </a>
          </div>

        </div>


		 
		 <div class="col-lg-3 col-6">
 	         <br>
			 
         <div id="s1" class="small-box  <?php echo "bg-success"; ?>" >
          <div class="inner">
          <h3 id="one" style="color: white;" id="one">
            <?php $conds['status'] = 1; 
			echo  $this->Item->count_all_by($conds); ?>
         </h3>

         <p style="color:white;font-size: 16px;"><?php echo "آگهی های تایید شده"; ?></p>
         </div>
         <div class="icon">
          <i class="<?php echo "fa fa-wpforms"; ?>"></i>
         </div>
          <a  id="status1" href="#" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i> مشاهده </a>
          </div>

        </div>
		

		  
		 <div class="col-lg-3 col-6">
 	         <br>
			 
         <div id="s2" class="small-box <?php echo "bg-danger"; ?>">
          <div class="inner">
          <h3 style="color: white;">
            <?php $conds['status'] = 0; echo  
	        $this->Item->count_all_by($conds); ?>
         </h3>

         <p style="color:white;font-size: 16px;"><?php echo "آگهی های در انتظار"; ?></p>
         </div>
         <div class="icon">
          <i class="<?php echo "fa fa-hand-paper-o"; ?>"></i>
         </div>
          <a id="status2" href="#" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i> مشاهده </a>
          </div>
 
          </div>
   
		
			 <div class="col-lg-3 col-6">
 	         <br>
			 
         <div id="s3" class="small-box <?php echo "bg-primary"; ?>">
          <div class="inner">
          <h3 style="color: white;">
            <?php $conds['status'] = 3; echo  
	        $this->Item->count_all_by($conds); ?>
         </h3>

         <p style="color:white;font-size: 16px;"><?php echo "آگهی های ویرایش شده"; ?></p>
         </div>
         <div class="icon">
          <i class="<?php echo "fa fa-pencil-square-o"; ?>"></i>
         </div>
          <a id="status3" href="#" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i> مشاهده </a>
          </div>


        </div>
		        </div>

		<center>
			 <div class="col-lg-3 col-6">
 	         <br>
			 
         <div id="s3" class="small-box <?php echo "bg-primary"; ?>">
          <div class="inner">

         <p style="color:white;font-size: 16px;"><?php echo "درج آگهی جدید"; ?></p>
         </div>
         <div class="icon">
          <i class="<?php echo "fa fa-pencil-square-o"; ?>"></i>
         </div>
          <a href="aitems/add" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i> درج </a>
          </div>


        </div>
		</center>
		
		
	
	</div>
</div>

<?php echo form_close(); ?>

<script>
	
<?php if ( $this->config->item( 'client_side_validation' ) == true ): ?>
	function jqvalidate() {
	$('#cat_id').on('change', function() {

			var catId = $(this).val();
			
			$.ajax({
				url: '<?php echo $module_site_url . '/get_all_sub_categories/';?>' + catId,
				method: 'GET',
				dataType: 'JSON',
				success:function(data){
					$('#sub_cat_id').html("");
					$.each(data, function(i, obj){
					    $('#sub_cat_id').append('<option value="'+ obj.id +'">' + obj.name + '</option>');
					});
					$('#name').val($('#name').val() + " ").blur();
				}
			});
		});
		data = $('select[name="status"]');
		
		 if(data.val() == '0') {
	 	 document.getElementById('s2').style.border = '3px solid black';
          }
		  
		if(data.val() == '1') {
	 	 document.getElementById('s1').style.border = '3px solid black';
          }
		  
		  if(data.val() == '3') {
	 	 document.getElementById('s3').style.border = '3px solid black';
          }
		  
		  
		$( "#status1" ).click(function() {
	     $('select[name="status"]').val('1')
	     document.getElementById('submit').click();	
	
        });
		
		$( "#status2" ).click(function() {
	     $('select[name="status"]').val('0')
	     document.getElementById('submit').click();	
        });
		
		$( "#status3" ).click(function() {
	     $('select[name="status"]').val('3')
	     document.getElementById('submit').click();	
        });
		
		$('#status').on('change', function() {
	 
         document.getElementById('submit').click();		
		
		});
}
	<?php endif; ?>
</script>