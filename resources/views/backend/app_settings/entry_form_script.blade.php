<script>
	function jqvalidate() {

		$(document).ready(function(){
			$('#app-form').validate({
				rules:{
					title:{
						required: true,
						minlength: 4
					}
				},
				messages:{
					title:{
						required: "لطفا عنوان را وارد کنید.",
						minlength: "طول عنوان باید بیشتر از 4 باشد"
					}
				}
			});
		});

		$('#us3').locationpicker({
            location: {latitude:  '<?php echo $app->lat;?>', longitude: '<?php echo $app->lng;?>'},
            radius: 300,
            inputBinding: {
                latitudeInput: $('#lat'),
                longitudeInput: $('#lng'),
                radiusInput: $('#us3-radius')
            },
            enableAutocomplete: true,
            onchanged: function (currentLocation, radius, isMarkerDropped) {
                // Uncomment line below to show alert on each Location Changed event
                //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
            }
        });
	}
</script>