<link rel="stylesheet" type="text/css" href="https://static.neshan.org/sdk/openlayers/5.3.0/ol.css">
<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList,URL"></script>
<script type="text/javascript" src="https://static.neshan.org/sdk/openlayers/5.3.0/ol.js"></script>
<style>
    #neshan_map_center_marker {
        width: 30px;
        height: 44px;
        background: transparent center center no-repeat;
        position: absolute;
        left: 50%;
        top: 50%;
        margin: -44px auto auto -15px;
    }
</style>
<section class="block" style="transform: none;">
    <div class="container" style="transform: none;">
        <?php if (validation_errors()): ?>
            <div class="alert alert-danger mb-2">
                <?php echo validation_errors(); ?>
            </div>
        <?php endif ?>
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title"><?php echo get_msg('prd_info') ?></h3>
            </div>
            <?php echo form_open_multipart(); ?>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('itm_title_label') ?>
                            </label>
                            <?php echo form_input(array(
                                'name' => 'title',
                                'value' => set_value('title', show_data(@$item->title), false),
                                'class' => 'form-control form-control-sm',
                                'placeholder' => get_msg('itm_title_label'),
                                'id' => 'title'

                            )); ?>

                        </div>

                        <div class="form-group div_rahn" style="display: none;">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                رهن
                            </label>

                            <?php echo form_input(array(
                                'name' => 'rahn',
                                'value' => set_value('rahn', show_data(@number_format($item->item_rahn)), false),
                                'class' => 'form-control form-control-sm',
                                'onkeyup' => 'javascript:this.value=separate(this.value);',
                                'placeholder' => 'رهن',
                                'id' => 'rahn'

                            )); ?>

                        </div>

                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('Prd_search_cat') ?>
                            </label>

                            <?php
                            $options = array();
                            $conds['status'] = 1;
                            $options[''] = get_msg('Prd_search_cat');
                            $categories = $this->Category->get_all_by($conds);
                            foreach ($categories->result() as $cat) {
                                $options[$cat->cat_id] = $cat->cat_name;
                            }

                            echo form_dropdown(
                                'cat_id',
                                $options,
                                set_value('cat_id', show_data(@$item->cat_id), false),
                                'class="form-control form-control-sm mr-3" id="cat_id"'
                            );
                            ?>
                        </div>

                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('itm_select_price') ?>
                            </label>

                            <?php
                            $options = array();
                            $conds['status'] = 1;
                            $options[''] = get_msg('itm_select_price');
                            $pricetypes = $this->Pricetype->get_all_by($conds);
                            foreach ($pricetypes->result() as $price) {
                                $options[$price->id] = $price->name;
                            }

                            echo form_dropdown(
                                'item_price_type_id',
                                $options,
                                set_value('item_price_type_id', show_data(@$item->item_price_type_id), false),
                                'class="form-control form-control-sm mr-3" id="item_price_type_id"'
                            );
                            ?>
                        </div>

                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('itm_select_location') ?>
                            </label>

                            <?php

                            $options = array();
                            $options[''] = get_msg('itm_select_location');
                            $locations = $this->Itemlocation->get_all();
                            foreach ($locations->result() as $location) {
                                $options[$location->id] = $location->name;
                            }

                            echo form_dropdown(
                                'item_location_id',
                                $options,
                                set_value('item_location_id', show_data(@$item->item_location_id), false),
                                'class="form-control form-control-sm mr-3" id="item_location_id"'
                            );
                            ?>
                        </div>

                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                منطقه (برای مشاهده مناطق لطفا ابتدا شهر را انتخاب کنید)
                            </label>

                            <?php

                            $options = array();

                            echo form_dropdown(
                                'item_location_area_id',
                                $options,
                                set_value('item_location_area_id', json_decode(show_data(@$item->item_location_area_id),true), false),
                                'class="form-control form-control-sm mr-3" id="item_location_area_id"'
                            );
                            ?>
                        </div>

                        <div class="form-group" style="   display:none;">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('itm_select_deal_option') ?>
                            </label>

                            <?php
                            $options = array();
                            $conds['status'] = 1;
                            $options[''] = get_msg('deal_option_id_label');
                            $deals = $this->Option->get_all_by($conds);
                            foreach ($deals->result() as $deal) {
                                $options[$deal->id] = $deal->name;
                            }

                            echo form_dropdown(
                                'deal_option_id',
                                $options,
                                set_value('deal_option_id', show_data(@$item->deal_option_id), false),
                                'class="form-control form-control-sm mr-3" id="deal_option_id"'
                            );
                            ?>
                        </div>

                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('item_description_label') ?>
                            </label>

                            <?php echo form_textarea(array(
                                'name' => 'description',
                                'value' => set_value('description', show_data(@$item->description), false),
                                'class' => 'form-control form-control-sm',
                                'placeholder' => get_msg('item_description_label'),
                                'id' => 'description',
                                'rows' => "5"
                            )); ?>

                        </div>

                        <div class="form-group" style="   display:none;">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('prd_high_info') ?>
                            </label>

                            <?php echo form_textarea(array(
                                'name' => 'highlight_info',
                                'value' => set_value('info', show_data(@$item->highlight_info), false),
                                'class' => 'form-control form-control-sm',
                                'placeholder' => "لطفا اطلاعات خواسته شده را پر کنید",
                                'id' => 'info',
                                'rows' => "3"
                            )); ?>

                        </div>
                        <!-- form group -->
                    </div>

                    <div class="col-md-6">

                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('itm_select_type') ?>
                            </label>

                            <?php

                            $options = array();
                            $options[''] = get_msg('itm_select_type');
                            $types = $this->Itemtype->get_all();
                            foreach ($types->result() as $typ) {
                                $options[$typ->id] = $typ->name;
                            }

                            echo form_dropdown(
                                'item_type_id',
                                $options,
                                set_value('item_type_id', show_data(@$item->item_type_id), false),
                                'class="form-control form-control-sm mr-3" id="item_type_id"'
                            );
                            ?>
                        </div>

                        <div class="form-group div_rahn" style="display: none;">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                اجاره
                            </label>

                            <?php echo form_input(array(
                                'name' => 'ejare',
                                'value' => set_value('ejare', show_data(@number_format($item->item_ejare)), false),
                                'class' => 'form-control form-control-sm',
                                'onkeyup' => 'javascript:this.value=separate(this.value);',
                                'placeholder' => 'اجاره',
                                'id' => 'ejare'

                            )); ?>

                        </div>

                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('Prd_search_subcat') ?>
                            </label>

                            <?php
                            if (isset($item)) {
                                $options = array();
                                $options[''] = get_msg('Prd_search_subcat');
                                $conds['cat_id'] = $item->cat_id;
                                $sub_cat = $this->Subcategory->get_all_by($conds);
                                foreach ($sub_cat->result() as $subcat) {
                                    $options[$subcat->id] = $subcat->name;
                                }
                                echo form_dropdown(
                                    'sub_cat_id',
                                    $options,
                                    set_value('sub_cat_id', show_data(@$item->sub_cat_id), false),
                                    'class="form-control form-control-sm mr-3" id="sub_cat_id"'
                                );

                            } else {
                                $conds['cat_id'] = $selected_cat_id;
                                $options = array();
                                $options[''] = get_msg('Prd_search_subcat');

                                echo form_dropdown(
                                    'sub_cat_id',
                                    $options,
                                    set_value('sub_cat_id', show_data(@$item->sub_cat_id), false),
                                    'class="form-control form-control-sm mr-3" id="sub_cat_id"'
                                );
                            }

                            ?>

                        </div>

                        <div class="form-group" id="price_div">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('price') ?>
                            </label>

                            <?php echo form_input(array(
                                'name' => 'price',
                                'value' => set_value('price', show_data(@number_format($item->price)), false),
                                'class' => 'form-control form-control-sm',
                                'onkeyup' => 'javascript:this.value=separate(this.value);',
                                'placeholder' => get_msg('price'),
                                'id' => 'price'

                            )); ?>

                        </div>

                        <div class="form-group" style="   display:none;">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('itm_select_currency') ?>
                            </label>

                            <?php
                            $options = array();
                            $conds['status'] = 1;
                            $options[''] = get_msg('itm_select_currency');
                            $currency = $this->Currency->get_all_by($conds);
                            foreach ($currency->result() as $curr) {
                                $options[$curr->id] = $curr->currency_short_form;
                            }

                            echo form_dropdown(
                                'item_currency_id',
                                $options,
                                set_value('item_currency_id', show_data(@$item->item_currency_id), false),
                                'class="form-control form-control-sm mr-3" id="item_currency_id"'
                            );
                            ?>
                        </div>

                        <div class="form-group" style="   display:none;">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('itm_select_condition_of_item') ?>
                            </label>

                            <?php
                            $options = array();
                            $conds['status'] = 1;
                            $options[''] = get_msg('condition_of_item');
                            $conditions = $this->Condition->get_all_by($conds);
                            foreach ($conditions->result() as $cond) {
                                $options[$cond->id] = $cond->name;
                            }

                            echo form_dropdown(
                                'condition_of_item_id',
                                $options,
                                set_value('condition_of_item_id', show_data(@$item->condition_of_item_id), false),
                                'class="form-control form-control-sm mr-3" id="condition_of_item_id"'
                            );
                            ?>
                        </div>

                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo get_msg('itm_address_label') ?>
                            </label>

                            <?php echo form_input(array(
                                'name' => 'address',
                                'value' => set_value('address', show_data(@$item->address), false),
                                'class' => 'form-control form-control-sm',
                                'placeholder' => get_msg('itm_address_label'),
                                'id' => 'address'
                            )); ?>

                        </div>

                        <div class="form-group">
                            <label> <span style="font-size: 17px; color: red;">*</span>
                                <?php echo "شماره تلفن" ?>
                            </label>

                            <?php echo form_input(array(
                                'name' => 'brand',
                                'value' => set_value('brand', show_data(@$item->brand), false),
                                'class' => 'form-control form-control-sm',
                                'placeholder' => "شماره تلفن",
                                'id' => 'brand'

                            )); ?>

                        </div>
                        <div id="moaveze" style="display: none">
                            <div class="form-group">
                                <label> <span style="font-size: 17px; color: red;">*</span>
                                    <?php echo "تمایل به معاوضه دارید ؟" ?>
                                </label>

                                <?php
                                $options = array();
                                $options[0] = 'خیر';
                                $options[1] = 'بله';

                                echo form_dropdown(
                                    'change_status',
                                    $options,
                                    set_value('change_status', show_data(@$item->change_status), false),
                                    'class="form-control form-control-sm mr-3" id="change_status"'
                                );
                                ?>

                            </div>
                            <div id="change_div" style="display: none;border: 2px solid #17a2b8;padding: 4px;">
                                <p class="alert alert-danger">
                                    مبلغ نقد موجود و مبلغ نقد درخواستی یکی باید تکمیل شود
                                </p>
                                <div class="form-group">
                                    <label>
                                        مبلغ نقد موجود(تومان)
                                    </label>

                                    <?php echo form_input(array(
                                        'name' => 'price_avl',
                                        'value' => set_value('price_avl', show_data(@number_format($item->price_avl)), false),
                                        'class' => 'form-control form-control-sm',
                                        'onkeyup' => 'javascript:this.value=separate(this.value);',
                                        'placeholder' => 'مبلغ نقد موجود(تومان)',
                                        'id' => 'price_avl'
                                    )); ?>

                                </div>
                                <div class="form-group">
                                    <label>
                                        مبلغ نقد درخواستی(تومان)
                                    </label>

                                    <?php echo form_input(array(
                                        'name' => 'price_req',
                                        'value' => set_value('price_req', show_data(@number_format($item->price_req)), false),
                                        'class' => 'form-control form-control-sm',
                                        'onkeyup' => 'javascript:this.value=separate(this.value);',
                                        'placeholder' => 'مبلغ نقد درخواستی(تومان)',
                                        'id' => 'price_req'
                                    )); ?>

                                </div>
                                <div class="form-group">
                                    <label> <span style="font-size: 17px; color: red;">*</span>
                                        مناطق (برای مشاهده مناطق لطفا ابتدا شهر را انتخاب کنید)
                                    </label>

                                    <?php

                                    $options = array();

                                    echo form_multiselect(
                                        'item_area_id[]',
                                        $options,
                                        set_value('item_area_id[]', json_decode(show_data(@$item->item_area_id),true), false),
                                        'class="form-control form-control-sm mr-3" id="item_area_id"'
                                    );
                                    ?>
                                </div>

                                <div class="form-group">
                                    <label> <span style="font-size: 17px; color: red;">*</span>
                                        نوع ملک
                                    </label>

                                    <?php

                                    $options = array();
                                    $cats = $this->Itemcat->get_all();
                                    foreach ($cats->result() as $cat) {
                                        $options[$cat->id] = $cat->name;
                                    }

                                    echo form_multiselect(
                                        'item_cat_id[]',
                                        $options,
                                        set_value('item_cat_id[]', json_decode(show_data(@$item->item_cat_id),true), false),
                                        'class="form-control form-control-sm mr-3" id="item_cat_id"'
                                    );
                                    ?>
                                </div>

                            </div>
                        </div>
                        <br>

                        <div <?php if (!$edit) { ?> style="display:none;" <?php } ?> class="form-group">
                            <div class="form-check" style="margin-right:5px;">
                                <label>

                                    <?php echo form_checkbox(array(
                                        'name' => 'is_sold_out',
                                        'id' => 'is_sold_out',
                                        'value' => 'accept',
                                        'checked' => set_checkbox('is_sold_out', 1, (@$item->is_sold_out == 1) ? true : false),
                                        'class' => 'form-check-input'
                                    )); ?>

                                    فروخته شد

                                </label>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-6">


                        <!--  <label><?php echo get_msg('deal_option_id_label') ?></label><br>
              <input type="radio" name="deal_option_id"
              <?php if (isset($item->deal_option_id) && $item->deal_option_id == "1") echo "checked"; ?>
              value="1"><?php echo get_msg('meet_up_label'); ?>
              <input type="radio" name="deal_option_id"
              <?php if (isset($item->deal_option_id) && $item->deal_option_id == "2") echo "checked"; ?>
              value="2"><?php echo get_msg('mailing_or_delivery_label'); ?> -->

                        <br><br>


                    </div>

                    <div class="col-md-6">
                        <div class="form-group" style="   display:none;">
                            <div class="form-check">
                                <label>

                                    <?php echo form_checkbox(array(
                                        'name' => 'business_mode',
                                        'id' => 'business_mode',
                                        'value' => 'accept',
                                        'checked' => set_checkbox('business_mode', 1, (@$item->business_mode == 1) ? true : false),
                                        'class' => 'form-check-input'
                                    )); ?>

                                    <?php echo get_msg('itm_business_mode'); ?>
                                    <br><?php echo get_msg('itm_show_shop') ?>
                                </label>
                            </div>
                        </div>


                        <!-- form group -->
                    </div>
                    <?php if (@$item->lat != '0' && @$item->lng != '0'): ?>

                        <div class="col-md-6">
                            <label> <span style="font-size: 17px; color: red;"></span>
                                در صورت عدم مشاهده نقشه میتوانید از <a href="https://www.latlong.net" target="_blank">این
                                    لینک</a> استفاده نمایید.
                            </label>
                            <div id="map" style="width: 100%; height: 300px;"></div>
                            <div class="clearfix">&nbsp;</div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label><?php echo get_msg('itm_lat_label') ?>
                                    <a href="#" class="tooltip-ps" data-toggle="tooltip"
                                       title="<?php echo get_msg('city_lat_label') ?>">
                  <span class='glyphicon glyphicon-info-sign menu-icon'>
                                    </a>
                                </label>

                                <br>

                                <?php
                                echo form_input(array(
                                    'type' => 'text',
                                    'name' => 'lat',
                                    'id' => 'lat',
                                    'class' => 'form-control',
                                    'placeholder' => '',
                                    'value' => ''
                                ));
                                ?>
                            </div>

                            <div class="form-group">
                                <label><?php echo get_msg('itm_lng_label') ?>
                                    <a href="#" class="tooltip-ps" data-toggle="tooltip"
                                       title="<?php echo get_msg('city_lng_tooltips') ?>">
                  <span class='glyphicon glyphicon-info-sign menu-icon'>
                                    </a>
                                </label>

                                <br>

                                <?php
                                echo form_input(array(
                                    'type' => 'text',
                                    'name' => 'lng',
                                    'id' => 'lng',
                                    'class' => 'form-control',
                                    'placeholder' => '',
                                    'value' => ''
                                ));
                                ?>
                            </div>
                            <!-- form group -->

                            <div class="form-check" >
                                <label>

                                    <?php echo form_checkbox(array(
                                        'name' => 'pointer',
                                        'id' => 'pointer',
                                        'value' => 'accept',
                                        'checked' => set_checkbox('pointer', 1, (@$item->pointer == 1) ? true : false),
                                        'class' => 'form-check-input'
                                    )); ?>

                                    نمایش دقیق موقعیت روی نقشه

                                </label>
                            </div>

                        </div>
                    <?php endif ?>


                    <div class="col-md-12">
                        <div class="row" style="display: block;">
                            <div class="form-group" style="display: block;">
                                <input type="file" id="files" style="display: none" name="images[]" multiple="multiple"
                                       accept="image/png, image/jpeg"/>
                                <label class="selectorfiles" for="files">افزودن تصویر</label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- row -->
            </div>

            <!-- Grid row -->
            <div class="gallery gallery-bsitem" id="gallery" style="margin-right: 15px; margin-bottom: 15px;">
                <?php
                $images = isset($images) ? $images : [];
                ?>
                <?php $i = 0;
                foreach ($images as $img) : ?>
                    <div class="mb-3 pics animation all 2">
                        <a style="background-image: url('<?php echo '/ssag/Melkekhoy/uploads/' . $img['img_path']; ?>');
                            background-position: center center;
                            background-repeat: no-repeat;background-size: contain;"
                           href="#<?php echo $i; ?>"></a>
                        <input type="hidden" name="updated_images[]" value="<?php echo $img['img_id']; ?>"/>
                        <button type="button" class="removegal">حذف</button>
                    </div>
                    <?php $i++; endforeach; ?>
            </div>
            <!-- Grid row -->
            <style>
                .gallery-bsitem {
                    display: flex;
                    flex-wrap: wrap;
                }

                .gallery-bsitem > div {
                    width: 300px;
                    height: 250px;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    background: #eee;
                    padding: 40px 15px;
                    margin: 15px;
                    border-radius: 5px;
                    flex-direction: column;
                    position: relative;
                }

                .gallery-bsitem > div a,
                .gallery-bsitem > div img {
                    width: 100%;
                    height: 100%;
                }

                .gallery-bsitem > div button {
                    position: absolute;
                    bottom: 0px;
                    left: 0px;
                    width: 100%;
                    height: 30px;
                    background: transparent;
                    border: 1px solid red;
                    color: red;
                    cursor: pointer;
                }

                .selectorfiles {
                    width: 100%;
                    height: 35px;
                    cursor: pointer;
                    background: green;
                    color: #fff;
                    display: flex;
                    text-align: center;
                    justify-content: center;
                    align-items: center;
                    border-radius: 3px;
                }
            </style>
            <script>
                jQuery(function () {
                    jQuery('body').on('click', 'button.removegal', function () {
                        const index = jQuery(this).attr('data-index');
                        if (index != undefined) {
                            const dt = new DataTransfer();
                            const input = document.getElementById('files');
                            const {files} = input;
                            for (let i = 0; i < files.length; i++) {
                                const file = files[i];
                                if (index !== i) ;
                                dt.items.add(file);
                            }
                            input.files = dt.files;
                        }
                        jQuery(this).parent().remove();
                    });
                    // var form_data = new FormData();

                    jQuery('#files').on('change', function (e) {
                        var files = e.target.files;
                        if (files.length > 7) {
                            return;
                        }
                        for (var i = 0; i < files.length; i++) {
                            var file = files[i];
                            var fileurl = window.URL.createObjectURL(file);
                            var r = Math.floor((Math.random() * 1000) + 1);
                            var template = '<div id="sub' + r + '" class="mb-3 pics animation all 2"><a  style="background-image: url(' + fileurl + ');background-position: center center;background-repeat: no-repeat;background-size: contain;" href="javascript:void(0);"></a><button type="button" class="removegal" data-index="' + i + '">حذف</button></div>';
                            jQuery('#gallery').append(template);
                            jQuery("#files").clone().prependTo("#sub" + r);
                        }
                    });
                });
            </script>

            <hr>
            <h3 class="text-center">
                اطلاعات محرمانه آگهی
            </h3>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            نام مالک
                        </label>
                        <?php echo form_input(array(
                            'name' => 'malek',
                            'value' => set_value('malek', show_data(@$item->malek), false),
                            'class' => 'form-control form-control-sm',
                            'placeholder' => 'مالک',
                            'id' => 'malek'

                        )); ?>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            شماره تماس
                        </label>
                        <?php echo form_input(array(
                            'name' => 'tamas_malek',
                            'value' => set_value('tamas_malek', show_data(@$item->tamas_malek), false),
                            'class' => 'form-control form-control-sm',
                            'placeholder' => 'شماره تماس',
                            'id' => 'tamas_malek'

                        )); ?>

                    </div>
                </div>
                <div class="col-md-12 mt-2">
                    <div class="form-group">
                        <label>
                            توضیحات محرمانه
                        </label>
                        <?php echo form_textarea(array(
                            'name' => 'desc_malek',
                            'value' => set_value('desc_malek', show_data(@$item->desc_malek), false),
                            'class' => 'form-control form-control-sm',
                            'placeholder' => 'توضیحات محرمانه',
                            'id' => 'desc_malek'

                        )); ?>

                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row" style="display: block;">
                        <div class="form-group" style="display: block;">
                            <input type="file" id="files_malek" style="display: none" name="images_malek[]" multiple="multiple"
                                   accept="image/png, image/jpeg"/>
                            <label class="selectorfiles" for="files_malek">افزودن تصویر محرمانه</label>
                        </div>
                    </div>
                </div>
                <!-- Grid row -->
                <div class="gallery gallery-bsitem" id="gallery_malek" style="margin-right: 15px; margin-bottom: 15px;">
                    <?php
                    $images_malek = isset($images_malek) ? $images_malek : [];
                    ?>
                    <?php $i = 0;
                    foreach ($images_malek as $img) : ?>
                        <div class="mb-3 pics animation all 2">
                            <a style="background-image: url('<?php echo '/ssag/Melkekhoy/uploads/' . $img['img_path']; ?>');
                                background-position: center center;
                                background-repeat: no-repeat;background-size: contain;"
                               href="#<?php echo $i; ?>"></a>
                            <input type="hidden" name="updated_images_malek[]" value="<?php echo $img['img_id']; ?>"/>
                            <button type="button" class="removegal">حذف</button>
                        </div>
                        <?php $i++; endforeach; ?>
                </div>
                <script>
                    jQuery(function () {

                        jQuery('#files_malek').on('change', function (e) {
                            var files = e.target.files;
                            if (files.length > 7) {
                                return;
                            }
                            for (var i = 0; i < files.length; i++) {
                                var file = files[i];
                                var fileurl = window.URL.createObjectURL(file);
                                var r = Math.floor((Math.random() * 1000) + 1);
                                var template = '<div id="sub_malek' + r + '" class="mb-3 pics animation all 2"><a style="background-image: url(' + fileurl + ');background-position: center center;background-repeat: no-repeat;background-size: contain;" href="javascript:void(0);"></a><button type="button" class="removegal" data-index="' + i + '">حذف</button></div>';
                                jQuery('#gallery_malek').append(template);
                                jQuery("#files_malek").clone().prependTo("#sub_malek" + r);
                            }


                        });
                    });
                </script>
            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-sm btn-primary">
                    <?php echo "ثبت آگهی" ?>
                </button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</section>
<script>
    function create_map(clat, clong) {
        var update = function () {
            center = ol.proj.toLonLat(myMap.getView().getCenter());
            jQuery('#lat').val(center[1]);
            jQuery('#lng').val(center[0]);
        }
        var latlong = [clong, clat];
        var myMap = new ol.Map({
            target: 'map',
            key: 'web.O8FFx7qxI6syqKxYUbkQlajvlUwHdaV7YlmQLe5O\n',
            maptype: 'dreamy',
            poi: true,
            traffic: false,
            view: new ol.View({
                center: ol.proj.fromLonLat(latlong),
                zoom: 16
            })
        });
        _markerEl = jQuery('<div id="neshan_map_center_marker" class="ol-unselectable" />').appendTo(jQuery('.ol-overlaycontainer-stopevent'));
        _markerEl.css('background-image', 'url("https://developers.neshan.org/tools/static-map-maker/images/marker_red.png?v=2")');

        update();
        myMap.getView().on('change:center', function () {
            update();
        });
    }
    <?php if($edit){ ?>

    create_map(<?php echo $item->lat;?>,<?php echo $item->lng;?>);

    <?php } else { ?>
    create_map(38.546409, 44.952996);
    <?php } ?>

    <?php if ( $this->config->item('client_side_validation') == true ): ?>

    function jqvalidate() {

        jQuery('#item-form').validate({
            rules: {

                cat_id: {
                    indexCheck: ""
                },
                sub_cat_id: {
                    indexCheck: ""
                }
            },
            messages: {

                cat_id: {
                    indexCheck: "<?php echo $this->lang->line('f_item_cat_required'); ?>"
                },
                sub_cat_id: {
                    indexCheck: "<?php echo $this->lang->line('f_item_subcat_required'); ?>"
                }
            },

            submitHandler: function (form) {
                if (jQuery("#item-form").valid()) {
                    form.submit();
                }
            }

        });

        jQuery.validator.addMethod("indexCheck", function (value, element) {

            if (value == 0) {
                return false;
            } else {
                return true;
            }
            ;

        });


    }

    <?php endif; ?>
    jQuery(document).ready(function () {

        /*jQuery('#item_price_type_id').on('change', function () {
            var price_type = jQuery('#item_price_type_id').val();
            if (price_type == 1 || price_type == 8 || price_type == 7) {
                jQuery("#price_div").show();
            } else {
                jQuery("#price_div").hide();
            }
        });*/
        jQuery('#item_type_id').on('change', function () {
            var type = jQuery('#item_type_id').val();
            if (type == 4) {
                jQuery("#moaveze").show();
            } else {
                jQuery("#moaveze").hide();
                jQuery("#change_div").hide();
                jQuery('#change_status').val(0);
            }

            if (type == 3) {
                jQuery(".div_rahn").show();
            } else {
                jQuery(".div_rahn").hide();
            }
        });
        jQuery('#change_status').on('change', function () {
            var type = jQuery('#change_status').val();
            if (type == 1) {
                jQuery("#change_div").show();
            } else {
                jQuery("#change_div").hide();
            }
        });

        <?php if($edit){ ?>

        var catId = jQuery('#item_location_id').val();
        var areaid=  '<?php echo $item->item_location_area_id; ?>';
        jQuery.ajax({
            url: '/index.php/user/get_areas/' + catId,
            method: 'GET',
            dataType: 'JSON',
            success: function (data) {
                // jQuery('#item_area_id').html("");
                jQuery('#item_location_area_id').html("");
                jQuery.each(data, function (i, obj) {
                    if(areaid==obj.id)
                    {
                        var selected='selected=""';
                    }else{
                        var selected='';
                    }
                    // jQuery('#item_area_id').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                    jQuery('#item_location_area_id').append('<option '+selected+' value="' + obj.id + '">' + obj.name + '</option>');
                });
                jQuery('#item_location_area_id').selectpicker('refresh');
                // jQuery('#item_area_id').selectpicker('refresh');
                // alert(areaid);
                //var operation_day = "<?php //echo implode(json_decode((@$item->item_area_id),true),',') ?>//";
                //var operation_day = "<?php //echo implode(json_decode((@$item->item_location_area_id),true),',') ?>//";
                // jQuery('#item_area_id').selectpicker('val', operation_day.split(",")); //split them and set value
                // jQuery('#item_location_area_id').selectpicker('val', operation_day.split(",")); //split them and set value
            }
        });

        <?php } ?>

        <?php if($edit ){ ?>

        // jQuery("#price_div").show();
        if(jQuery("#item_type_id").val()==4 ){
            jQuery("#moaveze").show();
            var type = jQuery('#change_status').val();
            if (type == 1) {
                jQuery("#change_div").show();
            }
        }

        if(jQuery("#item_type_id").val()==3 ){
            jQuery(".div_rahn").show();

        }

        var catId = jQuery('#item_location_id').val();
        //var areaid=  '<?php //echo $item->item_location_area_id; ?>//';
        jQuery.ajax({
            url: '/index.php/user/get_areas/' + catId,
            method: 'GET',
            dataType: 'JSON',
            success: function (data) {
                jQuery('#item_area_id').html("");
                // jQuery('#item_location_area_id').html("");
                jQuery.each(data, function (i, obj) {
                    /*if(areaid==obj.id)
                    {
                        var selected='selected=""';
                    }else{
                        var selected='';
                    }*/
                    jQuery('#item_area_id').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                    // jQuery('#item_location_area_id').append('<option '+selected+' value="' + obj.id + '">' + obj.name + '</option>');
                });
                // jQuery('#item_location_area_id').selectpicker('refresh');
                jQuery('#item_area_id').selectpicker('refresh');
                // alert(areaid);
                var operation_day = "<?php echo implode(json_decode((@$item->item_area_id),true),',') ?>";
                //var operation_day = "<?php //echo implode(json_decode((@$item->item_location_area_id),true),',') ?>//";
                jQuery('#item_area_id').selectpicker('val', operation_day.split(",")); //split them and set value
                // jQuery('#item_location_area_id').selectpicker('val', operation_day.split(",")); //split them and set value
            }
        });

        <?php } else{ ?>
        // jQuery("#price_div").hide();
        <?php }  ?>

        jQuery('#cat_id').on('change', function () {

            var value = jQuery('option:selected', this).text().replace(/Value\s/, '');

            var catId = jQuery(this).val();

            jQuery.ajax({
                url: 'get_sub_categories/' + catId,
                method: 'GET',
                dataType: 'JSON',
                success: function (data) {
                    jQuery('#sub_cat_id').html("");
                    jQuery.each(data, function (i, obj) {
                        jQuery('#sub_cat_id').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                    });
                    jQuery('#name').val(jQuery('#name').val() + " ").blur();
                    jQuery('#sub_cat_id').selectpicker('refresh');
                }
            });
        });
        jQuery('#item_location_id').on('change', function () {

            var value = jQuery('option:selected', this).text().replace(/Value\s/, '');

            var catId = jQuery(this).val();

            jQuery.ajax({
                url: 'get_areas/' + catId,
                method: 'GET',
                dataType: 'JSON',
                success: function (data) {
                    jQuery('#item_area_id').html("");
                    jQuery('#item_location_area_id').html("");
                    jQuery.each(data, function (i, obj) {
                        jQuery('#item_area_id').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                        jQuery('#item_location_area_id').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                    });
                    jQuery('#name').val(jQuery('#name').val() + " ").blur();
                    jQuery('#item_area_id').selectpicker('refresh');
                    jQuery('#item_location_area_id').selectpicker('refresh');
                }
            });
        });


        jQuery('#us3').locationpicker({
            location: {latitude: '<?php echo $item->lat;?>', longitude: '<?php echo $item->lng;?>'},
            radius: 300,
            inputBinding: {
                latitudeInput: jQuery('#lat'),
                longitudeInput: jQuery('#lng'),
                radiusInput: jQuery('#us3-radius')
            },
            enableAutocomplete: true,
            onchanged: function (currentLocation, radius, isMarkerDropped) {
                // Uncomment line below to show alert on each Location Changed event
                //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
            }
        });

        jQuery(function () {
            var selectedClass = "";
            jQuery(".filter").click(function () {
                selectedClass = jQuery(this).attr("data-rel");
                jQuery("#gallery").fadeTo(100, 0.1);
                jQuery("#gallery div").not("." + selectedClass).fadeOut().removeClass('animation');
                setTimeout(function () {
                    jQuery("." + selectedClass).fadeIn().addClass('animation');
                    jQuery("#gallery").fadeTo(300, 1);
                }, 300);
            });
        });

    });

    function separate(Number) {
        Number += '';
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        x = Number.split('.');
        y = x[0];
        z = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(y))
            y = y.replace(rgx, '$1' + ',' + '$2');
        return y + z;
    }

    jQuery('form').submit(function () {
        jQuery('#files').remove();
    });
</script>
<?php
// replace cover photo modal
$data = array(
    'title' => get_msg('upload_photo'),
    'img_type' => 'item',
    'img_parent_id' => @$item->id
);

//$this->load->view( $template_path .'/components/photo_upload_modal', $data );
//
//// delete cover photo modal
//$this->load->view( $template_path .'/components/delete_cover_photo_modal' );
?>