<link rel="stylesheet" type="text/css" href="https://static.neshan.org/sdk/openlayers/5.3.0/ol.css">
<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList,URL"></script>
<script type="text/javascript" src="https://static.neshan.org/sdk/openlayers/5.3.0/ol.js"></script>
<style>
    #neshan_map_center_marker {
        width: 30px;
        height: 44px;
        background: transparent center center no-repeat;
        position: absolute;
        left: 50%;
        top: 50%;
        margin: -44px auto auto -15px;
    }

    ul.ul-feature {
        list-style-type: none;
        padding-right: 0;
    }

    .bold {
        font-weight: bold !important;
    }

    .box-title.title-feature {
        display: block !important;
    }

    .col-md-4.flex {
        display: flex;
        justify-content: flex-start;
    }

    .flex span {
        font-size: 12px;
        margin-right: 2px;
    }


    .button-blue {
        background-color: #0077f0;
        border: 15px;
        color: white;
        border-radius: 15px;
        padding: 20px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
    }


</style>
<?php
$item->default_photo = $this->ps_adapter->get_default_photo($item->id, 'item');
if ($item->default_photo->img_path == '')
    $item->default_photo->img_path = 'no-product-image.png';
if ($item->user_profile_photo == '')
    $item->user_profile_photo = 'no-product-image.png';
?>
<section class="block" style="transform: none;">
    <div class="container" style="transform: none;">

        <div id="product-716"
             class="product type-product post-716 status-publish first instock product_cat-15 product_tag-98 product_tag-99 product_tag-100 product_tag-101 product_tag-102 has-post-thumbnail downloadable virtual purchasable product-type-simple"
             style="transform: none;">
            <div class="row flex-md-row" style="transform: none;">

                <!--============ Listing Detail =============================================================-->
                <div class="col-md-8 charsoogh-content">


                    <!--Description-->

                    <div class="theiaStickySidebar">
                        <style>.flickity-viewport {
                                transition: height 0.2s;
                            }

                            .center-cropped {
                                width: 100%;
                                height: 550px;
                                background-position: center center;
                                background-repeat: no-repeat;
                            }
                        </style>

                        <section>
                            <div id="singleowl"
                                 class="row items featured-carousel owl-carousel owl-rtl owl-loaded owl-drag">
                                <?php foreach ($images as $img) { ?>

                                    <div class="item">
                                        <a href="/ssag/Melkekhoy/uploads/<?php echo $img->img_path; ?>"
                                           data-lightbox="image-1">
                                            <img src="/ssag/Melkekhoy/uploads/<?php echo $img->img_path; ?>"/>
                                        </a>

                                    </div>
                                    <?php $im++;
                                } ?>

                            </div>
                        </section>
                        <section class="box">
                            <h2 class="box-title">توضیحات</h2>
                           <span class="collapse-arrow collapsed" data-toggle="collapse" href="#collapseContent3"
                          role="button" aria-expanded="true" aria-controls="collapseContent">
								<i class="fa fa-minus" aria-hidden="true"></i>
							</span>
                    <div class="b-line"></div>
                    <div class="collapse" id="collapseContent3" style="">
                                <?php echo nl2br($item->description); ?>
                            </div>
                        </section>

                        <section class="box">
                            <h2 class="box-title">ویژگی ها</h2>
                            <span class="collapse-arrow" data-toggle="collapse" href="#collapseContent2" role="button"
                                  aria-expanded="false" aria-controls="collapseContent">
								<i class="fa fa-minus" aria-hidden="true"></i>
							</span>
                            <div class="b-line"></div>
                            <div class="collapse show" id="collapseContent2">
                                <div style="margin-bottom: 15px" class="row">
                                    <?php if ($item->year) { ?>
                                        <div class="col-md-4 flex">
                                            <label class="bold">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/calendar.png"/>
                                                سال ساخت :
                                            </label>
                                            <span>
                                        <?php echo $item->year; ?>
                                        </span>
                                        </div>
                                    <?php } ?>
                                    <?php if ($item->room) { ?>
                                        <div class="col-md-4 flex">
                                            <label class="bold">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/open-door.png"/>
                                                تعداد اتاق :
                                            </label>
                                            <span>
                                        <?php echo $item->room; ?>
                                        </span>
                                        </div>
                                    <?php } ?>

                                    <?php if ($item->wc) { ?>
                                        <div class="col-md-4 flex">
                                            <label class="bold">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/toilet.png"/>
                                                سرویس بهداشتی :
                                            </label>
                                            <span>
                                        <?php echo $item->wc; ?>
                                        </span>
                                        </div>
                                    <?php } ?>
                                    <?php if ($item->bathroom) { ?>
                                        <div class="col-md-4 flex">
                                            <label class="bold">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/bed.png"/>
                                                تعداد حمام :
                                            </label>
                                            <span>
                                        <?php echo $item->bathroom; ?>
                                        </span>
                                        </div>
                                    <?php } ?>
                                    <?php if ($item->unit) { ?>
                                        <div class="col-md-4 flex">
                                            <label class="bold">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/apartment.png"/>
                                                تعداد واحد در طبقه :
                                            </label>
                                            <span>
                                        <?php echo $item->unit; ?>
                                        </span>
                                        </div>
                                    <?php } ?>
                                    <?php if ($item->floor) { ?>
                                        <div class="col-md-4 flex">
                                            <label class="bold">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/stairs.png"/>
                                                طبقه چندم :
                                            </label>
                                            <span>
                                        <?php echo $item->floor; ?>
                                        </span>
                                        </div>
                                    <?php } ?>
                                    <?php if ($item->floorcount) { ?>
                                        <div class="col-md-4 flex">
                                            <label class="bold">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/tiles.png"/>
                                                تعداد کل طبقات :
                                            </label>
                                            <span>
                                        <?php echo $item->floorcount; ?>
                                        </span>
                                        </div>
                                    <?php } ?>

                                    <?php if ($item->empty) { ?>
                                        <div class="col-md-4 flex">
                                            <label class="bold">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/moving-truck.png"/>
                                                وضعیت تخلیه :
                                            </label>
                                            <span>
                                        <?php echo $item->empty; ?>
                                        </span>
                                        </div>
                                    <?php } ?>
                                    <?php if ($item->metrazh > 0) { ?>
                                        <div class="col-md-4 flex">
                                            <label class="bold">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/sketch.png"/>
                                                متراژ زمین :
                                            </label>
                                            <span>
                                        <?php echo number_format($item->metrazh); ?>
                                        </span>
                                        </div>
                                    <?php } ?>
                                    <?php if ($item->zirbana > 0) { ?>
                                        <div class="col-md-4 flex">
                                            <label class="bold">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/area.png"/>
                                                متراژ زیربنا :
                                            </label>
                                            <span>
                                        <?php echo number_format($item->zirbana); ?>
                                        </span>
                                        </div>
                                    <?php } ?>
                                    <?php if ($item->toolbar > 0) { ?>
                                        <div class="col-md-4 flex">
                                            <label class="bold">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/metering.png"/>
                                                طول بر ملک :
                                            </label>
                                            <span>
                                        <?php echo number_format($item->toolbar); ?>
                                        </span>
                                        </div>
                                    <?php } ?>
                                    <?php if ($item->arz > 0) { ?>
                                        <div class="col-md-4 flex">
                                            <label class="bold">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/road.png"/>
                                                متراژ کوچه / خیابان :
                                            </label>
                                            <span>
                                        <?php echo number_format($item->arz); ?>
                                        </span>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div style="margin-bottom: 15px" class="row">
                                    <?php if (count(json_decode($item->direction, true))) { ?>
                                        <div class="col-md-3">
                                            <h5 class="text-center mt-3 box-title title-feature">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/compass.png"/>
                                                جهت ساختمان
                                            </h5>
                                            <ul class="ul-feature">
                                                <?php
                                                foreach (json_decode($item->direction, true) as $itm) { ?>
                                                    <li>
                                                        <i class="fa fa-check text-primary"></i>
                                                        <?php echo $itm; ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    <?php } ?>
                                    <?php if (count(json_decode($item->kaf, true))) { ?>
                                        <div class="col-md-3">
                                            <h5 class="text-center mt-3 box-title title-feature">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/draws.png"/>
                                                جنس کف
                                            </h5>
                                            <ul class="ul-feature">
                                                <?php
                                                foreach (json_decode($item->kaf, true) as $itm) { ?>
                                                    <li>
                                                        <i class="fa fa-check text-primary"></i>
                                                        <?php echo $itm; ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    <?php } ?>
                                    <?php if (count(json_decode($item->sarmayesh, true))) { ?>
                                        <div class="col-md-3">
                                            <h5 class="text-center mt-3 box-title title-feature">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/fan.png"/>
                                                سرمایش گرمایش
                                            </h5>
                                            <ul class="ul-feature">
                                                <?php
                                                foreach (json_decode($item->sarmayesh, true) as $itm) { ?>
                                                    <li>
                                                        <i class="fa fa-check text-primary"></i>
                                                        <?php echo $itm; ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    <?php } ?>
                                    <?php if (count(json_decode($item->moshakhase, true))) { ?>
                                        <div class="col-md-3">
                                            <h5 class="text-center mt-3 box-title title-feature">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/house (1).png"/>
                                                مشخصه ملک
                                            </h5>
                                            <ul class="ul-feature">
                                                <?php
                                                foreach (json_decode($item->moshakhase, true) as $itm) { ?>
                                                    <li>
                                                        <i class="fa fa-check text-primary"></i>
                                                        <?php echo $itm; ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    <?php } ?>
                                    <?php if (count(json_decode($item->abegarm, true))) { ?>
                                        <div class="col-md-3">
                                            <h5 class="text-center mt-3 box-title title-feature">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/warm-water.png"/>
                                                تامین کننده آب گرم
                                            </h5>
                                            <ul class="ul-feature">
                                                <?php
                                                foreach (json_decode($item->abegarm, true) as $itm) { ?>
                                                    <li>
                                                        <i class="fa fa-check text-primary"></i>
                                                        <?php echo $itm; ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    <?php } ?>
                                    <?php if (count(json_decode($item->kabinet, true))) { ?>
                                        <div class="col-md-3">
                                            <h5 class="text-center mt-3 box-title title-feature">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/cabinet.png"/>
                                                کابینت بندی
                                            </h5>
                                            <ul class="ul-feature">
                                                <?php
                                                foreach (json_decode($item->kabinet, true) as $itm) { ?>
                                                    <li>
                                                        <i class="fa fa-check text-primary"></i>
                                                        <?php echo $itm; ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    <?php } ?>
                                    <?php if (count(json_decode($item->divar, true))) { ?>
                                        <div class="col-md-3">
                                            <h5 class="text-center mt-3 box-title title-feature">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/brick-wall.png"/>
                                                جنس دیوارها
                                            </h5>
                                            <ul class="ul-feature">
                                                <?php
                                                foreach (json_decode($item->divar, true) as $itm) { ?>
                                                    <li>
                                                        <i class="fa fa-check text-primary"></i>
                                                        <?php echo $itm; ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    <?php } ?>
                                    <?php if (count(json_decode($item->nama, true))) { ?>
                                        <div class="col-md-3">
                                            <h5 class="text-center mt-3 box-title title-feature">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/house (2).png"/>
                                                نمای ساختمان
                                            </h5>
                                            <ul class="ul-feature">
                                                <?php
                                                foreach (json_decode($item->nama, true) as $itm) { ?>
                                                    <li>
                                                        <i class="fa fa-check text-primary"></i>
                                                        <?php echo $itm; ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    <?php } ?>
                                    <?php if (count(json_decode($item->amniati, true))) { ?>
                                        <div class="col-md-3">
                                            <h5 class="text-center mt-3 box-title title-feature">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/cctv-camera.png"/>
                                                امکانات امنیتی
                                            </h5>
                                            <ul class="ul-feature">
                                                <?php
                                                foreach (json_decode($item->amniati, true) as $itm) { ?>
                                                    <li>
                                                        <i class="fa fa-check text-primary"></i>
                                                        <?php echo $itm; ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    <?php } ?>
                                    <?php if (count(json_decode($item->refahi, true))) { ?>
                                        <div class="col-md-3">
                                            <h5 class="text-center mt-3 box-title title-feature">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/carousel.png"/>
                                                امکانات رفاهی تفریحی
                                            </h5>
                                            <ul class="ul-feature">
                                                <?php
                                                foreach (json_decode($item->refahi, true) as $itm) { ?>
                                                    <li>
                                                        <i class="fa fa-check text-primary"></i>
                                                        <?php echo $itm; ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    <?php } ?>
                                    <?php if (count(json_decode($item->ashpazkhane, true))) { ?>
                                        <div class="col-md-3">
                                            <h5 class="text-center mt-3 box-title title-feature">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/kitchen.png"/>
                                                مشخصه آشپزخانه
                                            </h5>
                                            <ul class="ul-feature">
                                                <?php
                                                foreach (json_decode($item->ashpazkhane, true) as $itm) { ?>
                                                    <li>
                                                        <i class="fa fa-check text-primary"></i>
                                                        <?php echo $itm; ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    <?php } ?>
                                    <?php if (count(json_decode($item->sanad, true))) { ?>
                                        <div class="col-md-3">
                                            <h5 class="text-center mt-3 box-title title-feature">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/contract.png"/>
                                                نوع سند
                                            </h5>
                                            <ul class="ul-feature">
                                                <?php
                                                foreach (json_decode($item->sanad, true) as $itm) { ?>
                                                    <li>
                                                        <i class="fa fa-check text-primary"></i>
                                                        <?php echo $itm; ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    <?php } ?>
                                    <?php if (count(json_decode($item->usemelk, true))) { ?>
                                        <div class="col-md-3">
                                            <h5 class="text-center mt-3 box-title title-feature">
                                                <img src="/ssag/Melkekhoy/assets/front/icons/house.png"/>
                                                کاربری ملک
                                            </h5>
                                            <ul class="ul-feature">
                                                <?php
                                                foreach (json_decode($item->usemelk, true) as $itm) { ?>
                                                    <li>
                                                        <i class="fa fa-check text-primary"></i>
                                                        <?php echo $itm; ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    <?php } ?>

                                </div>
                                <div class="row">
                                    <?php if ($item->amlak) { ?>
                                        <div class="col-md-3 flex">
                                            <label>
                                                <i class="fa fa-check text-success"></i>
                                                آگهی املاک
                                            </label>
                                        </div>
                                    <?php } ?>
                                    <?php if ($item->rahnchange) { ?>
                                        <div class="col-md-3 flex">
                                            <label>
                                                <i class="fa fa-check text-success"></i>
                                                تبدیل و رهن اجاره
                                            </label>
                                        </div>
                                    <?php } ?>
                                    <?php if ($item->asansor) { ?>
                                        <div class="col-md-3 flex">
                                            <label>
                                                <i class="fa fa-check text-success"></i>
                                                آسانسور
                                            </label>
                                        </div>
                                    <?php } ?>
                                    <?php if ($item->parking) { ?>
                                        <div class="col-md-3 flex">
                                            <label>
                                                <i class="fa fa-check text-success"></i>
                                                پارکینگ
                                            </label>
                                        </div>
                                    <?php } ?>
                                    <?php if ($item->anbari) { ?>
                                        <div class="col-md-3 flex">
                                            <label>
                                                <i class="fa fa-check text-success"></i>
                                                انباری
                                            </label>
                                        </div>
                                    <?php } ?>
                                    <?php if ($item->balkon) { ?>
                                        <div class="col-md-3 flex">
                                            <label>
                                                <i class="fa fa-check text-success"></i>
                                                بالکن
                                            </label>
                                        </div>
                                    <?php } ?>
                                    <?php if ($item->vam) { ?>
                                        <div class="col-md-3 flex">
                                            <label>
                                                <i class="fa fa-check text-success"></i>
                                                وام دارد
                                            </label>
                                        </div>
                                    <?php } ?>
                                    <?php if ($item->bazsazi) { ?>
                                        <div class="col-md-3 flex">
                                            <label>
                                                <i class="fa fa-check text-success"></i>
                                                بازسازی شده
                                            </label>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </section>


                    </div>
                </div>
                <!--============ Sidebar ==============-->
                <div class="col-md-4">

                    <div class="theiaStickySidebar">
                        <aside class="sidebar">
                            <section>
                                <div class="box">
                                    <a href="/index.php/profile/<?php echo $item->user_id; ?>">
                                        <div class="profile-box">
                                            <img src="/ssag/Melkekhoy/uploads/<?php echo $item->user_profile_photo; ?>"
                                                 alt="<?php echo $item->user_name; ?>"><?php echo $item->user_name; ?>
                                            <span class="advertiser">آگهی دهنده</span></div>
                                    </a>
                                    <div class="row contact-box">
                                        <?php if ($item->is_sold_out == 1) { ?>
                                            <button class="btn btn-primary info-btn col-md-6 col-sm-12 pull-right contact_info">
                                                فروخته شد
                                            </button>
                                        <?php } else { ?>
                                            <button type="button" data-toggle="modal" data-target="#contact_info"
                                                    class="btn button-blue info-btn col-sm-12 pull-right contact_info ">
                                                اطلاعات تماس
                                            </button>
                                        <?php } ?>
                                        <?php
                                        $data = array('item_id' => $item->id, 'user_id' => $this->session->userdata('user_id'));
                                        if ($this->Favourite->exists($data)) {
                                            ?>
                                            <button data-toggle="tooltip" data-placement="top" title="" type="button"
                                                    class="btn info-btn btn-primary btn-icon btn-framed col-md-6 col-sm-12 pull-right favorite"
                                                    data-original-title="لغو نشان" style="background-color: #e71c75 !important;
    border-color: #e71c75;
    box-shadow: none;
    color: #fff;"><i class="fa fa-bookmark-o"
                     aria-hidden="true"></i>
                                            </button>
                                        <?php } else { ?>
                                            <button data-toggle="tooltip" data-placement="top" title="" type="button"
                                                    class="btn info-btn btn-primary btn-icon btn-framed col-md-6 col-sm-12 pull-right favorite"
                                                    data-original-title="نشان کردن"><i class="fa fa-bookmark-o"
                                                                                       aria-hidden="true"></i>
                                            </button>
                                        <?php } ?>


                                    </div>

                                    <dl>
                                        <dt class="ad_visit_n">کد آگهی</dt>
                                        <dd id="ad-visit"><?php echo $item->item_code; ?></dd>
                                        <dt class="ad_visit_n">بازدید</dt>
                                        <dd id="ad-visit"><?php echo $item->touch_count; ?> بازدید</dd>
                                        <dt>دسته‌بندی</dt>
                                        <dd id="ad-category">
                                            <?php echo $item->subname; ?>
                                        </dd>
                                        <dt>شهر</dt>
                                        <dd id="ad-city">
                                            <?php echo $item->location_name; ?>
                                        </dd>
                                        <dt>منطقه</dt>
                                        <dd id="ad-city">
                                            <?php echo $item->area_name; ?>
                                        </dd>
                                        <dt>آدرس</dt>
                                        <dd id="ad-city">
                                            <?php echo $item->address; ?>
                                        </dd>
                                        <dt class="publish_date_n">تاریخ انتشار</dt>
                                        <dd class="publish_date_v">
                                            <?php
                                            echo ago($item->added_date);
                                            ?>
                                        </dd>
                                        <dt class="publish_date_n">نوع آگهی</dt>
                                        <dd class="publish_date_v">
                                            <?php
                                            echo $item->type_name;
                                            ?>
                                        </dd>
                                        <?php if ($item->pricetypeid == 1) { ?>
                                            <dt class="publish_date_n">قیمت</dt>
                                            <dd class="publish_date_v">
                                                توافقی
                                            </dd>
                                         <?php } else { ?>
                                            <?php if ($item->item_type_id == 3) { ?>
                                                <dt class="publish_date_n">رهن</dt>
                                                <dd class="publish_date_v">
                                                    <?php echo number_format($item->item_rahn); ?> تومان
                                                </dd>
                                                <dt class="publish_date_n">اجاره</dt>
                                                <dd class="publish_date_v">
                                                    <?php echo number_format($item->item_ejare); ?> تومان
                                                </dd>
                                            <?php } else { ?>
                                                <dt>قیمت</dt>
                                                <dd>
                                            <span class="price">
                                                 <?php echo number_format($item->price); ?>
                                                     تومان
                                            </span>
                                                </dd>
                                            <?php } ?>
                                        <?php } ?>

                                        <?php if ($item->change_status == 1) {
                                            ?>
                                            <div class="alert alert-info">
                                              
                                                    
                                                    
                                    <div class="warning_ad">
                                          <center>
                                        <h4>   اطلاعات معاوضه </h4>
                                        </center>
                                    </div>
                                    
                                    <div class="alert">
                                        </div>
                             
                                                <?php

                                                $areas = '';
                                                foreach (json_decode($item->item_area_id, true) as $area) {
                                                    $area = $this->Itemarea->get_one($area);
                                                    $areas .= $area->name . ',';
                                                }
                                                $cats = '';
                                                foreach (json_decode($item->item_cat_id, true) as $cat) {
                                                    $cat = $this->Itemcat->get_one($cat);
                                                    $cats .= $cat->name . ',';
                                                }


                                                ?>
                                                
                                                  
                                                    
                                                <?php if ($item->price_avl > 0) { ?>
                                            
                                                    <dt>مبلغ نقد موجود(تومان)</dt>
                                                    <dd>
                                            <span class="price">
                                                <?php echo number_format($item->price_avl); ?>
                                                     تومان
                                            </span>
                                                    </dd>
                                                    <?php
                                                }
                                                if ($item->price_req > 0) { ?>
                                                    <dt>مبلغ نقد درخواستی(تومان)</dt>
                                                    <dd>
                                            <span class="price">
                                                <?php echo number_format($item->price_req); ?>
                                                     تومان
                                            </span>
                                                    </dd>
                                                <?php } ?>
                                                <dt>مناطق</dt>
                                                <dd id="ad-category">
                                                    <?php echo trim($areas, ','); ?>
                                                </dd>
                                                <dt>نوع ملک</dt>
                                                <dd id="ad-category">
                                                    <?php echo trim($cats, ','); ?>
                                                </dd>
                                            </div>
                                        <?php } ?>

                                    </dl>

                                    <div class="warning_ad">
                                        <h4>ملک خوی هیچ‌گونه منفعت و مسئولیتی در قبال معامله شما ندارد.</h4>
                                    </div>
                                    <section class="report_ad">
                                        <a href="#" data-toggle="modal" data-target="#report_ads">
                                            <i class="fa fa-flag" aria-hidden="true"></i>
                                            گزارش تخلف آگهی
                                        </a>
                                    </section>
                                </div>
                            </section>
                            <?php if ($item->item_type_id != 1) { ?>
                                <section class="box mapbox" style="padding: 5px;">
                                    <div class="ribbon-featured rbch">
                                        <div class="ribbon-start"></div>
                                        <div class="ribbon-content">موقعیت روی نقشه</div>
                                        <div class="ribbon-end">
                                            <figure class="ribbon-shadow"></figure>
                                        </div>
                                    </div>
                                    <div id="map" style="width: 100%; height: 300px;">
                                        <div id="popup"></div>
                                    </div>

                                    <?php if ($item->pointer == 1) {
                                        $marker = "'http://melkekhoy.ir/ssag/Melkekhoy/assets/front/images/circle.png'";
                                    } else {
                                        $marker = "'https://developers.neshan.org/tools/static-map-maker/images/marker_red.png?v=2'";
                                    } ?>

                                    <?php if ($item->pointer == 0) { ?>
                                        <script>

                                            var latlong = [<?php echo $item->lng;?>,<?php echo $item->lat;?>];
                                            var myMap = new ol.Map({
                                                target: 'map',
                                                key: 'web.O8FFx7qxI6syqKxYUbkQlajvlUwHdaV7YlmQLe5O\n',
                                                maptype: 'dreamy',
                                                layers: [
                                                    new ol.layer.Vector({
                                                        source: new ol.source.Vector({
                                                            features: [new ol.Feature({
                                                                geometry: new ol.geom.Point(ol.proj.fromLonLat([<?php echo $item->lng;?>,<?php echo $item->lat;?>])),
                                                                name: 'Somewhere near Nottingham',
                                                            })]
                                                        }),
                                                        style: new ol.style.Style({
                                                            image: new ol.style.Icon({
                                                                anchor: [0.5, 46],
                                                                anchorXUnits: 'fraction',
                                                                anchorYUnits: 'pixels',
                                                                src: <?php echo $marker; ?>
                                                            })
                                                        })
                                                    })
                                                ],
                                                poi: true,
                                                traffic: false,
                                                view: new ol.View({
                                                    center: ol.proj.fromLonLat(latlong),
                                                    zoom: 16
                                                })
                                            });


                                        </script>
                                    <?php } else { ?>
                                        <script>
                                            const features = new Array(1);
                                            const stroke = new ol.style.Stroke({color: 'red', width: 2});
                                            const fill = new ol.style.Fill({color: 'rgba(255,0,0,0.2)'});
                                            const rsh = new ol.style.Style({
                                                fill: fill,
                                                stroke: stroke,
                                            });
                                            const coordinates = [<?php echo $item->lng;?>,<?php echo $item->lat;?>];
                                            features[0] = new ol.Feature(new ol.geom.Circle(ol.proj.fromLonLat([<?php echo $item->lng;?>,<?php echo $item->lat;?>]), 150));
                                            features[0].setStyle(rsh);
                                            const source = new ol.source.Vector({
                                                features: features,
                                            });

                                            const vectorLayer = new ol.layer.Vector({
                                                source: source,
                                            });


                                            var latlong = [<?php echo $item->lng;?>,<?php echo $item->lat;?>];
                                            var myMap = new ol.Map({
                                                target: 'map',
                                                key: 'web.O8FFx7qxI6syqKxYUbkQlajvlUwHdaV7YlmQLe5O\n',
                                                maptype: 'dreamy',
                                                layers: [
                                                    vectorLayer,
                                                    /*new ol.layer.Vector({
                                                        source: new ol.source.Vector({
                                                            features: [new ol.Feature({
                                                                geometry: new ol.geom.Point(ol.proj.fromLonLat([<?php echo $item->lng;?>,<?php echo $item->lat;?>])),
                                                        name: 'Somewhere near Nottingham',
                                                    })]
                                                }),
                                                style: new ol.style.Style({
                                                    image: new ol.style.Icon({
                                                        anchor: [0.5, 46],
                                                        anchorXUnits: 'fraction',
                                                        anchorYUnits: 'pixels',
                                                        src: <?php echo $marker; ?>
                                                    })
                                                })
                                            })*/
                                                ],
                                                poi: true,
                                                traffic: false,
                                                view: new ol.View({
                                                    center: ol.proj.fromLonLat(latlong),
                                                    zoom: 16
                                                })
                                            });


                                        </script>
                                    <?php } ?>


                                    <?php if ($item->pointer == 0) { ?>
                                        <a href="http://maps.google.com/maps?q=loc:<?php echo $item->lat; ?>,<?php echo $item->lng; ?> (<?php echo $item->title; ?>)"
                                           target="_blank">
                                            <button type="button" class="btn btn-primary info-btn location-send">
                                                مسیریابی
                                            </button>
                                        </a>
                                    <?php } ?>

                                </section>
                            <?php } ?>
                            <section class="share-link-box">
                                <div class="short-link">
                                    <span class="post-link__button" onclick="copyLinkFunction()" data-toggle="tooltip"
                                          data-placement="right" title="" data-original-title="کپی لینک"><i
                                                class="fa fa-files-o" aria-hidden="true"></i> کپی اشتراک گذاری</span>
                                    <input type="text" id="shortlink"
                                           value="http://www.melkekhoy.ir/index.php/v/<?php echo $item->id; ?>">
                                </div>

                            </section>

                            <div class="crunchify-social">
          <span>
            <i class="fa fa-share-alt" aria-hidden="true"></i> اشتراک گذاری          </span>
                                <a href="https://telegram.me/share/url?text=<?php echo $item->title; ?>&url=<?php echo current_url(); ?>"
                                   class="crunchify-link crunchify-telegram" target="_blank"><i
                                            class="fa fa-paper-plane"></i></a>
                                <a class="crunchify-link crunchify-facebook"
                                   href="https://www.facebook.com/sharer/sharer.php?u=<?php echo current_url(); ?>"
                                   target="_blank"><i class="fa fa-facebook"></i></a>
                                <a class="crunchify-link crunchify-whatsapp"
                                   href="whatsapp://send?text=<?php echo $item->title; ?><?php echo current_url(); ?>"
                                   target="_blank"><i class="fa fa-phone"></i></a>
                                <a class="crunchify-link crunchify-twitter"
                                   href="https://twitter.com/intent/tweet?text=<?php echo $item->title; ?>&amp;url=<?php echo current_url(); ?>&amp;via=Crunchify"
                                   target="_blank"><i class="fa fa-twitter"></i></a>

                            </div>

                        </aside>
                        <div class="resize-sensor"
                             style="position: absolute; inset: 0px; overflow: hidden; z-index: -1; visibility: hidden;">
                            <div class="resize-sensor-expand"
                                 style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;">
                                <div style="position: absolute; left: 0px; top: 0px; transition: all 0s ease 0s; width: 370px; height: 1017px;"></div>
                            </div>
                            <div class="resize-sensor-shrink"
                                 style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;">
                                <div style="position: absolute; left: 0; top: 0; transition: 0s; width: 200%; height: 200%"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--============ End Sidebar =================-->

                <?php if (count($oitems) != 0) { ?>
                    <div class="col-md-12">
                        <section class="block featured-block">

                            <div class="container">
                                <div class="section-title clearfix">


                                    <h2>سایر آگهی های کاربر</h2>
                                </div>

                                <div class="show_featured">
                                    <div id="featuredowl"
                                         class="row items featured-carousel owl-carousel owl-rtl owl-loaded owl-drag">
                                        <?php foreach ($oitems as $item2) {
                                            $item2->default_photo = $this->ps_adapter->get_default_photo($item2->id, 'item');
                                            if ($item2->default_photo->img_path == '')
                                                $item2->default_photo->img_path = 'no-product-image.png'
                                            ?>
                                            <div class="item  featured-4row product type-product post-1317 status-publish first instock product_cat-electronics product_cat-console-video-game-and-online has-post-thumbnail featured downloadable virtual purchasable product-type-simple">
                                                <div class="wrapper">

                                                    <div class="image">
                                                        <h4 class="location">
                                                            <a href="">
                                                                <?php echo $item2->location_name; ?>
                                                            </a>
                                                        </h4>
                                                        <?php if ($item2->is_sold_out == 1) { ?>
                                                            <h4 class="soldout">
                                                                فروخته شد
                                                            </h4>
                                                        <?php } ?>
                                                        <a href="/index.php/v/<?php echo $item2->id; ?>"
                                                           title="<?php echo $item2->title; ?>" class="title">
                                                            <img width="300" height="300"
                                                                 src="/ssag/Melkekhoy/uploads/<?php echo $item2->default_photo->img_path; ?>"
                                                                 class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                                 alt="" loading="lazy"
                                                                 sizes="(max-width: 300px) 100vw, 300px"> </a>
                                                    </div>
                                                    <span class="price">
                                        <?php if ($item2->pricetypeid == 1) {
                                            echo 'توافقی';
                                        } else {
                                            ?>
                                            <?php echo number_format($item2->price); ?>
                                            تومان
                                        <?php } ?>
                                    </span>
                                                    <div class="meta">
                                                        <a href="/index.php/v/<?php echo $item2->id; ?>"
                                                           title="<?php echo $item2->title; ?>" class="title">
                                                            <h2><?php echo $item2->title; ?></h2>
                                                        </a>
                                                        <figure>
                                                            <i class="fa fa-calendar-o"></i>
                                                            <?php
                                                            echo ago($item2->added_date);
                                                            ?>
                                                        </figure>
                                                        <figure>
                                                            <i class="fa fa-folder-open-o"></i>
                                                            <?php echo $item2->subname; ?>
                                                        </figure>
                                                    </div>
                                                    <!--end meta-->
                                                    <!--end description-->
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div>
                <?php } ?>

                <div class="col-md-12">

                    <style>
                        .realtor-imgbox {
                            width: 110px;
                            float: right;
                            margin-left: 15px;
                        }

                        .ui.image {
                            position: relative;
                            display: inline-block;
                            vertical-align: middle;
                            max-width: 100%;
                            background-color: transparent;
                        }

                        img.ui.image {
                            display: block;
                        }

                        .realtor-imgbox img {
                            width: 110px;
                            height: 110px;
                            border-radius: 55px;
                            border: 5px #f0f0f0 solid;
                        }

                        .contact-detail-info small {
                            font-weight: 500;
                            font-size: 13px;
                        }

                        .contact-info .contact-detail-info {
                            padding-bottom: 0px;
                            padding-top: 5px;
                            display: flex;
                            justify-content: space-between;
                        }

                        .contact-detail-col {
                            font-weight: 600;
                        }
                    </style>

                    <section class="box boxes white-box boxes-shadow pad20A mrg15B">
                        <h2 class="box-title">اطلاعات تماس با مشاور / آژانس</h2>
                        <div class="row">

                            <div class="col-lg-3 col-sm-12 contact-info agent-contact-info">


                                <div class="realtor-imgbox">
                                    <a href="/index.php/profile/<?php echo $item->user_id; ?>">
                                        <img src="/ssag/Melkekhoy/uploads/<?php echo $item->user_profile_photo; ?>"
                                             class="ui image circle">
                                    </a>

                                </div>
                            </div>
                            <div class="col-lg-5 col-sm-12 contact-info agent-contact-info">

                                &nbsp;

                                <div class="contact-detail-info">
                                    <small> دفتر ملک خوی:</small>
                                    <a href="tel:04436362010" id="sShowUserMobile" class="contact-detail-col">04436362010</a>
                                </div>
                                <div class="contact-detail-info">
                                    <small> مدیریت :</small>
                                    <a href="tel:09104405996" id="sShowUserMobile" class="contact-detail-col">09104405996</a>
                                </div>
                                <?php if ($item->whatsapp_number != '') { ?>
                                    <div class="contact-detail-info">
                                        <small> واتساپ کارشناس:</small>
                                        <a href="tel:<?php echo $item->whatsapp_number; ?>" id="sShowUserMobile"
                                           class="contact-detail-col"><?php echo $item->whatsapp_number; ?></a>
                                    </div>
                                <?php } ?>
                                <?php if ($item->personnel != '') { ?>
                                    <div class="contact-detail-info">
                                        <small> تلفن کارشناس:</small>
                                        <a href="tel:<?php echo $item->personnel; ?>" id="sShowUserMobile"
                                           class="contact-detail-col"><?php echo $item->personnel; ?></a>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="col-lg-4 col-lg-pull-3 col-sm-12 contact-btn">

                                <div class="text-center">
                                    <img src="http://www.melkekhoy.ir/ssag/Melkekhoy/assets/front/images/logo.png">
                                    <br>
                                </div>

                                <div class="text-center">
                                    <div class="contact-detail-info">
                                        <a class="btn btn-primary " href="#" data-toggle="modal"
                                           data-target="#report_ads">
                                            <i class="fa fa-flag" aria-hidden="true"></i>
                                            گزارش تخلف آگهی
                                        </a>
                                    </div>

                                    &nbsp;
                                    <div class="contact-detail-info mt-2">
                                        <small>مشاور:</small>
                                        <span class="contact-detail-col"><?php echo $item->user_name; ?> </span>
                                    </div>
                                    <div class="contact-detail-info mt-2">
                                        <small>کد ملک:</small>
                                        <span class="contact-detail-col"><?php echo $item->item_code; ?></span>
                                    </div>

                                </div>


                            </div>
                        </div>
                    </section>

                </div>


            </div>
        </div>
        <!-- Report Ad Modal -->
        <div class="modal fade" id="contact_info" tabindex="-1" role="dialog" aria-labelledby="contact_info"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="loading">
                        <div class="loader-show"></div>
                    </div>
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">تماس با آگهی دهنده</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="user_contact_info">
                            <dt><i class="fa fa-phone" aria-hidden="true"></i> دفتر ملک خوی:</dt>
                            <dd id="ch-phone"><a href="tel:04436362010">04436362010</a></dd>
                            <dt><i class="fa fa-phone" aria-hidden="true"></i> کارشناس فروش:</dt>
                            <dd id="ch-phone"><a href="tel:09104405996">09104405996</a></dd>


                            <?php if ($item->whatsapp_number != '') { ?>
                                <dt><i class="fa fa-phone" aria-hidden="true"></i> واتساپ کارشناس:</dt>
                                <dd id="ch-phone"><a
                                            href="tel:<?php echo $item->whatsapp_number; ?>"><?php echo $item->whatsapp_number; ?></a>
                                </dd>
                            <?php } ?>
                            <?php if ($item->personnel != '') { ?>

                                <dt><i class="fa fa-phone" aria-hidden="true"></i> تلفن کارشناس:</dt>
                                <dd id="ch-phone"><a
                                            href="tel:<?php echo $item->personnel; ?>"><?php echo $item->personnel; ?></a>
                                </dd>

                            <?php } ?>

                            <!--
                            <dt><i class="fa fa-phone" aria-hidden="true"></i> شماره تماس:</dt>
                            <dd id="ch-phone"><a href="tel:<?php /*echo $item->phone; */ ?>"><?php /*echo $item->phone; */ ?></a></dd>-->
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <div class="modal fade" id="report_ads" tabindex="-1" role="dialog" aria-labelledby="report_ads"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="loading">
                        <div class="loader-show"></div>
                    </div>
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">گزارش تخلف آگهی</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input name="ad_id" id="ad_id" type="hidden" value="<?php echo $item->id; ?>">
                        <h5>
                            درصورتی در متن این آگهی تخلفی صورت گرفته است گزارش دهید
                        </h5>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary small" data-dismiss="modal">لغو</button>
                        <button type="button" class="btn btn-danger small send_report">ارسال گزارش</button>
                        <div class="ad-loading">
                            <div class="ad-loader-show"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <script>
            var ajaxurl = "/index.php/add_report/<?php echo $item->id;?>";
            var ajax_favourite = "/index.php/ajax_favourite/<?php echo $item->id;?>";
            jQuery(function ($) {
                var data = {
                    'ad_id': jQuery('#ad_id').val(),
                };
                $('body').on('click', '.send_report', function () {
                    jQuery(".ad-loading").css("display", "block");
                    $.post(ajaxurl, data, function (response) {
                        alert(' گزارش آگهی با موفقیت ارسال شد');
                        jQuery(".ad-loading").css("display", "none");
                        jQuery('#report_ads').modal('toggle');
                    });
                });
                $('body').on('click', '.favorite', function () {
                    $.post(ajax_favourite, data, function (response) {
                        if (response == 1)
                            alert('آگهی از علاقه مندی شما حذف شد');
                        else if (response == 2)
                            alert('آگهی به علاقه مندی شما اضافه شد');
                        else if (response == 3)
                            alert('برای افزودن آگهی به علاقه مندی ها ابتدا وارد سایت شوید');
                        else
                            alert('خطا');
                        if (response != 3)
                            window.location.href = "<?php echo current_url(); ?>";
                    });
                });
            });
        </script>

    </div>
</section>