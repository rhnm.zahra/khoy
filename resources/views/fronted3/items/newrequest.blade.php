<?php
if (count($_POST)) {
    $item=new stdClass();
    $item=json_decode(json_encode($_POST), FALSE);
}
?>
<link rel="stylesheet" type="text/css" href="https://static.neshan.org/sdk/openlayers/5.3.0/ol.css">
<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList,URL"></script>
<script type="text/javascript" src="https://static.neshan.org/sdk/openlayers/5.3.0/ol.js"></script>
<style>
    #neshan_map_center_marker {
        width: 30px;
        height: 44px;
        background: transparent center center no-repeat;
        position: absolute;
        left: 50%;
        top: 50%;
        margin: -44px auto auto -15px;
    }

    .dropdown.bootstrap-select.swal2-select {
        display: none;
    }
    /*Title Search*/
          input[type="text"], input[type="email"], input[type="date"], input[type="time"], input[type="search"], input[type="password"], input[type="number"], input[type="url"], input[type="tel"], textarea.form-control, textarea,select  {
        box-shadow: inset 0 0 1rem 0 rgb(242 242 242 / 100%);
        background: rgb(242 242 242 / 100%);
        border-radius: 12px 12px 12px 12px;
        border: 1px solid grey ;
        font-size: 18px;
    
      
    }


    /*Other Boxs Under*/
       .bootstrap-select .btn.dropdown-toggle {
        box-shadow: inset 0 0 1rem 0 rgb(242 242 242 / 100%) !important;
        background: rgb(242 242 242 / 100%);
        border-radius: 12px 12px 12px 12px;
        border: 1px solid grey ;
       

}

    
    
    
    .alert-info {
        background-color: #17a2b84a;
    }
    .selectorfiles {
        background: #0077f2 !important;
    }
</style>

<section class="block" style="transform: none;">
    <div class="container" style="transform: none;">
        <?php if (validation_errors()): ?>
            <div class="alert alert-danger mb-2">
                <?php echo validation_errors(); ?>
            </div>
        <?php endif ?>
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title"><?php echo get_msg('prd_info') ?></h3>
            </div>
            <?php echo form_open_multipart('', 'id="formnewitem"'); ?>
            <div class="card-body">

                <p class="alert alert-info">
                    چه نوع ملکی را درخواست میکنید ؟ لطفا فیلدهای مد نظرتان را با دقت وارد کنید
                </p>

                <section class="box">
                    <h2 class="box-title">اطلاعات پایه</h2>
                    <span class="collapse-arrow" data-toggle="collapse" href="#collapseContent" role="button"
                          aria-expanded="true" aria-controls="collapseContent">
								<i class="fa fa-minus" aria-hidden="true"></i>
							</span>
                    <div class="b-line"></div>
                    <div class="collapse show" id="collapseContent" style="">

                        <div class="row">
                            <input type="hidden" value="1" name="item_type_id" />
                            <input type="hidden" name="cat_id" value="cat4633d382f6d87d0df8421ac4cb05eee3" />
                            <div class="form-group col-md-4">
                                <label> <span style="font-size: 11px; color: red;">*</span>
                                   نام زیر شاخه املاک درخواستی
                                </label>

                                <?php
                                    $options = array();
                                    $conds['cat_id'] = 'cat4633d382f6d87d0df8421ac4cb05eee3';
                                    $sub_cat = $this->Subcategory->get_all_by($conds);
                                    foreach ($sub_cat->result() as $subcat) {
                                        $options[$subcat->id] = $subcat->name;
                                    }
                                    echo form_dropdown(
                                        'sub_cat_id',
                                        $options,
                                        set_value('sub_cat_id', show_data(@$item->sub_cat_id), false),
                                        'class="form-control form-control-sm mr-3" id="sub_cat_id"'
                                    );

                                ?>

                            </div>
                            <div class="form-group col-md-3">
                                <label> <span style="font-size: 11px; color: red;">*</span>
                                    <?php echo "شماره تلفن" ?>
                                </label>

                                <?php echo form_input(array(
                                    'name' => 'brand',
                                    'value' => set_value('brand', show_data(@$item->brand), false),
                                    'class' => 'form-control form-control-sm',
                                    'placeholder' => "شماره تلفن",
                                    'id' => 'brand'

                                )); ?>

                            </div>

                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label>
                                    <label> <span style="font-size: 11px; color: red;">*</span></label>
                                        <?php echo get_msg('item_description_label') ?>
                                </label>

                                <?php echo form_textarea(array(
                                    'name' => 'description',
                                    'value' => set_value('description', show_data(@$item->description), false),
                                    'class' => 'form-control form-control-sm',
                                    'placeholder' => 'متن درخواست خود را اینجا بنویسید ... ',
                                    'id' => 'description',
                                    'rows' => "5"
                                )); ?>

                            </div>
                        </div>
                        
                        <center>
                            
                       
                        
                   <div>
                       
                                   <p class="alert ">
                    جهت رسیدگی هرچه سریعتر کارشناس به درخواست شما ، بعد از وارد کردن اطلاعات کادر بالا میتوانید ثبت درخواست را بزنید
                </p>
                
                                     <p class="alert ">
                    لازم به ذکر است با وارد نکردن اطلاعات تکمیلی کادرهای پایینی شما نمیتوانید در آن واحد نتیجه ی درخواستتان را از سیستم هوشمند ملک خوی مشاهده کنید .برای مشاهده آگهی های همسان با درخواست شما پرکردن اطلاعات کادرهای پایین این صفحه الزامی است . 
                </p>
                <button id="subtn" type="button" class="btn btn-sm btn-primary">
                    <?php echo "ثبت درخواست" ?>
                </button>
            </div>
             </center>
       
                    </div>
                </section>

                <section class="box">
                    <h2 class="box-title">اطلاعات اصلی</h2>
                    <span class="collapse-arrow" data-toggle="collapse" href="#collapseContent2" role="button"
                          aria-expanded="true" aria-controls="collapseContent">
								<i class="fa fa-minus" aria-hidden="true"></i>
							</span>
                    <div class="b-line"></div>
                    <div class="collapse show" id="collapseContent2" style="">

                        <div class="row">

                            <div class="form-group col-md-9">
                                <label> <span style="font-size: 11px; color: red;">*</span>
                                    <?php echo get_msg('itm_title_label') ?>
                                </label>
                                <?php echo form_input(array(
                                    'name' => 'title',
                                    'value' => set_value('title', show_data(@$item->title), false),
                                    'class' => 'form-control form-control-sm',
                                    'placeholder' => get_msg('itm_title_label'),
                                    'id' => 'title'

                                )); ?>

                            </div>

                            <div class="form-group col-md-3 div_rahn" style="display: none;">
                                <label> <span style="font-size: 11px; color: red;">*</span>
                                    رهن
                                </label>

                                <?php echo form_input(array(
                                    'name' => 'rahn',
                                    'value' => set_value('rahn', show_data(@number_format($item->item_rahn)), false),
                                    'class' => 'form-control form-control-sm',
                                    'onkeyup' => 'javascript:this.value=separate(this.value);',
                                    'placeholder' => 'رهن',
                                    'id' => 'rahn'

                                )); ?>

                            </div>
                            <div class="form-group col-md-3 div_rahn" style="display: none;">
                                <label> <span style="font-size: 11px; color: red;">*</span>
                                    اجاره
                                </label>

                                <?php echo form_input(array(
                                    'name' => 'ejare',
                                    'value' => set_value('ejare', show_data(@number_format($item->item_ejare)), false),
                                    'class' => 'form-control form-control-sm',
                                    'onkeyup' => 'javascript:this.value=separate(this.value);',
                                    'placeholder' => 'اجاره',
                                    'id' => 'ejare'

                                )); ?>

                            </div>

                            <input type="hidden" name="item_price_type_id" value="2" />
                            <div class="form-group col-md-3" id="price_div">
                                <label> <span style="font-size: 11px; color: red;">*</span>
                                    <?php echo get_msg('price') ?>
                                </label>

                                <?php echo form_input(array(
                                    'name' => 'price',
                                    'value' => set_value('price', show_data(@number_format($item->price)), false),
                                    'class' => 'form-control form-control-sm',
                                    'onkeyup' => 'javascript:this.value=separate(this.value);',
                                    'placeholder' => get_msg('price'),
                                    'id' => 'price'

                                )); ?>

                            </div>
                            <div class="form-group  col-md-3">
                                <label> <span style="font-size: 11px; color: red;">*</span>
                                    <?php echo get_msg('itm_select_location') ?>
                                </label>

                                <?php

                                $options = array();
                                $options[''] = get_msg('itm_select_location');
                                $locations = $this->Itemlocation->get_all();
                                foreach ($locations->result() as $location) {
                                    $options[$location->id] = $location->name;
                                }

                                echo form_dropdown(
                                    'item_location_id',
                                    $options,
                                    set_value('item_location_id', show_data(@$item->item_location_id), false),
                                    'class="form-control form-control-sm mr-3" id="item_location_id"'
                                );
                                ?>
                            </div>

                            <div class="form-group  col-md-3">
                                <label> <span style="font-size: 11px; color: red;">*</span>
                                    منطقه ( ابتدا شهر را انتخاب کنید)
                                </label>

                                <?php

                                $options = array();

                                echo form_dropdown(
                                    'item_location_area_id',
                                    $options,
                                    set_value('item_location_area_id', json_decode(show_data(@$item->item_location_area_id), true), false),
                                    'class="form-control form-control-sm mr-3"    id="item_location_area_id"'
                                );
                                ?>
                            </div>
                            <div class="form-group" style="display:none;">
                                <label> <span style="font-size: 11px; color: red;">*</span>
                                    <?php echo get_msg('itm_select_currency') ?>
                                </label>

                                <?php
                                $options = array();
                                $conds['status'] = 1;
                                $options[''] = get_msg('itm_select_currency');
                                $currency = $this->Currency->get_all_by($conds);
                                foreach ($currency->result() as $curr) {
                                    $options[$curr->id] = $curr->currency_short_form;
                                }

                                echo form_dropdown(
                                    'item_currency_id',
                                    $options,
                                    set_value('item_currency_id', show_data(@$item->item_currency_id), false),
                                    'class="form-control form-control-sm mr-3" id="item_currency_id"'
                                );
                                ?>
                            </div>
                            <div class="form-group" style="   display:none;">
                                <label> <span style="font-size: 11px; color: red;">*</span>
                                    <?php echo get_msg('itm_select_condition_of_item') ?>
                                </label>

                                <?php
                                $options = array();
                                $conds['status'] = 1;
                                $options[''] = get_msg('condition_of_item');
                                $conditions = $this->Condition->get_all_by($conds);
                                foreach ($conditions->result() as $cond) {
                                    $options[$cond->id] = $cond->name;
                                }

                                echo form_dropdown(
                                    'condition_of_item_id',
                                    $options,
                                    set_value('condition_of_item_id', show_data(@$item->condition_of_item_id), false),
                                    'class="form-control form-control-sm mr-3" id="condition_of_item_id"'
                                );
                                ?>
                            </div>
                            <div class="form-group" style="   display:none;">
                                <label> <span style="font-size: 11px; color: red;">*</span>
                                    <?php echo get_msg('prd_high_info') ?>
                                </label>

                                <?php echo form_textarea(array(
                                    'name' => 'highlight_info',
                                    'value' => set_value('info', show_data(@$item->highlight_info), false),
                                    'class' => 'form-control form-control-sm',
                                    'placeholder' => "لطفا اطلاعات خواسته شده را پر کنید",
                                    'id' => 'info',
                                    'rows' => "3"
                                )); ?>

                            </div>
                            <div class="form-group" style="   display:none;">
                                <label> <span style="font-size: 11px; color: red;">*</span>
                                    <?php echo get_msg('itm_select_deal_option') ?>
                                </label>

                                <?php
                                $options = array();
                                $conds['status'] = 1;
                                $options[''] = get_msg('deal_option_id_label');
                                $deals = $this->Option->get_all_by($conds);
                                foreach ($deals->result() as $deal) {
                                    $options[$deal->id] = $deal->name;
                                }

                                echo form_dropdown(
                                    'deal_option_id',
                                    $options,
                                    set_value('deal_option_id', show_data(@$item->deal_option_id), false),
                                    'class="form-control form-control-sm mr-3" id="deal_option_id"'
                                );
                                ?>
                            </div>
                            <input type="hidden" name="address" value="" />
                            <div class="form-group col-md-3">
                                <label>
                                    <?php echo "شماره واتساپ" ?>
                                </label>

                                <?php echo form_input(array(
                                    'name' => 'whatsapp_number',
                                    'value' => set_value('whatsapp_number', show_data(@$item->whatsapp_number), false),
                                    'class' => 'form-control form-control-sm',
                                    'placeholder' => "شماره واتساپ",
                                    'id' => 'whatsapp_number'

                                )); ?>

                            </div>
                            <div class="form-group col-md-3">
                                <label>
                                    <?php echo "شماره کارشناس" ?>
                                </label>

                                <?php echo form_input(array(
                                    'name' => 'personnel',
                                    'value' => set_value('personnel', show_data(@$item->personnel), false),
                                    'class' => 'form-control form-control-sm',
                                    'placeholder' => "شماره کارشناس",
                                    'id' => 'personnel'

                                )); ?>

                            </div>
                        </div>

                    </div>
                </section>

                <section class="box">
                    <h2 class="box-title">ویژگی ها</h2>
                    <span class="text-danger">
                        (جهت مشاهده ویژگی ها کلیک کنید)
                    </span>
                    <span class="collapse-arrow collapsed" data-toggle="collapse" href="#collapseContent3"
                          role="button" aria-expanded="true" aria-controls="collapseContent">
								<i class="fa fa-minus" aria-hidden="true"></i>
							</span>
                    <div class="b-line"></div>
                    <div class="collapse" id="collapseContent3" style="">

<div class="row">

                            <div class="form-group col-md-3" id="metrazh_div">
                                <label>
                                    متراژ زمین
                                </label>
                                <?php echo form_input(array(
                                    'name' => 'metrazh',
                                    'value' => set_value('metrazh', show_data(@number_format($item->metrazh)), false),
                                    'class' => 'form-control form-control-sm',
                                    'onkeyup' => 'javascript:this.value=separate(this.value);',
                                    'placeholder' => 'متراژ زمین',
                                    'id' => 'metrazh'

                                )); ?>

                            </div>


                            <div class="form-group col-md-3" id="zirbana_div">
                                <label>
                                    متراژ زیربنا
                                </label>
                                <?php echo form_input(array(
                                    'name' => 'zirbana',
                                    'value' => set_value('zirbana', show_data(@number_format($item->zirbana)), false),
                                    'class' => 'form-control form-control-sm',
                                    'onkeyup' => 'javascript:this.value=separate(this.value);',
                                    'placeholder' => 'متراژ زیربنا',
                                    'id' => 'zirbana'

                                )); ?>
                            </div>


                            <div class="form-group col-md-3">
                                <label>
                                    کاربری ملک
                                </label>
                                <?php
                                $options = array();
                                $options += ITEM_USE;

                                echo form_multiselect(
                                    'usemelk[]',
                                    $options,
                                    set_value('usemelk[]', json_decode(show_data(@$item->usemelk), true), false),
                                    'class="form-control form-control-sm mr-3" id="usemelk" title="انتخاب کنید" '
                                );
                                ?>
                            </div>


                            <div class="form-group col-md-3">
                                <label>
                                    نوع سند
                                </label>
                                <?php
                                $options = array();
                                $options += ITEM_SANAD;

                                echo form_multiselect(
                                    'sanad[]',
                                    $options,
                                    set_value('sanad[]', json_decode(show_data(@$item->sanad), true), false),
                                    'class="form-control form-control-sm mr-3" id="sanad" title="انتخاب کنید" '
                                );
                                ?>
                            </div>


                            <div class="form-group col-md-3">
                                <label>
                                    جهت ساختمان
                                </label>
                                <?php
                                $options = array();
                                //                                $options[''] = 'جهت ساختمان';
                                $options += ITEM_DIRECTION;

                                echo form_multiselect(
                                    'direction[]',
                                    $options,
                                    set_value('direction[]', json_decode(show_data(@$item->direction), true), false),
                                    'class="form-control form-control-sm mr-3" title="انتخاب کنید" id="direction"'
                                );
                                ?>
                            </div>


                            <div class="form-group col-md-3">
                                <label>
                                    وضعیت تخلیه
                                </label>
                                <?php
                                $options = array();
                                $options[''] = 'وضعیت تخلیه';
                                $options += ITEM_EMPTY;

                                echo form_dropdown(
                                    'empty',
                                    $options,
                                    set_value('empty', show_data(@$item->empty), false),
                                    'class="form-control form-control-sm mr-3" id="empty"'
                                );
                                ?>
                            </div>


                            <div class="form-group col-md-3">
                                <label>
                                    طبقه چندم
                                </label>
                                <?php
                                $options = array();
                                $options[''] = 'طبقه چندم';
                                $options += ITEM_FLOOR;

                                echo form_dropdown(
                                    'floor',
                                    $options,
                                    set_value('floor', show_data(@$item->floor), false),
                                    'class="form-control form-control-sm mr-3" id="floor"'
                                );
                                ?>
                            </div>


                            <div class="form-group col-md-3">
                                <label>
                                    تعداد واحد در هر طبقه
                                </label>
                                <?php
                                $options = array();
                                $options[''] = 'تعداد واحد در هر طبقه';
                                $options += ITEM_UNIT;

                                echo form_dropdown(
                                    'unit',
                                    $options,
                                    set_value('unit', show_data(@$item->unit), false),
                                    'class="form-control form-control-sm mr-3" id="unit"'
                                );
                                ?>
                            </div>


                            <div class="form-group col-md-3">
                                <label>
                                    تعداد کل طبقات
                                </label>
                                <?php
                                $options = array();
                                $options[''] = 'تعداد کل طبقات';
                                $options += ITEM_FLOORCOUNT;

                                echo form_dropdown(
                                    'floorcount',
                                    $options,
                                    set_value('floorcount', show_data(@$item->floorcount), false),
                                    'class="form-control form-control-sm mr-3" id="floorcount"'
                                );
                                ?>
                            </div>


                            <div class="form-group col-md-3">
                                <label>
                                    سال ساخت
                                </label>
                                <?php
                                $options = array();
                                $options[''] = 'سال ساخت';
                                $options += ITEM_YEAR;
                                echo form_dropdown(
                                    'year',
                                    $options,
                                    set_value('year', show_data(@$item->year), false),
                                    'class="form-control form-control-sm mr-3" id="year"'
                                );
                                ?>
                            </div>


                            <div class="form-group col-md-3">
                                <label>
                                    تعداد اتاق
                                </label>
                                <?php
                                $options = array();
                                $options[''] = 'تعداد اتاق';
                                $options += ITEM_ROOM;
                                echo form_dropdown(
                                    'room',
                                    $options,
                                    set_value('room', show_data(@$item->room), false),
                                    'class="form-control form-control-sm mr-3" id="room"'
                                );
                                ?>
                            </div>


                            <div class="form-group col-md-3">
                                <label>
                                    طول بر ملک
                                </label>
                                <?php echo form_input(array(
                                    'name' => 'toolbar',
                                    'value' => set_value('toolbar', show_data(@number_format($item->toolbar)), false),
                                    'class' => 'form-control form-control-sm',
                                    'onkeyup' => 'javascript:this.value=separate(this.value);',
                                    'placeholder' => 'طول بر ملک',
                                    'id' => 'toolbar'

                                )); ?>

                            </div>


                            <div class="form-group col-md-3">
                                <label>
                                    متراژ کوچه / خیابان
                                </label>
                                <?php echo form_input(array(
                                    'name' => 'arz',
                                    'value' => set_value('arz', show_data(@number_format($item->arz)), false),
                                    'class' => 'form-control form-control-sm',
                                    'onkeyup' => 'javascript:this.value=separate(this.value);',
                                    'placeholder' => 'متراژ کوچه / خیابان',
                                    'id' => 'arz'

                                )); ?>
                            </div>

                            <div class="form-group col-md-3">
                                <label>
                                    تعداد حمام
                                </label>
                                <?php
                                $options = array();
                                $options[''] = 'تعداد حمام';
                                $options += ITEM_BATHROOM;

                                echo form_dropdown(
                                    'bathroom',
                                    $options,
                                    set_value('bathroom', show_data(@$item->bathroom), false),
                                    'class="form-control form-control-sm mr-3" id="bathroom"'
                                );
                                ?>
                            </div>


                            <div class="form-group col-md-3">
                                <label>
                                    نوع سرویس بهداشتی
                                </label>
                                <?php
                                $options = array();
                                $options[''] = 'نوع سرویس بهداشتی';
                                $options += ITEM_WC;

                                echo form_dropdown(
                                    'wc',
                                    $options,
                                    set_value('wc', show_data(@$item->wc), false),
                                    'class="form-control form-control-sm mr-3" id="wc"'
                                );
                                ?>
                            </div>

                        </div>

                    </div>
                </section>


                <div class="row">


                    <div class="col-md-12" id="moaveze" style="margin-bottom:10px;display: none">

                        <div class="row">

                            <div class="form-group">
                                <label> <span style="font-size: 11px; color: red;">*</span>
                                    <?php echo "تمایل به معاوضه دارید ؟" ?>
                                </label>

                                <?php
                                $options = array();
                                $options[0] = 'خیر';
                                $options[1] = 'بله';

                                echo form_dropdown(
                                    'change_status',
                                    $options,
                                    set_value('change_status', show_data(@$item->change_status), false),
                                    'class="form-control form-control-sm mr-3" id="change_status"'
                                );
                                ?>

                            </div>
                            <div id="change_div" class="col-md-12"
                                 style="border: 2px solid rgb(255 255 255);
    padding: 4px;
    /* background-color: #b5e1e3; */
    background: linear-gradient(1deg, rgb(39 33 155) 0%, rgb(2 88 76) 0%, rgba(133,198,218,1) 54%, rgba(0,212,255,1) 100%);">

                                <h3 class="text-center mb-3 mt-3">
                                    کادر درج اطلاعات معاوضه ملک
                                </h3>

                                <div class="row">
                                    <p class="alert alert-danger col-md-12">
                                        مبلغ نقد موجود و مبلغ نقد درخواستی یکی باید تکمیل شود
                                    </p>
                                    <div class="form-group col-md-3">
                                        <label>
                                            مبلغ نقد موجود(تومان)
                                        </label>

                                        <?php echo form_input(array(
                                            'name' => 'price_avl',
                                            'value' => set_value('price_avl', show_data(@number_format($item->price_avl)), false),
                                            'class' => 'form-control form-control-sm',
                                            'onkeyup' => 'javascript:this.value=separate(this.value);',
                                            'placeholder' => 'مبلغ نقد موجود(تومان)',
                                            'id' => 'price_avl'
                                        )); ?>

                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>
                                            مبلغ نقد درخواستی(تومان)
                                        </label>

                                        <?php echo form_input(array(
                                            'name' => 'price_req',
                                            'value' => set_value('price_req', show_data(@number_format($item->price_req)), false),
                                            'class' => 'form-control form-control-sm',
                                            'onkeyup' => 'javascript:this.value=separate(this.value);',
                                            'placeholder' => 'مبلغ نقد درخواستی(تومان)',
                                            'id' => 'price_req'
                                        )); ?>

                                    </div>
                                    <div class="form-group col-md-3">
                                        <label> <span style="font-size: 11px; color: red;">*</span>
                                            مناطق (لطفا ابتدا شهر را انتخاب کنید)
                                        </label>

                                        <?php

                                        $options = array();

                                        echo form_multiselect(
                                            'item_area_id[]',
                                            $options,
                                            set_value('item_area_id[]', json_decode(show_data(@$item->item_area_id), true), false),
                                            'class="form-control form-control-sm mr-3"  title="انتخاب کنید"  id="item_area_id"'
                                        );
                                        ?>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label> <span style="font-size: 11px; color: red;">*</span>
                                            نوع ملک
                                        </label>

                                        <?php

                                        $options = array();
                                        $cats = $this->Itemcat->get_all();
                                        foreach ($cats->result() as $cat) {
                                            $options[$cat->id] = $cat->name;
                                        }

                                        echo form_multiselect(
                                            'item_cat_id[]',
                                            $options,
                                            set_value('item_cat_id[]', json_decode(show_data(@$item->item_cat_id), true), false),
                                            'class="form-control form-control-sm mr-3"  title="انتخاب کنید"  id="item_cat_id"'
                                        );
                                        ?>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="col-md-12" id="option">

                        <div class="row">

                            <div class="form-group col-md-3">
                                <label> <span style="font-size: 11px; color: red;">*</span>
                                    <?php echo "نیاز به ثبت جزئیات بیشتر دارید ؟" ?>
                                </label>

                                <?php
                                $options = array();
                                $options[0] = 'خیر';
                                $options[1] = 'بله';

                                echo form_dropdown(
                                    'option_status',
                                    $options,
                                    set_value('option_status', show_data(@$item->option_status), false),
                                    'class="form-control form-control-sm mr-3" id="option_status"'
                                );
                                ?>

                            </div>
                            <div id="option_div" class="col-md-12 mt-2 mb-3 pt-2"
                                 style="display: none;border: 2px solid #e885f8;padding: 4px;">

                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <label>
                                            جنس کف
                                        </label>

                                        <?php
                                        $options = array();
                                        //                                        $options[''] = 'جنس کف';
                                        $options += ITEM_KAF;

                                        echo form_multiselect(
                                            'kaf[]',
                                            $options,
                                            set_value('kaf[]', json_decode(show_data(@$item->kaf), true), false),
                                            'class="form-control form-control-sm mr-3"  title="انتخاب کنید" id="kaf"'
                                        );
                                        ?>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>
                                            سرمایش گرمایشی
                                        </label>

                                        <?php
                                        $options = array();
                                        //                                        $options[''] = 'سرمایش گرمایشی';
                                        $options += ITEM_SARMAYESH;

                                        echo form_multiselect(
                                            'sarmayesh[]',
                                            $options,
                                            set_value('sarmayesh[]', json_decode(show_data(@$item->sarmayesh), true), false),
                                            'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="sarmayesh"'
                                        );
                                        ?>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>
                                            مشخصه ملک
                                        </label>

                                        <?php
                                        $options = array();
                                        //                                        $options[''] = 'مشخصه ملک';
                                        $options += ITEM_MOSHAKHASE;

                                        echo form_multiselect(
                                            'moshakhase[]',
                                            $options,
                                            set_value('moshakhase[]', json_decode(show_data(@$item->moshakhase), true), false),
                                            'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="moshakhase"'
                                        );
                                        ?>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>
                                            تامین کننده آب گرم
                                        </label>

                                        <?php
                                        $options = array();
                                        //                                        $options[''] = 'تامین کننده آب گرم';
                                        $options += ITEM_ABEGARM;

                                        echo form_multiselect(
                                            'abegarm[]',
                                            $options,
                                            set_value('abegarm[]', json_decode(show_data(@$item->abegarm), true), false),
                                            'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="abegarm"'
                                        );
                                        ?>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>
                                            کابینت بندی
                                        </label>

                                        <?php
                                        $options = array();
                                        //                                        $options[''] = 'کابینت بندی';
                                        $options += ITEM_KABINET;

                                        echo form_multiselect(
                                            'kabinet[]',
                                            $options,
                                            set_value('kabinet[]', json_decode(show_data(@$item->kabinet), true), false),
                                            'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="kabinet"'
                                        );
                                        ?>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>
                                            جنس دیوارها
                                        </label>

                                        <?php
                                        $options = array();
                                        //                                        $options[''] = 'جنس دیوارها';
                                        $options += ITEM_DIVAR;

                                        echo form_multiselect(
                                            'divar[]',
                                            $options,
                                            set_value('divar[]', json_decode(show_data(@$item->divar), true), false),
                                            'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="divar"'
                                        );
                                        ?>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>
                                            نمای ساختمان
                                        </label>

                                        <?php
                                        $options = array();
                                        //                                        $options[''] = 'نمای ساختمان';
                                        $options += ITEM_NAMA;

                                        echo form_multiselect(
                                            'nama[]',
                                            $options,
                                            set_value('nama[]', json_decode(show_data(@$item->nama), true), false),
                                            'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="nama"'
                                        );
                                        ?>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>
                                            امکانات امنیتی
                                        </label>

                                        <?php
                                        $options = array();
                                        //                                        $options[''] = 'امکانات امنیتی';
                                        $options += ITEM_AMNIAT;

                                        echo form_multiselect(
                                            'amniat[]',
                                            $options,
                                            set_value('amniat[]', json_decode(show_data(@$item->amniat), true), false),
                                            'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="amniat"'
                                        );
                                        ?>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>
                                            امکانات رفاهی تفریحی
                                        </label>

                                        <?php
                                        $options = array();
                                        //                                        $options[''] = 'امکانات رفاهی تفریحی';
                                        $options += ITEM_REFAHI;

                                        echo form_multiselect(
                                            'refahi[]',
                                            $options,
                                            set_value('refahi[]', json_decode(show_data(@$item->refahi), true), false),
                                            'class="form-control form-control-sm mr-3"  title="انتخاب کنید" id="refahi"'
                                        );
                                        ?>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>
                                            مشخصه آشپزخانه
                                        </label>

                                        <?php
                                        $options = array();
                                        //                                        $options[''] = 'مشخصه آشپزخانه';
                                        $options += ITEM_ASHPAZKHANE;

                                        echo form_multiselect(
                                            'ashpazkhane[]',
                                            $options,
                                            set_value('ashpazkhane[]', json_decode(show_data(@$item->ashpazkhane), true), false),
                                            'class="form-control form-control-sm mr-3"  title="انتخاب کنید" id="ashpazkhane"'
                                        );
                                        ?>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="col-md-12" id="branchs">

                        <div class="row">

                            <div class="form-group col-md-3">
                                <label> <span style="font-size: 11px; color: red;">*</span>
                                    <?php echo "آیا کارشناس هستید ؟" ?>
                                </label>

                                <?php
                                $options = array();
                                $options[0] = 'خیر';
                                $options[1] = 'بله';

                                echo form_dropdown(
                                    'branch_status',
                                    $options,
                                    set_value('branch_status', show_data(@$item->branch_status), false),
                                    'class="form-control form-control-sm mr-3" id="branch_status"'
                                );
                                ?>

                            </div>
                            <div class="form-group branch_box col-md-3" style="display: none;">
                                <label> <span style="font-size: 11px; color: red;">*</span>
                                    شعبه ( ابتدا شهر را انتخاب کنید)
                                </label>

                                <?php

                                $options = array();

                                echo form_dropdown(
                                    'item_branch_id',
                                    $options,
                                    set_value('item_branch_id', json_decode(show_data(@$item->item_branch_id), true), false),
                                    'class="form-control form-control-sm mr-3"  id="item_branch_id"'
                                );
                                ?>
                            </div>
                            <div class="form-group branch_box col-md-3" style="display: none;">
                                <label> <span style="font-size: 11px; color: red;">*</span>
                                    کارشناسان ( ابتدا شعبه را انتخاب کنید)
                                </label>

                                <?php

                                $options = array();

                                echo form_multiselect(
                                    'item_branch_personnels[]',
                                    $options,
                                    set_value('item_branch_personnels[]', json_decode(show_data(@$item->item_branch_personnels), true), false),
                                    'class="form-control form-control-sm mr-3" title="انتخاب کارشناسان"   id="item_branch_personnels"'
                                );
                                ?>
                            </div>

                        </div>

                    </div>

                </div>

                <hr>

                <div class="row">
                    <div class="form-check col-md-12">
                        <label <?php if (!$edit) { ?> style="display:none;" <?php } ?>>

                            <?php echo form_checkbox(array(
                                'name' => 'is_sold_out',
                                'id' => 'is_sold_out',
                                'value' => 'accept',
                                'checked' => set_checkbox('is_sold_out', 1, (@$item->is_sold_out == 1) ? true : false),
                                'class' => 'form-check-input'
                            )); ?>

                            فروخته شد

                        </label>

                        <label>

                            <?php echo form_checkbox(array(
                                'name' => 'amlak',
                                'id' => 'amlak',
                                'value' => 'accept',
                                'checked' => set_checkbox('amlak', 1, (@$item->amlak == 1) ? true : false),
                                'class' => 'form-check-input'
                            )); ?>

                            املاک هستید ؟

                        </label>
                        <input type="hidden" name="rahnchange" value="" />
                        <label>

                            <?php echo form_checkbox(array(
                                'name' => 'asansor',
                                'id' => 'asansor',
                                'value' => 'accept',
                                'checked' => set_checkbox('asansor', 1, (@$item->asansor == 1) ? true : false),
                                'class' => 'form-check-input'
                            )); ?>

                            آسانسور

                        </label>
                        <label>

                            <?php echo form_checkbox(array(
                                'name' => 'parking',
                                'id' => 'parking',
                                'value' => 'accept',
                                'checked' => set_checkbox('parking', 1, (@$item->parking == 1) ? true : false),
                                'class' => 'form-check-input'
                            )); ?>

                            پارکینگ

                        </label>
                        <label>

                            <?php echo form_checkbox(array(
                                'name' => 'anbari',
                                'id' => 'anbari',
                                'value' => 'accept',
                                'checked' => set_checkbox('anbari', 1, (@$item->anbari == 1) ? true : false),
                                'class' => 'form-check-input'
                            )); ?>

                            انباری

                        </label>
                        <label>

                            <?php echo form_checkbox(array(
                                'name' => 'balkon',
                                'id' => 'balkon',
                                'value' => 'accept',
                                'checked' => set_checkbox('balkon', 1, (@$item->balkon == 1) ? true : false),
                                'class' => 'form-check-input'
                            )); ?>

                            بالکن

                        </label>
                        <label>

                            <?php echo form_checkbox(array(
                                'name' => 'vam',
                                'id' => 'vam',
                                'value' => 'accept',
                                'checked' => set_checkbox('vam', 1, (@$item->vam == 1) ? true : false),
                                'class' => 'form-check-input'
                            )); ?>

                            وام دارد

                        </label>
                        <label>

                            <?php echo form_checkbox(array(
                                'name' => 'bazsazi',
                                'id' => 'bazsazi',
                                'value' => 'accept',
                                'checked' => set_checkbox('bazsazi', 1, (@$item->bazsazi == 1) ? true : false),
                                'class' => 'form-check-input'
                            )); ?>

                            بازسازی شده

                        </label>
                    </div>

                </div>



                <div class="row">


                    <div class="col-md-6">


                        <!--  <label><?php echo get_msg('deal_option_id_label') ?></label><br>
              <input type="radio" name="deal_option_id"
              <?php if (isset($item->deal_option_id) && $item->deal_option_id == "1") echo "checked"; ?>
              value="1"><?php echo get_msg('meet_up_label'); ?>
              <input type="radio" name="deal_option_id"
              <?php if (isset($item->deal_option_id) && $item->deal_option_id == "2") echo "checked"; ?>
              value="2"><?php echo get_msg('mailing_or_delivery_label'); ?> -->

                        <br><br>


                    </div>

                    <div class="col-md-6">
                        <div class="form-group" style="   display:none;">
                            <div class="form-check">
                                <label>

                                    <?php echo form_checkbox(array(
                                        'name' => 'business_mode',
                                        'id' => 'business_mode',
                                        'value' => 'accept',
                                        'checked' => set_checkbox('business_mode', 1, (@$item->business_mode == 1) ? true : false),
                                        'class' => 'form-check-input'
                                    )); ?>

                                    <?php echo get_msg('itm_business_mode'); ?>
                                    <br><?php echo get_msg('itm_show_shop') ?>
                                </label>
                            </div>
                        </div>


                        <!-- form group -->
                    </div>



                    <div class="col-md-12">
                        <div class="row" style="display: block;">
                            <div class="form-group" style="display: block;">
                                <input type="file" id="files" style="display: none" name="images[]"
                                       accept="image/png, image/jpeg"/>
                                <label class="selectorfiles" for="files">افزودن تصویر</label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- row -->
            </div>

            <!-- Grid row -->
            <div class="gallery gallery-bsitem" id="gallery" style="margin-right: 15px; margin-bottom: 15px;">
                <?php
                $images = isset($images) ? $images : [];
                ?>
                <?php $i = 0;
                foreach ($images as $img) : ?>
                    <div class="mb-3 pics animation all 2">
                        <a style="background-image: url('<?php echo '/ssag/Melkekhoy/uploads/' . $img['img_path']; ?>');
                            background-position: center center;
                            background-repeat: no-repeat;background-size: contain;"
                           href="/ssag/Melkekhoy/uploads/<?php echo $img['img_path']; ?>"
                           data-lightbox="image-1"></a>
                        <input type="hidden" name="updated_images[]" value="<?php echo $img['img_id']; ?>"/>
                        <button type="button" class="removegal">حذف</button>
                    </div>
                    <?php $i++; endforeach; ?>
            </div>
            <!-- Grid row -->
            <style>
                .gallery-bsitem {
                    display: flex;
                    flex-wrap: wrap;
                }

                .gallery-bsitem > div {
                    width: 300px;
                    height: 250px;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    background: #eee;
                    padding: 40px 15px;
                    margin: 15px;
                    border-radius: 5px;
                    flex-direction: column;
                    position: relative;
                }

                .gallery-bsitem > div a,
                .gallery-bsitem > div img {
                    width: 100%;
                    height: 100%;
                }

                .gallery-bsitem > div button {
                    position: absolute;
                    bottom: 0px;
                    left: 0px;
                    width: 100%;
                    height: 30px;
                    background: transparent;
                    border: 1px solid red;
                    color: red;
                    cursor: pointer;
                }

                .selectorfiles {
                    width: 100%;
                    height: 35px;
                    cursor: pointer;
                    background: green;
                    color: #fff;
                    display: flex;
                    text-align: center;
                    justify-content: center;
                    align-items: center;
                    border-radius: 3px;
                }
            </style>
            <script>
                jQuery(function () {
                    jQuery('body').on('click', 'button.removegal', function () {
                        const index = jQuery(this).attr('data-index');
                        if (index != undefined) {
                            const dt = new DataTransfer();
                            const input = document.getElementById('files');
                            const {files} = input;
                            for (let i = 0; i < files.length; i++) {
                                const file = files[i];
                                if (index !== i) ;
                                dt.items.add(file);
                            }
                            input.files = dt.files;
                        }
                        jQuery(this).parent().remove();
                    });
                    // var form_data = new FormData();

                    jQuery('#files').on('change', function (e) {
                        var files = e.target.files;
                        if (files.length > 7) {
                            return;
                        }
                        for (var i = 0; i < files.length; i++) {
                            var file = files[i];
                            var fileurl = window.URL.createObjectURL(file);
                            var r = Math.floor((Math.random() * 1000) + 1);
                            var template = '<div id="sub' + r + '" class="mb-3 pics animation all 2"><a  style="background-image: url(' + fileurl + ');background-position: center center;background-repeat: no-repeat;background-size: contain;" href="javascript:void(0);"></a><button type="button" class="removegal" data-index="' + i + '">حذف</button></div>';
                            jQuery('#gallery').append(template);
                            jQuery("#files").clone().prependTo("#sub" + r);
                        }
                    });
                });
            </script>

            <hr>
            <h3 class="text-center">
                اطلاعات محرمانه آگهی
            </h3>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            نام مالک
                        </label>
                        <?php echo form_input(array(
                            'name' => 'malek',
                            'value' => set_value('malek', show_data(@$item->malek), false),
                            'class' => 'form-control form-control-sm',
                            'placeholder' => 'مالک',
                            'id' => 'malek'

                        )); ?>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            شماره تماس
                        </label>
                        <?php echo form_input(array(
                            'name' => 'tamas_malek',
                            'value' => set_value('tamas_malek', show_data(@$item->tamas_malek), false),
                            'class' => 'form-control form-control-sm',
                            'placeholder' => 'شماره تماس',
                            'id' => 'tamas_malek'

                        )); ?>

                    </div>
                </div>
                <div class="col-md-12 mt-2">
                    <div class="form-group">
                        <label>
                            توضیحات محرمانه
                        </label>
                        <?php echo form_textarea(array(
                            'name' => 'desc_malek',
                            'value' => set_value('desc_malek', show_data(@$item->desc_malek), false),
                            'class' => 'form-control form-control-sm',
                            'placeholder' => 'توضیحات محرمانه',
                            'id' => 'desc_malek'

                        )); ?>

                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row" style="display: block;">
                        <div class="form-group" style="display: block;">
                            <input type="file" id="files_malek" style="display: none" name="images_malek[]"
                                   accept="image/png, image/jpeg"/>
                            <label class="selectorfiles" for="files_malek">افزودن تصویر محرمانه</label>
                        </div>
                    </div>
                </div>
                <!-- Grid row -->
                <div class="gallery gallery-bsitem" id="gallery_malek"
                     style="margin-right: 15px; margin-bottom: 15px;">
                    <?php
                    $images_malek = isset($images_malek) ? $images_malek : [];
                    ?>
                    <?php $i = 0;
                    foreach ($images_malek as $img) : ?>
                        <div class="mb-3 pics animation all 2">
                            <a style="background-image: url('<?php echo '/ssag/Melkekhoy/uploads/' . $img['img_path']; ?>');
                                background-position: center center;
                                background-repeat: no-repeat;background-size: contain;"
                               href="/ssag/Melkekhoy/uploads/<?php echo $img['img_path']; ?>"
                               data-lightbox="image-1"></a>
                            <input type="hidden" name="updated_images_malek[]"
                                   value="<?php echo $img['img_id']; ?>"/>
                            <button type="button" class="removegal">حذف</button>
                        </div>
                        <?php $i++; endforeach; ?>
                </div>
                <script>
                    jQuery(function () {

                        jQuery('#files_malek').on('change', function (e) {
                            var files = e.target.files;
                            if (files.length > 7) {
                                return;
                            }
                            for (var i = 0; i < files.length; i++) {
                                var file = files[i];
                                var fileurl = window.URL.createObjectURL(file);
                                var r = Math.floor((Math.random() * 1000) + 1);
                                var template = '<div id="sub_malek' + r + '" class="mb-3 pics animation all 2"><a style="background-image: url(' + fileurl + ');background-position: center center;background-repeat: no-repeat;background-size: contain;" href="javascript:void(0);"></a><button type="button" class="removegal" data-index="' + i + '">حذف</button></div>';
                                jQuery('#gallery_malek').append(template);
                                jQuery("#files_malek").clone().prependTo("#sub_malek" + r);
                            }


                        });
                    });
                </script>
            </div>

            <div class="card-footer">
                <button id="subtn" type="button" class="btn btn-sm btn-primary">
                    <?php echo "ثبت درخواست" ?>
                </button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade"  data-controls-modal="modal_wait" data-backdrop="static" data-keyboard="false" id="modal_wait" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                لطفا منتظر بمانید...
                <br>
                اطلاعات در حال ثبت می باشد
            </div>
        </div>
    </div>
</div>
<script>
    function create_map(clat, clong) {
        var update = function () {
            center = ol.proj.toLonLat(myMap.getView().getCenter());
            jQuery('#lat').val(center[1]);
            jQuery('#lng').val(center[0]);
        }
        var latlong = [clong, clat];
        var myMap = new ol.Map({
            target: 'map',
            key: 'web.O8FFx7qxI6syqKxYUbkQlajvlUwHdaV7YlmQLe5O\n',
            maptype: 'dreamy',
            poi: true,
            traffic: false,
            view: new ol.View({
                center: ol.proj.fromLonLat(latlong),
                zoom: 16
            })
        });
        _markerEl = jQuery('<div id="neshan_map_center_marker" class="ol-unselectable" />').appendTo(jQuery('.ol-overlaycontainer-stopevent'));
        _markerEl.css('background-image', 'url("https://developers.neshan.org/tools/static-map-maker/images/marker_red.png?v=2")');

        update();
        myMap.getView().on('change:center', function () {
            update();
        });
    }
    <?php if($edit){ ?>

    create_map(<?php echo $item->lat;?>,<?php echo $item->lng;?>);

    <?php } else { ?>
    create_map(38.546409, 44.952996);
    <?php } ?>

    <?php if ( $this->config->item('client_side_validation') == true ): ?>

    function jqvalidate() {

        jQuery('#item-form').validate({
            rules: {

                cat_id: {
                    indexCheck: ""
                },
                sub_cat_id: {
                    indexCheck: ""
                }
            },
            messages: {

                cat_id: {
                    indexCheck: "<?php echo $this->lang->line('f_item_cat_required'); ?>"
                },
                sub_cat_id: {
                    indexCheck: "<?php echo $this->lang->line('f_item_subcat_required'); ?>"
                }
            },

            submitHandler: function (form) {
                if (jQuery("#item-form").valid()) {
                    form.submit();
                }
            }

        });

        jQuery.validator.addMethod("indexCheck", function (value, element) {

            if (value == 0) {
                return false;
            } else {
                return true;
            }
            ;

        });


    }

    <?php endif; ?>
    jQuery(document).ready(function () {

        /*jQuery('#item_price_type_id').on('change', function () {
            var price_type = jQuery('#item_price_type_id').val();
            if (price_type == 1 || price_type == 8 || price_type == 7) {
                jQuery("#price_div").show();
            } else {
                jQuery("#price_div").hide();
            }
        });*/

        jQuery('#subtn').click(function () {

            <?php if($edit) { ?>
            var ajaxurl = '/index.php/user/editrequest_ajax/<?php echo $item->id;?>'
            <?php } else { ?>
            var ajaxurl = '/index.php/user/addrequest_ajax'
            <?php } ?>

            jQuery.blockUI({message: 'لطفا تا ثبت شدن اطلاعات صبر کنید ...'});
            let formData = new FormData(jQuery('#formnewitem')[0]);
            jQuery.ajax({
                url: ajaxurl,
                type: 'POST',
                data: formData,
                dataType: 'text',
                processData: false,
                contentType: false,
                success: function (data) {
                    jQuery.unblockUI();
                    if (data == 1) {
                        Swal.fire({
                            icon: 'success',
                            title: 'اطلاعات شما با موفقیت ثبت گردید',
                            confirmButtonText: 'بستن',
                        });
                        // jQuery("#modal_wait").modal("hide");
                        window.location.href = "/index.php/user/items";

                    } else {
                        // jQuery("#modal_wait").modal("hide");
                        Swal.fire({
                            icon: 'error',
                            title: 'خطاهای زیر را بررسی کنید',
                            confirmButtonText: 'بستن',
                            html: data
                        });
                        // jQuery("#modal_wait").modal("hide");

                    }
                },
                error: function () {
                    jQuery.unblockUI();
                    alert('خطا');
                }
            });
            /*      jQuery.post('/index.php/user/additem_ajax', jQuery('#formnewitem').serialize(), function (data) {


                  });*/

        });

        jQuery('#sub_cat_id').on('change', function () {
            if(jQuery(this).val()=='subcat257c99f44d21c26ce7cfd138a0b6e697' || jQuery(this).val()=='subcat64c8a18d84ab74eb3f9830f7e4d0eed1')
            {
                jQuery('#metrazh_div').show();
                jQuery('#zirbana_div').hide();
                jQuery('#zirbana').val('0');

            }else if(jQuery(this).val()=='subcat46ce8ce3a8ee743904b993acde0e10a1'){
                jQuery('#metrazh_div').hide();
                jQuery('#metrazh').val('0');
                jQuery('#zirbana_div').show();
            }else{
                jQuery('#metrazh_div').show();
                jQuery('#zirbana_div').show();
            }
        });
        jQuery('#branch_status').on('change', function () {
            if(jQuery(this).val()==1){
                jQuery('.branch_box').show();
            }else{
                jQuery('.branch_box').hide();
            }
        });

        jQuery('#item_type_id').on('change', function () {
            var type = jQuery('#item_type_id').val();
            if (type == 4) {
                jQuery("#moaveze").show();
            } else {
                jQuery("#moaveze").hide();
                jQuery("#change_div").hide();
                jQuery('#change_status').val(0);
            }

            if (type == 3) {
                jQuery(".div_rahn").show();
                jQuery("#price_div").hide();
                // alert();
                jQuery("#price").val(0);
            } else {
                jQuery(".div_rahn").hide();
                jQuery("#price_div").show();
            }
        });
        jQuery('#change_status').on('change', function () {
            var type = jQuery('#change_status').val();
            if (type == 1) {
                jQuery("#change_div").show();
            } else {
                jQuery("#change_div").hide();
            }
        });
        jQuery('#option_status').on('change', function () {
            var type = jQuery('#option_status').val();
            if (type == 1) {
                jQuery("#option_div").show();
            } else {
                jQuery("#option_div").hide();
            }
        });

        <?php if($edit || count($_POST)){ ?>



        if(jQuery('#sub_cat_id').val()=='subcat257c99f44d21c26ce7cfd138a0b6e697' || jQuery('#sub_cat_id').val()=='subcat64c8a18d84ab74eb3f9830f7e4d0eed1')
        {
            jQuery('#metrazh_div').show();
            jQuery('#zirbana_div').hide();
            jQuery('#zirbana').val('0');

        }else if(jQuery('#sub_cat_id').val()=='subcat46ce8ce3a8ee743904b993acde0e10a1'){
            jQuery('#metrazh_div').hide();
            jQuery('#metrazh').val('0');
            jQuery('#zirbana_div').show();
        }else{
            jQuery('#metrazh_div').show();
            jQuery('#zirbana_div').show();
        }


        var catId = jQuery('#item_location_id').val();
        var areaid = '<?php echo $item->item_location_area_id; ?>';
        jQuery.ajax({
            url: '/index.php/user/get_areas/' + catId,
            method: 'GET',
            dataType: 'JSON',
            success: function (data) {
                // jQuery('#item_area_id').html("");
                jQuery('#item_location_area_id').html("");
                jQuery.each(data, function (i, obj) {
                    if (areaid == obj.id) {
                        var selected = 'selected=""';
                    } else {
                        var selected = '';
                    }
                    // jQuery('#item_area_id').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                    jQuery('#item_location_area_id').append('<option ' + selected + ' value="' + obj.id + '">' + obj.name + '</option>');
                });
                jQuery('#item_location_area_id').selectpicker('refresh');
                // jQuery('#item_area_id').selectpicker('refresh');
                // alert(areaid);
                //var operation_day = "<?php //echo implode(json_decode((@$item->item_area_id),true),',') ?>//";
                //var operation_day = "<?php //echo implode(json_decode((@$item->item_location_area_id),true),',') ?>//";
                // jQuery('#item_area_id').selectpicker('val', operation_day.split(",")); //split them and set value
                // jQuery('#item_location_area_id').selectpicker('val', operation_day.split(",")); //split them and set value
            }
        });

        <?php } ?>

        <?php if($edit || count($_POST)){ ?>

        // jQuery("#price_div").show();
        if (jQuery("#item_type_id").val() == 4) {
            jQuery("#moaveze").show();
            var type = jQuery('#change_status').val();
            if (type == 1) {
                jQuery("#change_div").show();
            }
        }

        var optype = jQuery('#option_status').val();
        if (optype == 1) {
            jQuery("#option_div").show();
        }

        if (jQuery("#item_type_id").val() == 3) {
            jQuery(".div_rahn").show();
            jQuery("#price_div").hide();
            // alert();
            jQuery("#price").val(0);

        } else {
            jQuery(".div_rahn").hide();
            jQuery("#price_div").show();
        }

        if(jQuery('#branch_status').val()==1){
            jQuery('.branch_box').show();
        }else{
            jQuery('.branch_box').hide();
        }

        var catId = jQuery('#item_location_id').val();
        //var areaid=  '<?php //echo $item->item_location_area_id; ?>//';
        jQuery.ajax({
            url: '/index.php/user/get_areas/' + catId,
            method: 'GET',
            dataType: 'JSON',
            success: function (data) {
                jQuery('#item_area_id').html("");
                // jQuery('#item_location_area_id').html("");
                jQuery.each(data, function (i, obj) {
                    /*if(areaid==obj.id)
                    {
                        var selected='selected=""';
                    }else{
                        var selected='';
                    }*/
                    jQuery('#item_area_id').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                    // jQuery('#item_location_area_id').append('<option '+selected+' value="' + obj.id + '">' + obj.name + '</option>');
                });
                // jQuery('#item_location_area_id').selectpicker('refresh');
                jQuery('#item_area_id').selectpicker('refresh');
                // alert(areaid);
                var operation_day = "<?php echo implode(json_decode((@$item->item_area_id), true), ',') ?>";
                //var operation_day = "<?php //echo implode(json_decode((@$item->item_location_area_id),true),',') ?>//";
                jQuery('#item_area_id').selectpicker('val', operation_day.split(",")); //split them and set value
                // jQuery('#item_location_area_id').selectpicker('val', operation_day.split(",")); //split them and set value
            }
        });

        var branchid = '<?php echo $item->item_branch_id; ?>';
        jQuery.ajax({
            url: '/index.php/user/get_branchs/' + catId,
            method: 'GET',
            dataType: 'JSON',
            success: function (data) {
                // jQuery('#item_area_id').html("");
                jQuery('#item_branch_id').html("");
                jQuery.each(data, function (i, obj) {
                    if (branchid == obj.id) {
                        var selected = 'selected=""';
                    } else {
                        var selected = '';
                    }
                    jQuery('#item_branch_id').append('<option ' + selected + ' value="' + obj.id + '">' + obj.name + '</option>');
                });
                jQuery('#item_branch_id').selectpicker('refresh');

                jQuery.ajax({
                    url: '/index.php/user/get_personnels/' + branchid,
                    method: 'GET',
                    dataType: 'JSON',
                    success: function (data) {
                        jQuery('#item_branch_personnels').html("");
                        jQuery.each(data, function (i, obj) {
                            jQuery('#item_branch_personnels').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                        });
                        jQuery('#item_branch_personnels').selectpicker('refresh');
                        var operation_day = "<?php echo implode(json_decode((@$item->item_branch_personnels), true), ',') ?>";
                        jQuery('#item_branch_personnels').selectpicker('val', operation_day.split(",")); //split them and set value
                    }

                });


            }

        });

        <?php } else{ ?>
        // jQuery("#price_div").hide();
        <?php }  ?>

        jQuery('#cat_id').on('change', function () {

            var value = jQuery('option:selected', this).text().replace(/Value\s/, '');

            var catId = jQuery(this).val();

            jQuery.ajax({
                url: 'get_sub_categories/' + catId,
                method: 'GET',
                dataType: 'JSON',
                success: function (data) {
                    jQuery('#sub_cat_id').html("");
                    jQuery.each(data, function (i, obj) {
                        jQuery('#sub_cat_id').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                    });
                    jQuery('#name').val(jQuery('#name').val() + " ").blur();
                    jQuery('#sub_cat_id').selectpicker('refresh');

                    if(jQuery('#sub_cat_id').val()=='subcat257c99f44d21c26ce7cfd138a0b6e697' || jQuery('#sub_cat_id').val()=='subcat64c8a18d84ab74eb3f9830f7e4d0eed1')
                    {
                        jQuery('#metrazh_div').show();
                        jQuery('#zirbana_div').hide();
                        jQuery('#zirbana').val('0');

                    }else if(jQuery('#sub_cat_id').val()=='subcat46ce8ce3a8ee743904b993acde0e10a1'){
                        jQuery('#metrazh_div').hide();
                        jQuery('#metrazh').val('0');
                        jQuery('#zirbana_div').show();
                    }else{
                        jQuery('#metrazh_div').show();
                        jQuery('#zirbana_div').show();
                    }
                }
            });
        });
        jQuery('#item_location_id').on('change', function () {

            var value = jQuery('option:selected', this).text().replace(/Value\s/, '');

            var catId = jQuery(this).val();

            jQuery.ajax({
                url: 'get_areas/' + catId,
                method: 'GET',
                dataType: 'JSON',
                success: function (data) {
                    jQuery('#item_area_id').html("");
                    jQuery('#item_location_area_id').html("");
                    jQuery.each(data, function (i, obj) {
                        jQuery('#item_area_id').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                        jQuery('#item_location_area_id').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                    });
                    jQuery('#name').val(jQuery('#name').val() + " ").blur();
                    jQuery('#item_area_id').selectpicker('refresh');
                    jQuery('#item_location_area_id').selectpicker('refresh');
                }
            });

            jQuery.ajax({
                url: 'get_branchs/' + catId,
                method: 'GET',
                dataType: 'JSON',
                success: function (data) {
                    jQuery('#item_branch_id').html("");
                    jQuery.each(data, function (i, obj) {
                        jQuery('#item_branch_id').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                    });
                    jQuery('#item_branch_id').selectpicker('refresh');


                    var bId = jQuery('#item_branch_id').val();
                    jQuery.ajax({
                        url: 'get_personnels/' + bId,
                        method: 'GET',
                        dataType: 'JSON',
                        success: function (data) {
                            jQuery('#item_branch_personnels').html("");
                            jQuery.each(data, function (i, obj) {
                                jQuery('#item_branch_personnels').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                            });
                            jQuery('#item_branch_personnels').selectpicker('refresh');
                        }
                    });
                }
            });


        });
        jQuery('#item_branch_id').on('change', function () {

            var value = jQuery('option:selected', this).text().replace(/Value\s/, '');

            var catId = jQuery(this).val();

            jQuery.ajax({
                url: '/index.php/user/get_personnels/' + catId,
                method: 'GET',
                dataType: 'JSON',
                success: function (data) {
                    jQuery('#item_branch_personnels').html("");
                    jQuery.each(data, function (i, obj) {
                        jQuery('#item_branch_personnels').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                    });
                    jQuery('#item_branch_personnels').selectpicker('refresh');
                }
            });

        });


        jQuery('#us3').locationpicker({
            location: {latitude: '<?php echo $item->lat;?>', longitude: '<?php echo $item->lng;?>'},
            radius: 300,
            inputBinding: {
                latitudeInput: jQuery('#lat'),
                longitudeInput: jQuery('#lng'),
                radiusInput: jQuery('#us3-radius')
            },
            enableAutocomplete: true,
            onchanged: function (currentLocation, radius, isMarkerDropped) {
                // Uncomment line below to show alert on each Location Changed event
                //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
            }
        });

        jQuery(function () {
            var selectedClass = "";
            jQuery(".filter").click(function () {
                selectedClass = jQuery(this).attr("data-rel");
                jQuery("#gallery").fadeTo(100, 0.1);
                jQuery("#gallery div").not("." + selectedClass).fadeOut().removeClass('animation');
                setTimeout(function () {
                    jQuery("." + selectedClass).fadeIn().addClass('animation');
                    jQuery("#gallery").fadeTo(300, 1);
                }, 300);
            });
        });

    });

    function separate(Number) {
        Number += '';
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        x = Number.split('.');
        y = x[0];
        z = x.length > 1 ? '.' + x[1] : '';
        var rgx = /([0123456789٠١٢٣٤٥٦٧٨٩۰۱۲۳۴۵۶۷۸۹]+)([0123456789٠١٢٣٤٥٦٧٨٩۰۱۲۳۴۵۶۷۸۹]{3})/;
        while (rgx.test(y))
            y = y.replace(rgx, '$1' + ',' + '$2');
        return y + z;
    }

    jQuery('form').submit(function () {
        jQuery('#files').remove();
    });
</script>
<?php
// replace cover photo modal
$data = array(
    'title' => get_msg('upload_photo'),
    'img_type' => 'item',
    'img_parent_id' => @$item->id
);

//$this->load->view( $template_path .'/components/photo_upload_modal', $data );
//
//// delete cover photo modal
//$this->load->view( $template_path .'/components/delete_cover_photo_modal' );
?>
<script>
    jQuery( document ).ready(function() {
        jQuery( "#price_req" ).keyup(function() {
            jQuery("#price_avl").prop('disabled', true);
            jQuery("#price_avl").val('0');
            if(jQuery("#price_req").val()=='')
            {
                jQuery("#price_avl").prop('disabled', false);
            }
        });
        jQuery( "#price_avl" ).keyup(function() {
            jQuery("#price_req").prop('disabled', true);
            jQuery("#price_req").val('0');
            if(jQuery("#price_avl").val()=='')
            {
                jQuery("#price_req").prop('disabled', false);
            }
        });
    });
</script>
