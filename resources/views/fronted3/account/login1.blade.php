<section class="block">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <article class="page-content clearfix">
                    <div class="woocommerce">
                        <div class="woocommerce-notices-wrapper"></div>
                        <div class="u-columns row" id="customer_login">

                            <div class="u-column1 col-md-6 col-sm-12 col-xs-12">

                                <div class="section-title clearfix">
                                    <h2>ورود</h2>
                                </div>

                                <?php echo form_open('user/login',array('class'=>'register')); ?>

                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                        <label for="email">ایمیل&nbsp;<span class="required">*</span></label>
                                        <input type="email" class="woocommerce-Input woocommerce-Input--text input-text"
                                               name="email" id="email" required="" autocomplete="email"
                                               value=""></p>
                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                        <label for="password">رمز عبور&nbsp;<span class="required">*</span></label>
                                        <span class="password-input"><input
                                                    class="woocommerce-Input woocommerce-Input--text input-text"
                                                    type="password" name="password" id="password"
                                                    autocomplete="current-password" required="">
                                        </span>
                                    </p>


                                    <p class="form-row">

                                        <button type="submit" class="woocommerce-Button button" name="login"
                                                value="ورود">ورود
                                        </button>
                                    </p>
                                    <p class="woocommerce-LostPassword lost_password">
                                        <a href="/index.php/user/reset">
                                            بازیابی رمز عبور
                                        </a>
                                    </p>


                               <?php echo form_close(); ?>

                            </div>

                            <div class="u-column2 col-md-6 col-sm-12 col-xs-12">
                                <div class="section-title clearfix">
                                    <h2>ثبت نام</h2>
                                </div>


                                <?php echo form_open('user/register',array('class'=>'register')); ?>
                                    <div class="loading" style="height: 100%;top: 0;">
                                        <div class="loader-show"></div>
                                    </div>

                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                        <label for="reg_email">آدرس ایمیل&nbsp;<span class="required">*</span></label>
                                        <input type="email" class="woocommerce-Input woocommerce-Input--text input-text"
                                               name="email" id="reg_email" autocomplete="email" required="" value="">
                                    </p>
                                    <p class="form-row form-row-wide">
                                        <label for="reg_billing_phone">نام کاربری<span class="required">*</span></label>
                                        <input type="text" class="input-text" name="username"
                                               id="username"  value="" required="">
                                    </p>
                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                        <label for="password">رمز عبور&nbsp;<span class="required">*</span></label>
                                        <span class="password-input"><input type="password"
                                                                            class="woocommerce-Input woocommerce-Input--text input-text"
                                                                            name="password" id="password"
                                                                            required=""></span>
                                    </p>

                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                        <label for="reg_password">تکرار رمز عبور&nbsp;<span class="required">*</span></label>
                                        <span class="password-input"><input type="password"
                                                                            class="woocommerce-Input woocommerce-Input--text input-text"
                                                                            name="conf_password" id="conf_password"
                                                                            required=""></span>
                                    </p>


                                    <div class="woocommerce-privacy-policy-text"></div>
                                    <p class="woocommerce-FormRow form-row">
                                        <button type="submit" class="woocommerce-Button button charsoogh_register"
                                                name="register" value="ثبت نام">ثبت نام
                                        </button>
                                    </p>
                                    <div class="final-register-result"></div>

                                <?php echo form_close(); ?>

                            </div>

                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>