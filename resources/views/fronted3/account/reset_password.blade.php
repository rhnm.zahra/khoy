<section class="block">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <article class="page-content clearfix">
                    <div class="woocommerce">
                        <div class="woocommerce-notices-wrapper"></div>
                        <div class="u-columns row" id="customer_login">

                            <div class="u-column1 col-md-6 col-sm-12 col-xs-12">

                                <div class="section-title clearfix">
                                    <h2>تغییر رمز عبور</h2>
                                </div>

                                <?php echo form_open(); ?>

                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                    <label for="password">رمز عبور جدید&nbsp;<span class="required">*</span></label>
                                    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text"
                                           name="password" id="password" required=""></p>

                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                    <label for="conf_password">تکرار رمز عبور جدید&nbsp;<span class="required">*</span></label>
                                    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text"
                                           name="conf_password" id="conf_password" required=""></p>


                                <p class="form-row">
                                    <button type="submit" class="woocommerce-Button button" name="login">تغییر رمز
                                    </button>
                                </p>


                                <?php echo form_close(); ?>

                            </div>


                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>