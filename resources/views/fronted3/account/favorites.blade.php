<section class="block" style="transform: none;">
    <div class="container" style="transform: none;">
        <ul class="row items ad-listing">
            <?php foreach ($items as $item) {
                $item->default_photo = $this->ps_adapter->get_default_photo($item->id, 'item');
                if ($item->default_photo->img_path == '')
                    $item->default_photo->img_path = 'no-product-image.png';
                ?>
                <li class="box-style-1 item col-lg-3 col-md-4 col-sm-12 product type-product post-1317 status-publish first instock product_cat-electronics product_cat-console-video-game-and-online has-post-thumbnail featured downloadable virtual purchasable product-type-simple">
                    <?php if ($item->feature == 1) { ?>
                        <div class="ribbon-featured">
                            <div class="ribbon-start"></div>
                            <div class="ribbon-content">آگهی ویژه</div>
                            <div class="ribbon-end">
                                <figure class="ribbon-shadow"></figure>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="wrapper">

                        <div class="image">
                            <h4 class="location">
                                <a href="">
                                    <?php echo $item->location_name; ?>
                                </a>
                            </h4>
                            <?php if ($item->is_sold_out == 1) { ?>
                                <h4 class="soldout">
                                    فروخته شد
                                </h4>
                            <?php } ?>
                            <a href="/index.php/v/<?php echo $item->id; ?>"
                               title="<?php echo $item->title; ?>" class="title">
                                <img width="300" height="300"
                                     src="/ssag/Melkekhoy/uploads/<?php echo $item->default_photo->img_path; ?>"
                                     class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                     alt="" loading="lazy"
                                     sizes="(max-width: 300px) 100vw, 300px"> </a>
                        </div>
                        <span class="price">
                                        <?php if ($item->price == 0) {
                                            echo $item->pricetype_name;
                                        } else {
                                            ?>
                                            <?php echo number_format($item->price); ?>
                                            تومان
                                        <?php } ?>
                                    </span>
                        <div class="meta">
                            <a href="/index.php/v/<?php echo $item->id; ?>"
                               title="<?php echo $item->title; ?>" class="title">
                                <h2><?php echo $item->title; ?></h2>
                            </a>
                            <figure>
                                <i class="fa fa-calendar-o"></i>
                                <?php
                                echo ago($item->added_date);
                                ?>
                            </figure>
                            <figure>
                                <i class="fa fa-folder-open-o"></i>
                                <?php echo $item->subname; ?>
                            </figure>
                        </div>
                        <!--end meta-->
                        <!--end description-->
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
</section>
