<?php
if ($userinfo->user_profile_photo == '')
    $userinfo->user_profile_photo = 'no-product-image.png';
?>
<section class="block" style="transform: none;">
    <div class="container" style="transform: none;">
        <div class="row">
            <div class="col-md-3" id="sidebar">
                <div class="theiaStickySidebar">
                    <nav class="profile-navigation">
                        <div class="top-nav-head">
                            <img src="/ssag/Melkekhoy/uploads/<?php echo $userinfo->user_profile_photo; ?>"
                                 alt="<?php echo $userinfo->user_name; ?>">
                            <h3><?php echo $userinfo->user_name; ?></h3>
                        </div>
                        <ul>
                            <li><strong><i class="fa fa-calendar" aria-hidden="true"></i> عضویت : </strong>
                                <?php echo jdate("Y/m/d", strtotime($userinfo->added_date)); ?>
                            </li>
                        </ul>
                        <div class="text-center mb-2">
                            <?php
                            $data = array('followed_user_id' => $userinfo->user_id, 'user_id' => $this->session->userdata('user_id'));
                            if ($this->Userfollow->exists($data)) {
                                ?>
                                <button class="btn btn-success follow">
                                    لغو دنبال کردن
                                </button>
                            <?php } else { ?>
                                <button class="btn btn-primary follow">
                                    دنبال کردن
                                </button>
                            <?php } ?>
                        </div>
                    </nav>
                </div>
            </div>
            <div class="col-md-9">
                <ul class="row items ad-listing">
                    <?php foreach ($items as $item) {
                        $item->default_photo = $this->ps_adapter->get_default_photo($item->id, 'item');
                        if ($item->default_photo->img_path == '')
                            $item->default_photo->img_path = 'no-product-image.png';
                        ?>
                        <li class="box-style-1 item col-lg-4 col-md-4 col-sm-12 product type-product post-1317 status-publish first instock product_cat-electronics product_cat-console-video-game-and-online has-post-thumbnail featured downloadable virtual purchasable product-type-simple">
                            <?php if ($item->feature == 1) { ?>
                                <div class="ribbon-featured">
                                    <div class="ribbon-start"></div>
                                    <div class="ribbon-content">آگهی ویژه</div>
                                    <div class="ribbon-end">
                                        <figure class="ribbon-shadow"></figure>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="wrapper">

                                <div class="image">
                                    <h4 class="location">
                                        <a href="">
                                            <?php echo $item->location_name; ?>
                                        </a>
                                    </h4>
                                    <?php if ($item->is_sold_out == 1) { ?>
                                        <h4 class="soldout">
                                            فروخته شد
                                        </h4>
                                    <?php } ?>
                                    <a href="/index.php/v/<?php echo $item->id; ?>"
                                       title="<?php echo $item->title; ?>" class="title">
                                        <img width="300" height="300"
                                             src="/ssag/Melkekhoy/uploads/<?php echo $item->default_photo->img_path; ?>"
                                             class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                             alt="" loading="lazy"
                                             sizes="(max-width: 300px) 100vw, 300px"> </a>
                                </div>
                                <span class="price">
                                        <?php if ($item->price == 0) {
                                            echo $item->pricetype_name;
                                        } else {
                                            ?>
                                            <?php echo number_format($item->price); ?>
                                            تومان
                                        <?php } ?>
                                    </span>
                                <div class="meta">
                                    <a href="/index.php/v/<?php echo $item->id; ?>"
                                       title="<?php echo $item->title; ?>" class="title">
                                        <h2><?php echo $item->title; ?></h2>
                                    </a>
                                    <figure>
                                        <i class="fa fa-calendar-o"></i>
                                        <?php
                                        echo ago($item->added_date);
                                        ?>
                                    </figure>
                                    <figure>
                                        <i class="fa fa-folder-open-o"></i>
                                        <?php echo $item->subname; ?>
                                    </figure>
                                </div>
                                <!--end meta-->
                                <!--end description-->
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</section>

<script>
    var ajax_follow = "/index.blade.php/ajax_follow/<?php echo $userinfo->user_id;?>";
    jQuery(function ($) {
        var data = {
            'ad_id': 1,
        };
        $('body').on('click', '.follow', function () {
            $.post(ajax_follow, data, function (response) {
                if (response == 1)
                    alert('لغو دنبال انجام شد');
                else if (response == 2)
                    alert('کاربر دنبال شد');
                else if (response == 3)
                    alert('برای دنبال کردن کاربران ابتدا وارد سایت شوید');
                else
                    alert('خطا');
                if (response != 3)
                    window.location.href = "<?php echo current_url(); ?>";
            });
        });
    });
</script>
