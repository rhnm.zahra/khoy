<section class="block" style="transform: none;">
    <div class="container" style="transform: none;">
        <ul class="row items ad-listing">
            <?php foreach ($items as $item) {
                $item->default_photo = $this->ps_adapter->get_default_photo($item->id, 'item');
                if ($item->default_photo->img_path == '')
                    $item->default_photo->img_path = 'no-product-image.png';
                ?>
                <li class="box-style-1 item col-lg-3 col-md-4 col-sm-12 product type-product post-1317 status-publish first instock product_cat-electronics product_cat-console-video-game-and-online has-post-thumbnail featured downloadable virtual purchasable product-type-simple">
                    <?php if ($item->feature == 1) { ?>
                        <div class="ribbon-featured">
                            <div class="ribbon-start"></div>
                            <div class="ribbon-content">آگهی ویژه</div>
                            <div class="ribbon-end">
                                <figure class="ribbon-shadow"></figure>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="wrapper">

                        <div class="image">
                            <h4 class="location">
                                <a href="">
                                    <?php echo $item->location_name; ?>
                                </a>
                            </h4>
                            <?php if ($item->is_sold_out == 1) { ?>
                                <h4 class="soldout">
                                    فروخته شد
                                </h4>
                            <?php } ?>
                            <a href="/index.php/v/<?php echo $item->id; ?>"
                               title="<?php echo $item->title; ?>" class="title">
                                <img width="300" height="300"
                                     src="/ssag/Melkekhoy/uploads/<?php echo $item->default_photo->img_path; ?>"
                                     class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                     alt="" loading="lazy"
                                     sizes="(max-width: 300px) 100vw, 300px"> </a>
                        </div>
                        <span class="price">
                                        <?php if ($item->price == 0) {
                                            echo $item->pricetype_name;
                                        } else {
                                            ?>
                                            <?php echo number_format($item->price); ?>
                                            تومان
                                        <?php } ?>
                                    </span>
                        <div class="meta">
                            <a href="/index.php/v/<?php echo $item->id; ?>"
                               title="<?php echo $item->title; ?>" class="title">
                                <h2><?php echo $item->title; ?></h2>
                            </a>
                            <figure>
                                <i class="fa fa-calendar-o"></i>
                                <?php
                                echo ago($item->added_date);
                                ?>
                            </figure>
                            <figure>
                                <i class="fa fa-folder-open-o"></i>
                                <?php echo $item->subname; ?>
                            </figure>
                        </div>
                        <!--end meta-->
                        <!--end description-->
                        <div style="padding: 4px;text-align: center;">
                            <?php if ($item->item_type_id==1) { ?>
                                <a style="width: 48%;" class="btn btn-gray mb-2" href="/index.php/user/editrequest/<?php echo $item->id;?>">
                                    <i class="fa fa-pencil"></i>
                                    ویرایش
                                </a>
                            <?php } else { ?>
                            <a style="width: 48%;" class="btn btn-gray mb-2" href="/index.php/user/edititem/<?php echo $item->id;?>">
                                <i class="fa fa-pencil"></i>
                                ویرایش
                            </a>
                            <?php } ?>
                            <a style="width: 48%;"  onclick="return confirm('مایل به حذف هستید ؟')" class="btn btn-gray mb-2  text-danger" href="/index.php/user/deleteitem/<?php echo $item->id;?>">
                                <i class="fa fa-remove text-danger"></i>
                                حذف
                            </a>
                            <a style="width: 48%;" class="btn btn-gray mb-2" href="/index.php/user/hideitem/<?php echo $item->id;?>">
                                <i class="fa fa-eye"></i>
                                <?php if ($item->item_hidden==0) { ?>
                                    <span class="" >
                                    مخفی کن
                                </span>
                                <?php } else { ?>
                                    <span class="text-center" >
لغو مخفی
                                </span>
                                <?php } ?>
                            </a>
                            <a style="width: 48%;"  class="btn btn-gray mb-2 text-center">
                                <?php if ($item->status==1) { ?>
                                    <span class="text-success" >
                                    منتشر شده
                                </span>
                                <?php } else { ?>
                                    <span class="text-warning text-center" >
در انتظار تایید
                                </span>
                                <?php } ?>
                            </a>
                        </div>
                        <?php if ($item->change_status==1) { ?>
                        <div style="padding: 4px;text-align: center;">
                            <a class="btn btn-info" style="width: 100%;"  href="/index.php/user/change/<?php echo $item->id;?>">
                                محاسبه معاوضه
                            </a>
                        </div>
                        <?php } ?>
                        <?php if ($item->item_type_id==1) { ?>
                        <div style="padding: 4px;text-align: center;">
                            <a class="btn btn-info" style="width: 100%;background-color: #81007f;border:none;" href="/index.php/user/request/<?php echo $item->id;?>">
                                جستجوی درخواست
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
</section>
