<style>
    .hero .hero-wrapper {
        padding-bottom: 3rem;
    }

    .hero .page-title {
        padding-top: 2rem;
    }

    #filter-aside {
        /*background-color: #eff0f2;*/
        padding-bottom: 20px;
        /*height: 30%;*/
        /*width:110%;*/

    }




      input[type="text"], input[type="email"], input[type="date"], input[type="time"], input[type="search"], input[type="password"], input[type="number"], input[type="url"], input[type="tel"], textarea.form-control, textarea,select {
        box-shadow: inset 0 0 1rem 0 rgb(253 253 253 / 100%);
        background: rgb(253 253 253 / 100%);
        border-radius: 12px 12px 12px 12px;




    }
       .bootstrap-select .btn.dropdown-toggle {
        box-shadow: inset 0 0 1rem 0 rgb(253 253 253 / 100%) !important;
        background: rgb(253 253 253 / 100%);
        border-radius: 12px 12px 12px 12px;

    }


        .noUi-connect {
        background: #0076ef !important;
    }
    .section-title h2:before {
        background: #800080;
    }
    .section-title h2:after {
        background: #800080;
    }
    .text-danger {
        color: #2a2a2a!important;
    }

    .button-blue {
  background-color: #0077f0;
  border: 15px;
  color: white;
  border-radius:12px;
  padding: 20px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
}

     .button-green {
  background-color: #558b2f;
  border: 15px;
  color: white;
  border-radius:12px;
  padding: 20px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
}


  .bk-sefid{
      background:#fdfdfd;
  }


/*img {*/
/*  border-radius: 15%;*/
/*}*/


</style>
<div class="row bk-sefid">
    <div style="display: none;" class="col-md-12" id="filterbtnbox">
        <button  id="filterbtn"
           class="btn btn-primary text-caps btn-rounded btn-framed">
            <i class="fa fa-search" aria-hidden="true"></i> جستجو کنید </button>
    </div>
    <div class="col-md-3" id="content">
        <div id="filter-aside" class="theiaStickySidebar">
            <div style="height: 600px;overflow-y: scroll;overflow-x:clip;">
                <form class="hero-form form woocommerce-product-search" role="search" method="get">
                    <div class="container">
                        <!--Main Form-->
                        <div class="main-search-form style-1">
                            <div class="form-row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <input name="name" type="text" class="form-control" id="name"
                                               placeholder="کلمه یا کد ملک را وارد کنید"
                                               value="<?php echo $_GET['name']; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <select name="city"
                                                id="city"
                                                class="postform">
                                            <option value="">تمام شهر‌ها</option>
                                            <?php foreach ($this->locations as $location) { ?>
                                                <option <?php if ($_GET['city'] == $location->id) { ?> selected="" <?php } ?>
                                                        class="level-0"
                                                        value="<?php echo $location->id; ?>"><?php echo $location->name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <select name="city_area"
                                                id="city_area"
                                                class="postform">
                                            <option value="">همه مناطق</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <select name="cat"
                                                id="cat"
                                                class="postform">
                                            <option value="" selected="selected">همه دسته‌بندی‌ها</option>
                                            <?php foreach ($this->subcategories as $subcat) { ?>
                                                <option <?php if ($_GET['cat'] == $subcat->id) { ?> selected="" <?php } ?>
                                                        class="level-0"
                                                        value="<?php echo $subcat->id; ?>"><?php echo $subcat->name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <select name="sort"
                                                id="sort"
                                                class="postform">
                                            <option <?php if ($_GET['sort'] == 'new') { ?> selected="" <?php } ?>
                                                    class="level-0"
                                                    value="new">
                                                جدیدترین ها
                                            </option>
                                            <option <?php if ($_GET['sort'] == 'favorite') { ?> selected="" <?php } ?>
                                                    class="level-0"
                                                    value="favorite">
                                                محبوبترین ها
                                            </option>
                                            <option <?php if ($_GET['sort'] == 'asc') { ?> selected="" <?php } ?>
                                                    class="level-0"
                                                    value="asc">
                                                بر اساس نام صعودی
                                            </option>
                                            <option <?php if ($_GET['sort'] == 'asc') { ?> selected="" <?php } ?>
                                                    class="level-0"
                                                    value="desc">
                                                بر اساس نام نزولی
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <select name="type"
                                                id="type"
                                                class="postform">
                                            <option value="">همه نوع آگهی</option>
                                            <?php foreach ($this->types as $t) {
                                                if ($t->id != 'itm_typeca43569d153f50979548a1f7d3d95b27') {
                                                    ?>
                                                    <option <?php if ($_GET['type'] == $t->id) { ?> selected="" <?php } ?>
                                                            class="level-0"
                                                            value="<?php echo $t->id; ?>"><?php echo $t->name; ?></option>
                                                <?php }
                                            } ?>
                                        </select>
                                    </div>
                                </div>


                                <div id="pricesearchbox" class="col-md-12">
                                    <div>
                                        <div class="text-danger text-center">
                                            قیمت از
                                            <span id="minprice_label"></span>
                                            تا
                                            <span id="maxprice_label"></span>

                                        </div>
                                        <div class="mt-3 mb-4" id="price_slider"></div>

                                        <input id="minprice" type="hidden" name="minprice"/>
                                        <input id="maxprice" type="hidden" name="maxprice"/>

                                        <script>
                                            var priceslider = document.getElementById('price_slider');

                                            <?php if($_GET['minprice']) { ?>
                                            var price_s =<?php echo $_GET['minprice'];?>;
                                            <?php } else{ ?>
                                            var price_s = 1000000;
                                            <?php } ?>
                                            <?php if($_GET['maxprice']) { ?>
                                            var price_e =<?php echo $_GET['maxprice'];?>;
                                            <?php } else{ ?>
                                            var price_e = 10000000000;
                                            <?php } ?>

                                            noUiSlider.create(priceslider, {
                                                start: [price_s, price_e],
                                                connect: true,
                                                tooltips: false,
                                                /*tooltips:
                                                    [wNumb({
                                                        decimals: 0,
                                                        thousand: ',',
                                                        prefix: 'از ',
                                                        suffix: ' تومان',
                                                    }), wNumb({
                                                        decimals: 0,
                                                        thousand: ',',
                                                        prefix: 'تا ',
                                                        suffix: ' تومان',
                                                    })],*/
                                                step: 1000000,
                                                direction: 'rtl',
                                                range: {
                                                    'min': 1000000,
                                                    'max': 10000000000
                                                }, format: wNumb({
                                                    decimals: 0,
                                                })
                                            });


                                            var price_start = document.getElementById('minprice');
                                            var price_end = document.getElementById('maxprice');
                                            var priceInputs = [price_start, price_end];
                                            var price_start_label = document.getElementById('minprice_label');
                                            var price_end_label = document.getElementById('maxprice_label');
                                            var priceInputs_label = [price_start_label, price_end_label];

                                            priceslider.noUiSlider.on('update', function (values, handle) {
                                                priceInputs[handle].value = values[handle];
                                                var moneyFormat = wNumb({
                                                    decimals: 0,
                                                    thousand: ','
                                                });
                                                if (parseInt(values[handle]) == 10000000000)
                                                    priceInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' تومان و بیشتر';
                                                else
                                                    priceInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' تومان';
                                            });

                                        </script>
                                    </div>
                                </div>

                                <div style="display: none;" class="rahnejarebox col-md-12">
                                    <div>
                                        <div class="text-danger text-center">
                                            رهن از
                                            <span id="minrahn_label"></span>
                                            تا
                                            <span id="maxrahn_label"></span>

                                        </div>
                                        <div class="mt-3 mb-4" id="rahn_slider"></div>

                                        <input id="minrahn" type="hidden" name="minrahn"/>
                                        <input id="maxrahn" type="hidden" name="maxrahn"/>

                                        <script>
                                            var rahnslider = document.getElementById('rahn_slider');

                                            <?php if($_GET['minrahn']) { ?>
                                            var rahn_s =<?php echo $_GET['minrahn'];?>;
                                            <?php } else{ ?>
                                            var rahn_s = 1000000;
                                            <?php } ?>
                                            <?php if($_GET['maxrahn']) { ?>
                                            var rahn_e =<?php echo $_GET['maxrahn'];?>;
                                            <?php } else{ ?>
                                            var rahn_e = 10000000000;
                                            <?php } ?>

                                            noUiSlider.create(rahnslider, {
                                                start: [rahn_s, rahn_e],
                                                connect: true,
                                                tooltips: false,
                                                /*tooltips:
                                                    [wNumb({
                                                        decimals: 0,
                                                        thousand: ',',
                                                        prefix: 'از ',
                                                        suffix: ' تومان',
                                                    }), wNumb({
                                                        decimals: 0,
                                                        thousand: ',',
                                                        prefix: 'تا ',
                                                        suffix: ' تومان',
                                                    })],*/
                                                step: 1000000,
                                                direction: 'rtl',
                                                range: {
                                                    'min': 1000000,
                                                    'max': 10000000000
                                                }, format: wNumb({
                                                    decimals: 0,
                                                })
                                            });


                                            var rahn_start = document.getElementById('minrahn');
                                            var rahn_end = document.getElementById('maxrahn');
                                            var rahnInputs = [rahn_start, rahn_end];
                                            var rahn_start_label = document.getElementById('minrahn_label');
                                            var rahn_end_label = document.getElementById('maxrahn_label');
                                            var rahnInputs_label = [rahn_start_label, rahn_end_label];

                                            rahnslider.noUiSlider.on('update', function (values, handle) {
                                                rahnInputs[handle].value = values[handle];
                                                var moneyFormat = wNumb({
                                                    decimals: 0,
                                                    thousand: ','
                                                });
                                                if (parseInt(values[handle]) == 10000000000)
                                                    rahnInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' تومان و بیشتر';
                                                else
                                                    rahnInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' تومان';
                                            });

                                        </script>
                                    </div>
                                </div>

                                <div style="display: none;" class="rahnejarebox col-md-12">
                                    <div>
                                        <div class="text-danger text-center">
                                            اجاره از
                                            <span id="minejare_label"></span>
                                            تا
                                            <span id="maxejare_label"></span>

                                        </div>
                                        <div class="mt-3 mb-4" id="ejare_slider"></div>

                                        <input id="minejare" type="hidden" name="minejare"/>
                                        <input id="maxejare" type="hidden" name="maxejare"/>

                                        <script>
                                            var ejareslider = document.getElementById('ejare_slider');

                                            <?php if($_GET['minejare']) { ?>
                                            var ejare_s =<?php echo $_GET['minejare'];?>;
                                            <?php } else{ ?>
                                            var ejare_s = 1000000;
                                            <?php } ?>
                                            <?php if($_GET['maxejare']) { ?>
                                            var ejare_e =<?php echo $_GET['maxejare'];?>;
                                            <?php } else{ ?>
                                            var ejare_e = 10000000000;
                                            <?php } ?>

                                            noUiSlider.create(ejareslider, {
                                                start: [ejare_s, ejare_e],
                                                connect: true,
                                                tooltips: false,
                                                /*tooltips:
                                                    [wNumb({
                                                        decimals: 0,
                                                        thousand: ',',
                                                        prefix: 'از ',
                                                        suffix: ' تومان',
                                                    }), wNumb({
                                                        decimals: 0,
                                                        thousand: ',',
                                                        prefix: 'تا ',
                                                        suffix: ' تومان',
                                                    })],*/
                                                step: 1000000,
                                                direction: 'rtl',
                                                range: {
                                                    'min': 1000000,
                                                    'max': 10000000000
                                                }, format: wNumb({
                                                    decimals: 0,
                                                })
                                            });


                                            var ejare_start = document.getElementById('minejare');
                                            var ejare_end = document.getElementById('maxejare');
                                            var ejareInputs = [ejare_start, ejare_end];
                                            var ejare_start_label = document.getElementById('minejare_label');
                                            var ejare_end_label = document.getElementById('maxejare_label');
                                            var ejareInputs_label = [ejare_start_label, ejare_end_label];

                                            ejareslider.noUiSlider.on('update', function (values, handle) {
                                                ejareInputs[handle].value = values[handle];
                                                var moneyFormat = wNumb({
                                                    decimals: 0,
                                                    thousand: ','
                                                });
                                                if (parseInt(values[handle]) == 10000000000)
                                                    ejareInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' تومان و بیشتر';
                                                else
                                                    ejareInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' تومان';
                                            });

                                        </script>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="form-group col-md-12">

                                        <div>
                                            <div class="text-danger text-center">
                                                متراژ زمین از
                                                <span id="minmetrazh_label"></span>
                                                تا
                                                <span id="maxmetrazh_label"></span>

                                            </div>
                                            <div class="mt-3" id="metrazh_slider"></div>

                                            <input id="metrazh_start" type="hidden" name="metrazh_start"/>
                                            <input id="metrazh_end" type="hidden" name="metrazh_end"/>

                                            <script>
                                                var metrazhslider = document.getElementById('metrazh_slider');

                                                <?php if($_GET['metrazh_start']) { ?>
                                                var metrazh_s =<?php echo $_GET['metrazh_start'];?>;
                                                <?php } else{ ?>
                                                var metrazh_s = 0;
                                                <?php } ?>
                                                <?php if($_GET['metrazh_end']) { ?>
                                                var metrazh_e =<?php echo $_GET['metrazh_end'];?>;
                                                <?php } else{ ?>
                                                var metrazh_e = 10000;
                                                <?php } ?>

                                                noUiSlider.create(metrazhslider, {
                                                    start: [metrazh_s, metrazh_e],
                                                    connect: true,
                                                    tooltips: false,
                                                    /*tooltips:
                                                        [wNumb({
                                                            decimals: 0,
                                                            prefix: 'از ',
                                                            suffix: ' متر',
                                                        }), wNumb({
                                                            decimals: 0,
                                                            prefix: 'تا ',
                                                            suffix: ' متر',
                                                        })],*/
                                                    step: 1,
                                                    direction: 'rtl',
                                                    range: {
                                                        'min': 0,
                                                        'max': 10000
                                                    }, format: wNumb({
                                                        decimals: 0,
                                                    })
                                                });


                                                var metrazh_start = document.getElementById('metrazh_start');
                                                var metrazh_end = document.getElementById('metrazh_end');
                                                var metrazhInputs = [metrazh_start, metrazh_end];
                                                var metrazh_start_label = document.getElementById('minmetrazh_label');
                                                var metrazh_end_label = document.getElementById('maxmetrazh_label');
                                                var metrazhInputs_label = [metrazh_start_label, metrazh_end_label];
                                                metrazhslider.noUiSlider.on('update', function (values, handle) {
                                                    metrazhInputs[handle].value = values[handle];
                                                    var moneyFormat = wNumb({
                                                        decimals: 0,
                                                        thousand: ','
                                                    });
                                                    if (parseInt(values[handle]) == 10000)
                                                        metrazhInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' متر و بیشتر';
                                                    else
                                                        metrazhInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' متر';
                                                });

                                            </script>
                                        </div>

                                    </div>
                                    <div class="form-group col-md-12">

                                        <div>
                                            <div class="text-danger text-center">
                                                متراژ زیربنا از
                                                <span id="minzirbana_label"></span>
                                                تا
                                                <span id="maxzirbana_label"></span>

                                            </div>
                                            <div class="mt-3" id="zirbana_slider"></div>

                                            <input id="zirbana_start" type="hidden" name="zirbana_start"/>
                                            <input id="zirbana_end" type="hidden" name="zirbana_end"/>

                                            <script>
                                                var zirbanaslider = document.getElementById('zirbana_slider');

                                                <?php if($_GET['zirbana_start']) { ?>
                                                var zirbana_s =<?php echo $_GET['zirbana_start'];?>;
                                                <?php } else{ ?>
                                                var zirbana_s = 0;
                                                <?php } ?>
                                                <?php if($_GET['zirbana_end']) { ?>
                                                var zirbana_e =<?php echo $_GET['zirbana_end'];?>;
                                                <?php } else{ ?>
                                                var zirbana_e = 500;
                                                <?php } ?>

                                                noUiSlider.create(zirbanaslider, {
                                                    start: [zirbana_s, zirbana_e],
                                                    connect: true,
                                                    tooltips: false,
                                                    /*tooltips:
                                                        [wNumb({
                                                            decimals: 0,
                                                            prefix: 'از ',
                                                            suffix: ' متر',
                                                        }), wNumb({
                                                            decimals: 0,
                                                            prefix: 'تا ',
                                                            suffix: ' متر',
                                                        })],*/
                                                    step: 1,
                                                    direction: 'rtl',
                                                    range: {
                                                        'min': 0,
                                                        'max': 500
                                                    }, format: wNumb({
                                                        decimals: 0,
                                                    })
                                                });


                                                var zirbana_start = document.getElementById('zirbana_start');
                                                var zirbana_end = document.getElementById('zirbana_end');
                                                var zirbanaInputs = [zirbana_start, zirbana_end];
                                                var zirbana_start_label = document.getElementById('minzirbana_label');
                                                var zirbana_end_label = document.getElementById('maxzirbana_label');
                                                var zirbanaInputs_label = [zirbana_start_label, zirbana_end_label];
                                                zirbanaslider.noUiSlider.on('update', function (values, handle) {
                                                    zirbanaInputs[handle].value = values[handle];
                                                    var moneyFormat = wNumb({
                                                        decimals: 0,
                                                        thousand: ','
                                                    });
                                                    if (parseInt(values[handle]) == 500)
                                                        zirbanaInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' متر و بیشتر';
                                                    else
                                                        zirbanaInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' متر';
                                                });


                                            </script>
                                        </div>

                                    </div>
                                    <div class="form-group col-md-12">

                                        <div>

                                            <div class="text-danger text-center">
                                                طول بر ملک از
                                                <span id="mintoolbar_label"></span>
                                                تا
                                                <span id="maxtoolbar_label"></span>

                                            </div>
                                            <div class="mt-3" id="toolbar_slider"></div>

                                            <input id="toolbar_start" type="hidden" name="toolbar_start"/>
                                            <input id="toolbar_end" type="hidden" name="toolbar_end"/>

                                            <script>
                                                var toolbarslider = document.getElementById('toolbar_slider');

                                                <?php if($_GET['toolbar_start']) { ?>
                                                var toolbar_s =<?php echo $_GET['toolbar_start'];?>;
                                                <?php } else{ ?>
                                                var toolbar_s = 0;
                                                <?php } ?>
                                                <?php if($_GET['toolbar_end']) { ?>
                                                var toolbar_e =<?php echo $_GET['toolbar_end'];?>;
                                                <?php } else{ ?>
                                                var toolbar_e = 500;
                                                <?php } ?>

                                                noUiSlider.create(toolbarslider, {
                                                    start: [toolbar_s, toolbar_e],
                                                    connect: true,
                                                    tooltips: false,
                                                    /*tooltips:
                                                        [wNumb({
                                                            decimals: 0,
                                                            prefix: 'از ',
                                                            suffix: ' متر',
                                                        }), wNumb({
                                                            decimals: 0,
                                                            prefix: 'تا ',
                                                            suffix: ' متر',
                                                        })],*/
                                                    step: 1,
                                                    direction: 'rtl',
                                                    range: {
                                                        'min': 0,
                                                        'max': 500
                                                    }, format: wNumb({
                                                        decimals: 0,
                                                    })
                                                });


                                                var toolbar_start = document.getElementById('toolbar_start');
                                                var toolbar_end = document.getElementById('toolbar_end');
                                                var toolbarInputs = [toolbar_start, toolbar_end];
                                                var toolbar_start_label = document.getElementById('mintoolbar_label');
                                                var toolbar_end_label = document.getElementById('maxtoolbar_label');
                                                var toolbarInputs_label = [toolbar_start_label, toolbar_end_label];
                                                toolbarslider.noUiSlider.on('update', function (values, handle) {
                                                    toolbarInputs[handle].value = values[handle];
                                                    var moneyFormat = wNumb({
                                                        decimals: 0,
                                                        thousand: ','
                                                    });
                                                    if (parseInt(values[handle]) == 500)
                                                        toolbarInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' متر و بیشتر';
                                                    else
                                                        toolbarInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' متر';
                                                });


                                            </script>
                                        </div>

                                    </div>
                                    <div class="form-group col-md-12">

                                        <div>

                                            <div class="text-danger text-center">
                                                متراژ کوچه / خیابان از
                                                <span id="minarz_label"></span>
                                                تا
                                                <span id="maxarz_label"></span>

                                            </div>
                                            <div class="mt-3" id="arz_slider"></div>

                                            <input id="arz_start" type="hidden" name="arz_start"/>
                                            <input id="arz_end" type="hidden" name="arz_end"/>

                                            <script>
                                                var arzslider = document.getElementById('arz_slider');

                                                <?php if($_GET['arz_start']) { ?>
                                                var arz_s =<?php echo $_GET['arz_start'];?>;
                                                <?php } else{ ?>
                                                var arz_s = 0;
                                                <?php } ?>
                                                <?php if($_GET['arz_end']) { ?>
                                                var arz_e =<?php echo $_GET['arz_end'];?>;
                                                <?php } else{ ?>
                                                var arz_e = 100;
                                                <?php } ?>

                                                noUiSlider.create(arzslider, {
                                                    start: [arz_s, arz_e],
                                                    connect: true,
                                                    tooltips: false,
                                                    /*tooltips:
                                                        [wNumb({
                                                            decimals: 0,
                                                            prefix: 'از ',
                                                            suffix: ' متر',
                                                        }), wNumb({
                                                            decimals: 0,
                                                            prefix: 'تا ',
                                                            suffix: ' متر',
                                                        })],*/
                                                    step: 1,
                                                    direction: 'rtl',
                                                    range: {
                                                        'min': 0,
                                                        'max': 100
                                                    }, format: wNumb({
                                                        decimals: 0,
                                                    })
                                                });


                                                var arz_start = document.getElementById('arz_start');
                                                var arz_end = document.getElementById('arz_end');
                                                var arzInputs = [arz_start, arz_end];
                                                var arz_start_label = document.getElementById('minarz_label');
                                                var arz_end_label = document.getElementById('maxarz_label');
                                                var arzInputs_label = [arz_start_label, arz_end_label];

                                                arzslider.noUiSlider.on('update', function (values, handle) {
                                                    arzInputs[handle].value = values[handle];
                                                    var moneyFormat = wNumb({
                                                        decimals: 0,
                                                        thousand: ','
                                                    });
                                                    if (parseInt(values[handle]) == 100)
                                                        arzInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' متر و بیشتر';
                                                    else
                                                        arzInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' متر';
                                                });


                                            </script>
                                        </div>

                                    </div>
                                </div>


                                <div style="display: none;" class="col-md-2 col-sm-2">
                                    <div class="form-group">
                                        <select name="ptype"
                                                id="ptype"
                                                class="postform">
                                            <option value="" selected="selected">همه نوع قیمت</option>
                                            <?php foreach ($this->ptypes as $pt) { ?>
                                                <option <?php if ($_GET['ptype'] == $pt->id) { ?> selected="" <?php } ?>
                                                        class="level-0"
                                                        value="<?php echo $pt->id; ?>"><?php echo $pt->name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>


                            </div>
                            <div class="row">
                                <div class="form-check col-md-12 mb-3 mt-3">


                                    <hr>

                                    <p>
                                        فقط آگهی های :
                                    </p>


                                    <table>
                                        <tr>
                                            <td><label>

                                                    <?php echo form_checkbox(array(
                                                        'name' => 'is_change',
                                                        'id' => 'is_change',
                                                        'value' => 'accept',
                                                        'checked' => set_checkbox('is_change', 1, ($_GET['is_change']) ? true : false),
                                                        'class' => 'form-check-input'
                                                    )); ?>

                                                    معاوضه

                                                </label></td>
                                            <td><label>

                                                    <?php echo form_checkbox(array(
                                                        'name' => 'amlak',
                                                        'id' => 'amlak',
                                                        'value' => 'accept',
                                                        'checked' => set_checkbox('amlak', 1, ($_GET['amlak']) ? true : false),
                                                        'class' => 'form-check-input'
                                                    )); ?>

                                                    املاک
                                                </label></td>
                                        </tr>
                                        <tr>
                                            <td><label>

                                                    <?php echo form_checkbox(array(
                                                        'name' => 'is_feature',
                                                        'id' => 'is_feature',
                                                        'value' => 'accept',
                                                        'checked' => set_checkbox('is_feature', 1, ($_GET['is_feature']) ? true : false),
                                                        'class' => 'form-check-input'
                                                    )); ?>

                                                    ویژه

                                                </label></td>
                                            <td><label>

                                                    <?php echo form_checkbox(array(
                                                        'name' => 'is_auction',
                                                        'id' => 'is_auction',
                                                        'value' => 'accept',
                                                        'checked' => set_checkbox('is_auction', 1, ($_GET['is_auction']) ? true : false),
                                                        'class' => 'form-check-input'
                                                    )); ?>

                                                    مزایده
                                                </label></td>
                                        </tr>
                                        <tr>
                                            <td><label>

                                                    <?php echo form_checkbox(array(
                                                        'name' => 'ax',
                                                        'id' => 'ax',
                                                        'value' => 'accept',
                                                        'checked' => set_checkbox('ax', 1, ($_GET['ax']) ? true : false),
                                                        'class' => 'form-check-input'
                                                    )); ?>

                                                    عکس دار

                                                </label></td>
                                            <td>
                                                <label>

                                                    <?php echo form_checkbox(array(
                                                        'name' => 'rahnchange',
                                                        'id' => 'rahnchange',
                                                        'value' => 'accept',
                                                        'checked' => set_checkbox('rahnchange', 1, ($_GET['rahnchange']) ? true : false),
                                                        'class' => 'form-check-input'
                                                    )); ?>

                                                    تبدیل رهن و اجاره

                                                </label></td>
                                        </tr>
                                    </table>


                                    <hr>

                                    <p>
                                        امکانات :
                                    </p>

                                    <table>
                                        <tr>
                                            <td><label>

                                                    <?php echo form_checkbox(array(
                                                        'name' => 'asansor',
                                                        'id' => 'asansor',
                                                        'value' => 'accept',
                                                        'checked' => set_checkbox('asansor', 1, ($_GET['asansor']) ? true : false),
                                                        'class' => 'form-check-input'
                                                    )); ?>

                                                    آسانسور

                                                </label></td>
                                            <td><label>

                                                    <?php echo form_checkbox(array(
                                                        'name' => 'parking',
                                                        'id' => 'parking',
                                                        'value' => 'accept',
                                                        'checked' => set_checkbox('parking', 1, ($_GET['parking']) ? true : false),
                                                        'class' => 'form-check-input'
                                                    )); ?>

                                                    پارکینگ

                                                </label></td>
                                        </tr>
                                        <tr>
                                            <td><label>

                                                    <?php echo form_checkbox(array(
                                                        'name' => 'anbari',
                                                        'id' => 'anbari',
                                                        'value' => 'accept',
                                                        'checked' => set_checkbox('anbari', 1, ($_GET['anbari']) ? true : false),
                                                        'class' => 'form-check-input'
                                                    )); ?>

                                                    انباری

                                                </label></td>
                                            <td><label>

                                                    <?php echo form_checkbox(array(
                                                        'name' => 'balkon',
                                                        'id' => 'balkon',
                                                        'value' => 'accept',
                                                        'checked' => set_checkbox('balkon', 1, ($_GET['balkon']) ? true : false),
                                                        'class' => 'form-check-input'
                                                    )); ?>

                                                    بالکن

                                                </label></td>
                                        </tr>
                                        <tr>
                                            <td><label>

                                                    <?php echo form_checkbox(array(
                                                        'name' => 'vam',
                                                        'id' => 'vam',
                                                        'value' => 'accept',
                                                        'checked' => set_checkbox('vam', 1, ($_GET['vam']) ? true : false),
                                                        'class' => 'form-check-input'
                                                    )); ?>

                                                    وام دارد

                                                </label></td>
                                            <td><label>

                                                    <?php echo form_checkbox(array(
                                                        'name' => 'bazsazi',
                                                        'id' => 'bazsazi',
                                                        'value' => 'accept',
                                                        'checked' => set_checkbox('bazsazi', 1, ($_GET['bazsazi']) ? true : false),
                                                        'class' => 'form-check-input'
                                                    )); ?>

                                                    بازسازی شده

                                                </label></td>
                                        </tr>
                                    </table>

                                    <hr>
                                </div>

                            </div>
                        </div>


                        <div id="advance_div" <?php if ($_GET['advance_search'] != 1) { ?> class="hide" <?php } ?>
                             style="margin-bottom: 20px;">


                            <p>
                                ویژگی ها :
                            </p>

                            <input type="hidden" value="<?php echo $_GET['advance_search']; ?>" name="advance_search"
                                   id="advance_search"/>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>
                                        سال ساخت
                                    </label>

                                    <?php
                                    $options = array();
                                    //                                    $options[''] = 'سال ساخت';
                                    $options += ITEM_YEAR;
                                    echo form_multiselect(
                                        'year[]',
                                        $options,
                                        set_value('year[]', show_data($_GET['year']), false),
                                        'class="form-control form-control-sm mr-3" title="انتخاب کنید" id="year"'
                                    );
                                    ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>
                                        تعداد اتاق
                                    </label>

                                    <?php
                                    $options = array();
                                    //                                    $options[''] = 'تعداد اتاق';
                                    $options += ITEM_ROOM;
                                    echo form_multiselect(
                                        'room[]',
                                        $options,
                                        set_value('room[]', show_data($_GET['room']), false),
                                        'class="form-control form-control-sm mr-3" title="انتخاب کنید" id="room"'
                                    );
                                    ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>
                                        نوع سند
                                    </label>

                                    <?php
                                    $options = array();
                                    //                                    $options[''] = 'نوع سند';
                                    $options += ITEM_SANAD;

                                    echo form_multiselect(
                                        'sanad[]',
                                        $options,
                                        set_value('sanad[]', show_data($_GET['sanad']), false),
                                        'class="form-control form-control-sm mr-3" title="انتخاب کنید" id="sanad"'
                                    );
                                    ?>
                                </div>


                                <div class="form-group col-md-6">
                                    <label>
                                        کاربری ملک
                                    </label>

                                    <?php
                                    $options = array();
                                    //                                    $options[''] = 'کاربری ملک';
                                    $options += ITEM_USE;

                                    echo form_multiselect(
                                        'usemelk[]',
                                        $options,
                                        set_value('usemelk[]', show_data($_GET['usemelk']), false),
                                        'class="form-control form-control-sm mr-3" title="انتخاب کنید" id="usemelk"'
                                    );
                                    ?>
                                </div>


                                <div class="form-group col-md-6">
                                    <label>
                                        تعداد واحد در هر طبقه
                                    </label>

                                    <?php
                                    $options = array();
                                    //                                    $options[''] = 'تعداد واحد در هر طبقه';
                                    $options += ITEM_UNIT;

                                    echo form_multiselect(
                                        'unit[]',
                                        $options,
                                        set_value('unit[]', show_data($_GET['unit']), false),
                                        'class="form-control form-control-sm mr-3" title="انتخاب کنید" id="unit"'
                                    );
                                    ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>
                                        تعداد کل طبقات
                                    </label>

                                    <?php
                                    $options = array();
                                    $options[''] = 'تعداد کل طبقات';
                                    $options += ITEM_FLOORCOUNT;

                                    echo form_dropdown(
                                        'floorcount',
                                        $options,
                                        set_value('floorcount', show_data($_GET['floorcount']), false),
                                        'class="form-control form-control-sm mr-3" title="انتخاب کنید" id="floorcount"'
                                    );
                                    ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>
                                        طبقه چندم
                                    </label>

                                    <?php
                                    $options = array();
                                    //                                    $options[''] = 'طبقه چندم';
                                    $options += ITEM_FLOOR;

                                    echo form_multiselect(
                                        'floor[]',
                                        $options,
                                        set_value('floor[]', show_data($_GET['floor']), false),
                                        'class="form-control form-control-sm mr-3" title="انتخاب کنید" id="floor"'
                                    );
                                    ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>
                                        جهت ساختمان
                                    </label>

                                    <?php
                                    $options = array();
                                    //                                    $options[''] = 'جهت ساختمان';
                                    $options += ITEM_DIRECTION;

                                    echo form_multiselect(
                                        'direction[]',
                                        $options,
                                        set_value('direction[]', show_data($_GET['direction'], true), false),
                                        'class="form-control form-control-sm mr-3" title="انتخاب کنید" title="انتخاب کنید" id="direction"'
                                    );
                                    ?>

                                </div>


                                <div class="form-group col-md-6">
                                    <p>
                                        ریز جزئیات :
                                    <hr>
                                    </p>

                                </div>


                                <div class="form-group col-md-6">
                                    <p>

                                    </p>

                                </div>


                                <div class="form-group col-md-6">
                                    <label>
                                        نوع سرویس بهداشتی
                                    </label>

                                    <?php
                                    $options = array();
                                    //                                    $options[''] = 'تعداد سرویس بهداشتی';
                                    $options += ITEM_WC;

                                    echo form_multiselect(
                                        'wc[]',
                                        $options,
                                        set_value('wc[]', show_data($_GET['wc']), false),
                                        'class="form-control form-control-sm mr-3" title="انتخاب کنید" id="wc"'
                                    );
                                    ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>
                                        تعداد حمام
                                    </label>

                                    <?php
                                    $options = array();
                                    //                                    $options[''] = 'تعداد حمام';
                                    $options += ITEM_BATHROOM;

                                    echo form_multiselect(
                                        'bathroom[]',
                                        $options,
                                        set_value('bathroom[]', show_data($_GET['bathroom']), false),
                                        'class="form-control form-control-sm mr-3" title="انتخاب کنید" id="bathroom"'
                                    );
                                    ?>
                                </div>

                                <div class="form-group col-md-6">
                                    <label>
                                        وضعیت تخلیه
                                    </label>

                                    <?php
                                    $options = array();
                                    //                                    $options[''] = 'وضعیت تخلیه';
                                    $options += ITEM_EMPTY;

                                    echo form_multiselect(
                                        'empty[]',
                                        $options,
                                        set_value('empty[]', show_data($_GET['empty']), false),
                                        'class="form-control form-control-sm mr-3" title="انتخاب کنید" id="empty"'
                                    );
                                    ?>
                                </div>


                                <div class="form-group col-md-6">
                                    <label>
                                        جنس کف
                                    </label>

                                    <?php
                                    $options = array();
                                    //                                    $options[''] = 'جنس کف';
                                    $options += ITEM_KAF;

                                    echo form_multiselect(
                                        'kaf[]',
                                        $options,
                                        set_value('kaf[]', show_data($_GET['kaf'], true), false),
                                        'class="form-control form-control-sm mr-3"  title="انتخاب کنید" id="kaf"'
                                    );
                                    ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>
                                        سرمایش گرمایشی
                                    </label>

                                    <?php
                                    $options = array();
                                    //                                    $options[''] = 'سرمایش گرمایشی';
                                    $options += ITEM_SARMAYESH;

                                    echo form_multiselect(
                                        'sarmayesh[]',
                                        $options,
                                        set_value('sarmayesh[]', show_data($_GET['sarmayesh'], true), false),
                                        'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="sarmayesh"'
                                    );
                                    ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>
                                        مشخصه ملک
                                    </label>

                                    <?php
                                    $options = array();
                                    //                                    $options[''] = 'مشخصه ملک';
                                    $options += ITEM_MOSHAKHASE;

                                    echo form_multiselect(
                                        'moshakhase[]',
                                        $options,
                                        set_value('moshakhase[]', show_data($_GET['moshakhase'], true), false),
                                        'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="moshakhase"'
                                    );
                                    ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>
                                        تامین کننده آب گرم
                                    </label>

                                    <?php
                                    $options = array();
                                    //                                    $options[''] = 'تامین کننده آب گرم';
                                    $options += ITEM_ABEGARM;

                                    echo form_multiselect(
                                        'abegarm[]',
                                        $options,
                                        set_value('abegarm[]', show_data($_GET['ashpazkhane'], true), false),
                                        'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="abegarm"'
                                    );
                                    ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>
                                        کابینت بندی
                                    </label>

                                    <?php
                                    $options = array();
                                    //                                    $options[''] = 'کابینت بندی';
                                    $options += ITEM_KABINET;

                                    echo form_multiselect(
                                        'kabinet[]',
                                        $options,
                                        set_value('kabinet[]', show_data($_GET['kabinet'], true), false),
                                        'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="kabinet"'
                                    );
                                    ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>
                                        جنس دیوارها
                                    </label>

                                    <?php
                                    $options = array();
                                    //                                    $options[''] = 'جنس دیوارها';
                                    $options += ITEM_DIVAR;

                                    echo form_multiselect(
                                        'divar[]',
                                        $options,
                                        set_value('divar[]', show_data($_GET['divar'], true), false),
                                        'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="divar"'
                                    );
                                    ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>
                                        نمای ساختمان
                                    </label>

                                    <?php
                                    $options = array();
                                    //                                    $options[''] = 'نمای ساختمان';
                                    $options += ITEM_NAMA;

                                    echo form_multiselect(
                                        'nama[]',
                                        $options,
                                        set_value('nama[]', show_data($_GET['nama'], true), false),
                                        'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="nama"'
                                    );
                                    ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>
                                        امکانات امنیتی
                                    </label>

                                    <?php
                                    $options = array();
                                    //                                    $options[''] = 'امکانات امنیتی';
                                    $options += ITEM_AMNIAT;

                                    echo form_multiselect(
                                        'amniat[]',
                                        $options,
                                        set_value('amniat[]', show_data($_GET['amniat'], true), false),
                                        'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="amniat"'
                                    );
                                    ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>
                                        امکانات رفاهی تفریحی
                                    </label>

                                    <?php
                                    $options = array();
                                    //                                    $options[''] = 'امکانات رفاهی تفریحی';
                                    $options += ITEM_REFAHI;

                                    echo form_multiselect(
                                        'refahi[]',
                                        $options,
                                        set_value('refahi[]', show_data($_GET['refahi'], true), false),
                                        'class="form-control form-control-sm mr-3"  title="انتخاب کنید" id="refahi"'
                                    );
                                    ?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>
                                        مشخصه آشپزخانه
                                    </label>

                                    <?php
                                    $options = array();
                                    //                                    $options[''] = 'مشخصه آشپزخانه';
                                    $options += ITEM_ASHPAZKHANE;

                                    echo form_multiselect(
                                        'ashpazkhane[]',
                                        $options,
                                        set_value('ashpazkhane[]', show_data($_GET['ashpazkhane'], true), false),
                                        'class="form-control form-control-sm mr-3"  title="انتخاب کنید" id="ashpazkhane"'
                                    );
                                    ?>
                                </div>

                            </div>


                        </div>
                        <div>
                            <div class="form-row">

                                <div class="col-md-6 col-sm-6">
                                    <button type="submit" class="button-blue btn-primary width-100">جستجو</button>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <button type="button" id="btn_advance" class="button-green btn-success width-100">پیشرفته
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div>
            <?php if (count($_GET) == 0) { ?>


                <section class="block featured-block">

                    <?php
                    $feature_items = $this->Front->get_feature_items();
                    ?>


                    <center>

                        <center>


                       <br/>
                       <br/>

        <div class="section-title clearfix">
                        <a style="display: block;margin-top: -75px;margin-bottom: 10px;"
                           href="http://www.melkekhoy.ir/"><img
                                    src="http://www.melkekhoy.ir/ssag/Melkekhoy/uploads/thumbnail/banner_up_main.jpg" border="0"
                                    title="ملک خوی" alt=" ملک خوی" width="1100" height="200"/></a>

                    </div>
                    
                    

 </center>
                        <div class="row">
                            <!--<div class="col-md-6">-->
                            <!--    <a target="_blank" rel="nofollow" href="">-->
                            <!--        <img style="width: 80%;" border="0"-->
                            <!--             src="http://www.melkekhoy.ir/ssag/Melkekhoy/uploads/thumbnail/new_baner01.jpg"-->
                            <!--             title="ملک خوی"-->
                            <!--             alt="خرید و فروش ، رهن و اجاره املاک ، سامانه تخصصی ملک خوی" vspace="1"></a>-->
                            <!--</div>-->
                            <!--<div class="col-md-6">-->
                            <!--    <a target="_blank" rel="nofollow" href="">-->
                            <!--        <img style="width: 80%;" border="0"-->
                            <!--             src="http://www.melkekhoy.ir/ssag/Melkekhoy/uploads/thumbnail/new_baner01.jpg"-->
                            <!--             title="ملک خوی"-->
                            <!--             alt="خرید و فروش ، رهن و اجاره املاک ، سامانه تخصصی ملک خوی" vspace="1"></a>-->
                            <!--</div>-->
                        </div>

                    </center>

                    <div class="container">
                        <div class="section-title clearfix">


                            <h2>آگهی های ویژه</h2>
                        </div>

                        <div class="show_featured">
                            <div id="featuredowl"
                                 class="row items featured-carousel owl-carousel owl-rtl owl-loaded owl-drag">
                                <?php foreach ($feature_items as $item) {
                                    $item->default_photo = $this->ps_adapter->get_default_photo($item->id, 'item');
                                    if ($item->default_photo->img_path == '')
                                        $item->default_photo->img_path = 'no-product-image.png'
                                    ?>
                                    <div class="item  featured-4row product type-product post-1317 status-publish first instock product_cat-electronics product_cat-console-video-game-and-online has-post-thumbnail featured downloadable virtual purchasable product-type-simple">
                                        <div class="ribbon-featured">
                                            <div class="ribbon-start"></div>
                                            <div class="ribbon-content">آگهی ویژه</div>
                                            <div class="ribbon-end">
                                                <figure class="ribbon-shadow"></figure>
                                            </div>
                                        </div>
                                        <div class="wrapper">
                                            <?php
                                            $data = array('item_id' => $item->id, 'user_id' => $this->session->userdata('user_id'));
                                            if ($this->Favourite->exists($data)) {
                                                $clfv = 'favorited-ad';
                                            } else {
                                                $clfv = '';
                                            }
                                            ?>
                                            <span id="fav_<?php echo $item->id; ?>" class="favorite_1313 bookmark favorite <?php echo $clfv;?>"
                                                  data-item="<?php echo $item->id; ?>" data-toggle="tooltip"
                                                  data-placement="top" title="" data-original-title="افزودن به علاقه‌مندی">افزودن به علاقه‌مندی</span>
                                            <span class="ad_visit"><?php echo $item->touch_count; ?> بازدید</span>
                                            <div class="image">
                                                <h4 class="location">
                                                    <a href="">
                                                        <?php echo $item->location_name; ?>
                                                        > <?php echo $item->area_name; ?>
                                                    </a>
                                                </h4>
                                                <?php if ($item->is_sold_out == 1) { ?>
                                                    <h4 class="soldout">
                                                        فروخته شد
                                                    </h4>
                                                <?php } ?>
                                                <a href="/index.php/v/<?php echo $item->id; ?>"
                                                   title="<?php echo $item->title; ?>" class="title">
                                                    <img width="300" height="300"
                                                         src="/ssag/Melkekhoy/uploads/<?php echo $item->default_photo->img_path; ?>"
                                                         class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                         alt="" loading="lazy"

                                                         sizes="(max-width: 300px) 100vw, 300px"> </a>
                                            </div>
                                            <span class="price">
                                        <?php if ($item->pricetypeid == 1) {
                                            echo 'توافقی';
                                        } else if($item->item_type_id==3){
                                            echo number_format($item->item_ejare);
                                        }
                                        else {
                                            ?>
                                            <?php echo number_format($item->price); ?>
                                            تومان
                                        <?php } ?>
                                    </span>
                                            <div class="meta">
                                                <a href="/index.php/v/<?php echo $item->id; ?>"
                                                   title="<?php echo $item->title; ?>" class="title">
                                                    <h2><?php echo $item->title; ?></h2>
                                                </a>
                                                <figure>
                                                    <i class="fa fa-calendar-o"></i>
                                                    <?php
                                                    echo ago($item->added_date);
                                                    ?>
                                                </figure>
                                                <figure>
                                                    <i class="fa fa-folder-open-o"></i>
                                                    <?php echo $item->subname; ?>
                                                </figure>
                                            </div>
                                            <!--end meta-->
                                            <!--end description-->
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </section>
            <?php } ?>
            <section class="block ad-list-block">
                <?php
                $items = $this->Front->get_items(12, 1, $this->input->get());
                ?>


                <div class="container">
                    <div class="section-title clearfix">
                        <a style="display: block;margin-top: -75px;margin-bottom: 10px;"
                           href="http://www.melkekhoy.ir/index.php/change/items"><img
                                    src="http://www.melkekhoy.ir/ssag/Melkekhoy/uploads/banner_moaveze.jpg" border="0"
                                    title="سیستم معاوضه ملک خوی" alt="معاوضه ملک خوی" width="1200" height="330"/></a>


                        <h2>آگهی ها</h2>
                    </div>


                    <ul class="row items ad-listing">
                        <?php foreach ($items as $item) {
                            $item->default_photo = $this->ps_adapter->get_default_photo($item->id, 'item');
                            if ($item->default_photo->img_path == '')
                                $item->default_photo->img_path = 'no-product-image.png';
                            ?>
                            <li class="box-style-1 item col-lg-3 col-md-3 col-sm-12 product type-product post-1317 status-publish first instock product_cat-electronics product_cat-console-video-game-and-online has-post-thumbnail featured downloadable virtual purchasable product-type-simple">
                                <?php
                                if ($item->change_status == 1) { ?>
                                    <div class="ribbon-featured rbch">
                                        <div class="ribbon-start"></div>
                                        <div class="ribbon-content">معاوضه</div>
                                        <div class="ribbon-end">
                                            <figure class="ribbon-shadow"></figure>
                                        </div>
                                    </div>

                                    <?php if ($item->feature == 1) { ?>
                                        <div class="ribbon-featured" style="top:4rem;">
                                            <div class="ribbon-start"></div>
                                            <div class="ribbon-content">آگهی ویژه</div>
                                            <div class="ribbon-end">
                                                <figure class="r styibbon-shadow"></figure>
                                            </div>
                                        </div>
                                    <?php } ?>


                                <?php } else { ?>
                                    <div class="ribbon-featured rbreq2">
                                        <div class="ribbon-start"></div>
                                        <div class="ribbon-content"><?php echo $item->type_name; ?></div>
                                        <div class="ribbon-end">
                                            <figure class="ribbon-shadow"></figure>
                                        </div>
                                    </div>

                                    <?php if ($item->feature == 1) { ?>
                                        <div class="ribbon-featured" style="top:4rem;">
                                            <div class="ribbon-start"></div>
                                            <div class="ribbon-content">آگهی ویژه</div>
                                            <div class="ribbon-end">
                                                <figure class="r styibbon-shadow"></figure>
                                            </div>
                                        </div>
                                    <?php } ?>

                                <?php }
                                ?>
                                <div class="wrapper">
                                    <?php
                                    $data = array('item_id' => $item->id, 'user_id' => $this->session->userdata('user_id'));
                                    if ($this->Favourite->exists($data)) {
                                        $clfv = 'favorited-ad';
                                    } else {
                                        $clfv = '';
                                    }
                                    ?>
                                    <span id="fav_<?php echo $item->id; ?>" class="favorite_1313 bookmark favorite <?php echo $clfv;?>"
                                          data-item="<?php echo $item->id; ?>" data-toggle="tooltip"
                                          data-placement="top" title="" data-original-title="افزودن به علاقه‌مندی">افزودن به علاقه‌مندی</span>
                                    <span class="ad_visit"><?php echo $item->touch_count; ?> بازدید</span>
                                    <div class="image">
                                        <h4 class="location">
                                            <a href="">
                                                <?php echo $item->location_name; ?> > <?php echo $item->area_name; ?>
                                            </a>
                                        </h4>
                                        <?php if ($item->is_sold_out == 1) { ?>
                                            <h4 class="soldout">
                                                فروخته شد
                                            </h4>
                                        <?php } ?>
                                        <a href="/index.php/v/<?php echo $item->id; ?>"
                                           title="<?php echo $item->title; ?>" class="title">
                                            <img width="300" height="300"
                                                 src="/ssag/Melkekhoy/uploads/<?php echo $item->default_photo->img_path; ?>"
                                                 class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                 alt="" loading="lazy"
                                                 sizes="(max-width: 300px) 100vw, 300px"> </a>
                                    </div>
                                    <span class="price">
                                        <?php if ($item->pricetypeid == 1) {
                                            echo 'توافقی';
                                        } else if($item->item_type_id==3){
                                            echo number_format($item->item_ejare);
                                        }
                                        else {
                                            ?>
                                            <?php echo number_format($item->price); ?>
                                            تومان
                                        <?php } ?>
                                    </span>
                                    <div class="meta">
                                        <a href="/index.php/v/<?php echo $item->id; ?>"
                                           title="<?php echo $item->title; ?>" class="title">
                                            <h2><?php echo $item->title; ?></h2>
                                        </a>
                                        <figure>
                                            <i class="fa fa-calendar-o"></i>
                                            <?php
                                            echo ago($item->added_date);
                                            ?>
                                        </figure>
                                        <figure>
                                            <i class="fa fa-folder-open-o"></i>
                                            <?php echo $item->subname; ?>
                                        </figure>
                                    </div>
                                    <!--end meta-->
                                    <!--end description-->
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                    <div class="center ad-load-more">
                        <div class="ad-loading">
                            <div class="ad-loader-show"></div>
                        </div>
                        <?php if (count($items) == 12) { ?>
                            <div class="btn btn-primary btn-framed btn-rounded loadmore">
                                آگهی بیشتر...
                            </div>
                        <?php } ?>
                    </div>
                </div>

            </section>
        </div>
    </div>
</div>


<script>
    var ajaxurl = "index.php/more_item_ajax";
    var page = 2;
    jQuery(function ($) {
        $('body').on('click', '.loadmore', function () {
            jQuery(".ad-loading").css("display", "block");
            jQuery(".loadmore").hide();
            var data = {
                'page': page,
                'meta': '<?php echo json_encode($_GET)?>',
            };

            $.post(ajaxurl, data, function (response) {
                $('.ad-listing').append('<span class="scroll-span"></span>');
                $('.ad-listing').append(response);
                var allSelects = document.getElementsByClassName("scroll-span");
                var lastSelect = allSelects[allSelects.length - 1];
                lastSelect.scrollIntoView(true);
                page++;
                $('[data-toggle="tooltip"]').tooltip();
                $(".ribbon-featured").each(function () {
                    var thisText = $(this).text();
                    $(this).html("");
                    $(this).append(
                        "<div class='ribbon-start'></div>" +
                        "<div class='ribbon-content'>" + thisText + "</div>" +
                        "<div class='ribbon-end'>" +
                        "<figure class='ribbon-shadow'></figure>" +
                        "</div>"
                    );
                });
                jQuery(".ad-loading").css("display", "none");
                jQuery(".loadmore").show();
            });
        });
    });


    jQuery('#city').on('change', function () {

        var value = jQuery('option:selected', this).text().replace(/Value\s/, '');

        var catId = jQuery(this).val();

        jQuery.ajax({
            url: '/index.php/user/get_areas/' + catId,
            method: 'GET',
            dataType: 'JSON',
            success: function (data) {
                jQuery('#city_area').html("");
                jQuery('#city_area').append('<option value="">همه مناطق</option>');
                jQuery.each(data, function (i, obj) {
                    jQuery('#city_area').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                });
                jQuery('#city_area').selectpicker('refresh');
            }
        });


    });

    jQuery('#type').on('change', function () {

        var tid = jQuery(this).val();

        if(tid==3)
        {
            jQuery('.rahnejarebox').show();
            jQuery('#pricesearchbox').hide();
        }else{
            jQuery('.rahnejarebox').hide();
            jQuery('#pricesearchbox').show();
        }

    });

    <?php if($_GET['type'] != '') { ?>
    var tid = jQuery('#type').val();
    if(tid==3)
    {
        jQuery('.rahnejarebox').show();
        jQuery('#pricesearchbox').hide();
    }else{
        jQuery('.rahnejarebox').hide();
        jQuery('#pricesearchbox').show();
    }
    <?php } ?>

    <?php if($_GET['city'] != '') { ?>

    var catId = '<?php echo $_GET['city'];?>';
    var areaId = '<?php echo $_GET['city_area'];?>';

    jQuery.ajax({
        url: '/index.php/user/get_areas/' + catId,
        method: 'GET',
        dataType: 'JSON',
        success: function (data) {
            jQuery('#city_area').html("");
            jQuery('#city_area').append('<option value="">همه مناطق</option>');
            jQuery.each(data, function (i, obj) {

                if (areaId == obj.id) {
                    var selected = 'selected=""';
                } else {
                    var selected = '';
                }

                jQuery('#city_area').append('<option ' + selected + ' value="' + obj.id + '">' + obj.name + '</option>');
            });
            jQuery('#city_area').selectpicker('refresh');
        }
    });

    <?php } ?>




    function separate(Number) {
        Number += '';
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        x = Number.split('.');
        y = x[0];
        z = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(y))
            y = y.replace(rgx, '$1' + ',' + '$2');
        return y + z;
    }

</script>
<script>
    jQuery(function ($) {
        var data = {
            'ad_id': jQuery(this).data('item'),
        };
        $('body').on('click', '.favorite', function () {
        var id=jQuery(this).data('item');
            var ajax_favourite = "/index.php/ajax_favourite/" + jQuery(this).data('item');
            $.post(ajax_favourite, data, function (response) {
                if (response == 1)
                {
                    alert('آگهی از علاقه مندی شما حذف شد');
                    jQuery('#fav_'+id).removeClass('favorited-ad');
                }
                else if (response == 2)
                {
                    alert('آگهی به علاقه مندی شما اضافه شد');
                    jQuery('#fav_'+id).addClass('favorited-ad');
                }
                else if (response == 3)
                    alert('برای افزودن آگهی به علاقه مندی ها ابتدا وارد سایت شوید');
                else
                    alert('خطا');
                // if (response != 3)
                    // jQuery(this).addClass('favorited-ad');
                    //window.location.href = "<?php //echo current_url(); ?>//";
            });
        });
    });
</script>