<section class="block ad-list-block">


    <style>

                    /*Title Search*/
          input[type="text"], input[type="email"], input[type="date"], input[type="time"], input[type="search"], input[type="password"], input[type="number"], input[type="url"], input[type="tel"], textarea.form-control, textarea,select  {
        box-shadow: inset 0 0 1rem 0 rgb(255 255 255 / 100%);
        background: rgb(255 255 255 / 100%);
        border-radius: 12px 12px 12px 12px;
        border: 1px solid grey ;
        font-size: 15px;


    }
                    .gray_img {
                        -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
                        filter: grayscale(100%);
                    }

    /*Other Boxs Under*/
       .bootstrap-select .btn.dropdown-toggle {
        box-shadow: inset 0 0 1rem 0 rgb(255 255 255 / 100%) !important;
        background: rgb(255 255 255 / 100%);
        border-radius: 12px 12px 12px 12px;
        border: 1px solid grey ;


}
    </style>

    <?php
    if ($_GET['price_avl'] != '' and $_GET['price_req'] != '') { ?>

        <p class="alert alert-danger">
            مقدر نقد درخواستی و نقد موجود همزمان نمیتواند مقدار داشته باشد
        </p>

    <?php } else {
        $items = $this->Front->get_change_items($this->input->get());
        $items2 = $items;
        ?>
        <div class="container">
            <div class="section-title clearfix">
                <h2>آگهی های معاوضه</h2>
            </div>
            <ul class="row items ad-listing">
                <?php foreach ($items as $item) {
                    $item->default_photo = $this->ps_adapter->get_default_photo($item->id, 'item');
                    if ($item->default_photo->img_path == '')
                        $item->default_photo->img_path = 'no-product-image.png';
                    ?>
                    <li class="box-style-1 item col-lg-3 col-md-4 col-sm-12 product type-product post-1317 status-publish first instock product_cat-electronics product_cat-console-video-game-and-online has-post-thumbnail featured downloadable virtual purchasable product-type-simple">
                        <?php if ($item->feature == 1) { ?>
                            <div class="ribbon-featured">
                                <div class="ribbon-start"></div>
                                <div class="ribbon-content">آگهی ویژه</div>
                                <div class="ribbon-end">
                                    <figure class="ribbon-shadow"></figure>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="wrapper">
                            <?php
                            $xc = $this->Front->check_user_touch($this->session->userdata('user_id'), $item->id);
                            ?>
                            <?php
                            $data = array('item_id' => $item->id, 'user_id' => $this->session->userdata('user_id'));
                            if ($this->Favourite->exists($data)) {
                                $clfv = 'favorited-ad';
                            } else {
                                $clfv = '';
                            }
                            ?>
                            <span id="fav_<?php echo $item->id; ?>" class="favorite_1313 bookmark favorite <?php echo $clfv;?>"
                                  data-item="<?php echo $item->id; ?>" data-toggle="tooltip"
                                  data-placement="top" title="" data-original-title="افزودن به علاقه‌مندی">افزودن به علاقه‌مندی</span>
                            <span class="ad_visit"><?php echo $item->touch_count; ?> بازدید</span>

                            <div class="image">
                                <h4 class="location">
                                    <a href="">
                                        <?php echo $item->location_name; ?>
                                        > <?php echo $item->area_name; ?>
                                    </a>
                                </h4>
                                <?php if ($item->is_sold_out == 1) { ?>
                                    <h4 class="soldout">
                                        فروخته شد
                                    </h4>
                                <?php } ?>
                                <a href="/index.php/v/<?php echo $item->id; ?>"
                                   title="<?php echo $item->title; ?>" class="title">
                                    <img width="300" height="300"
                                         src="/ssag/Melkekhoy/uploads/<?php echo $item->default_photo->img_path; ?>"
                                         class="<?php if(is_object($xc)){ ?> gray_img <?php } ?> attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                         alt="" loading="lazy"
                                         sizes="(max-width: 300px) 100vw, 300px"> </a>
                            </div>
                            <span class="price">
                                        <?php if ($item->pricetypeid == 1) {
                                            echo 'توافقی';
                                        } else if($item->item_type_id==3){
                                            echo number_format($item->item_ejare);
                                        } else {
                                            ?>
                                            <?php echo number_format($item->price); ?>
                                            تومان
                                        <?php } ?>
                                    </span>
                            <div class="meta">
                                <a href="/index.php/v/<?php echo $item->id; ?>"
                                   title="<?php echo $item->title; ?>" class="title">
                                    <h2><?php echo $item->title; ?></h2>
                                </a>
                                <figure>
                                    <i class="fa fa-calendar-o"></i>
                                    <?php
                                    echo ago($item->added_date);
                                    ?>
                                </figure>
                                <figure>
                                    <i class="fa fa-folder-open-o"></i>
                                    <?php echo $item->subname; ?>
                                </figure>
                            </div>
                            <!--end meta-->
                            <!--end description-->
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>

        <?php if ($_GET['price_avl'] != '' || $_GET['price_req'] != '') {
            $items = $this->Front->get_change_items3($this->input->get());
            $items=array_diff($items,$items2);
            ?>
            <div class="container">
                <div class="section-title clearfix">
                    <h2>آگهی های مشابه معاوضه</h2>
                </div>
                <ul class="row items ad-listing">
                    <?php foreach ($items as $item) {
                        $item->default_photo = $this->ps_adapter->get_default_photo($item->id, 'item');
                        if ($item->default_photo->img_path == '')
                            $item->default_photo->img_path = 'no-product-image.png';
                        ?>
                        <li class="box-style-1 item col-lg-3 col-md-4 col-sm-12 product type-product post-1317 status-publish first instock product_cat-electronics product_cat-console-video-game-and-online has-post-thumbnail featured downloadable virtual purchasable product-type-simple">
                            <?php if ($item->feature == 1) { ?>
                                <div class="ribbon-featured">
                                    <div class="ribbon-start"></div>
                                    <div class="ribbon-content">آگهی ویژه</div>
                                    <div class="ribbon-end">
                                        <figure class="ribbon-shadow"></figure>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="wrapper">
                                <?php
                                $xc = $this->Front->check_user_touch($this->session->userdata('user_id'), $item->id);
                                ?>
                                <?php
                                $data = array('item_id' => $item->id, 'user_id' => $this->session->userdata('user_id'));
                                if ($this->Favourite->exists($data)) {
                                    $clfv = 'favorited-ad';
                                } else {
                                    $clfv = '';
                                }
                                ?>
                                <span id="fav_<?php echo $item->id; ?>" class="favorite_1313 bookmark favorite <?php echo $clfv;?>"
                                      data-item="<?php echo $item->id; ?>" data-toggle="tooltip"
                                      data-placement="top" title="" data-original-title="افزودن به علاقه‌مندی">افزودن به علاقه‌مندی</span>
                                <span class="ad_visit"><?php echo $item->touch_count; ?> بازدید</span>

                                <div class="image">
                                    <h4 class="location">
                                        <a href="">
                                            <?php echo $item->location_name; ?>
                                            > <?php echo $item->area_name; ?>
                                        </a>
                                    </h4>
                                    <?php if ($item->is_sold_out == 1) { ?>
                                        <h4 class="soldout">
                                            فروخته شد
                                        </h4>
                                    <?php } ?>
                                    <a href="/index.php/v/<?php echo $item->id; ?>"
                                       title="<?php echo $item->title; ?>" class="title">
                                        <img width="300" height="300"
                                             src="/ssag/Melkekhoy/uploads/<?php echo $item->default_photo->img_path; ?>"
                                             class="<?php if(is_object($xc)){ ?> gray_img <?php } ?> attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                             alt="" loading="lazy"
                                             sizes="(max-width: 300px) 100vw, 300px"> </a>
                                </div>
                                <span class="price">
                                        <?php if ($item->pricetypeid == 1) {
                                            echo 'توافقی';
                                        } else if($item->item_type_id==3){
                                            echo number_format($item->item_ejare);
                                        } else {
                                            ?>
                                            <?php echo number_format($item->price); ?>
                                            تومان
                                        <?php } ?>
                                    </span>
                                <div class="meta">
                                    <a href="/index.php/v/<?php echo $item->id; ?>"
                                       title="<?php echo $item->title; ?>" class="title">
                                        <h2><?php echo $item->title; ?></h2>
                                    </a>
                                    <figure>
                                        <i class="fa fa-calendar-o"></i>
                                        <?php
                                        echo ago($item->added_date);
                                        ?>
                                    </figure>
                                    <figure>
                                        <i class="fa fa-folder-open-o"></i>
                                        <?php echo $item->subname; ?>
                                    </figure>
                                </div>
                                <!--end meta-->
                                <!--end description-->
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        <?php } ?>

    <?php } ?>
</section>

<script>


    <?php if($_GET['mycity'] != '') { ?>
    var mycatId = jQuery('#mycity').val();
    jQuery.ajax({
        url: '/index.php/user/get_areas/' + catId,
        method: 'GET',
        dataType: 'JSON',
        success: function (data) {
            jQuery('#myarea').html("");
            jQuery('#myarea').append('<option  value="">همه مناطق</option>');
            jQuery.each(data, function (i, obj) {
                jQuery('#myarea').append('<option  value="' + obj.id + '">' + obj.name + '</option>');
            });
            jQuery('#myarea').selectpicker('refresh');
        }
    });
    <?php } ?>
    <?php if($_GET['city'] != '') { ?>

    var catId = jQuery('#city').val();
    jQuery.ajax({
        url: '/index.php/user/get_areas/' + catId,
        method: 'GET',
        dataType: 'JSON',
        success: function (data) {
            jQuery('#area').html("");
            jQuery('#area').append('<option disabled="" value="">همه مناطق</option>');
            jQuery.each(data, function (i, obj) {
                jQuery('#area').append('<option  value="' + obj.id + '">' + obj.name + '</option>');
            });
            jQuery('#area').selectpicker('refresh');
        }
    });


    <?php } ?>

    jQuery('#city').on('change', function () {

        var value = jQuery('option:selected', this).text().replace(/Value\s/, '');

        var catId = jQuery(this).val();

        jQuery.ajax({
            url: '/index.php/user/get_areas/' + catId,
            method: 'GET',
            dataType: 'JSON',
            success: function (data) {
                jQuery('#area').html("");
                jQuery('#area').append('<option disabled="" value="">همه مناطق</option>');
                jQuery.each(data, function (i, obj) {
                    jQuery('#area').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                });
                jQuery('#area').selectpicker('refresh');
            }
        });


    });


    jQuery('#mycity').on('change', function () {

        var value = jQuery('option:selected', this).text().replace(/Value\s/, '');

        var catId = jQuery(this).val();

        jQuery.ajax({
            url: '/index.php/user/get_areas/' + catId,
            method: 'GET',
            dataType: 'JSON',
            success: function (data) {
                jQuery('#myarea').html("");
                jQuery('#myarea').append('<option  value="">همه مناطق</option>');
                jQuery.each(data, function (i, obj) {
                    jQuery('#myarea').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                });
                jQuery('#myarea').selectpicker('refresh');
            }
        });


    });

    jQuery(document).ready(function () {
        jQuery("#price_req").keyup(function () {
            jQuery("#price_avl").prop('disabled', true);
            jQuery("#price_avl").val('');
            if (jQuery("#price_req").val() == '') {
                jQuery("#price_avl").prop('disabled', false);
            }
        });
        jQuery("#price_avl").keyup(function () {
            jQuery("#price_req").prop('disabled', true);
            jQuery("#price_req").val('');
            if (jQuery("#price_avl").val() == '') {
                jQuery("#price_req").prop('disabled', false);
            }
        });
    });

    function separate(Number) {
        Number += '';
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        x = Number.split('.');
        y = x[0];
        z = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(y))
            y = y.replace(rgx, '$1' + ',' + '$2');
        return y + z;
    }

</script>
<script>
    jQuery(function ($) {
        var data = {
            'ad_id': jQuery(this).data('item'),
        };
        $('body').on('click', '.favorite', function () {
            var id=jQuery(this).data('item');
            var ajax_favourite = "/index.blade.php/ajax_favourite/" + jQuery(this).data('item');
            $.post(ajax_favourite, data, function (response) {
                if (response == 1)
                {
                    alert('آگهی از علاقه مندی شما حذف شد');
                    jQuery('#fav_'+id).removeClass('favorited-ad');
                }
                else if (response == 2)
                {
                    alert('آگهی به علاقه مندی شما اضافه شد');
                    jQuery('#fav_'+id).addClass('favorited-ad');
                }
                else if (response == 3)
                    alert('برای افزودن آگهی به علاقه مندی ها ابتدا وارد سایت شوید');
                else
                    alert('خطا');
                // if (response != 3)
                // jQuery(this).addClass('favorited-ad');
                //window.location.href = "<?php //echo current_url(); ?>//";
            });
        });
    });
</script>
