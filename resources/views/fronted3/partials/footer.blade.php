</main>
</div>
<footer class="footer" style="padding-bottom: 130px;">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="brand-sec">
                    <a href="/" class="brand">
                        <img src="/ssag/Melkekhoy/assets/front/images/logo.png" alt="">
                    </a>
                    <p>
                        <?php echo $this->about_app->about_title; ?>
                    </p>

                </div>
            </div>
            




            <!--end col-md-3-->
            <div class="col-md-4">
                <h2>تماس با ما</h2>
                
                <!-- Statsfa Website Analytics Start --><script data-host="https://statsfa.com" data-dnt="false" src="https://statsfa.com/js/script.js" id="ZwSg9rf6GA" async defer></script><!-- Statsfa Website Analytics End -->
                
                <script data-host="https://statsfa.com" data-dnt="false" src="https://statsfa.com/js/script.js" id="ZwSg9rf6GA" async defer></script>


                <address>
                    <p><strong><i class="fa fa-phone" aria-hidden="true"></i> شماره تماس: </strong>
                        <?php echo $this->about_app->about_phone; ?>
                    </p>
                    <p><strong><i class="fa fa-envelope" aria-hidden="true"></i> ایمیل: </strong>
                        <?php echo $this->about_app->about_email; ?>
                    </p>
                </address>
                <div class="row app_download">
                    <div class="app_badge">
                        <a href="#">
                            <img src="/ssag/Melkekhoy/assets/front/images/google-play-badge.png" alt="google play">
                        </a>
                    </div>
                    <div class="app_badge">
                        <a href="#">
                            <img src="/ssag/Melkekhoy/assets/front/images/bazaar.png" alt="bazaar">
                        </a>
                    </div>
                </div>
            </div>
            <!--end col-md-4-->
        </div>
        <!--end row-->
        <div class="row cp-footer">
            <div class="col-lg-6 copyright">
                <i class="fa fa-copyright" aria-hidden="true"></i>
                کلیه حقوق این وب سایت متعلق به ملک خوی می باشد ، کاری از شرکت پیشرو تجارت آتیه نوین داده
            </div>
            <div class="col-lg-6 social text-left">
                <ul>
                    <li><a href="<?php echo $this->about_app->pinterest; ?>" target="_blank"><i
                                    class="fa fa-paper-plane"
                                    aria-hidden="true"></i></a>
                    </li>
                    <li><a href="<?php echo $this->about_app->instagram; ?>" target="_blank"><i class="fa fa-instagram"
                                                                                                aria-hidden="true"></i></a>
                    </li>
                    <li><a href="<?php echo $this->about_app->facebook; ?>" target="_blank"><i
                                    class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                    <li><a href="<?php echo $this->about_app->twitter; ?>" target="_blank"><i
                                    class="fa fa-twitter-square"
                                    aria-hidden="true"></i></a>
                    </li>
                    <li><a href="<?php echo $this->about_app->youtube; ?>" target="_blank"><i class="fa fa-youtube"
                                                                                              aria-hidden="true"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="background">
        <div class="background-image original-size"
             style="background-image: url(https://melkekhoy.ir/demo/charsoogh/main/wp-content/themes/charsoogh/ssag/Melkekhoy/assets/img/footer-background-icons.jpg);">
            <img src="/ssag/Melkekhoy/assets/front/images/footer-background-icons.jpg" alt="">
        </div>
        <!--end background-image-->
    </div>
    <!--end background-->
</footer>


<div class="d-md-none d-lg-none" id="#menu_footer"
     style="display:flex;justify-content: space-evenly;padding: 10px 0px;background-color: #b30753; position: fixed;bottom: 0;width: 100%;z-index: 999;">

    <div class="text-center">
        <a href="/">
            <i class="text-white fa fa-list"></i>
            <br>
            <span class="text-white">
        لیست آگهی
        </a>
        </span>
    </div>
    <div class="text-center">
        <a href="/index.php/change/items">
            <i class="text-white fa fa-list"></i>
            <br>
            <span class="text-white">
        معاوضه
        </span>
        </a>
    </div>
    <div class="text-center">
        <a href="/index.php/user/additem">
            <i class="text-white fa fa-plus"></i>
            <br>
            <span class="text-white">
        ثبت آگهی
        </span>
        </a>
    </div>
    <div class="text-center">
        <a href="/index.php/user/items">
            <i class="text-white fa fa-user"></i>
            <br>
            <span class="text-white">
       حساب کاربری
        </span>
        </a>
    </div>

</div>
<div class="modal left fade" id="charsoogh_category_show" tabindex="-1" role="dialog"
     aria-labelledby="charsoogh_category_show">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">دسته‌بندی‌ها</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="vertical-menu">
                    <?php foreach ($this->categories as $catid => $category) { ?>
                        <li class="cat-item cat-item-106 dropdown-submenu">
                            <a class="dropdown-child"
                               title="<?php echo $category['name']; ?>"><?php echo $category['name']; ?></a>
                            <ul class="children">
                                <?php foreach ($category['sub'] as $subid => $subname) { ?>
                                    <li class="cat-item cat-item-107"><a
                                                href=""
                                                title="<?php echo $subname; ?>">
                                            <?php echo $subname; ?>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>

            </div>
        </div>
    </div>
</div>
<a href="javascript:" id="return-to-top" style="display: none;"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
<script>
    jQuery(document).ready(function ($) {
        // ===== Scroll to Top ====
        $(window).scroll(function () {
            if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
                $('#return-to-top').fadeIn(200);    // Fade in the arrow
            } else {
                $('#return-to-top').fadeOut(200);   // Else fade out the arrow
            }
        });
        $('#return-to-top').click(function () {      // When arrow is clicked
            $('body,html').animate({
                scrollTop: 0                       // Scroll to top of body
            }, 500);
        });
    });
    jQuery('#featuredowl').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        rtl: true,
        dots: false,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    })
    jQuery('#singleowl').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        rtl: true,
        dots: false,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    })

    jQuery('#btn_advance').click(function () {
        // jQuery('#advance_div').show();
        jQuery('#advance_div').toggleClass('hide');
        if (jQuery('#advance_div').hasClass('hide')) {
            jQuery('#advance_search').val(0);
        } else {
            jQuery('#advance_search').val(1);
        }
    });
    jQuery('#filterbtn').click(function () {
        if (jQuery('#filter-aside').css('display') == 'none') {
            jQuery('#filter-aside').show();
        } else {
            jQuery('#filter-aside').hide();
        }
    });

    /*jQuery('.range').ionRangeSlider({
        type: "double",
        min: 0,
        max: 1000,
        from: 100,
        to: 200,
        prefix: "متر"

    });*/


</script>
<script src="/ssag/Melkekhoy/assets/front/js/jquery.blockUI.min.js" id="jquery-blockui-js"></script>
<script src="/ssag/Melkekhoy/assets/front/js/cart-fragments.min.js" id="wc-cart-fragments-js"></script>
<script src="/ssag/Melkekhoy/assets/front/js/leaflet.js" id="leaf-script-js"></script>
<script src="/ssag/Melkekhoy/assets/front/js/leaflet-search.js" id="leaflet-search-js"></script>
<script src="/ssag/Melkekhoy/assets/front/js/leaflet-providers.js" id="leaflet-providers-js"></script>
<script src="/ssag/Melkekhoy/assets/front/js/Leaflet.fullscreen.min.js" id="Leaflet-fullscreen-js"></script>
<script src="/ssag/Melkekhoy/assets/front/dist/js/lightbox.js"></script>
<script src="/ssag/Melkekhoy/assets/front/js/sticky.js"></script>
<script>
    // var sticky = new Sticky('#filter-aside');


</script>

</body>

</html>