<?php
if ($userinfo->user_profile_photo == '')
    $userinfo->user_profile_photo = 'no-product-image.png';
?>
<section class="block">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-3">
                <a class="btn btn-primary" href="/index.php/user/followers">
                    <?php echo $userinfo->follower_count;?> دنبال کننده
                </a>
                <a class="btn btn-primary" href="/index.php/user/following">
                    <?php echo $userinfo->following_count;?> دنبال شونده
                </a>
            </div>
            <div class="col-md-12">
                <article class="page-content clearfix">
                    <div class="woocommerce">
                        <div class="woocommerce-notices-wrapper"></div>
                        <div class="u-columns row" id="customer_login">


                            <div class="u-column2 col-md-6 col-sm-12 col-xs-12">
                                <div class="section-title clearfix">
                                    <h2>ویرایش پروفایل</h2>
                                </div>


                                <?php echo form_open_multipart('user/profile',array('class'=>'register')); ?>
                                <div class="loading" style="height: 100%;top: 0;">
                                    <div class="loader-show"></div>
                                </div>
                                <nav class="profile-navigation">
                                    <div class="top-nav-head" style="margin-bottom: 0px;">
                                        <img src="/ssag/Melkekhoy/uploads/<?php echo $userinfo->user_profile_photo; ?>"
                                             alt="<?php echo $userinfo->user_name; ?>">
                                    </div>
                                </nav>
                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                    <label for="reg_email">تصویر پروفایل</label>
                                    <input  type="file" class="woocommerce-Input woocommerce-Input--text input-text"
                                           name="image" id="image" >
                                </p>

                                <p class="form-row form-row-wide">
                                    <label for="user_phone">تلفن همراه</label>
                                    <input type="text" class="input-text" name="user_phone" readonly=""
                                           id="user_phone"  value="<?php echo $userinfo->mobile;?>">
                                </p>

                                <!--<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                    <label for="reg_email">آدرس ایمیل&nbsp;</label>
                                    <input type="email" class="woocommerce-Input woocommerce-Input--text input-text"
                                           name="email" id="reg_email" value="<?php /*echo $userinfo->user_email;*/?>">
                                </p>-->
                                <p class="form-row form-row-wide">
                                    <label for="reg_billing_phone">نام کاربری<span class="required">*</span></label>
                                    <input type="text" class="input-text" name="username"
                                           id="username" required="" value="<?php echo $userinfo->user_name;?>">
                                </p>
                                <p class="form-row form-row-wide">
                                    <label for="user_about_me">درباره من</label>
                                    <textarea type="text" class="input-text" name="user_about_me"
                                           id="user_about_me"  ><?php echo $userinfo->user_about_me;?></textarea>
                                </p>


                                <div class="woocommerce-privacy-policy-text"></div>
                                <p class="woocommerce-FormRow form-row">
                                    <button type="submit" class="woocommerce-Button button charsoogh_register"
                                            name="register" value="ویرایش پروفایل">ویرایش پروفایل
                                    </button>
                                </p>
                                <div class="final-register-result"></div>

                                <?php echo form_close(); ?>

                            </div>

                            <div class="u-column1 col-md-6 col-sm-12 col-xs-12">

                                <div class="section-title clearfix">
                                    <h2>تغییر رمز عبور</h2>
                                </div>

                                <?php echo form_open('user/change_pass',array('class'=>'register')); ?>

                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                    <label for="password">رمز عبور&nbsp;<span class="required">*</span></label>
                                    <span class="password-input"><input type="password"
                                                                        class="woocommerce-Input woocommerce-Input--text input-text"
                                                                        name="password" id="password"
                                                                        required=""></span>
                                </p>

                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                    <label for="reg_password">تکرار رمز عبور&nbsp;<span class="required">*</span></label>
                                    <span class="password-input"><input type="password"
                                                                        class="woocommerce-Input woocommerce-Input--text input-text"
                                                                        name="conf_password" id="conf_password"
                                                                        required=""></span>
                                </p>

                                <p class="form-row">

                                    <button type="submit" class="woocommerce-Button button" name="login"
                                            value="تغییر رمز عبور">تغییر رمز عبور
                                    </button>
                                </p>


                                <?php echo form_close(); ?>

                            </div>


                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>