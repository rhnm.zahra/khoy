<section class="block">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <article class="page-content clearfix">
                    <div class="woocommerce">
                        <div class="woocommerce-notices-wrapper"></div>
                        <div class="u-columns row" id="customer_login">

                            <div class="u-column1 col-md-6 col-sm-12 col-xs-12">

                                <div class="section-title clearfix">
                                    <h2>بازیابی رمز عبور</h2>
                                </div>

                                <?php echo form_open(); ?>

                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                    <label for="email">ایمیل&nbsp;<span class="required">*</span></label>
                                    <input type="email" class="woocommerce-Input woocommerce-Input--text input-text"
                                           name="email" id="email" required="" autocomplete="email"
                                           value=""></p>


                                <p class="form-row">
                                    <button type="submit" class="woocommerce-Button button" name="login"
                                            value="ورود">بازیابی
                                    </button>
                                </p>


                                <?php echo form_close(); ?>

                            </div>


                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>