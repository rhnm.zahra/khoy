<section class="block" style="transform: none;">
    <div class="container" style="transform: none;">
        <ul class="row items ad-listing">
            <?php foreach ($items as $userinfo) {
                if ($userinfo->user_profile_photo == '')
                    $userinfo->user_profile_photo = 'no-product-image.png';
                ?>
                <div class="col-md-3" >
                    <nav class="profile-navigation">
                        <a href="/index.php/profile/<?php echo $userinfo->user_id; ?>">

                        <div class="top-nav-head">
                            <img src="/ssag/Melkekhoy/uploads/<?php echo $userinfo->user_profile_photo; ?>"
                                 alt="<?php echo $userinfo->user_name; ?>">
                            <h3><?php echo $userinfo->user_name; ?></h3>
                        </div>
                        <ul>
                            <li><strong><i class="fa fa-calendar" aria-hidden="true"></i> عضویت : </strong>
                                <?php echo jdate("Y/m/d", strtotime($userinfo->added_date)); ?>
                            </li>
                        </ul>

                        </a>
                    </nav>
                </div>
            <?php } ?>
        </ul>
    </div>
</section>
