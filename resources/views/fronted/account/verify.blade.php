<section class="block">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <article class="page-content clearfix">
                    <div class="woocommerce">
                        <div class="woocommerce-notices-wrapper"></div>
                        <div class="u-columns row" id="customer_login">

                            <div class="u-column1 col-md-6 offset-3 col-sm-12 col-xs-12">

                                <div class="section-title clearfix">
                                    <h2>کد ارسال شده به تلفن همراه خود را وارد نمایید</h2>
                                </div>

                                <?php echo form_open('user/verify',array('class'=>'register')); ?>

                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                    <label for="email">کد تایید&nbsp;<span class="required">*</span></label>
                                    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text"
                                           name="vcode" id="vcode" required=""
                                           value=""></p>



                                <p class="form-row">

                                    <button type="submit" class="woocommerce-Button button" name="login"
                                            value="اعتبار سنجی">اعتبار سنجی
                                    </button>
                                    <button style="display: none;" type="button" id="resend" class="woocommerce-Button button" name="login">
                                        ارسال مجدد کد
                                    </button>
                                    <span id="safeTimerDisplay" class="text-danger mt-2">
        2:00
    </span>
                                </p>


                                <?php echo form_close(); ?>

                            </div>


                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>
<script>
    function time_convert(num) {
        var minutes = Math.floor(num / 60);
        var seconds = num % 60;
        return minutes + ":" + seconds;
    }

    function timer() {
        jQuery('#safeTimerDisplay').html('2:00');
        var sec = 119;
        var timer = setInterval(function () {
            document.getElementById('safeTimerDisplay').innerHTML = time_convert(sec);
            sec--;
            if (sec < 0) {
                clearInterval(timer);
                jQuery('#resend').show();
                jQuery('#safeTimerDisplay').hide();

            }
        }, 1000);
    }
    timer();

    jQuery('#resend').click(function () {
        jQuery.post('/index.blade.php/home/set_sms_verify', function (data) {

        });
        jQuery('#resend').hide();
        jQuery('#safeTimerDisplay').show();
        timer();
    });

</script>
