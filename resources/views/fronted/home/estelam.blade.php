<section class="block">
    <div class="container">
        <div class="row">
            <div class="col-md-3 mb-2">
                <div class="card">

                    <div class="mb-4">

                    </div>
                    <h3 class="text-center">
                        <a href="https://estate.ssaa.ir/" target="_blank">
                            تصدیق اصالت سند
                        </a>
                    </h3>
                </div>
            </div>
            <div class="col-md-3 mb-2">
                <div class="card">

                    <div class="mb-4">

                    </div>
                    <h3 class="text-center">
                        <a href="https://confirm.ssaa.ir/" target="_blank">
                            تصدیق وکالت نامه
                        </a>
                    </h3>
                </div>
            </div>
            <div class="col-md-3 mb-2">
                <div class="card">

                    <div class="mb-4">

                    </div>
                    <h3 class="text-center">
                        <a href="https://epostcode.post.ir/codefinder" target="_blank">
                            تصدیق کد پستی
                        </a>
                    </h3>
                </div>
            </div>
            <div class="col-md-3 mb-2">
                <div class="card">

                    <div class="mb-4">

                    </div>
                    <h3 class="text-center">
                        <a href="https://www.cbi.ir/EstelamSayad/19689.aspx" target="_blank">
                            تصدیق چک صیادی
                        </a>
                    </h3>
                </div>
            </div>
            <div class="col-md-3 mb-2">
                <div class="card">

                    <div class="mb-4">

                    </div>
                    <h3 class="text-center">
                        <a href="https://confirm.ssaa.ir/" target="_blank">
                           سند و اوراق دفاتر اسناد
                        </a>
                    </h3>
                </div>
            </div>
            <div class="col-md-3 mb-2">
                <div class="card">

                    <div class="mb-4">

                    </div>
                    <h3 class="text-center">
                        <a href="https://exitban.ssaa.ir/" target="_blank">
                            ممنوع الخروجی اجرای اسناد
                        </a>
                    </h3>
                </div>
            </div>
            <div class="col-md-3 mb-2">
                <div class="card">

                    <div class="mb-4">

                    </div>
                    <h3 class="text-center">
                        <a href="https://inquirystatus.ssaa.ir/" target="_blank">
                            استعلام الکترونیکی ملک
                        </a>
                    </h3>
                </div>
            </div>
            <div class="col-md-3 mb-2">
                <div class="card">

                    <div class="mb-4">

                    </div>
                    <h3 class="text-center">
                        <a href="http://www.sabtemelk.ir/" target="_blank">
                            درخواست صدور سند
                        </a>
                    </h3>
                </div>
            </div>
            <div class="col-md-3 mb-2">
                <div class="card">

                    <div class="mb-4">

                    </div>
                    <h3 class="text-center">
                        <a href="https://notaryofficemaxdoc.ssaa.ir" target="_blank">
                         دفاتر ثبت اسناد
                        </a>
                    </h3>
                </div>
            </div>
            <div class="col-md-3 mb-2">
                <div class="card">

                    <div class="mb-4">

                    </div>
                    <h3 class="text-center">
                        <a href="https://ilenc.ssaa.ir/" target="_blank">
استعلام شناسه ملی
                        </a>
                    </h3>
                </div>
            </div>
            <div class="col-md-3 mb-2">
                <div class="card">

                    <div class="mb-4">

                    </div>
                    <h3 class="text-center">
                        <a href="https://www.dana-insurance.com/CheckNationalCode" target="_blank">
بررسی صحت کد ملی
                        </a>
                    </h3>
                </div>
            </div>
            <div class="col-md-3 mb-2">
                <div class="card">

                    <div class="mb-4">

                    </div>
                    <h3 class="text-center">
                        <a href="https://kitset.ir/other/search-national-city-number" target="_blank">
استعلام کد ملی شهرها
                        </a>
                    </h3>
                </div>
            </div>
        </div>
    </div>
</section>