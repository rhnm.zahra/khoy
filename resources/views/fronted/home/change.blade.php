<section class="block ad-list-block">


    <style>

                    /*Title Search*/
          input[type="text"], input[type="email"], input[type="date"], input[type="time"], input[type="search"], input[type="password"], input[type="number"], input[type="url"], input[type="tel"], textarea.form-control, textarea,select  {
        box-shadow: inset 0 0 1rem 0 rgb(255 255 255 / 100%);
        background: rgb(255 255 255 / 100%);
        border-radius: 12px 12px 12px 12px;
        border: 1px solid grey ;
        font-size: 15px;


    }
                    .gray_img {
                        -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
                        filter: grayscale(100%);
                    }

    /*Other Boxs Under*/
       .bootstrap-select .btn.dropdown-toggle {
        box-shadow: inset 0 0 1rem 0 rgb(255 255 255 / 100%) !important;
        background: rgb(255 255 255 / 100%);
        border-radius: 12px 12px 12px 12px;
        border: 1px solid grey ;


}
    </style>

    <?php
    if ($_GET['price_avl'] != '' and $_GET['price_req'] != '') { ?>

        <p class="alert alert-danger">
مقدار نقد درخواستی و نقد موجود همزمان نمیتواند مقدار داشته باشند
        </p>

    <?php } else {
        $items = $this->Front->get_change_items($this->input->get());
        $items2 = $items;
        ?>

        <center>
            <div  class="alert alert-warning">


        <h4 >
با ثبت آگهی فروش و انتخاب گزینه ی ( تمایل به معاوضه دارم ) میتوانید با سیستم بسیار پیشرفته معاوضه ملک خوی براحتی از بخش آگهی های من ملک مناسب جهت معاوضه با ملک خود را بدون محدودیت پیدا کنید .
        </h4>
          &#160;
            <center>

                                       <a href="http://www.melkekhoy.ir/index.php/user/additem" rel="nofollow" class="btn btn-outline-success and-btn ">
                    <i class="fa " aria-hidden="true"></i>
ثبت آگهی معاوضه
                     </a>
                     </div>
                     </center>

        </center>
        <div class="container">
            <div class="section-title clearfix">
                <h2>آگهی های معاوضه</h2>
            </div>
            <ul class="row items ad-listing">
                <?php foreach ($items as $item) {
                    $item->default_photo = $this->ps_adapter->get_default_photo($item->id, 'item');
                    if ($item->default_photo->img_path == '')
                        $item->default_photo->img_path = 'no-product-image.png';
                    ?>
                    <li class="box-style-1 item col-lg-3 col-md-3 col-sm-12 product type-product post-1317 status-publish first instock product_cat-electronics product_cat-console-video-game-and-online has-post-thumbnail featured downloadable virtual purchasable product-type-simple">
                        <div class="wrapper">
                            <?php
                            $data = array('item_id' => $item->id, 'user_id' => $this->session->userdata('user_id'));
                            if ($this->Favourite->exists($data)) {
                                $clfv = 'favorited-ad';
                            } else {
                                $clfv = '';
                            }
                            ?>
                            <?php if ($item->feature == 1) { ?>
                                <span class="ad_visit">ویژه</span>
                            <?php } ?>
                            <div class="image">
                                <a href="/index.php/v/<?php echo $item->id; ?>"
                                   title="<?php echo $item->title; ?>" class="title">
                                    <img width="300" height="300"
                                         src="/ssag/Melkekhoy/uploads/<?php echo $item->default_photo->img_path; ?>"
                                         class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                         alt="" loading="lazy"
                                         sizes="(max-width: 300px) 100vw, 300px"> </a>
                            </div>
                            <span class="metaitem">
                                              <div style="display: flex;justify-content: space-between;">
                                                  <span>
                                                      <?php echo $item->type_name; ?>  |  <?php echo $item->subname; ?>
                                                  </span>
                                                  <span>
                                                      <span class="item-bazdid">
                                                      <?php echo $item->touch_count; ?> بازدید
                                                      </span>
<span id="fav_<?php echo $item->id; ?>"
      class="favorite_1313 item-bookmark favorite <?php echo $clfv; ?>"
      data-item="<?php echo $item->id; ?>" data-toggle="tooltip"
      data-placement="top" title="" data-original-title="افزودن به علاقه‌مندی"></span>

                                                  </span>
                                              </div>
                                        </span>
                            <div class="meta">
                                <a href="/index.php/v/<?php echo $item->id; ?>"
                                   title="<?php echo $item->title; ?>" class="title">
                                    <h2><?php echo $item->title; ?></h2>
                                </a>
                                <div class="item-meta-price">
                                    <?php if ($item->pricetypeid == 1) {
                                        echo 'توافقی';
                                    } else if ($item->item_type_id == 3) {
                                        echo number_format($item->item_ejare);
                                    } else {
                                        ?>
                                        <?php echo number_format($item->price); ?>
                                        تومان
                                    <?php } ?>
                                </div>
                                <div class="flex-between" style="margin-bottom: 8px;">
                                    <figure>
                                        <i class="fa fa-calendar-o"></i>
                                        <?php
                                        echo ago($item->added_date);
                                        ?>
                                    </figure>
                                    <figure>
                                        <i class="fa fa-map-marker"></i>
                                        <?php echo $item->location_name; ?>
                                        > <?php echo $item->area_name; ?>
                                    </figure>
                                </div>
                                <div class="flex-between">
                                    <?php if ($item->metrazh and $item->zirbana) { ?>
                                        <figure>
                                            <img src="/ssag/Melkekhoy/assets/front/icons/area.png">
                                            <b>
                                                <?php echo $item->metrazh; ?>
                                            </b> متر عرصه
                                            |
                                            <b>
                                                <?php echo $item->zirbana; ?>
                                            </b> متر اعیان
                                        </figure>
                                    <?php } ?>
                                    <?php if ($item->room) { ?>
                                        <figure>
                                            <img src="/ssag/Melkekhoy/assets/front/icons/open-door.png">
                                            <b>
                                                <?php echo ITEM_ROOM[$item->room]; ?>
                                            </b>
                                            خوابه
                                        </figure>
                                    <?php } ?>
                                </div>

                            </div>
                            <!--end meta-->
                            <!--end description-->
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>

        <?php if ($_GET['price_avl'] != '' || $_GET['price_req'] != '') {
            $items = $this->Front->get_change_items3($this->input->get());
            $items=array_diff($items,$items2);
            ?>
            <div class="container">
                <div class="section-title clearfix">
                    <h2>آگهی های مشابه معاوضه</h2>
                </div>
                <ul class="row items ad-listing">
                    <?php foreach ($items as $item) {
                        $item->default_photo = $this->ps_adapter->get_default_photo($item->id, 'item');
                        if ($item->default_photo->img_path == '')
                            $item->default_photo->img_path = 'no-product-image.png';
                        ?>
                        <li class="box-style-1 item col-lg-3 col-md-3 col-sm-12 product type-product post-1317 status-publish first instock product_cat-electronics product_cat-console-video-game-and-online has-post-thumbnail featured downloadable virtual purchasable product-type-simple">
                            <div class="wrapper">
                                <?php
                                $data = array('item_id' => $item->id, 'user_id' => $this->session->userdata('user_id'));
                                if ($this->Favourite->exists($data)) {
                                    $clfv = 'favorited-ad';
                                } else {
                                    $clfv = '';
                                }
                                ?>
                                <?php if ($item->feature == 1) { ?>
                                    <span class="ad_visit">ویژه</span>
                                <?php } ?>
                                <div class="image">
                                    <a href="/index.php/v/<?php echo $item->id; ?>"
                                       title="<?php echo $item->title; ?>" class="title">
                                        <img width="300" height="300"
                                             src="/ssag/Melkekhoy/uploads/<?php echo $item->default_photo->img_path; ?>"
                                             class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                             alt="" loading="lazy"
                                             sizes="(max-width: 300px) 100vw, 300px"> </a>
                                </div>
                                <span class="metaitem">
                                              <div style="display: flex;justify-content: space-between;">
                                                  <span>
                                                      <?php echo $item->type_name; ?>  |  <?php echo $item->subname; ?>
                                                  </span>
                                                  <span>
                                                      <span class="item-bazdid">
                                                      <?php echo $item->touch_count; ?> بازدید
                                                      </span>
<span id="fav_<?php echo $item->id; ?>"
      class="favorite_1313 item-bookmark favorite <?php echo $clfv; ?>"
      data-item="<?php echo $item->id; ?>" data-toggle="tooltip"
      data-placement="top" title="" data-original-title="افزودن به علاقه‌مندی"></span>

                                                  </span>
                                              </div>
                                        </span>
                                <div class="meta">
                                    <a href="/index.php/v/<?php echo $item->id; ?>"
                                       title="<?php echo $item->title; ?>" class="title">
                                        <h2><?php echo $item->title; ?></h2>
                                    </a>
                                    <div class="item-meta-price">
                                        <?php if ($item->pricetypeid == 1) {
                                            echo 'توافقی';
                                        } else if ($item->item_type_id == 3) {
                                            echo number_format($item->item_ejare);
                                        } else {
                                            ?>
                                            <?php echo number_format($item->price); ?>
                                            تومان
                                        <?php } ?>
                                    </div>
                                    <div class="flex-between" style="margin-bottom: 8px;">
                                        <figure>
                                            <i class="fa fa-calendar-o"></i>
                                            <?php
                                            echo ago($item->added_date);
                                            ?>
                                        </figure>
                                        <figure>
                                            <i class="fa fa-map-marker"></i>
                                            <?php echo $item->location_name; ?>
                                            > <?php echo $item->area_name; ?>
                                        </figure>
                                    </div>
                                    <div class="flex-between">
                                        <?php if ($item->metrazh and $item->zirbana) { ?>
                                            <figure>
                                                <img src="/ssag/Melkekhoy/assets/front/icons/area.png">
                                                <b>
                                                    <?php echo $item->metrazh; ?>
                                                </b> متر عرصه
                                                |
                                                <b>
                                                    <?php echo $item->zirbana; ?>
                                                </b> متر اعیان
                                            </figure>
                                        <?php } ?>
                                        <?php if ($item->room) { ?>
                                            <figure>
                                                <img src="/ssag/Melkekhoy/assets/front/icons/open-door.png">
                                                <b>
                                                    <?php echo ITEM_ROOM[$item->room]; ?>
                                                </b>
                                                خوابه
                                            </figure>
                                        <?php } ?>
                                    </div>

                                </div>
                                <!--end meta-->
                                <!--end description-->
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        <?php } ?>

    <?php } ?>
</section>

<script>


    <?php if($_GET['mycity'] != '') { ?>
    var mycatId = jQuery('#mycity').val();
    jQuery.ajax({
        url: '/index.php/user/get_areas/' + catId,
        method: 'GET',
        dataType: 'JSON',
        success: function (data) {
            jQuery('#myarea').html("");
            jQuery('#myarea').append('<option  value="">همه مناطق</option>');
            jQuery.each(data, function (i, obj) {
                jQuery('#myarea').append('<option  value="' + obj.id + '">' + obj.name + '</option>');
            });
            jQuery('#myarea').selectpicker('refresh');
        }
    });
    <?php } ?>
    <?php if($_GET['city'] != '') { ?>

    var catId = jQuery('#city').val();
    jQuery.ajax({
        url: '/index.php/user/get_areas/' + catId,
        method: 'GET',
        dataType: 'JSON',
        success: function (data) {
            jQuery('#area').html("");
            jQuery('#area').append('<option disabled="" value="">همه مناطق</option>');
            jQuery.each(data, function (i, obj) {
                jQuery('#area').append('<option  value="' + obj.id + '">' + obj.name + '</option>');
            });
            jQuery('#area').selectpicker('refresh');
        }
    });


    <?php } ?>

    jQuery('#city').on('change', function () {

        var value = jQuery('option:selected', this).text().replace(/Value\s/, '');

        var catId = jQuery(this).val();

        jQuery.ajax({
            url: '/index.php/user/get_areas/' + catId,
            method: 'GET',
            dataType: 'JSON',
            success: function (data) {
                jQuery('#area').html("");
                jQuery('#area').append('<option disabled="" value="">همه مناطق</option>');
                jQuery.each(data, function (i, obj) {
                    jQuery('#area').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                });
                jQuery('#area').selectpicker('refresh');
            }
        });


    });


    jQuery('#mycity').on('change', function () {

        var value = jQuery('option:selected', this).text().replace(/Value\s/, '');

        var catId = jQuery(this).val();

        jQuery.ajax({
            url: '/index.php/user/get_areas/' + catId,
            method: 'GET',
            dataType: 'JSON',
            success: function (data) {
                jQuery('#myarea').html("");
                jQuery('#myarea').append('<option  value="">همه مناطق</option>');
                jQuery.each(data, function (i, obj) {
                    jQuery('#myarea').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                });
                jQuery('#myarea').selectpicker('refresh');
            }
        });


    });

    jQuery(document).ready(function () {
        jQuery("#price_req").keyup(function () {
            jQuery("#price_avl").prop('disabled', true);
            jQuery("#price_avl").val('');
            if (jQuery("#price_req").val() == '') {
                jQuery("#price_avl").prop('disabled', false);
            }
        });
        jQuery("#price_avl").keyup(function () {
            jQuery("#price_req").prop('disabled', true);
            jQuery("#price_req").val('');
            if (jQuery("#price_avl").val() == '') {
                jQuery("#price_req").prop('disabled', false);
            }
        });
    });

    function separate(Number) {
        Number += '';
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        x = Number.split('.');
        y = x[0];
        z = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(y))
            y = y.replace(rgx, '$1' + ',' + '$2');
        return y + z;
    }

</script>
<script>
    jQuery(function ($) {
        var data = {
            'ad_id': jQuery(this).data('item'),
        };
        $('body').on('click', '.favorite', function () {
            var id=jQuery(this).data('item');
            var ajax_favourite = "/index.blade.php/ajax_favourite/" + jQuery(this).data('item');
            $.post(ajax_favourite, data, function (response) {
                if (response == 1)
                {
                    alert('آگهی از علاقه مندی شما حذف شد');
                    jQuery('#fav_'+id).removeClass('favorited-ad');
                }
                else if (response == 2)
                {
                    alert('آگهی به علاقه مندی شما اضافه شد');
                    jQuery('#fav_'+id).addClass('favorited-ad');
                }
                else if (response == 3)
                    alert('برای افزودن آگهی به علاقه مندی ها ابتدا وارد سایت شوید');
                else
                    alert('خطا');
                // if (response != 3)
                // jQuery(this).addClass('favorited-ad');
                //window.location.href = "<?php //echo current_url(); ?>//";
            });
        });
    });
</script>
