<section class="block">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <?php if (validation_errors()): ?>
                    <div class="alert alert-danger mb-2">
                        <?php echo validation_errors(); ?>
                    </div>
                <?php endif ?>
                <article class="page-content clearfix justify-content">
                    <?php  echo form_open('contact', array('role'=>'form')); ?>
                        <p class="form-row form-row-wide">
                            <label for="name">نام&nbsp;<span class="required">*</span></label>
                            <input type="text" class="input-text" name="name" id="name" />
                        </p>
                        <p class="form-row form-row-wide">
                            <label for="email">آدرس ایمیل&nbsp;<span class="required">*</span></label>
                            <input type="email" class="input-text" name="email" id="email" />
                        </p>
                        <p class="form-row form-row-wide">
                            <label for="phone">شماره تماس&nbsp;</label>
                            <input type="text" class="input-text" name="phone" id="phone"/>
                        </p>
                        <p class="form-row form-row-wide">
                            <label for="text">پیام شما&nbsp;<span class="required">*</span></label>
                            <textarea class="input-text" name="text" id="text"></textarea>
                        </p>
                        <p class="form-row form-row-wide">
                            <input type="submit" class="btn btn-success" value="ارسال">
                        </p>
                    <?php echo form_close(); ?>
                </article>
            </div>
        </div>
    </div>
</section>