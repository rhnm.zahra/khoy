<style>
    .hero .hero-wrapper {
        padding-bottom: 3rem;
    }

    .hero .page-title {
        padding-top: 2rem;
    }

    #filter-aside {
        /*background-color: #eff0f2;*/
        padding-bottom: 20px;
        /*height: 30%;*/
        /*width:110%;*/

    }


    input[type="text"], input[type="email"], input[type="date"], input[type="time"], input[type="search"], input[type="password"], input[type="number"], input[type="url"], input[type="tel"], textarea.form-control, textarea, select {
        box-shadow: inset 0 0 1rem 0 rgb(253 253 253 / 100%);
        background: rgb(253 253 253 / 100%);
        border-radius: 12px 12px 12px 12px;


    }

    .bootstrap-select .btn.dropdown-toggle {
        box-shadow: inset 0 0 1rem 0 rgb(253 253 253 / 100%) !important;
        background: rgb(253 253 253 / 100%);
        border-radius: 12px 12px 12px 12px;

    }


    .noUi-connect {
        background: #0076ef !important;
    }

    /*.section-title h2:before {
        background: #800080;
    }

    .section-title h2:after {
        background: #800080;
    }*/

    .text-danger {
        color: #2a2a2a !important;
    }

    .button-blue {
        background-color: #0077f0;
        border: 15px;
        color: white;
        border-radius: 12px;
        padding: 20px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
    }

    .button-green {
        background-color: #558b2f;
        border: 15px;
        color: white;
        border-radius: 12px;
        padding: 20px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
    }


    .bk-sefid {
        background: #fdfdfd;
    }


    /*img {*/
    /*  border-radius: 15%;*/
    /*}*/


</style>
<div id="over_black"></div>
<div id="filter-aside" class="filter-aside2 theiaStickySidebar">
    <div id="filter-aside-box" >
        <form action="/index.php/auction/items" class="hero-form form woocommerce-product-search" role="search" method="get">

            <div class="container">

                <button type="button" class="btn-close" id="btn-filter-close">X</button>

                       &#160;
                <center>


                     <a class="navbar-brand" href="/">
                            <img src="/ssag/Melkekhoy/assets/front/images/logo.png" alt="">
                        </a>
                        </center>


                <!--Main Form-->
                <div class="main-search-form style-1">
                    <div class="form-row">
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group">
                                <input name="name" type="text" class="form-control" id="name"
                                       placeholder="کلمه یا کد ملک را وارد کنید"
                                       value="<?php echo $_GET['name']; ?>">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <select name="city"
                                        id="city"
                                        class="postform">
                                    <option value="">تمام شهر‌ها</option>
                                    <?php foreach ($this->locations as $location) { ?>
                                        <option <?php if ($_GET['city'] == $location->id) { ?> selected="" <?php } ?>
                                                class="level-0"
                                                value="<?php echo $location->id; ?>"><?php echo $location->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <select name="city_area"
                                        id="city_area"
                                        class="postform">
                                    <option value="">همه مناطق</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <select name="cat"
                                        id="cat"
                                        class="postform">
                                    <option value="" selected="selected">همه دسته‌بندی‌ها</option>
                                    <?php foreach ($this->subcategories as $subcat) { ?>
                                        <option <?php if ($_GET['cat'] == $subcat->id) { ?> selected="" <?php } ?>
                                                class="level-0"
                                                value="<?php echo $subcat->id; ?>"><?php echo $subcat->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <select name="sort"
                                        id="sort"
                                        class="postform">
                                    <option <?php if ($_GET['sort'] == 'new') { ?> selected="" <?php } ?>
                                            class="level-0"
                                            value="new">
                                        جدیدترین ها
                                    </option>
                                    <option <?php if ($_GET['sort'] == 'favorite') { ?> selected="" <?php } ?>
                                            class="level-0"
                                            value="favorite">
                                        محبوبترین ها
                                    </option>
                                    <option <?php if ($_GET['sort'] == 'asc') { ?> selected="" <?php } ?>
                                            class="level-0"
                                            value="asc">
                                        بر اساس نام صعودی
                                    </option>
                                    <option <?php if ($_GET['sort'] == 'asc') { ?> selected="" <?php } ?>
                                            class="level-0"
                                            value="desc">
                                        بر اساس نام نزولی
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div style="display: none;" class="col-md-12 col-sm-12">
                            <div class="form-group">
                                <select name="type"
                                        id="type"
                                        class="postform">
                                    <option value="">همه نوع آگهی</option>
                                    <?php foreach ($this->types as $t) {
                                        if ($t->id != 'itm_typeca43569d153f50979548a1f7d3d95b27') {
                                            ?>
                                            <option <?php if ($_GET['type'] == $t->id) { ?> selected="" <?php } ?>
                                                    class="level-0"
                                                    value="<?php echo $t->id; ?>"><?php echo $t->name; ?></option>
                                        <?php }
                                    } ?>
                                </select>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div>
                                <div class="text-danger text-center">
                                    قیمت از
                                    <span id="minprice_label"></span>
                                    تا
                                    <span id="maxprice_label"></span>

                                </div>
                                <div class="mt-3 mb-4" id="price_slider"></div>

                                <input id="minprice" type="hidden" name="minprice"/>
                                <input id="maxprice" type="hidden" name="maxprice"/>

                                <script>
                                    var priceslider = document.getElementById('price_slider');

                                    <?php if($_GET['minprice']) { ?>
                                    var price_s =<?php echo $_GET['minprice'];?>;
                                    <?php } else{ ?>
                                    var price_s = 1000000;
                                    <?php } ?>
                                    <?php if($_GET['maxprice']) { ?>
                                    var price_e =<?php echo $_GET['maxprice'];?>;
                                    <?php } else{ ?>
                                    var price_e = 10000000000;
                                    <?php } ?>

                                    noUiSlider.create(priceslider, {
                                        start: [price_s, price_e],
                                        connect: true,
                                        tooltips: false,
                                        /*tooltips:
                                            [wNumb({
                                                decimals: 0,
                                                thousand: ',',
                                                prefix: 'از ',
                                                suffix: ' تومان',
                                            }), wNumb({
                                                decimals: 0,
                                                thousand: ',',
                                                prefix: 'تا ',
                                                suffix: ' تومان',
                                            })],*/
                                        step: 1000000,
                                        direction: 'rtl',
                                        range: {
                                            'min': 1000000,
                                            'max': 10000000000
                                        }, format: wNumb({
                                            decimals: 0,
                                        })
                                    });


                                    var price_start = document.getElementById('minprice');
                                    var price_end = document.getElementById('maxprice');
                                    var priceInputs = [price_start, price_end];
                                    var price_start_label = document.getElementById('minprice_label');
                                    var price_end_label = document.getElementById('maxprice_label');
                                    var priceInputs_label = [price_start_label, price_end_label];

                                    priceslider.noUiSlider.on('update', function (values, handle) {
                                        priceInputs[handle].value = values[handle];
                                        var moneyFormat = wNumb({
                                            decimals: 0,
                                            thousand: ','
                                        });
                                        if (parseInt(values[handle]) == 10000000000)
                                            priceInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' تومان و بیشتر';
                                        else
                                            priceInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' تومان';
                                    });

                                </script>
                            </div>
                        </div>


                        <div class="row">
                            <div class="form-group col-md-12">

                                <div>
                                    <div class="text-danger text-center">
                                        متراژ زمین از
                                        <span id="minmetrazh_label"></span>
                                        تا
                                        <span id="maxmetrazh_label"></span>

                                    </div>
                                    <div class="mt-3" id="metrazh_slider"></div>

                                    <input id="metrazh_start" type="hidden" name="metrazh_start"/>
                                    <input id="metrazh_end" type="hidden" name="metrazh_end"/>

                                    <script>
                                        var metrazhslider = document.getElementById('metrazh_slider');

                                        <?php if($_GET['metrazh_start']) { ?>
                                        var metrazh_s =<?php echo $_GET['metrazh_start'];?>;
                                        <?php } else{ ?>
                                        var metrazh_s = 0;
                                        <?php } ?>
                                        <?php if($_GET['metrazh_end']) { ?>
                                        var metrazh_e =<?php echo $_GET['metrazh_end'];?>;
                                        <?php } else{ ?>
                                        var metrazh_e = 10000;
                                        <?php } ?>

                                        noUiSlider.create(metrazhslider, {
                                            start: [metrazh_s, metrazh_e],
                                            connect: true,
                                            tooltips: false,
                                            /*tooltips:
                                                [wNumb({
                                                    decimals: 0,
                                                    prefix: 'از ',
                                                    suffix: ' متر',
                                                }), wNumb({
                                                    decimals: 0,
                                                    prefix: 'تا ',
                                                    suffix: ' متر',
                                                })],*/
                                            step: 1,
                                            direction: 'rtl',
                                            range: {
                                                'min': 0,
                                                'max': 10000
                                            }, format: wNumb({
                                                decimals: 0,
                                            })
                                        });


                                        var metrazh_start = document.getElementById('metrazh_start');
                                        var metrazh_end = document.getElementById('metrazh_end');
                                        var metrazhInputs = [metrazh_start, metrazh_end];
                                        var metrazh_start_label = document.getElementById('minmetrazh_label');
                                        var metrazh_end_label = document.getElementById('maxmetrazh_label');
                                        var metrazhInputs_label = [metrazh_start_label, metrazh_end_label];
                                        metrazhslider.noUiSlider.on('update', function (values, handle) {
                                            metrazhInputs[handle].value = values[handle];
                                            var moneyFormat = wNumb({
                                                decimals: 0,
                                                thousand: ','
                                            });
                                            if (parseInt(values[handle]) == 10000)
                                                metrazhInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' متر و بیشتر';
                                            else
                                                metrazhInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' متر';
                                        });

                                    </script>
                                </div>

                            </div>
                            <div class="form-group col-md-12">

                                <div>
                                    <div class="text-danger text-center">
                                        متراژ زیربنا از
                                        <span id="minzirbana_label"></span>
                                        تا
                                        <span id="maxzirbana_label"></span>

                                    </div>
                                    <div class="mt-3" id="zirbana_slider"></div>

                                    <input id="zirbana_start" type="hidden" name="zirbana_start"/>
                                    <input id="zirbana_end" type="hidden" name="zirbana_end"/>

                                    <script>
                                        var zirbanaslider = document.getElementById('zirbana_slider');

                                        <?php if($_GET['zirbana_start']) { ?>
                                        var zirbana_s =<?php echo $_GET['zirbana_start'];?>;
                                        <?php } else{ ?>
                                        var zirbana_s = 0;
                                        <?php } ?>
                                        <?php if($_GET['zirbana_end']) { ?>
                                        var zirbana_e =<?php echo $_GET['zirbana_end'];?>;
                                        <?php } else{ ?>
                                        var zirbana_e = 500;
                                        <?php } ?>

                                        noUiSlider.create(zirbanaslider, {
                                            start: [zirbana_s, zirbana_e],
                                            connect: true,
                                            tooltips: false,
                                            /*tooltips:
                                                [wNumb({
                                                    decimals: 0,
                                                    prefix: 'از ',
                                                    suffix: ' متر',
                                                }), wNumb({
                                                    decimals: 0,
                                                    prefix: 'تا ',
                                                    suffix: ' متر',
                                                })],*/
                                            step: 1,
                                            direction: 'rtl',
                                            range: {
                                                'min': 0,
                                                'max': 500
                                            }, format: wNumb({
                                                decimals: 0,
                                            })
                                        });


                                        var zirbana_start = document.getElementById('zirbana_start');
                                        var zirbana_end = document.getElementById('zirbana_end');
                                        var zirbanaInputs = [zirbana_start, zirbana_end];
                                        var zirbana_start_label = document.getElementById('minzirbana_label');
                                        var zirbana_end_label = document.getElementById('maxzirbana_label');
                                        var zirbanaInputs_label = [zirbana_start_label, zirbana_end_label];
                                        zirbanaslider.noUiSlider.on('update', function (values, handle) {
                                            zirbanaInputs[handle].value = values[handle];
                                            var moneyFormat = wNumb({
                                                decimals: 0,
                                                thousand: ','
                                            });
                                            if (parseInt(values[handle]) == 500)
                                                zirbanaInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' متر و بیشتر';
                                            else
                                                zirbanaInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' متر';
                                        });


                                    </script>
                                </div>

                            </div>
                            <div class="form-group col-md-12">

                                <div>

                                    <div class="text-danger text-center">
                                        طول بر ملک از
                                        <span id="mintoolbar_label"></span>
                                        تا
                                        <span id="maxtoolbar_label"></span>

                                    </div>
                                    <div class="mt-3" id="toolbar_slider"></div>

                                    <input id="toolbar_start" type="hidden" name="toolbar_start"/>
                                    <input id="toolbar_end" type="hidden" name="toolbar_end"/>

                                    <script>
                                        var toolbarslider = document.getElementById('toolbar_slider');

                                        <?php if($_GET['toolbar_start']) { ?>
                                        var toolbar_s =<?php echo $_GET['toolbar_start'];?>;
                                        <?php } else{ ?>
                                        var toolbar_s = 0;
                                        <?php } ?>
                                        <?php if($_GET['toolbar_end']) { ?>
                                        var toolbar_e =<?php echo $_GET['toolbar_end'];?>;
                                        <?php } else{ ?>
                                        var toolbar_e = 500;
                                        <?php } ?>

                                        noUiSlider.create(toolbarslider, {
                                            start: [toolbar_s, toolbar_e],
                                            connect: true,
                                            tooltips: false,
                                            /*tooltips:
                                                [wNumb({
                                                    decimals: 0,
                                                    prefix: 'از ',
                                                    suffix: ' متر',
                                                }), wNumb({
                                                    decimals: 0,
                                                    prefix: 'تا ',
                                                    suffix: ' متر',
                                                })],*/
                                            step: 1,
                                            direction: 'rtl',
                                            range: {
                                                'min': 0,
                                                'max': 500
                                            }, format: wNumb({
                                                decimals: 0,
                                            })
                                        });


                                        var toolbar_start = document.getElementById('toolbar_start');
                                        var toolbar_end = document.getElementById('toolbar_end');
                                        var toolbarInputs = [toolbar_start, toolbar_end];
                                        var toolbar_start_label = document.getElementById('mintoolbar_label');
                                        var toolbar_end_label = document.getElementById('maxtoolbar_label');
                                        var toolbarInputs_label = [toolbar_start_label, toolbar_end_label];
                                        toolbarslider.noUiSlider.on('update', function (values, handle) {
                                            toolbarInputs[handle].value = values[handle];
                                            var moneyFormat = wNumb({
                                                decimals: 0,
                                                thousand: ','
                                            });
                                            if (parseInt(values[handle]) == 500)
                                                toolbarInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' متر و بیشتر';
                                            else
                                                toolbarInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' متر';
                                        });


                                    </script>
                                </div>

                            </div>
                            <div class="form-group col-md-12">

                                <div>

                                    <div class="text-danger text-center">
                                        متراژ کوچه / خیابان از
                                        <span id="minarz_label"></span>
                                        تا
                                        <span id="maxarz_label"></span>

                                    </div>
                                    <div class="mt-3" id="arz_slider"></div>

                                    <input id="arz_start" type="hidden" name="arz_start"/>
                                    <input id="arz_end" type="hidden" name="arz_end"/>

                                    <script>
                                        var arzslider = document.getElementById('arz_slider');

                                        <?php if($_GET['arz_start']) { ?>
                                        var arz_s =<?php echo $_GET['arz_start'];?>;
                                        <?php } else{ ?>
                                        var arz_s = 0;
                                        <?php } ?>
                                        <?php if($_GET['arz_end']) { ?>
                                        var arz_e =<?php echo $_GET['arz_end'];?>;
                                        <?php } else{ ?>
                                        var arz_e = 100;
                                        <?php } ?>

                                        noUiSlider.create(arzslider, {
                                            start: [arz_s, arz_e],
                                            connect: true,
                                            tooltips: false,
                                            /*tooltips:
                                                [wNumb({
                                                    decimals: 0,
                                                    prefix: 'از ',
                                                    suffix: ' متر',
                                                }), wNumb({
                                                    decimals: 0,
                                                    prefix: 'تا ',
                                                    suffix: ' متر',
                                                })],*/
                                            step: 1,
                                            direction: 'rtl',
                                            range: {
                                                'min': 0,
                                                'max': 100
                                            }, format: wNumb({
                                                decimals: 0,
                                            })
                                        });


                                        var arz_start = document.getElementById('arz_start');
                                        var arz_end = document.getElementById('arz_end');
                                        var arzInputs = [arz_start, arz_end];
                                        var arz_start_label = document.getElementById('minarz_label');
                                        var arz_end_label = document.getElementById('maxarz_label');
                                        var arzInputs_label = [arz_start_label, arz_end_label];

                                        arzslider.noUiSlider.on('update', function (values, handle) {
                                            arzInputs[handle].value = values[handle];
                                            var moneyFormat = wNumb({
                                                decimals: 0,
                                                thousand: ','
                                            });
                                            if (parseInt(values[handle]) == 100)
                                                arzInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' متر و بیشتر';
                                            else
                                                arzInputs_label[handle].innerHTML = moneyFormat.to(parseInt(values[handle])) + ' متر';
                                        });


                                    </script>
                                </div>

                            </div>
                        </div>


                        <div style="display: none;" class="col-md-2 col-sm-2">
                            <div class="form-group">
                                <select name="ptype"
                                        id="ptype"
                                        class="postform">
                                    <option value="" selected="selected">همه نوع قیمت</option>
                                    <?php foreach ($this->ptypes as $pt) { ?>
                                        <option <?php if ($_GET['ptype'] == $pt->id) { ?> selected="" <?php } ?>
                                                class="level-0"
                                                value="<?php echo $pt->id; ?>"><?php echo $pt->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="form-check col-md-12 mb-3 mt-3">


                            <hr>

                            <p>
                                امکانات :
                            </p>

                            <table>
                                <tr>
                                    <td><label>

                                            <?php echo form_checkbox(array(
                                                'name' => 'asansor',
                                                'id' => 'asansor',
                                                'value' => 'accept',
                                                'checked' => set_checkbox('asansor', 1, ($_GET['asansor']) ? true : false),
                                                'class' => 'form-check-input'
                                            )); ?>

                                            آسانسور

                                        </label></td>
                                    <td><label>

                                            <?php echo form_checkbox(array(
                                                'name' => 'parking',
                                                'id' => 'parking',
                                                'value' => 'accept',
                                                'checked' => set_checkbox('parking', 1, ($_GET['parking']) ? true : false),
                                                'class' => 'form-check-input'
                                            )); ?>

                                            پارکینگ

                                        </label></td>
                                </tr>
                                <tr>
                                    <td><label>

                                            <?php echo form_checkbox(array(
                                                'name' => 'anbari',
                                                'id' => 'anbari',
                                                'value' => 'accept',
                                                'checked' => set_checkbox('anbari', 1, ($_GET['anbari']) ? true : false),
                                                'class' => 'form-check-input'
                                            )); ?>

                                            انباری

                                        </label></td>
                                    <td><label>

                                            <?php echo form_checkbox(array(
                                                'name' => 'balkon',
                                                'id' => 'balkon',
                                                'value' => 'accept',
                                                'checked' => set_checkbox('balkon', 1, ($_GET['balkon']) ? true : false),
                                                'class' => 'form-check-input'
                                            )); ?>

                                            بالکن

                                        </label></td>
                                </tr>
                                <tr>
                                    <td><label>

                                            <?php echo form_checkbox(array(
                                                'name' => 'vam',
                                                'id' => 'vam',
                                                'value' => 'accept',
                                                'checked' => set_checkbox('vam', 1, ($_GET['vam']) ? true : false),
                                                'class' => 'form-check-input'
                                            )); ?>

                                            وام دارد

                                        </label></td>
                                    <td><label>

                                            <?php echo form_checkbox(array(
                                                'name' => 'bazsazi',
                                                'id' => 'bazsazi',
                                                'value' => 'accept',
                                                'checked' => set_checkbox('bazsazi', 1, ($_GET['bazsazi']) ? true : false),
                                                'class' => 'form-check-input'
                                            )); ?>

                                            بازسازی شده

                                        </label></td>
                                </tr>
                            </table>


                            <hr>
                        </div>

                    </div>
                </div>


                <div id="advance_div" <?php if ($_GET['advance_search'] != 1) { ?> class="hide" <?php } ?>
                     style="margin-bottom: 20px;">


                    <p>
                        ویژگی ها :
                    </p>

                    <input type="hidden" value="<?php echo $_GET['advance_search']; ?>" name="advance_search"
                           id="advance_search"/>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>
                                سال ساخت
                            </label>

                            <?php
                            $options = array();
                            //                                    $options[''] = 'سال ساخت';
                            $options += ITEM_YEAR;
                            echo form_multiselect(
                                'year[]',
                                $options,
                                set_value('year[]', show_data($_GET['year']), false),
                                'class="form-control form-control-sm mr-3" title="انتخاب کنید" id="year"'
                            );
                            ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label>
                                تعداد اتاق
                            </label>

                            <?php
                            $options = array();
                            //                                    $options[''] = 'تعداد اتاق';
                            $options += ITEM_ROOM;
                            echo form_multiselect(
                                'room[]',
                                $options,
                                set_value('room[]', show_data($_GET['room']), false),
                                'class="form-control form-control-sm mr-3" title="انتخاب کنید" id="room"'
                            );
                            ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label>
                                نوع سند
                            </label>

                            <?php
                            $options = array();
                            //                                    $options[''] = 'نوع سند';
                            $options += ITEM_SANAD;

                            echo form_multiselect(
                                'sanad[]',
                                $options,
                                set_value('sanad[]', show_data($_GET['sanad']), false),
                                'class="form-control form-control-sm mr-3" title="انتخاب کنید" id="sanad"'
                            );
                            ?>
                        </div>


                        <div class="form-group col-md-6">
                            <label>
                                کاربری ملک
                            </label>

                            <?php
                            $options = array();
                            //                                    $options[''] = 'کاربری ملک';
                            $options += ITEM_USE;

                            echo form_multiselect(
                                'usemelk[]',
                                $options,
                                set_value('usemelk[]', show_data($_GET['usemelk']), false),
                                'class="form-control form-control-sm mr-3" title="انتخاب کنید" id="usemelk"'
                            );
                            ?>
                        </div>


                        <div class="form-group col-md-6">
                            <label>
                                تعداد واحد در هر طبقه
                            </label>

                            <?php
                            $options = array();
                            //                                    $options[''] = 'تعداد واحد در هر طبقه';
                            $options += ITEM_UNIT;

                            echo form_multiselect(
                                'unit[]',
                                $options,
                                set_value('unit[]', show_data($_GET['unit']), false),
                                'class="form-control form-control-sm mr-3" title="انتخاب کنید" id="unit"'
                            );
                            ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label>
                                تعداد کل طبقات
                            </label>

                            <?php
                            $options = array();
                            $options[''] = 'تعداد کل طبقات';
                            $options += ITEM_FLOORCOUNT;

                            echo form_dropdown(
                                'floorcount',
                                $options,
                                set_value('floorcount', show_data($_GET['floorcount']), false),
                                'class="form-control form-control-sm mr-3" title="انتخاب کنید" id="floorcount"'
                            );
                            ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label>
                                طبقه چندم
                            </label>

                            <?php
                            $options = array();
                            //                                    $options[''] = 'طبقه چندم';
                            $options += ITEM_FLOOR;

                            echo form_multiselect(
                                'floor[]',
                                $options,
                                set_value('floor[]', show_data($_GET['floor']), false),
                                'class="form-control form-control-sm mr-3" title="انتخاب کنید" id="floor"'
                            );
                            ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label>
                                جهت ساختمان
                            </label>

                            <?php
                            $options = array();
                            //                                    $options[''] = 'جهت ساختمان';
                            $options += ITEM_DIRECTION;

                            echo form_multiselect(
                                'direction[]',
                                $options,
                                set_value('direction[]', show_data($_GET['direction'], true), false),
                                'class="form-control form-control-sm mr-3" title="انتخاب کنید" title="انتخاب کنید" id="direction"'
                            );
                            ?>

                        </div>


                        <div class="form-group col-md-6">
                            <p>
                                ریز جزئیات :
                            <hr>
                            </p>

                        </div>


                        <div class="form-group col-md-6">
                            <p>

                            </p>

                        </div>


                        <div class="form-group col-md-6">
                            <label>
                                نوع سرویس بهداشتی
                            </label>

                            <?php
                            $options = array();
                            //                                    $options[''] = 'تعداد سرویس بهداشتی';
                            $options += ITEM_WC;

                            echo form_multiselect(
                                'wc[]',
                                $options,
                                set_value('wc[]', show_data($_GET['wc']), false),
                                'class="form-control form-control-sm mr-3" title="انتخاب کنید" id="wc"'
                            );
                            ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label>
                                تعداد حمام
                            </label>

                            <?php
                            $options = array();
                            //                                    $options[''] = 'تعداد حمام';
                            $options += ITEM_BATHROOM;

                            echo form_multiselect(
                                'bathroom[]',
                                $options,
                                set_value('bathroom[]', show_data($_GET['bathroom']), false),
                                'class="form-control form-control-sm mr-3" title="انتخاب کنید" id="bathroom"'
                            );
                            ?>
                        </div>

                        <div class="form-group col-md-6">
                            <label>
                                وضعیت تخلیه
                            </label>

                            <?php
                            $options = array();
                            //                                    $options[''] = 'وضعیت تخلیه';
                            $options += ITEM_EMPTY;

                            echo form_multiselect(
                                'empty[]',
                                $options,
                                set_value('empty[]', show_data($_GET['empty']), false),
                                'class="form-control form-control-sm mr-3" title="انتخاب کنید" id="empty"'
                            );
                            ?>
                        </div>


                        <div class="form-group col-md-6">
                            <label>
                                جنس کف
                            </label>

                            <?php
                            $options = array();
                            //                                    $options[''] = 'جنس کف';
                            $options += ITEM_KAF;

                            echo form_multiselect(
                                'kaf[]',
                                $options,
                                set_value('kaf[]', show_data($_GET['kaf'], true), false),
                                'class="form-control form-control-sm mr-3"  title="انتخاب کنید" id="kaf"'
                            );
                            ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label>
                                سرمایش گرمایشی
                            </label>

                            <?php
                            $options = array();
                            //                                    $options[''] = 'سرمایش گرمایشی';
                            $options += ITEM_SARMAYESH;

                            echo form_multiselect(
                                'sarmayesh[]',
                                $options,
                                set_value('sarmayesh[]', show_data($_GET['sarmayesh'], true), false),
                                'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="sarmayesh"'
                            );
                            ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label>
                                مشخصه ملک
                            </label>

                            <?php
                            $options = array();
                            //                                    $options[''] = 'مشخصه ملک';
                            $options += ITEM_MOSHAKHASE;

                            echo form_multiselect(
                                'moshakhase[]',
                                $options,
                                set_value('moshakhase[]', show_data($_GET['moshakhase'], true), false),
                                'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="moshakhase"'
                            );
                            ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label>
                                تامین کننده آب گرم
                            </label>

                            <?php
                            $options = array();
                            //                                    $options[''] = 'تامین کننده آب گرم';
                            $options += ITEM_ABEGARM;

                            echo form_multiselect(
                                'abegarm[]',
                                $options,
                                set_value('abegarm[]', show_data($_GET['ashpazkhane'], true), false),
                                'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="abegarm"'
                            );
                            ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label>
                                کابینت بندی
                            </label>

                            <?php
                            $options = array();
                            //                                    $options[''] = 'کابینت بندی';
                            $options += ITEM_KABINET;

                            echo form_multiselect(
                                'kabinet[]',
                                $options,
                                set_value('kabinet[]', show_data($_GET['kabinet'], true), false),
                                'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="kabinet"'
                            );
                            ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label>
                                جنس دیوارها
                            </label>

                            <?php
                            $options = array();
                            //                                    $options[''] = 'جنس دیوارها';
                            $options += ITEM_DIVAR;

                            echo form_multiselect(
                                'divar[]',
                                $options,
                                set_value('divar[]', show_data($_GET['divar'], true), false),
                                'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="divar"'
                            );
                            ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label>
                                نمای ساختمان
                            </label>

                            <?php
                            $options = array();
                            //                                    $options[''] = 'نمای ساختمان';
                            $options += ITEM_NAMA;

                            echo form_multiselect(
                                'nama[]',
                                $options,
                                set_value('nama[]', show_data($_GET['nama'], true), false),
                                'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="nama"'
                            );
                            ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label>
                                امکانات امنیتی
                            </label>

                            <?php
                            $options = array();
                            //                                    $options[''] = 'امکانات امنیتی';
                            $options += ITEM_AMNIAT;

                            echo form_multiselect(
                                'amniat[]',
                                $options,
                                set_value('amniat[]', show_data($_GET['amniat'], true), false),
                                'class="form-control form-control-sm mr-3" title="انتخاب کنید"  id="amniat"'
                            );
                            ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label>
                                امکانات رفاهی تفریحی
                            </label>

                            <?php
                            $options = array();
                            //                                    $options[''] = 'امکانات رفاهی تفریحی';
                            $options += ITEM_REFAHI;

                            echo form_multiselect(
                                'refahi[]',
                                $options,
                                set_value('refahi[]', show_data($_GET['refahi'], true), false),
                                'class="form-control form-control-sm mr-3"  title="انتخاب کنید" id="refahi"'
                            );
                            ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label>
                                مشخصه آشپزخانه
                            </label>

                            <?php
                            $options = array();
                            //                                    $options[''] = 'مشخصه آشپزخانه';
                            $options += ITEM_ASHPAZKHANE;

                            echo form_multiselect(
                                'ashpazkhane[]',
                                $options,
                                set_value('ashpazkhane[]', show_data($_GET['ashpazkhane'], true), false),
                                'class="form-control form-control-sm mr-3"  title="انتخاب کنید" id="ashpazkhane"'
                            );
                            ?>
                        </div>

                    </div>


                </div>
                <div>
               <div class="form-row">

                        <div class="col-md-6 col-sm-6">
                            <button type="submit" class="button-blue btn-primary width-100">جستجو</button>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <button type="button" id="btn_advance" class="button-green btn-success width-100">پیشرفته
                            </button>
                        </div>
                    </div>
                    &#160;
                </div>
                &#160;
            </div>

        </form>
    </div>
</div>



<div class="sc-aa67b437-0 UQXBo">
    <div data-testid="h-hero" class="rui__nzvvnr-0 cuKXxb sc-aa67b437-3 iVzJB">
        <div class="hero-image">
            <picture data-lazy="force-loaded" class="rui__mnm7tm-0 dgwujn">
                <img alt="realtor.com for sale home page" src="http://www.melkekhoy.ir/ssag/Melkekhoy/uploads/thumbnail/auction_header.jpg" srcset="
      http://www.melkekhoy.ir/ssag/Melkekhoy/uploads/thumbnail/auction_header.jpg 480w,
      http://www.melkekhoy.ir/ssag/Melkekhoy/uploads/thumbnail/auction_header.jpg 768w,
      http://www.melkekhoy.ir/ssag/Melkekhoy/uploads/thumbnail/auction_header.jpg 996w,
      http://www.melkekhoy.ir/ssag/Melkekhoy/uploads/thumbnail/auction_header.jpg 1280w" role="presentation">
            </picture>
        </div>
        <div class="hero-content">
            <div class="rui__sc-9syhji-0 gsxrrh"><h1 data-testid="h-header"
                                                     class="rui__sc-19ei9fn-0 bzlkGQ rui__sc-6n0m70-0 iQHXOG">
                    <!--<?php echo $site_title; ?>-->
                    مزایده املاک در سامانه تخصصی ملک خوی
                </h1>
                <center>
                <h2 class="rui__sc-19ei9fn-0 cqArxn rui__qeenva-0 ljrOFI ">
سرمایه جایی می رود که آرامش باشد ...
                </h2>
                </center>
                &#160;
                <div data-testid="autocomplete-row" class="sc-aa67b437-1 jTPXzE">
                    <div class="sc-1c9c0937-1 lbcpMz">
                        <div role="search" class="rui__p9wrku-4 kexutx search-wrapper" data-testid="search-wrapper">
                            <form action="/index.php/auction/items" method="get" data-testid="s-box" aria-label="homes for sale" class="search-form">
                                <div height="58px" width="100%" class="rui__t0orcd-0 dymayr sc-1c9c0937-0 bviwTS">
                                    <div data-testid="Search Box" class="rui__sc-6dlz08-0 bISAJe rui__p9wrku-0 bTYndR">
                                        <div data-testid="input-wrapper" class="input-wrapper"><input
                                                    data-testid="input-element" role="combobox" aria-haspopup="listbox"
                                                    aria-autocomplete="list" aria-expanded="false" autocomplete="off"
                                                    value="<?php echo $_GET['name'];?>" id="searchbox-input"
                                                    class="rui__kzcwvy-0 cXsigV input autocomplete-input "
                                                    placeholder="کلمه یا کد ملک را وارد کنید..."
                                                    spellcheck="false"
                                                    name="name"
                                                    data-name="input-element"></div>
                                    </div>
                                    <button class="rui__ermeke-1 bOSAjA rui__p9wrku-1 ugMaj" aria-label="Search"
                                            type="submit"><i data-testid="search-btn-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                 fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                                            </svg>
                                        </i></button>
                                    <button id="btn-filter-show" class="rui__ermeke-1 bOSAjA rui__p9wrku-1 ugMaj ugMajB" aria-label="Search"
                                            type="button"><i data-testid="search-btn-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                 fill="currentColor" class="bi bi-funnel" viewBox="0 0 16 16">
                                                <path d="M1.5 1.5A.5.5 0 0 1 2 1h12a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.128.334L10 8.692V13.5a.5.5 0 0 1-.342.474l-3 1A.5.5 0 0 1 6 14.5V8.692L1.628 3.834A.5.5 0 0 1 1.5 3.5v-2zm1 .5v1.308l4.372 4.858A.5.5 0 0 1 7 8.5v5.306l2-.666V8.5a.5.5 0 0 1 .128-.334L13.5 3.308V2h-11z"/>
                                            </svg>
                                        </i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row bk-sefid">
    <div class="col-md-12">
        <div>
            <section class="block ad-list-block">
                <?php
                $items = $this->Front->get_items(12, 1, $this->input->get());
                ?>
                <div class="container-2">
                    <div class="section-title clearfix">
                        <h2> آگهی های مزایده</h2>
                    </div>
                    <div class="row items ad-listing">
                        <?php foreach ($items as $item) {
                            $item->default_photo = $this->ps_adapter->get_default_photo($item->id, 'item');
                            if ($item->default_photo->img_path == '')
                                $item->default_photo->img_path = 'no-product-image.png';
                            ?>
                            <div class="box-style-1 item col-lg-3 col-md-6 col-sm-6 col-xs-12 product type-product post-1317 status-publish first instock product_cat-electronics product_cat-console-video-game-and-online has-post-thumbnail featured downloadable virtual purchasable product-type-simple">
                                <div class="wrapper">
                                    <?php
                                    $data = array('item_id' => $item->id, 'user_id' => $this->session->userdata('user_id'));
                                    if ($this->Favourite->exists($data)) {
                                        $clfv = 'favorited-ad';
                                    } else {
                                        $clfv = '';
                                    }
                                    ?>
                                    <?php if ($item->feature == 1) { ?>
                                        <span class="ad_visit">ویژه</span>
                                    <?php } ?>
                                    <div class="image">
                                        <a href="/index.php/v/<?php echo $item->id; ?>"
                                           title="<?php echo $item->title; ?>" class="title">
                                            <img width="300" height="300"
                                                 src="/ssag/Melkekhoy/uploads/<?php echo $item->default_photo->img_path; ?>"
                                                 class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                 alt="" loading="lazy"
                                                 sizes="(max-width: 300px) 100vw, 300px"> </a>
                                    </div>
                                    <span class="metaitem">
                                              <div style="display: flex;justify-content: space-between;">
                                                  <span>
                                                      <?php echo $item->type_name; ?>  |  <?php echo $item->subname; ?>
                                                  </span>
                                                  <span>
                                                      <span class="item-bazdid">
                                                      <?php echo $item->touch_count; ?> بازدید
                                                      </span>
<span id="fav_<?php echo $item->id; ?>"
      class="favorite_1313 item-bookmark favorite <?php echo $clfv; ?>"
      data-item="<?php echo $item->id; ?>" data-toggle="tooltip"
      data-placement="top" title="" data-original-title="افزودن به علاقه‌مندی"></span>

                                                  </span>
                                              </div>
                                        </span>
                                    <div class="meta">
                                        <a href="/index.php/v/<?php echo $item->id; ?>"
                                           title="<?php echo $item->title; ?>" class="title">
                                            <h2><?php echo $item->title; ?></h2>
                                        </a>
                                        <div class="item-meta-price">
                                            <?php if ($item->pricetypeid == 1) {
                                                echo 'توافقی';
                                            } else if ($item->item_type_id == 3) {
                                                echo number_format($item->item_ejare);
                                            } else {
                                                ?>
                                                <?php echo number_format($item->price); ?>
                                                تومان
                                            <?php } ?>
                                        </div>
                                        <div class="flex-between" style="margin-bottom: 8px;">
                                            <figure>
                                                <i class="fa fa-calendar-o"></i>
                                                <?php
                                                echo ago($item->added_date);
                                                ?>
                                            </figure>
                                            <figure>
                                                <i class="fa fa-map-marker"></i>
                                                <?php echo $item->location_name; ?>
                                                > <?php echo $item->area_name; ?>
                                            </figure>
                                        </div>
                                        <div class="flex-between">
                                            <?php if ($item->metrazh and $item->zirbana) { ?>
                                                <figure>
                                                    <img src="/ssag/Melkekhoy/assets/front/icons/area.png">
                                                    <b>
                                                        <?php echo $item->metrazh; ?>
                                                    </b> متر عرصه
                                                    |
                                                    <b>
                                                        <?php echo $item->zirbana; ?>
                                                    </b> متر عیان
                                                </figure>
                                            <?php } ?>
                                            <?php if ($item->room) { ?>
                                                <figure>
                                                    <img src="/ssag/Melkekhoy/assets/front/icons/open-door.png">
                                                    <b>
                                                        <?php echo ITEM_ROOM[$item->room]; ?>
                                                    </b>
                                                    خوابه
                                                </figure>
                                            <?php } ?>
                                        </div>

                                    </div>
                                    <!--end meta-->
                                    <!--end description-->
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="center ad-load-more">
                        <div class="ad-loading">
                            <div class="ad-loader-show"></div>
                        </div>
                        <?php if (count($items) == 12) { ?>
                            <div class="btn btn-primary btn-framed btn-rounded loadmore">
                                آگهی بیشتر...
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </section>

        </div>
    </div>
</div>


<script>
    var ajaxurl = "/index.blade.php/more_item_ajax";
    var page = 2;
    jQuery(function ($) {
        $('body').on('click', '.loadmore', function () {
            jQuery(".ad-loading").css("display", "block");
            jQuery(".loadmore").hide();
            var data = {
                'page': page,
                'meta': '<?php echo json_encode($_GET)?>',
            };

            $.post(ajaxurl, data, function (response) {
                $('.ad-listing').append('<span class="scroll-span"></span>');
                $('.ad-listing').append(response);
                var allSelects = document.getElementsByClassName("scroll-span");
                var lastSelect = allSelects[allSelects.length - 1];
                lastSelect.scrollIntoView(true);
                page++;
                $('[data-toggle="tooltip"]').tooltip();
                $(".ribbon-featured").each(function () {
                    var thisText = $(this).text();
                    $(this).html("");
                    $(this).append(
                        "<div class='ribbon-start'></div>" +
                        "<div class='ribbon-content'>" + thisText + "</div>" +
                        "<div class='ribbon-end'>" +
                        "<figure class='ribbon-shadow'></figure>" +
                        "</div>"
                    );
                });
                jQuery(".ad-loading").css("display", "none");
                jQuery(".loadmore").show();
            });
        });
    });



    jQuery('#city').on('change', function () {

        var value = jQuery('option:selected', this).text().replace(/Value\s/, '');

        var catId = jQuery(this).val();

        jQuery.ajax({
            url: '/index.php/user/get_areas/' + catId,
            method: 'GET',
            dataType: 'JSON',
            success: function (data) {
                jQuery('#city_area').html("");
                jQuery('#city_area').append('<option value="">همه مناطق</option>');
                jQuery.each(data, function (i, obj) {
                    jQuery('#city_area').append('<option value="' + obj.id + '">' + obj.name + '</option>');
                });
                jQuery('#city_area').selectpicker('refresh');
            }
        });


    });

    jQuery('#type').on('change', function () {

        var tid = jQuery(this).val();

        if (tid == 3) {
            jQuery('.rahnejarebox').show();
            jQuery('#pricesearchbox').hide();
        } else {
            jQuery('.rahnejarebox').hide();
            jQuery('#pricesearchbox').show();
        }

    });

    <?php if($_GET['type'] != '') { ?>
    var tid = jQuery('#type').val();
    if (tid == 3) {
        jQuery('.rahnejarebox').show();
        jQuery('#pricesearchbox').hide();
    } else {
        jQuery('.rahnejarebox').hide();
        jQuery('#pricesearchbox').show();
    }
    <?php } ?>

    <?php if($_GET['city'] != '') { ?>

    var catId = '<?php echo $_GET['city'];?>';
    var areaId = '<?php echo $_GET['city_area'];?>';

    jQuery.ajax({
        url: '/index.php/user/get_areas/' + catId,
        method: 'GET',
        dataType: 'JSON',
        success: function (data) {
            jQuery('#city_area').html("");
            jQuery('#city_area').append('<option value="">همه مناطق</option>');
            jQuery.each(data, function (i, obj) {

                if (areaId == obj.id) {
                    var selected = 'selected=""';
                } else {
                    var selected = '';
                }

                jQuery('#city_area').append('<option ' + selected + ' value="' + obj.id + '">' + obj.name + '</option>');
            });
            jQuery('#city_area').selectpicker('refresh');
        }
    });

    <?php } ?>




    function separate(Number) {
        Number += '';
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        Number = Number.replace(',', '');
        x = Number.split('.');
        y = x[0];
        z = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(y))
            y = y.replace(rgx, '$1' + ',' + '$2');
        return y + z;
    }

</script>
<script>
    jQuery(function ($) {
        var data = {
            'ad_id': jQuery(this).data('item'),
        };
        $('body').on('click', '.favorite', function () {
            var id = jQuery(this).data('item');
            var ajax_favourite = "/index.blade.php/ajax_favourite/" + jQuery(this).data('item');
            $.post(ajax_favourite, data, function (response) {
                if (response == 1) {
                    alert('آگهی از علاقه مندی شما حذف شد');
                    jQuery('#fav_' + id).removeClass('favorited-ad');
                } else if (response == 2) {
                    alert('آگهی به علاقه مندی شما اضافه شد');
                    jQuery('#fav_' + id).addClass('favorited-ad');
                } else if (response == 3)
                    alert('برای افزودن آگهی به علاقه مندی ها ابتدا وارد سایت شوید');
                else
                    alert('خطا');
                // if (response != 3)
                // jQuery(this).addClass('favorited-ad');
                //window.location.href = "<?php //echo current_url(); ?>//";
            });
        });
    });
</script>
