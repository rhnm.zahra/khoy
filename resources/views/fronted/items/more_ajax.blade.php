<?php foreach ($items as $item) {
    $item->default_photo = $this->ps_adapter->get_default_photo($item->id, 'item');
    if ($item->default_photo->img_path == '')
        $item->default_photo->img_path = 'no-product-image.png';
    ?>
    <div class="box-style-1 item col-lg-3 col-md-3 col-sm-12 product type-product post-1317 status-publish first instock product_cat-electronics product_cat-console-video-game-and-online has-post-thumbnail featured downloadable virtual purchasable product-type-simple">
        <div class="wrapper">
            <?php
            $data = array('item_id' => $item->id, 'user_id' => $this->session->userdata('user_id'));
            if ($this->Favourite->exists($data)) {
                $clfv = 'favorited-ad';
            } else {
                $clfv = '';
            }
            ?>
            <?php if ($item->feature == 1) { ?>
                <span class="ad_visit">ویژه</span>
            <?php } ?>
            <div class="image">
                <a href="/index.php/v/<?php echo $item->id; ?>"
                   title="<?php echo $item->title; ?>" class="title">
                    <img width="300" height="300"
                         src="/ssag/Melkekhoy/uploads/<?php echo $item->default_photo->img_path; ?>"
                         class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                         alt="" loading="lazy"
                         sizes="(max-width: 300px) 100vw, 300px"> </a>
            </div>
            <span class="metaitem">
                                              <div style="display: flex;justify-content: space-between;">
                                                  <span>
                                                      <?php echo $item->type_name; ?>  |  <?php echo $item->subname; ?>
                                                  </span>
                                                  <span>
                                                      <span class="item-bazdid">
                                                      <?php echo $item->touch_count; ?> بازدید
                                                      </span>
<span id="fav_<?php echo $item->id; ?>"
      class="favorite_1313 item-bookmark favorite <?php echo $clfv; ?>"
      data-item="<?php echo $item->id; ?>" data-toggle="tooltip"
      data-placement="top" title="" data-original-title="افزودن به علاقه‌مندی"></span>

                                                  </span>
                                              </div>
                                        </span>
            <div class="meta">
                <a href="/index.php/v/<?php echo $item->id; ?>"
                   title="<?php echo $item->title; ?>" class="title">
                    <h2><?php echo $item->title; ?></h2>
                </a>
                <div class="item-meta-price">
                    <?php if ($item->pricetypeid == 1) {
                        echo 'توافقی';
                    } else if ($item->item_type_id == 3) {
                        echo number_format($item->item_ejare);
                    } else {
                        ?>
                        <?php echo number_format($item->price); ?>
                        تومان
                    <?php } ?>
                </div>
                <div class="flex-between" style="margin-bottom: 8px;">
                    <figure>
                        <i class="fa fa-calendar-o"></i>
                        <?php
                        echo ago($item->added_date);
                        ?>
                    </figure>
                    <figure>
                        <i class="fa fa-map-marker"></i>
                        <?php echo $item->location_name; ?>
                        > <?php echo $item->area_name; ?>
                    </figure>
                </div>
                <div class="flex-between">
                    <?php if ($item->metrazh and $item->zirbana) { ?>
                        <figure>
                            <img src="/ssag/Melkekhoy/assets/front/icons/area.png">
                            <b>
                                <?php echo $item->metrazh; ?>
                            </b> متر عرصه
                            |
                            <b>
                                <?php echo $item->zirbana; ?>
                            </b> متر عیان
                        </figure>
                    <?php } ?>
                    <?php if ($item->room) { ?>
                        <figure>
                            <img src="/ssag/Melkekhoy/assets/front/icons/open-door.png">
                            <b>
                                <?php echo ITEM_ROOM[$item->room]; ?>
                            </b>
                            خوابه
                        </figure>
                    <?php } ?>
                </div>

            </div>
            <!--end meta-->
            <!--end description-->
        </div>
    </div>
<?php } ?>
<?php if (count($items) < 12) { ?>
<script>
    jQuery(".loadmore").remove();
</script>
<?php } ?>
