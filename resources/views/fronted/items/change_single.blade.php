<?php
$item_s->default_photo = $this->ps_adapter->get_default_photo($item_s->id, 'item');
if ($item_s->default_photo->img_path == '')
    $item_s->default_photo->img_path = 'no-product-image.png';
if ($item_s->user_profile_photo == '')
    $item_s->user_profile_photo = 'no-product-image.png';
?>
<style>
    .gray_img {
        -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
        filter: grayscale(100%);
    }









</style>
<section class="block" style="transform: none;">
    <div class="container" style="transform: none;">

        <div id="product-716"
             class="product type-product post-716 status-publish first instock product_cat-15 product_tag-98 product_tag-99 product_tag-100 product_tag-101 product_tag-102 has-post-thumbnail downloadable virtual purchasable product-type-simple"
             style="transform: none;">
            <div class="row flex-md-row" style="transform: none;">

                <!--============ Listing Detail =============================================================-->
                <!--<div class="col-md-8 charsoogh-content" id="content">-->
                <div class="col-md-8" >

                    <?php if(count($items)) { ?>

                    <ul class="row items ad-listing">
                        <?php foreach ($items as $item) {
                            $item->default_photo = $this->ps_adapter->get_default_photo($item->id, 'item');
                            if ($item->default_photo->img_path == '')
                                $item->default_photo->img_path = 'no-product-image.png';
                            ?>
                            <li class="box-style-1 item col-lg-6 col-md-6 col-sm-12 product type-product post-1317 status-publish first instock product_cat-electronics product_cat-console-video-game-and-online has-post-thumbnail featured downloadable virtual purchasable product-type-simple">
                                <div class="wrapper">
                                    <?php
                                    $data = array('item_id' => $item->id, 'user_id' => $this->session->userdata('user_id'));
                                    if ($this->Favourite->exists($data)) {
                                        $clfv = 'favorited-ad';
                                    } else {
                                        $clfv = '';
                                    }
                                    ?>
                                    <?php if ($item->feature == 1) { ?>
                                        <span class="ad_visit">ویژه</span>
                                    <?php } ?>
                                    <div class="image">
                                        <a href="/index.php/v/<?php echo $item->id; ?>"
                                           title="<?php echo $item->title; ?>" class="title">
                                            <img width="300" height="300"
                                                 src="/ssag/Melkekhoy/uploads/<?php echo $item->default_photo->img_path; ?>"
                                                 class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                 alt="" loading="lazy"
                                                 sizes="(max-width: 300px) 100vw, 300px"> </a>
                                    </div>
                                    <span class="metaitem">
                                              <div style="display: flex;justify-content: space-between;">
                                                  <span>
                                                      <?php echo $item->type_name; ?>  |  <?php echo $item->subname; ?>
                                                  </span>
                                                  <span>
                                                      <span class="item-bazdid">
                                                      <?php echo $item->touch_count; ?> بازدید
                                                      </span>
<span id="fav_<?php echo $item->id; ?>"
      class="favorite_1313 item-bookmark favorite <?php echo $clfv; ?>"
      data-item="<?php echo $item->id; ?>" data-toggle="tooltip"
      data-placement="top" title="" data-original-title="افزودن به علاقه‌مندی"></span>

                                                  </span>
                                              </div>
                                        </span>
                                    <div class="meta">
                                        <a href="/index.php/v/<?php echo $item->id; ?>"
                                           title="<?php echo $item->title; ?>" class="title">
                                            <h2><?php echo $item->title; ?></h2>
                                        </a>
                                        <div class="item-meta-price">
                                            <?php if ($item->pricetypeid == 1) {
                                                echo 'توافقی';
                                            } else if ($item->item_type_id == 3) {
                                                echo number_format($item->item_ejare);
                                            } else {
                                                ?>
                                                <?php echo number_format($item->price); ?>
                                                تومان
                                            <?php } ?>
                                        </div>
                                        <div class="flex-between" style="margin-bottom: 8px;">
                                            <figure>
                                                <i class="fa fa-calendar-o"></i>
                                                <?php
                                                echo ago($item->added_date);
                                                ?>
                                            </figure>
                                            <figure>
                                                <i class="fa fa-map-marker"></i>
                                                <?php echo $item->location_name; ?>
                                                > <?php echo $item->area_name; ?>
                                            </figure>
                                        </div>
                                        <div class="flex-between">
                                            <?php if ($item->metrazh and $item->zirbana) { ?>
                                                <figure>
                                                    <img src="/ssag/Melkekhoy/assets/front/icons/area.png">
                                                    <b>
                                                        <?php echo $item->metrazh; ?>
                                                    </b> متر عرصه
                                                    |
                                                    <b>
                                                        <?php echo $item->zirbana; ?>
                                                    </b> متر عیان
                                                </figure>
                                            <?php } ?>
                                            <?php if ($item->room) { ?>
                                                <figure>
                                                    <img src="/ssag/Melkekhoy/assets/front/icons/open-door.png">
                                                    <b>
                                                        <?php echo ITEM_ROOM[$item->room]; ?>
                                                    </b>
                                                    خوابه
                                                </figure>
                                            <?php } ?>
                                        </div>

                                    </div>
                                    <!--end meta-->
                                    <!--end description-->
                                </div>
                            </li>
                        <?php } ?>
                    </ul>

                    <?php } else{ ?>
                    <p class="alert alert-danger">
                        فعلا آگهی مطابق با آگهی شما جهت معاوضه با این ملک درج نشده است !
                    </p>
                    <?php } ?>

                </div>
                <!--============ Sidebar ==============-->
                <div class="col-md-4" id="sidebar">

                    <div class="theiaStickySidebar">
                        <aside class="sidebar">
                            <section>
                                <div class="box">
                                    <a href="/index.php/profile/<?php echo $item_s->user_id; ?>">
                                        <div class="profile-box">
                                            <img src="/ssag/Melkekhoy/uploads/<?php echo $item_s->user_profile_photo; ?>"
                                                 alt="<?php echo $item_s->user_name; ?>"><?php echo $item_s->user_name; ?>
                                            <span class="advertiser">آگهی دهنده</span></div>
                                    </a>
                                    <div class="row contact-box">
                                        <?php if ($item_s->is_sold_out == 1) { ?>
                                            <button class="btn btn-primary info-btn col-md-6 col-sm-12 pull-right contact_info">
                                                فروخته شد
                                            </button>
                                        <?php } else { ?>
                                            <button type="button" data-toggle="modal" data-target="#contact_info"
                                                    class="btn btn-primary info-btn col-md-6 col-sm-12 pull-right contact_info">
                                                اطلاعات تماس
                                            </button>
                                        <?php } ?>

                                    </div>

                                    <dl>
                                        <dt class="ad_visit_n">کد آگهی</dt>
                                        <dd id="ad-visit"><?php echo $item_s->item_code; ?></dd>
                                        <dt class="ad_visit_n">بازدید</dt>
                                        <dd id="ad-visit"><?php echo $item_s->touch_count; ?> بازدید</dd>
                                        <dt>دسته‌بندی</dt>
                                        <dd id="ad-category">
                                            <?php echo $item_s->subname; ?>
                                        </dd>
                                        <dt>شهر</dt>
                                        <dd id="ad-city">
                                            <?php echo $item_s->location_name; ?>
                                        </dd>
                                        <dt>منطقه</dt>
                                        <dd id="ad-city">
                                            <?php echo $item_s->area_name; ?>
                                        </dd>
                                        <dt>آدرس</dt>
                                        <dd id="ad-city">
                                            <?php echo $item_s->address; ?>
                                        </dd>
                                        <dt class="publish_date_n">تاریخ انتشار</dt>
                                        <dd class="publish_date_v">
                                            <?php
                                            echo ago($item_s->added_date);
                                            ?>
                                        </dd>
                                        <dt class="publish_date_n">نوع آگهی</dt>
                                        <dd class="publish_date_v">
                                            <?php
                                            echo $item_s->type_name;
                                            ?>
                                        </dd>
                                        <?php if ($item->item_type_id == 3) { ?>
                                            <dt class="publish_date_n">رهن</dt>
                                            <dd class="publish_date_v">
                                                <?php echo number_format($item->item_rahn); ?> تومان
                                            </dd>
                                            <dt class="publish_date_n">اجاره</dt>
                                            <dd class="publish_date_v">
                                                <?php echo number_format($item->item_ejare); ?> تومان
                                            </dd>
                                        <?php } ?>

                                        <dt>قیمت</dt>
                                        <dd>
                                            <span class="price">
                                                 <?php if ($item_s->pricetypeid == 1) {
                                                     echo 'توافقی';
                                                 } else {
                                                     ?>
                                                     <?php echo number_format($item_s->price); ?>
                                                     تومان
                                                 <?php } ?>
                                            </span>
                                        </dd>
                                        <?php if ($item_s->change_status == 1) {

                                            $areas='';
                                            foreach(json_decode($item_s->item_area_id,true) as $area)
                                            {
                                                $area=  $this->Itemarea->get_one( $area );
                                                $areas.=$area->name.',';
                                            }
                                            $cats='';
                                            foreach(json_decode($item_s->item_cat_id,true) as $cat)
                                            {
                                                $cat=  $this->Itemcat->get_one( $cat );
                                                $cats.=$cat->name.',';
                                            }

                                            ?>

                                        <div style="background: linear-gradient(17deg, rgb(124 119 211) 0%, rgb(59 153 140) 0%, rgba(133,198,218,1) 54%, rgba(0,212,255,1) 100%);
    padding: 7px;">
                                            <?php if ($item_s->price_avl >0) { ?>
                                                <dt>مبلغ نقد موجود(تومان)</dt>
                                                <dd>
                                            <span class="price">
                                                <?php echo number_format($item_s->price_avl); ?>
                                                     تومان
                                            </span>
                                                </dd>
                                                <?php
                                            }
                                            if ($item_s->price_req >0) { ?>
                                                <dt>مبلغ نقد درخواستی(تومان)</dt>
                                                <dd>
                                            <span class="price">
                                                <?php echo number_format($item_s->price_req); ?>
                                                     تومان
                                            </span>
                                                </dd>
                                            <?php } ?>
                                            <dt>مناطق</dt>
                                            <dd id="ad-category">
                                                <?php echo trim($areas,','); ?>
                                            </dd>
                                            <dt>نوع ملک</dt>
                                            <dd id="ad-category">
                                                <?php echo trim($cats,','); ?>
                                            </dd>
                                        </div>
                                        <?php } ?>
                                    </dl>

                                </div>
                            </section>


                        </aside>
                        <div class="resize-sensor"
                             style="position: absolute; inset: 0px; overflow: hidden; z-index: -1; visibility: hidden;">
                            <div class="resize-sensor-expand"
                                 style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;">
                                <div style="position: absolute; left: 0px; top: 0px; transition: all 0s ease 0s; width: 370px; height: 1017px;"></div>
                            </div>
                            <div class="resize-sensor-shrink"
                                 style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;">
                                <div style="position: absolute; left: 0; top: 0; transition: 0s; width: 200%; height: 200%"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--============ End Sidebar =================-->
            </div>
        </div>
        <!-- Report Ad Modal -->
        <div class="modal fade" id="contact_info" tabindex="-1" role="dialog" aria-labelledby="contact_info"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="loading">
                        <div class="loader-show"></div>
                    </div>
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">تماس با آگهی دهنده</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="user_contact_info">
                            <dt><i class="fa fa-phone" aria-hidden="true"></i> دفتر ملک خوی:</dt>
                            <dd id="ch-phone"><a href="tel:04436362010">04436362010</a></dd>
                            <dt><i class="fa fa-phone" aria-hidden="true"></i> کارشناس فروش:</dt>
                            <dd id="ch-phone"><a href="tel:09104405996">09104405996</a></dd><!--
                            <dt><i class="fa fa-phone" aria-hidden="true"></i> شماره تماس:</dt>
                            <dd id="ch-phone"><a href="tel:<?php /*echo $item->phone; */?>"><?php /*echo $item->phone; */?></a></dd>-->
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <script>
            var ajaxurl = "/index.blade.php/add_report/<?php echo $item_s->id;?>";
            var ajax_favourite = "/index.blade.php/ajax_favourite/<?php echo $item_s->id;?>";
            jQuery(function ($) {
                var data = {
                    'ad_id': jQuery('#ad_id').val(),
                };
                $('body').on('click', '.send_report', function () {
                    jQuery(".ad-loading").css("display", "block");
                    $.post(ajaxurl, data, function (response) {
                        alert(' گزارش آگهی با موفقیت ارسال شد');
                        jQuery(".ad-loading").css("display", "none");
                        jQuery('#report_ads').modal('toggle');
                    });
                });
                $('body').on('click', '.favorite', function () {
                    $.post(ajax_favourite, data, function (response) {
                        if (response == 1)
                            alert('آگهی از علاقه مندی شما حذف شد');
                        else if (response == 2)
                            alert('آگهی به علاقه مندی شما اضافه شد');
                        else if (response == 3)
                            alert('برای افزودن آگهی به علاقه مندی ها ابتدا وارد سایت شوید');
                        else
                            alert('خطا');
                        if (response != 3)
                            window.location.href = "<?php echo current_url(); ?>";
                    });
                });
            });
        </script>

    </div>
</section>
