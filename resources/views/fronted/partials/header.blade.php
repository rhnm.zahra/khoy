<!DOCTYPE html>
<html dir="rtl" lang="fa-IR">
<head>
    <!-- Statsfa Website Analytics Start --><script data-host="https://statsfa.com" data-dnt="false" src="https://statsfa.com/js/script.js" id="ZwSg9rf6GA" async defer></script><!-- Statsfa Website Analytics End -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/ssag/Melkekhoy/assets/front/css/style.css" type="text/css" media="screen">
    <link rel="shortcut icon" type="image/png" href="/ssag/Melkekhoy/assets/img/favicon.ico">
    <link rel="apple-touch-icon" href="/ssag/Melkekhoy/assets/img/favicon.ico">
    <meta property="og:image" content="/ssag/Melkekhoy/assets/img/favicon.ico">
    <meta name="twitter:image" content="/ssag/Melkekhoy/assets/img/favicon.ico">
    <title>
        <?php echo $meta_title . ' | ' . $this->about_app->about_name; ?>
    </title>
    <meta name="robots" content="max-image-preview:large">
    <link rel="dns-prefetch" href="https://s.w.org/">
    <link href="/ssag/Melkekhoy/assets/front/dist/css/lightbox.css" rel="stylesheet"/>
    <link rel="stylesheet" id="wp-block-library-rtl-css" href="/ssag/Melkekhoy/assets/front/css/style-rtl.min.css"
          media="all">
    <link rel="stylesheet" id="wc-blocks-vendors-style-css"
          href="/ssag/Melkekhoy/assets/front/css/wc-blocks-vendors-style.css"
          media="all">
    <link rel="stylesheet" id="wc-blocks-style-rtl-css" href="/ssag/Melkekhoy/assets/front/css/wc-blocks-style-rtl.css"
          media="all">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <style id="global-styles-inline-css">
        body {
            --wp--preset--color--black: #000000;
            --wp--preset--color--cyan-bluish-gray: #abb8c3;
            --wp--preset--color--white: #ffffff;
            --wp--preset--color--pale-pink: #f78da7;
            --wp--preset--color--vivid-red: #cf2e2e;
            --wp--preset--color--luminous-vivid-orange: #ff6900;
            --wp--preset--color--luminous-vivid-amber: #fcb900;
            --wp--preset--color--light-green-cyan: #7bdcb5;
            --wp--preset--color--vivid-green-cyan: #00d084;
            --wp--preset--color--pale-cyan-blue: #8ed1fc;
            --wp--preset--color--vivid-cyan-blue: #0693e3;
            --wp--preset--color--vivid-purple: #9b51e0;
            --wp--preset--gradient--vivid-cyan-blue-to-vivid-purple: linear-gradient(135deg, rgba(6, 147, 227, 1) 0%, rgb(155, 81, 224) 100%);
            --wp--preset--gradient--light-green-cyan-to-vivid-green-cyan: linear-gradient(135deg, rgb(122, 220, 180) 0%, rgb(0, 208, 130) 100%);
            --wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange: linear-gradient(135deg, rgba(252, 185, 0, 1) 0%, rgba(255, 105, 0, 1) 100%);
            --wp--preset--gradient--luminous-vivid-orange-to-vivid-red: linear-gradient(135deg, rgba(255, 105, 0, 1) 0%, rgb(207, 46, 46) 100%);
            --wp--preset--gradient--very-light-gray-to-cyan-bluish-gray: linear-gradient(135deg, rgb(238, 238, 238) 0%, rgb(169, 184, 195) 100%);
            --wp--preset--gradient--cool-to-warm-spectrum: linear-gradient(135deg, rgb(74, 234, 220) 0%, rgb(151, 120, 209) 20%, rgb(207, 42, 186) 40%, rgb(238, 44, 130) 60%, rgb(251, 105, 98) 80%, rgb(254, 248, 76) 100%);
            --wp--preset--gradient--blush-light-purple: linear-gradient(135deg, rgb(255, 206, 236) 0%, rgb(152, 150, 240) 100%);
            --wp--preset--gradient--blush-bordeaux: linear-gradient(135deg, rgb(254, 205, 165) 0%, rgb(254, 45, 45) 50%, rgb(107, 0, 62) 100%);
            --wp--preset--gradient--luminous-dusk: linear-gradient(135deg, rgb(255, 203, 112) 0%, rgb(199, 81, 192) 50%, rgb(65, 88, 208) 100%);
            --wp--preset--gradient--pale-ocean: linear-gradient(135deg, rgb(255, 245, 203) 0%, rgb(182, 227, 212) 50%, rgb(51, 167, 181) 100%);
            --wp--preset--gradient--electric-grass: linear-gradient(135deg, rgb(202, 248, 128) 0%, rgb(113, 206, 126) 100%);
            --wp--preset--gradient--midnight: linear-gradient(135deg, rgb(2, 3, 129) 0%, rgb(40, 116, 252) 100%);
            --wp--preset--duotone--dark-grayscale: url('#wp-duotone-dark-grayscale');
            --wp--preset--duotone--grayscale: url('#wp-duotone-grayscale');
            --wp--preset--duotone--purple-yellow: url('#wp-duotone-purple-yellow');
            --wp--preset--duotone--blue-red: url('#wp-duotone-blue-red');
            --wp--preset--duotone--midnight: url('#wp-duotone-midnight');
            --wp--preset--duotone--magenta-yellow: url('#wp-duotone-magenta-yellow');
            --wp--preset--duotone--purple-green: url('#wp-duotone-purple-green');
            --wp--preset--duotone--blue-orange: url('#wp-duotone-blue-orange');
            --wp--preset--font-size--small: 13px;
            --wp--preset--font-size--medium: 20px;
            --wp--preset--font-size--large: 36px;
            --wp--preset--font-size--x-large: 42px;
        }

        .has-black-color {
            color: var(--wp--preset--color--black) !important;
        }

        .has-cyan-bluish-gray-color {
            color: var(--wp--preset--color--cyan-bluish-gray) !important;
        }

        .has-white-color {
            color: var(--wp--preset--color--white) !important;
        }

        .has-pale-pink-color {
            color: var(--wp--preset--color--pale-pink) !important;
        }

        .has-vivid-red-color {
            color: var(--wp--preset--color--vivid-red) !important;
        }

        .has-luminous-vivid-orange-color {
            color: var(--wp--preset--color--luminous-vivid-orange) !important;
        }

        .has-luminous-vivid-amber-color {
            color: var(--wp--preset--color--luminous-vivid-amber) !important;
        }

        .has-light-green-cyan-color {
            color: var(--wp--preset--color--light-green-cyan) !important;
        }

        .has-vivid-green-cyan-color {
            color: var(--wp--preset--color--vivid-green-cyan) !important;
        }

        .has-pale-cyan-blue-color {
            color: var(--wp--preset--color--pale-cyan-blue) !important;
        }

        .has-vivid-cyan-blue-color {
            color: var(--wp--preset--color--vivid-cyan-blue) !important;
        }

        .has-vivid-purple-color {
            color: var(--wp--preset--color--vivid-purple) !important;
        }

        .has-black-background-color {
            background-color: var(--wp--preset--color--black) !important;
        }

        .has-cyan-bluish-gray-background-color {
            background-color: var(--wp--preset--color--cyan-bluish-gray) !important;
        }

        .has-white-background-color {
            background-color: var(--wp--preset--color--white) !important;
        }

        .has-pale-pink-background-color {
            background-color: var(--wp--preset--color--pale-pink) !important;
        }

        .has-vivid-red-background-color {
            background-color: var(--wp--preset--color--vivid-red) !important;
        }

        .has-luminous-vivid-orange-background-color {
            background-color: var(--wp--preset--color--luminous-vivid-orange) !important;
        }

        .has-luminous-vivid-amber-background-color {
            background-color: var(--wp--preset--color--luminous-vivid-amber) !important;
        }

        .has-light-green-cyan-background-color {
            background-color: var(--wp--preset--color--light-green-cyan) !important;
        }

        #btn_advance {
            padding: 1.5rem 1.6rem;
        }

        .hide {
            display: none;
        }

        /*.hero .hero-wrapper {
            padding-bottom: 0rem !important;
        }
*/
        .has-vivid-green-cyan-background-color {
            background-color: var(--wp--preset--color--vivid-green-cyan) !important;
        }

        .has-pale-cyan-blue-background-color {
            background-color: var(--wp--preset--color--pale-cyan-blue) !important;
        }

        .has-vivid-cyan-blue-background-color {
            background-color: var(--wp--preset--color--vivid-cyan-blue) !important;
        }

        .has-vivid-purple-background-color {
            background-color: var(--wp--preset--color--vivid-purple) !important;
        }

        .has-black-border-color {
            border-color: var(--wp--preset--color--black) !important;
        }

        .has-cyan-bluish-gray-border-color {
            border-color: var(--wp--preset--color--cyan-bluish-gray) !important;
        }

        .has-white-border-color {
            border-color: var(--wp--preset--color--white) !important;
        }

        .has-pale-pink-border-color {
            border-color: var(--wp--preset--color--pale-pink) !important;
        }

        .has-vivid-red-border-color {
            border-color: var(--wp--preset--color--vivid-red) !important;
        }

        .has-luminous-vivid-orange-border-color {
            border-color: var(--wp--preset--color--luminous-vivid-orange) !important;
        }

        .has-luminous-vivid-amber-border-color {
            border-color: var(--wp--preset--color--luminous-vivid-amber) !important;
        }

        .has-light-green-cyan-border-color {
            border-color: var(--wp--preset--color--light-green-cyan) !important;
        }

        .has-vivid-green-cyan-border-color {
            border-color: var(--wp--preset--color--vivid-green-cyan) !important;
        }

        .has-pale-cyan-blue-border-color {
            border-color: var(--wp--preset--color--pale-cyan-blue) !important;
        }

        .has-vivid-cyan-blue-border-color {
            border-color: var(--wp--preset--color--vivid-cyan-blue) !important;
        }

        .has-vivid-purple-border-color {
            border-color: var(--wp--preset--color--vivid-purple) !important;
        }

        .has-vivid-cyan-blue-to-vivid-purple-gradient-background {
            background: var(--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple) !important;
        }

        .has-light-green-cyan-to-vivid-green-cyan-gradient-background {
            background: var(--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan) !important;
        }

        .has-luminous-vivid-amber-to-luminous-vivid-orange-gradient-background {
            background: var(--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange) !important;
        }

        .has-luminous-vivid-orange-to-vivid-red-gradient-background {
            background: var(--wp--preset--gradient--luminous-vivid-orange-to-vivid-red) !important;
        }

        .has-very-light-gray-to-cyan-bluish-gray-gradient-background {
            background: var(--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray) !important;
        }

        .has-cool-to-warm-spectrum-gradient-background {
            background: var(--wp--preset--gradient--cool-to-warm-spectrum) !important;
        }

        .has-blush-light-purple-gradient-background {
            background: var(--wp--preset--gradient--blush-light-purple) !important;
        }

        .has-blush-bordeaux-gradient-background {
            background: var(--wp--preset--gradient--blush-bordeaux) !important;
        }

        .has-luminous-dusk-gradient-background {
            background: var(--wp--preset--gradient--luminous-dusk) !important;
        }

        .has-pale-ocean-gradient-background {
            background: var(--wp--preset--gradient--pale-ocean) !important;
        }

        .has-electric-grass-gradient-background {
            background: var(--wp--preset--gradient--electric-grass) !important;
        }

        .has-midnight-gradient-background {
            background: var(--wp--preset--gradient--midnight) !important;
        }

        .has-small-font-size {
            font-size: var(--wp--preset--font-size--small) !important;
        }

        .has-medium-font-size {
            font-size: var(--wp--preset--font-size--medium) !important;
        }

        .has-large-font-size {
            font-size: var(--wp--preset--font-size--large) !important;
        }

        .has-x-large-font-size {
            font-size: var(--wp--preset--font-size--x-large) !important;
        }
    </style>
    <link rel="stylesheet" id="woocommerce-layout-rtl-css"
          href="/ssag/Melkekhoy/assets/front/css/woocommerce-layout-rtl.css" media="all">
    <link rel="stylesheet" id="woocommerce-smallscreen-rtl-css"
          href="/ssag/Melkekhoy/assets/front/css/woocommerce-smallscreen-rtl.css"
          media="only screen and (max-width: 768px)">
    <link rel="stylesheet" id="woocommerce-general-rtl-css" href="/ssag/Melkekhoy/assets/front/css/woocommerce-rtl.css"
          media="all">
    <style id="woocommerce-inline-inline-css">
        .woocommerce form .form-row .required {
            visibility: visible;
        }
    </style>
    <link rel="stylesheet" id="icofont-css" href="/ssag/Melkekhoy/assets/front/css/icofont.min.css" media="all">
    <link rel="stylesheet" id="leaflet-css" href="/ssag/Melkekhoy/assets/front/css/leaflet.css" media="all">
    <link rel="stylesheet" id="owl-carousel-css" href="/ssag/Melkekhoy/assets/front/css/owl.carousel.min.css"
          media="all">
    <link rel="stylesheet" id="bootstrap-select-css" href="/ssag/Melkekhoy/assets/front/css/bootstrap-select.min.css"
          media="all">
    <link rel="stylesheet" id="bootstrap-min-rtl-css" href="/ssag/Melkekhoy/assets/front/css/bootstrap.min-rtl.css"
          media="all">
    <link rel="stylesheet" id="font-awesome-css" href="/ssag/Melkekhoy/assets/font-awesome/css/font-awesome.min.css"
          media="all">
    <link rel="stylesheet" id="theme-rtl-css" href="/ssag/Melkekhoy/assets/front/css/rtl.css" media="all">
    <link rel="stylesheet" id="dropzone-css" href="/ssag/Melkekhoy/assets/front/css/dropzone.min.css" media="all">
    <link rel="stylesheet" id="glightbox-css" href="/ssag/Melkekhoy/assets/front/css/glightbox.min.css" media="all">
    <script src="/ssag/Melkekhoy/assets/front/js/jquery.min.js" id="jquery-core-js"></script>
    <script src="/ssag/Melkekhoy/assets/front/js/jquery-migrate.min.js" id="jquery-migrate-js"></script>
    <script src="/ssag/Melkekhoy/assets/front/js/popper.min.js" id="popper-js"></script>
    <script src="/ssag/Melkekhoy/assets/front/js/bootstrap.js" id="bootstrap-js"></script>
    <script src="/ssag/Melkekhoy/assets/front/js/jquery-confirm.js" id="jquery-confirm-js"></script>
    <script src="/ssag/Melkekhoy/assets/front/js/iCheck.js" id="iCheck-js"></script>
    <script src="/ssag/Melkekhoy/assets/front/js/selectize.js" id="selectize-js"></script>
    <script src="/ssag/Melkekhoy/assets/front/js/bootstrap-select.min.js" id="bootstrap-select-js"></script>
    <script src="/ssag/Melkekhoy/assets/front/js/owl-carousel.js" id="owl-carousel-js"></script>
    <script src="/ssag/Melkekhoy/assets/front/js/bootstrap-validat.js" id="bootstrap-validat-js"></script>
    <script src="/ssag/Melkekhoy/assets/front/js/pattern-validation.js" id="pattern-validation-js"></script>
    <script src="/ssag/Melkekhoy/assets/front/js/charsoogh.js" id="charsoogh-js"></script>
    <script src="/ssag/Melkekhoy/assets/front/js/dropzone.min.js" id="dropzone-js"></script>
    <script src="/ssag/Melkekhoy/assets/front/js/jquery-validate.bootstrap-tooltip.min.js" id="validate-js"></script>
    <script src="/ssag/Melkekhoy/assets/front/js/flickity.pkgd.js" id="flickity-js"></script>

    <script src="/ssag/Melkekhoy/assets/front/js/num2persian.js" id="num2persian-js"></script>
    <script src="/ssag/Melkekhoy/assets/front/js/glightbox.min.js" id="glightbox-js"></script>
    <style id="theia-sticky-sidebar-stylesheet-TSS">.theiaStickySidebar:after {
            content: "";
            display: table;
            clear: both;
        }</style>

    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js"></script>-->

    <script src="/ssag/Melkekhoy/assets/front/wNumb.min.js"></script>
    <link href="/ssag/Melkekhoy/assets/front/nouislider.css" rel="stylesheet">
    <script src="/ssag/Melkekhoy/assets/front/nouislider.js"></script>
    <style>
        .noUi-connect {
            background: #b30753;
        }

        #menu_footer {
            height: 77px !important;
            background-color: #e71c75;
            position: fixed;
            bottom: 0;
            width: 100%;
            z-index: 999;
        }
    </style>

    <link href="/ssag/Melkekhoy/assets/front/css/theme2.css?v=7" rel="stylesheet"/>

</head>
<body class="rtl home blog woocommerce-page theme-charsoogh elementor-default elementor-kit-1346">
<div class="page home-page">
    <div id="appbaner" class="rui__vur8lw-0 YZJVK sc-e2115056-0 nchaP ">
        <button id="close-app-banner" aria-label="Close" class=" jjBvzL close-icon" data-testid="close-button">   ✖   </button>
        <svg style="display: none;" viewBox="0 0 24 24" data-testid="core-icon" aria-hidden="true" focusable="false" class="app-icon"
             style="display: inline-block; width: 1em; height: 1em; font-size: 40px; color: rgb(255, 255, 255); fill: currentcolor;">
            <path d="M12.813 10.041l-.004.005c-.329.312-.7.74-1.011 1.57a255.598 255.598 0 01.034-1.755c0-.1-.07-.205-.225-.205H8.681v.251l.077.007h.006c.235.02.459.04.664.188a.737.737 0 01.331.518c.06.256.06 1.004.06 1.323v4.244c0 .43-.015 1.028-.08 1.424-.118.388-.246.506-.501.634a1.87 1.87 0 01-.524.136l-.073.008v.25h4.37v-.25l-.076-.008a1.5 1.5 0 01-.487-.118c-.132-.049-.24-.167-.36-.384-.174-.37-.276-.78-.276-1.791v-1.77c0-.531.023-1.047.04-1.277.2-1.41.961-2.547 1.46-2.547.192 0 .315.206.452.539.16.377.383.782 1.03.782.643 0 .978-.543.978-1.079 0-.368-.106-.664-.33-.912-.237-.237-.619-.37-1.074-.37-.753 0-1.226.313-1.555.587z"
                  fill="#393838"></path>
            <path fill-rule="evenodd" clip-rule="evenodd"
                  d="M6 0a6 6 0 00-6 6v12a6 6 0 006 6h12a6 6 0 006-6V6a6 6 0 00-6-6H6zm-.255 12.247H3L12.01 3 21 12.247h-2.743v8.293H5.745v-8.293z"
                  fill="#393838"></path>
        </svg>
        <div class="headlines">
            <div class="rui__sc-19ei9fn-0 CfrEu rui__sc-1c1xfqu-0 kQjDfT title">
                اپلیکیشن جامع ملک خوی
            </div>
        </div>
        <a class="download-button" data-testid="button-install" tabindex="0"
           href="#">
            دانلود اپلیکیشن
        </a></div>
    <header class="hero">
        <div class="hero-wrapper">
            <div class="main-navigation">

                <div class="container-2">
                    <nav class="navbar navbar-expand-lg navbar-light justify-content-between">
                        <button id="navsidebtn" class="navbar-toggler" type="button">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <a class="navbar-brand" href="/">
                            <img src="/ssag/Melkekhoy/assets/front/images/logo.png" alt="">
                        </a>
                        <div class="collapse navbar-collapse" id="navbar">
                            <!--Main navigation list-->
                            <ul id="menu-%d9%85%d9%86%d9%88-%d8%a7%d8%b5%d9%84%db%8c" class="navbar-nav">
                                <!--<li id="menu-item-1511"-->
                                <!--    class="<?php if ($menu_index == 'home') { ?> active <?php } ?> menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home nav-item menu-item-1511">-->
                                <!--    <a href="/" class="nav-link">آگهی ها</a>-->
                                <!--</li>-->
                                <li id="menu-item-1511"
                                    class="<?php if ($menu_index == 'change') { ?> active change_bg <?php } ?> menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home nav-item menu-item-1511">
                                    <a href="/index.php/change/items" class="nav-link">آگهی معاوضه</a>
                                </li>
                                <li id="menu-item-1511"
                                    class="<?php if ($menu_index == 'request') { ?> active request_bg <?php } ?> menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home nav-item menu-item-1511">
                                    <a href="/index.php/request/items" class="nav-link">آگهی درخواست</a>
                                </li>

                                     <li id="menu-item-1511"
                                    class="<?php if ($menu_index == 'auction') { ?> active auction_bg <?php } ?> menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home nav-item menu-item-1511">
                                    <a href="/index.php/auction/items" class="nav-link">مزایــده</a>
                                </li>

                                <li id="menu-item-1511"
                                    class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home nav-item menu-item-1511">
                                    <a href="https://cafebazaar.ir/app/com.apps.Melk_Khoy" class="nav-link">دانلود
                                        اپلیکیشن</a>
                                </li>



                                <li id="menu-item-1263"
                                    class="<?php if ($menu_index == 'about') { ?> active <?php } ?> menu-item menu-item-type-post_type menu-item-object-page nav-item menu-item-1263">
                                    <a href="/index.php/about" class="nav-link">درباره
                                        ما</a>
                                </li>
                                <li id="menu-item-1264"
                                    class="<?php if ($menu_index == 'contact') { ?> active <?php } ?> menu-item menu-item-type-post_type menu-item-object-page nav-item menu-item-1264">
                                    <a href="/index.php/contact" class="nav-link">تماس با
                                        ما</a></li>
                                <!--<li id="menu-item-1264"-->
                                <!--    class="<?php if ($menu_index == 'rules') { ?> active <?php } ?> menu-item menu-item-type-post_type menu-item-object-page nav-item menu-item-1264">-->
                                <!--    <a href="/index.blade.php/rules" class="nav-link">-->
                                <!--        قوانین و مقررات-->
                                <!--    </a></li>-->
                                <li id="menu-item-1264"
                                    class="<?php if ($menu_index == 'estelam') { ?> active <?php } ?> menu-item menu-item-type-post_type menu-item-object-page nav-item menu-item-1264">
                                    <a href="/index.php/estelam" class="nav-link">
                                        استعلام
                                    </a></li>
                            </ul>
                        </div>
                        <ol class="nav-btn">
                            <li style="display: none;" class="top-category-btn">
                                <button type="button" class="btn btn-primary text-caps btn-rounded btn-framed"
                                        data-toggle="modal" data-target="#charsoogh_category_show"><i
                                            class="fa fa-align-justify" aria-hidden="true"></i> دسته‌بندی‌ها
                                </button>
                            </li>
                            <li class="nav-item submit_ad">
                                <a href="/index.php/user/addrequest"
                                   class="btn-theme-2">
                                    ثبت درخواست
                                </a>
                            </li>
                            <li class="nav-item submit_ad">
                                <a href="/index.php/user/additem"
                                   class="btn-theme-2">
                                    ثبت اگهی رایگان </a>
                            </li>

                        </ol>
                        <?php if ($this->session->userdata('user_id')) { ?>
                            <div class="loginbox" id="log1in">
                                <button id="usersidedeskbtn" class="rui__ermeke-0 KNvzR user-profile_header-icon"><span
                                            class="user-profile_header-signup">حساب کاربری</span><span></span></button>
                            </div>
                        <?php } else { ?>
                            <div class="loginbox" id="log1in">
                                <a class="rui__ermeke-0 KNvzR user-profile_header-icon"
                                   href="/index.php/user/login"><span
                                            class="user-profile_header-signup">ورود / ثبت نام</span><span></span></a>
                            </div>
                        <?php } ?>

                        <div class="loginbox" id="log2in" style="display: none;">
                            <button id="usersidebtn" class="rui__ermeke-0 KNvzR user-profile_header-icon"><span
                                        class="user-profile_header-signup">
                                    <i class="fa fa-user"></i>
                                </span><span></span></button>
                        </div>
                    </nav>
                </div>
            </div>
            <?php /*if ($search_request == 1 || $search_auction == 1) { */?><!--
                <div class="page-title">
                    <div class="container">
                        <div class="center website_header_slogan">
                            <?php /*echo $site_title; */?>
                        </div>
                    </div>
                </div>
            --><?php /*} */?>
            <?php if ($search_change == 1) { ?>
                <div class="page-title">
                    <div class="container">
                        <div class="center website_header_slogan">
                            سیستم معاوضه اختصاصی املاک ملک خوی
                        </div>
                        <p style="text-align: center;margin-top: 10px;background-color: #17a2b84a;padding: 7px;">
                            بصورت هوشمند املاک موجود رو بررسی کن و موارد قابل معاوضه و مطابق شرایط ات رو پیدا کن .
                        </p>
                    </div>
                </div>
                <form class="hero-form form woocommerce-product-search" role="search" method="get">
                    <div class="container">
                        <!--Main Form-->
                        <div class="main-search-form style-1">
                            <div class="form-row">
                                <div class="col-md-2 col-sm-3">
                                    <div class="form-group">
                                        <input name="price" onkeyup="javascript:this.value=separate(this.value);"
                                               type="text" class="form-control" id="price"
                                               placeholder="قیمت ملک (تومان)" value="<?php echo $_GET['price']; ?>">
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-3">
                                    <div class="form-group">
                                        <input name="price_avl" onkeyup="javascript:this.value=separate(this.value);"
                                               type="text" class="form-control" id="price_avl"
                                               placeholder="مبلغ نقد موجود (تومان)"
                                               value="<?php echo $_GET['price_avl']; ?>">
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-3">
                                    <div class="form-group">
                                        <input name="price_req" onkeyup="javascript:this.value=separate(this.value);"
                                               type="text" class="form-control" id="price_req"
                                               placeholder="مبلغ نقد درخواستی (تومان)"
                                               value="<?php echo $_GET['price_req']; ?>">
                                    </div>
                                </div>

                                <div class="col-md-2 col-sm-2">
                                    <div class="form-group">
                                        <select name="mycat"
                                                id="mycat"
                                                multiple=""
                                                placeholder="نوع ملک من"
                                                class="postform">
                                            <option value="" disabled="">نوع ملک من</option>
                                            <?php
                                            $options = array();
                                            $cats = $this->Itemcat->get_all();
                                            foreach ($cats->result() as $cat) {
                                                $options[$cat->id] = $cat->name;
                                            }
                                            foreach ($options as $k => $pt) { ?>
                                                <option <?php if (in_array($k, $_GET['mycat'])) { ?> selected="" <?php } ?>
                                                        class="level-0"
                                                        value="<?php echo $k; ?>"><?php echo $pt; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <div class="form-group">
                                        <select name="mycity"
                                                id="mycity"
                                                class="postform">
                                            <option value="">شهر ملک من</option>
                                            <?php foreach ($this->locations as $location) { ?>
                                                <option <?php if ($_GET['mycity'] == $location->id) { ?> selected="" <?php } ?>
                                                        class="level-0"
                                                        value="<?php echo $location->id; ?>"><?php echo $location->name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <div class="form-group">
                                        <select title="همه مناطق" name="myarea"
                                                id="myarea"
                                                class="postform">
                                            <option selected="" value="">منطقه ملک من</option>

                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="form-row">
                                <div class="col-md-2 col-sm-3">
                                    <div class="form-group">
                                        <select name="sort"
                                                id="sort"
                                                class="postform">
                                            <option <?php if ($_GET['sort'] == 'new') { ?> selected="" <?php } ?>
                                                    class="level-0"
                                                    value="new">
                                                جدیدترین ها
                                            </option>
                                            <option <?php if ($_GET['sort'] == 'favorite') { ?> selected="" <?php } ?>
                                                    class="level-0"
                                                    value="favorite">
                                                محبوبترین ها
                                            </option>
                                            <option <?php if ($_GET['sort'] == 'asc') { ?> selected="" <?php } ?>
                                                    class="level-0"
                                                    value="asc">
                                                بر اساس نام صعودی
                                            </option>
                                            <option <?php if ($_GET['sort'] == 'asc') { ?> selected="" <?php } ?>
                                                    class="level-0"
                                                    value="desc">
                                                بر اساس نام نزولی
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <div class="form-group">
                                        <select name="cat[]"
                                                id="cat"
                                                multiple=""
                                                title="همه مناطق"
                                                placeholder="نوع ملک معاوضه"
                                                class="postform">
                                            <option value="" disabled="">نوع ملک معاوضه</option>
                                            <?php
                                            $options = array();
                                            $cats = $this->Itemcat->get_all();
                                            foreach ($cats->result() as $cat) {
                                                $options[$cat->id] = $cat->name;
                                            }
                                            foreach ($options as $k => $pt) { ?>
                                                <option <?php if (in_array($k, $_GET['cat'])) { ?> selected="" <?php } ?>
                                                        class="level-0"
                                                        value="<?php echo $k; ?>"><?php echo $pt; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <div class="form-group">
                                        <select name="city"
                                                id="city"
                                                class="postform">
                                            <option value="">شهر ملک معاوضه</option>
                                            <?php foreach ($this->locations as $location) { ?>
                                                <option <?php if ($_GET['city'] == $location->id) { ?> selected="" <?php } ?>
                                                        class="level-0"
                                                        value="<?php echo $location->id; ?>"><?php echo $location->name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <div class="form-group">
                                        <select multiple="" name="area[]"
                                                id="area"
                                                class="postform">
                                            <option selected="" value="">مناطق ملک معاوضه</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <button type="submit" class="btn btn-info width-100">پیدا کن</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            <?php } ?>
            <div class="background">
                <div class="background-image original-size">
                    <img src="/ssag/Melkekhoy/assets/front/images/hero-background-icons.jpg" alt="">
                </div>
            </div>
            <?php if ($search_change == 0 && $search_home == 0 && $search_auction == 0 && $search_request == 0) { ?>
                <style>
                    .hero .page-title {
                        padding-top: 1rem;
                    }

                    .hero .hero-wrapper {
                        padding-bottom: 1rem;
                    }
                </style>
                <div class="page-title">
                    <div class="container clearfix">
                        <div class="float-xs-none">
                            <h1>
                                <?php echo $site_title; ?>
                            </h1>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </header>
    <div id="navside" class="theiaStickySidebar">
        <button type="button" class="btn-close" id="btn-nav-close">   ✖   </button>
                        &#160;
                <center>


                     <a class="navbar-brand" href="/">
                            <img src="/ssag/Melkekhoy/assets/front/images/logo.png" alt="">
                        </a>
                        </center>
        <ul id="menu-%d9%85%d9%86%d9%88-%d8%a7%d8%b5%d9%84%db%8c" class="navbar-nav">
            <li id="menu-item-1511"
                class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home nav-item menu-item-1511">
                <a href="/" class="nav-link"> صفحه اصلی</a>
            </li>
            <li id="menu-item-1511"
                class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home nav-item menu-item-1511">
                <a href="/index.php/change/items" class="nav-link">آگهی معاوضه</a>
            </li>
            <li id="menu-item-1511"
                class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home nav-item menu-item-1511">
                <a href="/index.php/request/items" class="nav-link">آگهی درخواست</a>
            </li>

               <li id="menu-item-1511"
                class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home nav-item menu-item-1511">
                <a href="/index.php/auction/items" class="nav-link">آگهی مزایده</a>
            </li>

            <li id="menu-item-1511"
                class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home nav-item menu-item-1511">
                <a href="https://cafebazaar.ir/app/com.apps.Melk_Khoy" class="nav-link">دانلود
                    اپلیکیشن</a>
            </li>



            <li id="menu-item-1263"
                class="menu-item menu-item-type-post_type menu-item-object-page nav-item menu-item-1263">
                <a href="/index.php/about" class="nav-link">درباره
                    ما</a>
            </li>
            <li id="menu-item-1264"
                class="menu-item menu-item-type-post_type menu-item-object-page nav-item menu-item-1264">
                <a href="/index.php/contact" class="nav-link">تماس با
                    ما</a></li>
            <!--<li id="menu-item-1264"-->
            <!--    class="<?php if ($menu_index == 'rules') { ?> active <?php } ?> menu-item menu-item-type-post_type menu-item-object-page nav-item menu-item-1264">-->
            <!--    <a href="/index.blade.php/rules" class="nav-link">-->
            <!--        قوانین و مقررات-->
            <!--    </a></li>-->
            <li id="menu-item-1264"
                class="menu-item menu-item-type-post_type menu-item-object-page nav-item menu-item-1264">
                <a href="/index.php/estelam" class="nav-link">
                    استعلام
                </a></li>
        </ul>
    </div>
    <div id="userside" class="theiaStickySidebar">
        <button type="button" class="btn-close" id="btn-usernav-close">   ✖   </button>
                        &#160;
                <center>


                     <a class="navbar-brand" href="/">
                            <img src="/ssag/Melkekhoy/assets/front/images/logo.png" alt="">
                        </a>
                        </center>
        <ul class="navbar-nav">
            <?php if ($this->session->userdata('user_id')) { ?>
                <li>
                    <a class="nav-link" href="/index.php/user/profile">
                        <i class="fa fa-user"></i> پروفایل </a>
                </li>
                <li>
                    <a class="nav-link" href="/index.php/user/favorites">
                        <i class="fa fa-bookmark"></i> نشان ها </a>
                </li>
                <li>
                    <a class="nav-link" href="/index.php/user/lastviews">
                        <i class="fa fa-eye"></i> بازدیدهای اخیر </a>
                </li>
                <li>
                    <a class="nav-link" href="/index.php/user/items">
                        <i class="fa fa-list"></i> آگهی های من </a>
                </li>

                <li>
                    <a class="nav-link" href="/index.php/user/logout">
                        <i class="fa fa-sign-out"></i> خروج </a>
                </li>
            <?php } else { ?>
                <li>
                    <a class="nav-link" href="/index.php/user/login">
                        ورود / ثبت نام </a>
                </li>
            <?php } ?>
        </ul>
    </div>
    <main class="content">
        <?php if ($this->session->flashdata('success')) { ?>
            <p class="alert alert-success mt-3">
                <?php echo $this->session->flashdata('success'); ?>
            </p>
        <?php } ?>
        <?php if ($this->session->flashdata('error')){ ?>
        <p class="alert alert-danger mt-3">
            <?php echo $this->session->flashdata('error'); ?>
        </p>
<?php } ?>
