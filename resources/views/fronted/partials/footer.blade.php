</main>
</div>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-6 text-center">
                <i class="fa fa-list footer-i-icon"></i>
                <h4 class="footer-i-title">
                    <div class="mb-3">
                        
     
            <?php 
			echo $this->Item->count_all_by(); ?>
   
         
                    </div>
                    کل آگهی ها
                </h4>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 text-center">
                <i class="fa fa-list footer-i-icon"></i>
                <h4 class="footer-i-title">
                    <div class="mb-3">
30.000 + 
                    </div>
           کل بازدیدها
                </h4>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 text-center">
                <i class="fa fa-list footer-i-icon"></i>
                <h4 class="footer-i-title">
                    <div class="mb-3">
               700 +
                    </div>
      تعداد کاربران
                </h4>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6 text-center">
                <i class="fa fa-list footer-i-icon"></i>
                <h4 class="footer-i-title">
                    <div class="mb-3">
                  150 +
                    </div>
        تعداد کارشناسان
                </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h4 class="col-title-footer">
                    <i class="fa fa-tags"></i>
                    برچسب ها
                </h4>
                <div class="col-body-footer">
                    <a class="label">آپارتمان</a>
                    <a class="label">آپارتمان</a>
                    <a class="label">آپارتمان</a>
                </div>
            </div>
            <div class="col-md-4">
                <h4 class="col-title-footer">
                    <i class="fa fa-envelope"></i>
                    خبرنامه
                </h4>
                <div class="col-body-footer">
                    <div class="fnews">
                        <div class="news-icon"><i style="color:gray;" class="fa "></i></div>
                        <form class="form-inline feed-form">
                            <div class="form-group">
                                <label class="sr-only" for="feedemail">ایمیل</label>
                                <div class="input-group">
                                    <input type="text" style="border-radius: 0.3rem;" name="email" id="email"
                                           class="form-control" placeholder="ایمیل خود را وارد نمایید ..."
                                           required="required">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-success news-btn">عضویت</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="feed-msg">
                            برای با خبر شدن از جدیدترین مطالب و همچنین پیشنهادهای ویژه ما ایمیل خود را وارد نمایید.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <h4 class="col-title-footer">
                    <i class="fa fa-certificate"></i>
                    اعتماد شما افتخار ماست
                </h4>
                <div class="col-body-footer">
                    
               
                        
                    
                    
                     <a class="navbar-brand" href="/">
                              <center>
                            <img src="/ssag/Melkekhoy/uploads/thumbnail/logo_enemad.png" alt="">
                             <img src="/ssag/Melkekhoy/uploads/thumbnail/samandehi-logo.png" alt="">
                    
                               </center>
                        </a>
                      

                </div>
            </div>
        </div>

        <div class="row addr-con">
            <div class="col-lg-9 col-12 addr-text-con">

                <div class="addr-text col-md-6 col-12 text-white"><i class="fa fa-map-marker" aria-hidden="true"></i>
                    خوی - شیخ نوایی - روبروی پالادیوم
                </div>

                <div class="ftel col-md-6 col-12">

                                                <span class="foot-tel">
                            <span class="fa fa-phone" aria-hidden="true"></span>
                            <span class="top-val" id="site-tel">
                            09104405996                            </span>
                        </span>

                    <span class="foot-tel">
                            <span class="fa fa-envelope" aria-hidden="true"></span>
                            <span class="top-val" id="site-email">
                           Melkkhoy@gmail.com                           </span>
                        </span>

                </div>
            </div>

            <div class="col-lg-3 col-12 apps-icon">

                <a href="#" rel="nofollow" class="btn btn-outline-success and-btn">
                    <i class="fa fa-android" aria-hidden="true"></i>
                    نسخه آندروید </a>

                <a href="#" rel="nofollow" class="btn btn-outline-secondary ios-btn">
                    <i class="fa fa-apple" aria-hidden="true"></i>
                    نسخه IOS </a>

            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-12">
                <div class="footer-text">
                    کلیه حقوق این وب سایت متعلق به ملک خوی می باشد ، کاری از شرکت پیشرو تجارت آتیه نوین داده
                </div>
            </div>
            <div class="col-lg-6 col-12">
                <nav class="footer-nav">
                    <div class="menu-%d9%81%d9%87%d8%b1%d8%b3%d8%aa-%d9%be%d8%a7%db%8c%db%8c%d9%86-%d8%b3%d8%a7%db%8c%d8%aa-container">
                        <ul id="menu-%d9%81%d9%87%d8%b1%d8%b3%d8%aa-%d9%be%d8%a7%db%8c%db%8c%d9%86-%d8%b3%d8%a7%db%8c%d8%aa"
                            class="menu">
                            <li id="menu-item-163"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-163">
                                <a href="/index.php/about">
                                    درباره
                                    ما</a></li>
                            <li id="menu-item-162"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-162">
                                <a href="/index.php/contact">
                                    تماس
                                    با ما</a></li>
                            <li id="menu-item-160"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-160">
                                <a  href="/index.php/estelam">
                                    استعلام
                                </a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>

    </div>

    <!--end background-->
</footer>


<div class="d-md-none d-lg-none" id="#menu_footer"
     style="display:flex;justify-content: space-evenly;padding: 10px 0px;background-color: #b30753; position: fixed;bottom: 0;width: 100%;z-index: 999;">

    <div class="text-center">
        <a href="/">
            <i class="text-white fa fa-list"></i>
            <br>
            <span class="text-white">
        لیست آگهی
        </a>
        </span>
    </div>
    <div class="text-center">
        <a href="/index.php/user/addrequest">
            <i class="text-white fa fa-plus"></i>
            <br>
            <span class="text-white">
        درخواست
        </span>
        </a>
    </div>
    <div class="text-center">
        <a href="/index.php/user/additem">
            <i class="text-white fa fa-plus"></i>
            <br>
            <span class="text-white">
        ثبت آگهی
        </span>
        </a>
    </div>
    <div class="text-center">
        <a href="/index.php/user/items">
            <i class="text-white fa fa-user"></i>
            <br>
            <span class="text-white">
       حساب کاربری
        </span>
        </a>
    </div>

</div>
<div class="modal left fade" id="charsoogh_category_show" tabindex="-1" role="dialog"
     aria-labelledby="charsoogh_category_show">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">دسته‌بندی‌ها</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="vertical-menu">
                    <?php foreach ($this->categories as $catid => $category) { ?>
                        <li class="cat-item cat-item-106 dropdown-submenu">
                            <a class="dropdown-child"
                               title="<?php echo $category['name']; ?>"><?php echo $category['name']; ?></a>
                            <ul class="children">
                                <?php foreach ($category['sub'] as $subid => $subname) { ?>
                                    <li class="cat-item cat-item-107"><a
                                                href=""
                                                title="<?php echo $subname; ?>">
                                            <?php echo $subname; ?>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>

            </div>
        </div>
    </div>
</div>
<a href="javascript:" id="return-to-top" style="display: none;"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
<script>
    jQuery(document).ready(function ($) {


        jQuery('.type-search-a').on('click', function () {
            jQuery( ".type-search-a" ).removeClass('active');
            jQuery( this ).addClass('active');
            jQuery( '#type2' ).val(jQuery(this).data('type'));
        });
        jQuery('#close-app-banner').on('click', function () {
            jQuery( "#appbaner" ).hide();
        });
        jQuery('#btn-nav-close').on('click', function () {
            jQuery( "#navside" ).toggle( "slide" );
            jQuery( "#over_black" ).hide();
            jQuery( "#over_black" ).css('z-index',0);
        });
        jQuery('#btn-usernav-close').on('click', function () {
            jQuery( "#userside" ).toggle( "slide" );
            jQuery( "#over_black" ).hide();
            jQuery( "#over_black" ).css('z-index',0);
        });
        jQuery('#btn-filter-close').on('click', function () {
            jQuery( "#filter-aside" ).toggle( "slide" );
            jQuery( "#over_black" ).hide();
            jQuery( "#over_black" ).css('z-index',0);
        });
        jQuery('#over_black').on('click', function () {
            jQuery( "#filter-aside" ).hide();
            jQuery( "#navside" ).hide();
            jQuery( "#userside" ).hide();
            jQuery( "#over_black" ).hide();
            jQuery( "#over_black" ).css('z-index',0);
        });

        jQuery('#btn-filter-show').on('click', function () {
            jQuery( "#filter-aside" ).toggle( "slide" );
            jQuery( "#over_black" ).show();
            jQuery( "#over_black" ).css('z-index',100);
        });
        jQuery('#navsidebtn').on('click', function () {
            jQuery( "#navside" ).toggle( "slide" );
            jQuery( "#over_black" ).show();
            jQuery( "#over_black" ).css('z-index',100);
        });
        jQuery('#usersidebtn').on('click', function () {
            jQuery( "#userside" ).toggle( "slide" );
            jQuery( "#over_black" ).show();
            jQuery( "#over_black" ).css('z-index',100);
        });
        jQuery('#usersidedeskbtn').on('click', function () {
            jQuery( "#userside" ).toggle( "slide" );
            jQuery( "#over_black" ).show();
            jQuery( "#over_black" ).css('z-index',100);
        });

        // ===== Scroll to Top ====
        $(window).scroll(function () {
            if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
                $('#return-to-top').fadeIn(200);    // Fade in the arrow
            } else {
                $('#return-to-top').fadeOut(200);   // Else fade out the arrow
            }
        });
        $('#return-to-top').click(function () {      // When arrow is clicked
            $('body,html').animate({
                scrollTop: 0                       // Scroll to top of body
            }, 500);
        });
    });
    jQuery('#featuredowl').owlCarousel({
        loop: false,
        rewind: true,
        margin: 10,
        nav: true,
        rtl: true,
        dots: false,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 4
            }
        }
    })
    jQuery('.featuredowl').owlCarousel({
        margin: 10,
        nav: true,
        rtl: true,
        dots: false,
        loop: false,
        rewind: true,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 4
            }
        }
    })
    jQuery('#singleowl').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        rtl: true,
        dots: false,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    })

    jQuery('#btn_advance').click(function () {
        // jQuery('#advance_div').show();
        jQuery('#advance_div').toggleClass('hide');
        if (jQuery('#advance_div').hasClass('hide')) {
            jQuery('#advance_search').val(0);
        } else {
            jQuery('#advance_search').val(1);
        }
    });
    jQuery('#filterbtn').click(function () {
        if (jQuery('#filter-aside').css('display') == 'none') {
            jQuery('#filter-aside').show();
        } else {
            jQuery('#filter-aside').hide();
        }
    });

    /*jQuery('.range').ionRangeSlider({
        type: "double",
        min: 0,
        max: 1000,
        from: 100,
        to: 200,
        prefix: "متر"

    });*/


</script>
<script src="/ssag/Melkekhoy/assets/front/js/jquery.blockUI.min.js" id="jquery-blockui-js"></script>
<script src="/ssag/Melkekhoy/assets/front/js/cart-fragments.min.js" id="wc-cart-fragments-js"></script>
<script src="/ssag/Melkekhoy/assets/front/js/leaflet.js" id="leaf-script-js"></script>
<script src="/ssag/Melkekhoy/assets/front/js/leaflet-search.js" id="leaflet-search-js"></script>
<script src="/ssag/Melkekhoy/assets/front/js/leaflet-providers.js" id="leaflet-providers-js"></script>
<script src="/ssag/Melkekhoy/assets/front/js/Leaflet.fullscreen.min.js" id="Leaflet-fullscreen-js"></script>
<script src="/ssag/Melkekhoy/assets/front/dist/js/lightbox.js"></script>
<script src="/ssag/Melkekhoy/assets/front/js/sticky.js"></script>
<script>
    // var sticky = new Sticky('#filter-aside');


</script>

</body>

</html>